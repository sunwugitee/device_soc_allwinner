# device_allwinner

#### Description
​		Under the device/soc/ Allwinner/directory, host the sample code developed by Zhuhai Yanguo Technology Co., LTD based on T507 Little Bear Development board.




#### Get codes steps
```
mkdir openharmony

cd openharmony

repo init -u https://gitee.com/openharmony-sig/manifest.git -b master --no-repo-verify -m devboard_seed.xml 

repo sync

repo forall -c 'git lfs pull'

./build/prebuilts_download.sh
```



#### Directory Structure

```
/device/soc/allwinner
├── build                      
├── device                     
├── drivers                    
├── hardware                   
├── LICENSE
├── R818
├── README.en.md
├── README.md
├── T507                       
├── tags 
├── tools                      
└── XR806
```




#### Compiling steps
```
1. cd ./device/soc/allwinner/build

2. python3 patch.py T507             # Patching harmony system patches

3. ./build.sh load                   # Prepare the linux4.19 environment
prepare environment...
ACTION List: mkload;========
Execute command: mkload
start to load kernel file
All available ic:
   0. r818
   1. t507
Choice [t507]:
All available kern_ver:
   0. linux-4.19
   1. linux-5.10
Choice [linux-4.19]:
delete old kernel file success
......

4. ./build.sh config                # Select board configuration
Welcome to mkscript setup progress   
All available platform:
   0. linux
Choice [linux]:                     
All available linux_dev:
   0. bsp
Choice [bsp]:
All available kernel version:
INFO: default kernel version is linux-4.19, if reselet, please run ./build.sh load again
All available ic:
INFO: default ic type is t507, if reselet, please run ./build.sh load again
All available board:               
   0. demo2.0_bearpi
   1. demo2.0_harmony
Choice [demo2.0_bearpi]:
All available flash:
   0. default
   1. nor
Choice [default]:
sun50iw9p1smp_t507_L2_defconfig
......

5. ./build.sh

6. cd ../../../.. 

7. ./build.sh --product-name T507 --gn-args linux_kernel_version=\"linux-4.19\"

8. cd ./device/soc/allwinner/build

9. ./build.sh pack                    # pack image file

10. cd ../../../../out

# The image file is：t507_linux_demo2.0_harmony_uart0.img
```


#### Burn with LiveSuit_V1.0.0.1

```
1. Connect the data cable and serial cable;

2, open the burning software, select a good image file to burn;

3. Use tools such as MobaXterm to open the serial port. Press and hold down the keyboard key "2" before the serial port can be read and written, and then press the reset button of the machine.
```




# device_allwinner

## 目录介绍

```shell
device/soc/allwinner
├── Kconfig.liteos_m.defconfig				# kconfig 默认宏配置
├── Kconfig.liteos_m.series					# 同系列芯片配置
├── Kconfig.liteos_m.soc					# 芯片SOC配置
├── xradio									# 芯片系列名称
│   ├── <commom>							# XR系列芯片公共代码				
│   ├── xr806								# 芯片SOC名称
│   └── ...									# 芯片SOC名称
│
├── vradio                                  # 芯片系列名称
└── ...										# 芯片系列名称
```

## 环境搭建

请参照[OpenHarmony-QuickStart](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-lite-env-setup.md)。

## 获取源码

1. 安装码云repo工具，可以执行如下命令

```shell
curl -s https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > /usr/local/bin/repo  
#如果没有权限，可下载至其他目录，并将其配置到环境变量中
chmod a+x /usr/local/bin/repo
pip3 install -i https://repo.huaweicloud.com/repository/pypi/simple requests
```

2. 设置镜像源（可选）

```shell
vim ~/.bashrc
#在文件的最后输入以下内容
export PATH=~/bin:$PATH
export REPO_URL=https://mirrors.tuna.tsinghua.edu.cn/git/git-repo/
#设置完成后重启shell
#设置为清华镜像源后，下载源码时如果提示server certificate verification failed，输入export GIT_SSL_NO_VERIFY=1后重新下载即可。
```

3. 下载源码

   源码的下载依赖[manifest仓](https://gitee.com/openharmony-sig/manifest)，当前manifest仓的管理思路是一个开发板对应一个.xml文件，所以开发者想要下载指定代码的时候，repo 命令中需要添加 -m 来指定.xml文件，以xr806的下载为例。

   方式一（推荐）：通过repo + ssh 下载（需注册公钥，请参考[码云帮助中心](https://gitee.com/help/articles/4191)）。

   ```
   repo init -u ssh://git@gitee.com/openharmony-sig/manifest.git -b master --no-repo-verify -m devboard_xr806.xml
   repo sync -c
   repo forall -c 'git lfs pull'
   ```

   方式二：通过repo + https 下载。

   ```
   repo init -u https://gitee.com/openharmony-sig/manifest.git -b master --no-repo-verify -m devboard_xr806.xml
   repo sync -c
   repo forall -c 'git lfs pull'
   ```

   也可以把manifest仓fork到本地，修改.xml文件的内容，删除多余的三方库后，再下载，可以节省空间。

   ```
   repo init -u https://gitee.com/vyagoo/manifest.git -b master --no-repo-verify -m devboard_xr806.xml
   repo sync -c
   repo forall -c 'git lfs pull'
   ```

## 使用说明

不同系列的芯片编译、烧录方式有差异，请点击链接跳转到指定芯片的编译说明。

- [xradio](./xradio/README.md)：包含芯片XR806。

## 相关仓

[vendor_allwinner](https://gitee.com/openharmony-sig/vendor_allwinner_xr806)

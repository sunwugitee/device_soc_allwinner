/*
 * Copyright (C) 2017 XRADIO TECHNOLOGY CO., LTD. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the
 *       distribution.
 *    3. Neither the name of XRADIO TECHNOLOGY CO., LTD. nor the names of
 *       its contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#if !defined(MBEDTLS_CONFIG_FILE)
#include "mbedtls/config.h"
#else
#include MBEDTLS_CONFIG_FILE
#endif

#if defined(MBEDTLS_BIGNUM_C)

#include "mbedtls/bignum.h"
#include "mbedtls/bn_mul.h"
#include "mbedtls/platform_util.h"

#include <string.h>

#if defined(MBEDTLS_PLATFORM_C)
#include "mbedtls/platform.h"
#else
#include <stdio.h>
#include <stdlib.h>
#define mbedtls_printf printf
#define mbedtls_calloc calloc
#define mbedtls_free free
#endif

#ifdef MBEDTLS_BIGNUM_CACHE_ENABLED
#include "sys/list.h"
#include "sys/sys_heap.h"
#define mbedtls_malloc_mutex_lock malloc_mutex_lock
#define mbedtls_malloc_mutex_unlock malloc_mutex_unlock
#endif

#define MPI_VALIDATE_RET(cond) \
    MBEDTLS_INTERNAL_VALIDATE_RET(cond, MBEDTLS_ERR_MPI_BAD_INPUT_DATA)
#define MPI_VALIDATE(cond) \
    MBEDTLS_INTERNAL_VALIDATE(cond)

#define ciL (sizeof(mbedtls_mpi_uint)) /* chars in limb  */
#define biL (ciL << 3)                 /* bits  in limb  */
#define biH (ciL << 2)                 /* half limb size */

#define MPI_SIZE_T_MAX ((size_t)-1) /* SIZE_T_MAX is not standard */

#ifdef MBEDTLS_BIGNUM_CACHE_ENABLED

struct bignum_cache
{
    size_t n;
    mbedtls_mpi_uint *p;
    struct list_head node;
};

static LIST_HEAD_DEF(bignum_cache_list);
static unsigned int bignum_cache_ref;

int mbedtls_bignum_cache_init(void)
{
    mbedtls_malloc_mutex_lock();

    if (bignum_cache_ref == 0)
    {
        INIT_LIST_HEAD(&bignum_cache_list);
    }
    bignum_cache_ref++;

    mbedtls_malloc_mutex_unlock();

    return 0;
}

void mbedtls_bignum_cache_deinit(void)
{
    struct bignum_cache *cache;
    struct bignum_cache *next;

    mbedtls_malloc_mutex_lock();
    bignum_cache_ref--;
    if (bignum_cache_ref != 0)
    {
        mbedtls_malloc_mutex_unlock();
        return;
    }

    list_for_each_entry_safe(cache, next, &bignum_cache_list, node)
    {
        list_del(&cache->node);
        mbedtls_free(cache->p);
        mbedtls_free(cache);
    }
    mbedtls_malloc_mutex_unlock();
}

static mbedtls_mpi_uint *mbedtls_bignum_cache_get(size_t n)
{
    mbedtls_mpi_uint *p;
    struct bignum_cache *cache;

    mbedtls_malloc_mutex_lock();

    list_for_each_entry(cache, &bignum_cache_list, node)
    {
        if (cache->n == n)
        {
            p = cache->p;
            cache->n = 0;
            cache->p = NULL;
            mbedtls_malloc_mutex_unlock();
            mbedtls_mpi_zeroize(p, n);
            return p;
        }
    }

    mbedtls_malloc_mutex_unlock();
    return mbedtls_calloc(n, ciL);
}

static void mbedtls_bignum_cache_put(size_t n, mbedtls_mpi_uint *p)
{
    struct bignum_cache *cache;

    mbedtls_malloc_mutex_lock();

    list_for_each_entry(cache, &bignum_cache_list, node)
    {
        if (cache->n == 0)
        {
            cache->n = n;
            cache->p = p;
            mbedtls_malloc_mutex_unlock();
            return;
        }
    }

    if (bignum_cache_ref)
    {
        cache = mbedtls_calloc(1, sizeof(struct bignum_cache));
        if (cache == NULL)
        {
            goto exit;
        }
        cache->n = n;
        cache->p = p;
        list_add(&cache->node, &bignum_cache_list);
        mbedtls_malloc_mutex_unlock();
        return;
    }

exit:
    mbedtls_malloc_mutex_unlock();
    mbedtls_free(p);
}

#else

int mbedtls_bignum_cache_init(void)
{
    return 0;
}

void mbedtls_bignum_cache_deinit(void)
{
}

#endif

#endif /* MBEDTLS_BIGNUM_C */
/*
 * Copyright (C) 2021 XRADIO TECHNOLOGY CO., LTD. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the
 *       distribution.
 *    3. Neither the name of XRADIO TECHNOLOGY CO., LTD. nor the names of
 *       its contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <string.h>
#include "os.h"
#include "iot_flash.h"
#include "iot_errno.h"
#include "common/cmd/cmd.h"
#include "cmd_hm.h"

static enum cmd_status cmd_flash_init_exec(char *cmd)
{
    IoTFlashInit();
    return CMD_STATUS_OK;
}

static enum cmd_status cmd_flash_deinit_exec(char *cmd)
{
    IoTFlashDeinit();
    return CMD_STATUS_OK;
}

static enum cmd_status cmd_erase_exec(char *cmd)
{
    uint32_t cnt, add, size;

    cnt = cmd_sscanf(cmd, "add=%x size=%d", &add, &size);
    if (cnt != 2) {
        HMCMD_ERR("err cmd:%s\n", cmd);
        return CMD_STATUS_INVALID_ARG;
    }

    return (IoTFlashErase(add, size) == IOT_SUCCESS) ? CMD_STATUS_OK :
                                 CMD_STATUS_FAIL;
}

static enum cmd_status cmd_write_exec(char *cmd)
{
    uint32_t cnt, add, size, res;

    char erase[8];
    unsigned char *wbuf = NULL;

    cnt = cmd_sscanf(cmd, "add=%x size=%d %7s", &add, &size, erase);
    if ((cnt != 3) && (cnt != 2)) {
        HMCMD_ERR("err cmd:%s\n", cmd);
        return CMD_STATUS_INVALID_ARG;
    }
    if (size == 0) {
        CMD_ERR("size is 0!!\n");
        return CMD_STATUS_INVALID_ARG;
    }

    wbuf = malloc(size);
    if (wbuf == NULL) {
        CMD_ERR("no memory\n");
        return CMD_STATUS_FAIL;
    }

    memset(wbuf, 0xaa, size);
    if (cmd_strcmp(erase, "erase") == 0) {
        res = IoTFlashWrite(add, size, wbuf, 1);
    } else {
        res = IoTFlashWrite(add, size, wbuf, 0);
    }

    free(wbuf);

    return (res == IOT_SUCCESS) ? CMD_STATUS_OK : CMD_STATUS_FAIL;
}

static enum cmd_status cmd_read_exec(char *cmd)
{
    uint32_t cnt, add, size, res;
    unsigned char *rbuf = NULL;

    cnt = cmd_sscanf(cmd, "add=%x size=%d", &add, &size);
    if (cnt != 2) {
        HMCMD_ERR("err cmd:%s\n", cmd);
        return CMD_STATUS_INVALID_ARG;
    }
    if (size == 0) {
        CMD_ERR("size is 0!!\n");
        return CMD_STATUS_INVALID_ARG;
    }

    rbuf = malloc(size);
    if (rbuf == NULL) {
        CMD_ERR("no memory\n");
        return CMD_STATUS_FAIL;
    }

    res = IoTFlashRead(add, size, rbuf);
    cmd_hm_print_uint8_array(rbuf, size);
    free(rbuf);

    return (res == IOT_SUCCESS) ? CMD_STATUS_OK : CMD_STATUS_FAIL;
}

static enum cmd_status cmd_flash_help_exec(char *cmd)
{
}

static const struct cmd_data g_iot_flash_cmds[] = {
    { "init", cmd_flash_init_exec, "  " },
    { "deinit", cmd_flash_deinit_exec, " " },
    { "erase", cmd_erase_exec, " " },
    { "write", cmd_write_exec, " " },
    { "read", cmd_read_exec, " " },
    { "help", cmd_flash_help_exec, " " },
};

enum cmd_status cmd_hm_iot_flash_exec(char *cmd)
{
    return cmd_exec(cmd, g_iot_flash_cmds, cmd_nitems(g_iot_flash_cmds));
}

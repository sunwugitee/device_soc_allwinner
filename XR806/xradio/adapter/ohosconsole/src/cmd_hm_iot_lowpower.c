/*
 * Copyright (C) 2021 XRADIO TECHNOLOGY CO., LTD. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the
 *       distribution.
 *    3. Neither the name of XRADIO TECHNOLOGY CO., LTD. nor the names of
 *       its contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <string.h>
#include "lowpower.h"
#include "iot_errno.h"
#include "common/cmd/cmd.h"
#include "cmd_hm.h"

static enum cmd_status cmd_lowpower_lpc_init_exec(char *cmd)
{
    return (LpcInit() == IOT_SUCCESS) ? CMD_STATUS_OK : CMD_STATUS_FAIL;
}

static enum cmd_status cmd_lowpower_set_lpc_type_exec(char *cmd)
{
    uint32_t cnt, ohos_type;
    char type[8] = { 0 };
    cnt = cmd_sscanf(cmd, "%7s", type);
    if (cnt != 1) {
        HMCMD_ERR("err cmd:%s\n", cmd);
        return CMD_STATUS_INVALID_ARG;
    }

    if (cmd_strcmp(type, "light") == 0) {
        ohos_type = LIGHT_SLEEP;
    } else if (cmd_strcmp(type, "deep") == 0) {
        ohos_type = DEEP_SLEEP;
    } else {
        HMCMD_ERR("invalid type %s\n", type);
        return CMD_STATUS_INVALID_ARG;
    }

    return (LpcSetType(ohos_type) == IOT_SUCCESS) ? CMD_STATUS_OK :
                                  CMD_STATUS_FAIL;
}

static const struct cmd_data g_iot_lowpower_cmds[] = {
    { "init", cmd_lowpower_lpc_init_exec, CMD_DESC("lowpower command") },
    { "settype", cmd_lowpower_set_lpc_type_exec,
      CMD_DESC("lowpower command") },

};

enum cmd_status cmd_hm_iot_lowpower_exec(char *cmd)
{
    return cmd_exec(cmd, g_iot_lowpower_cmds,
            cmd_nitems(g_iot_lowpower_cmds));
}

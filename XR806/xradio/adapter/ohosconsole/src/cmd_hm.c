/*
 * Copyright (C) 2021 XRADIO TECHNOLOGY CO., LTD. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the
 *       distribution.
 *    3. Neither the name of XRADIO TECHNOLOGY CO., LTD. nor the names of
 *       its contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <string.h>
#include "os.h"
//#include "cmd_util.h"
#include "common/cmd/cmd.h"
#include "cmd_hm.h"
#include "cmd_hm_iot_gpio.h"
#include "cmd_hm_iot_uart.h"
#include "cmd_hm_iot_pwm.h"
#include "cmd_hm_iot_i2c.h"
#include "cmd_hm_iot_flash.h"
#include "cmd_hm_iot_reset.h"
// #include "cmd_hm_iot_watchdog.h"
#include "cmd_hm_iot_lowpower.h"
#include "cmd_hm_net_sta.h"
#include "cmd_hm_net_ap.h"
#include "cmd_hm_mem.h"

static enum cmd_status cmd_hm_help_exec(char *cmd);

void cmd_hm_print_uint8_array(uint8_t *buf, int32_t size)
{
    for (int i = 0; i < size; i++) {
        if (i % 8 == 0) {
            printf("  ");
        }
        if (i % 32 == 0) {
            printf("\n");
        }
        printf("%02x ", buf[i]);
    }
    printf("\n");
}

static const struct cmd_data g_iot_cmds[] = {
    { "gpio", cmd_hm_iot_gpio_exec, CMD_DESC("gpio command") },
    { "uart", cmd_hm_iot_uart_exec, CMD_DESC("uart command") },
    { "i2c", cmd_hm_iot_i2c_exec, CMD_DESC("i2c command") },
    { "flash", cmd_hm_iot_flash_exec, CMD_DESC("flash command") },
    { "pwm", cmd_hm_iot_pwm_exec, CMD_DESC("pwm command") },
    { "reset", cmd_hm_iot_reset_exec, CMD_DESC("reset command") },
    // { "wdg", cmd_hm_iot_watchdog_exec, CMD_DESC("watchdog command") },
    { "lpc", cmd_hm_iot_lowpower_exec, CMD_DESC("lowpower command") },
};

static enum cmd_status cmd_hm_iot_exec(char *cmd)
{
    return cmd_exec(cmd, g_iot_cmds, cmd_nitems(g_iot_cmds));
}

static const struct cmd_data g_net_cmds[] = {
    { "sta", cmd_hm_net_sta_exec, CMD_DESC("net sta command") },
    { "ap", cmd_hm_net_ap_exec, CMD_DESC("net ap command") },
};

static enum cmd_status cmd_hm_net_exec(char *cmd)
{
    return cmd_exec(cmd, g_net_cmds, cmd_nitems(g_net_cmds));
}

static const struct cmd_data g_mem_cmds[] = {
    { "info", cmd_hm_mem_info_exec, CMD_DESC("net sta command") },
};

static enum cmd_status cmd_hm_mem_exec(char *cmd)
{
    return cmd_exec(cmd, g_mem_cmds, cmd_nitems(g_mem_cmds));
}

static const struct cmd_data g_harmony_cmds[] = {
    { "iot", cmd_hm_iot_exec, CMD_DESC("harmony iot command") },
    { "net", cmd_hm_net_exec, CMD_DESC("harmony net command") },
    { "mem", cmd_hm_mem_exec, CMD_DESC("harmony mem command") },
    { "help", cmd_hm_help_exec, CMD_DESC("harmeny help command") },
};

static enum cmd_status cmd_hm_help_exec(char *cmd)
{
    return cmd_help_exec(g_harmony_cmds, cmd_nitems(g_harmony_cmds), 8);
}

enum cmd_status cmd_hm_exec(char *cmd)
{
    return cmd_exec(cmd, g_harmony_cmds, cmd_nitems(g_harmony_cmds));
}

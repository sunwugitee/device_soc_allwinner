/*
 * Copyright (C) 2021 XRADIO TECHNOLOGY CO., LTD. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the
 *       distribution.
 *    3. Neither the name of XRADIO TECHNOLOGY CO., LTD. nor the names of
 *       its contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <string.h>
#include "os.h"
#include "iot_errno.h"
#include "common/cmd/cmd.h"
#include "cmd_hm.h"
#include "iot_pwm.h"

static enum cmd_status cmd_pwm_init_exec(char *cmd)
{
    uint32_t port;

    int cnt = cmd_sscanf(cmd, "p=%d", &port);
    if (cnt != 1) {
        return CMD_STATUS_INVALID_ARG;
    }

    return (IoTPwmInit(port) == IOT_SUCCESS) ? CMD_STATUS_OK :
                             CMD_STATUS_FAIL;
}

static enum cmd_status cmd_pwm_deinit_exec(char *cmd)
{
    uint32_t port;

    int cnt = cmd_sscanf(cmd, "p=%d", &port);
    if (cnt != 1) {
        return CMD_STATUS_INVALID_ARG;
    }

    return (IoTPwmDeinit(port) == IOT_SUCCESS) ? CMD_STATUS_OK :
                               CMD_STATUS_FAIL;
}

static enum cmd_status cmd_pwm_start_exec(char *cmd)
{
    uint32_t port, duty, freq;

    int cnt = cmd_sscanf(cmd, "p=%d d=%d f=%d", &port, &duty, &freq);
    if (cnt != 3) {
        return CMD_STATUS_INVALID_ARG;
    }

    return (IoTPwmStart(port, duty, freq) == IOT_SUCCESS) ? CMD_STATUS_OK :
                                      CMD_STATUS_FAIL;
}

static enum cmd_status cmd_pwm_stop_exec(char *cmd)
{
    uint32_t port;

    int cnt = cmd_sscanf(cmd, "p=%d", &port);
    if (cnt != 1) {
        return CMD_STATUS_INVALID_ARG;
    }

    return (IoTPwmStop(port) == IOT_SUCCESS) ? CMD_STATUS_OK :
                             CMD_STATUS_FAIL;
}

static const struct cmd_data g_iot_pwm_cmds[] = {
    { "init", cmd_pwm_init_exec, CMD_DESC("pwm command") },
    { "deinit", cmd_pwm_deinit_exec, CMD_DESC("pwm command") },
    { "start", cmd_pwm_start_exec, CMD_DESC("pwm command") },
    { "stop", cmd_pwm_stop_exec, CMD_DESC("pwm command") },
};

enum cmd_status cmd_hm_iot_pwm_exec(char *cmd)
{
    return cmd_exec(cmd, g_iot_pwm_cmds, cmd_nitems(g_iot_pwm_cmds));
}

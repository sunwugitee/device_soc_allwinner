/*
 * Copyright (C) 2021 XRADIO TECHNOLOGY CO., LTD. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the
 *       distribution.
 *    3. Neither the name of XRADIO TECHNOLOGY CO., LTD. nor the names of
 *       its contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @addtogroup IotHardware
 * @{
 *
 * @brief Provides APIs for operating devices,
 * including flash, GPIO, I2C, PWM, UART, and watchdog APIs.
 *
 *
 *
 * @since 2.2
 * @version 2.2
 */
#include "iot_errno.h"
#include "driver/chip/hal_flash.h"
#include "image/flash.h"

#define MFLASH 0

/**
 * @brief Reads data from a flash memory address.
 *
 * This function reads a specified length of data from a specified flash memory address.
 *
 * @param flashOffset Indicates the address of the flash memory from which data is to read.
 * @param size Indicates the length of the data to read.
 * @param ramData Indicates the pointer to the RAM for storing the read data.
 * @return Returns {@link IOT_SUCCESS} if the data is read successfully;
 * returns {@link IOT_FAILURE} otherwise. For details about other return values, see the chip description.
 * @since 2.2
 * @version 2.2
 */
unsigned int IoTFlashRead(unsigned int flashOffset, unsigned int size,
              unsigned char *ramData)
{
    flash_rw(MFLASH, flashOffset, ramData, size, 0);
    return IOT_SUCCESS;
}

/**
 * @brief Writes data to a flash memory address.
 *
 * This function writes a specified length of data to a specified flash memory address.
 *
 * @param flashOffset Indicates the address of the flash memory to which data is to be written.
 * @param size Indicates the length of the data to write.
 * @param ramData Indicates the pointer to the RAM for storing the data to write.
 * @param doErase Specifies whether to automatically erase existing data.
 * @return Returns {@link IOT_SUCCESS} if the data is written successfully;
 * returns {@link IOT_FAILURE} otherwise. For details about other return values, see the chip description.
 * @since 2.2
 * @version 2.2
 */
unsigned int IoTFlashWrite(unsigned int flashOffset, unsigned int size,
               const unsigned char *ramData, unsigned char doErase)
{
    if (doErase) {
        unsigned int EarseSize = (((size >> 12) + 1) << 12);
        flash_erase(MFLASH, flashOffset, EarseSize);
    }
    flash_rw(MFLASH, flashOffset, (void *)ramData, size, 1);
    return IOT_SUCCESS;
}

/**
 * @brief Erases data in a specified flash memory address.
 *
 * @param flashOffset Indicates the flash memory address.
 * @param size Indicates the data length in bytes.
 * @return Returns {@link IOT_SUCCESS} if the data is erased successfully;
 * returns {@link IOT_FAILURE} otherwise. For details about other return values, see the chip description.
 * @since 2.2
 * @version 2.2
 */
unsigned int IoTFlashErase(unsigned int flashOffset, unsigned int size)
{
    unsigned int EarseSize = 0;
    EarseSize = (((size >> 12) + 1) << 12);
    if (flash_erase(MFLASH, flashOffset, EarseSize) == 0) {
        return IOT_SUCCESS;
    } else {
        printf("IoTFlashErase error\r\n");
        return IOT_FAILURE;
    }
}

/**
 * @brief Initializes a flash device.
 *
 * @return Returns {@link IOT_SUCCESS} if the flash device is initialized;
 * returns {@link IOT_FAILURE} otherwise. For details about other return values, see the chip description.
 * @since 2.2
 * @version 2.2
 */
unsigned int IoTFlashInit(void)
{
    return IOT_SUCCESS;
}

/**
 * @brief Deinitializes a flash device.
 *
 * @return Returns {@link IOT_SUCCESS} if the flash device is deinitialized;
 * returns {@link IOT_FAILURE} otherwise. For details about other return values, see the chip description.
 * @since 2.2
 * @version 2.2
 */
unsigned int IoTFlashDeinit(void)
{
    return IOT_SUCCESS;
}

/** @} */

/*
 * Copyright (C) 2021 XRADIO TECHNOLOGY CO., LTD. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the
 *       distribution.
 *    3. Neither the name of XRADIO TECHNOLOGY CO., LTD. nor the names of
 *       its contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @addtogroup power
 * @{
 *
 * @brief Provides device power management functions.
 *
 * This module is used to reboot the device and set low power consumption for the device. \n
 *
 * @since 2.2
 * @version 2.2
 */
#include "iot_errno.h"
#include "sys/interrupt.h"
#include "driver/chip/hal_prcm.h"
#include "driver/chip/hal_wdg.h"

#include "driver/chip/hal_nvic.h"
#include "driver/chip/hal_global.h"
#include "driver/chip/system_chip.h"

#define RESET_HANDLER (*((volatile uint32_t *)0x4))

/**
 * @brief Reboots the device using different causes.
 *
 *
 *
 * @param cause Indicates the reboot cause.
 * @since 2.2
 * @version 2.2
 */
void RebootDevice(unsigned int cause)
{
    //printf("RebootDevice cause=%d\r\n", cause);

    switch (cause) {
    case 0: {
        HAL_PRCM_SetCPUABootFlag(PRCM_CPUA_BOOT_FROM_COLD_RESET);
        HAL_WDG_Reboot();
    } break;
#if 0
        case 1:
        {
            uint32_t handler;

            HAL_PRCM_SetCPUABootFlag(PRCM_CPUA_BOOT_FROM_COLD_RESET);
            handler = RESET_HANDLER;
#ifdef __CONFIG_CPU_CM4F
            handler |= 0x1; /* set thumb bit */
#endif
            arch_irq_disable();
            HAL_PRCM_DisableSys2();
            HAL_PRCM_DisableSys2Power();
            HAL_GlobalInit();
            SystemDeInit(0);

            SCB->VTOR = 0x0;
            __set_CONTROL(0); /* reset to Privileged Thread mode and use MSP */
            __DSB();
            __ISB();
            ((NVIC_IRQHandler)handler)();
        }
        break;
#endif

    default:
        break;
    }
}

/** @} */

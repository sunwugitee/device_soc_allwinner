/*
 * Copyright (C) 2021 XRADIO TECHNOLOGY CO., LTD. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the
 *       distribution.
 *    3. Neither the name of XRADIO TECHNOLOGY CO., LTD. nor the names of
 *       its contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @addtogroup IotHardware
 * @{
 *
 * @brief Provides APIs for operating devices,
 * including flash, GPIO, I2C, PWM, UART, and watchdog APIs.
 *
 *
 *
 * @since 2.2
 * @version 2.2
 */
#include "iot_errno.h"
#include "driver/chip/hal_i2c.h"

/**
 * @brief Writes data to an I2C device.
 *
 *
 *
 * @param id Indicates the I2C device ID.
 * @param deviceAddr Indicates the I2C device address.
 * @param data Indicates the pointer to the data to write.
 * @param dataLen Indicates the length of the data to write.
 * @return Returns {@link IOT_SUCCESS} if the data is written to the I2C device successfully;
 * returns {@link IOT_FAILURE} otherwise. For details about other return values, see the chip description.
 * @since 2.2
 * @version 2.2
 */
unsigned int IoTI2cWrite(unsigned int id, unsigned short deviceAddr,
                         const unsigned char *data, unsigned int dataLen)
{
    unsigned int len = HAL_I2C_Master_Transmit_IT(id, deviceAddr,
                                                  (uint8_t *)data, dataLen);
    if (len != dataLen) {
        return IOT_FAILURE;
    }
    return IOT_SUCCESS;
}

/**
 * @brief Reads data from an I2C device.
 *
 * The data read will be saved to the address specified by <b>i2cData</b>.
 *
 * @param id Indicates the I2C device ID.
 * @param deviceAddr Indicates the I2C device address.
 * @param data Indicates the pointer to the data to read.
 * @param dataLen Indicates the length of the data to read.
 * @return Returns {@link IOT_SUCCESS} if the data is read from the I2C device successfully;
 * returns {@link IOT_FAILURE} otherwise. For details about other return values, see the chip description.
 * @since 2.2
 * @version 2.2
 */
unsigned int IoTI2cRead(unsigned int id, unsigned short deviceAddr,
                        unsigned char *data, unsigned int dataLen)
{
    unsigned int len =
            HAL_I2C_Master_Receive_IT(id, deviceAddr, (uint8_t *)data, dataLen);
    if (len != dataLen) {
        return IOT_FAILURE;
    }

    return IOT_SUCCESS;
}

/**
 * @brief Sets the baud rate for an I2C device.
 *
 * @param id Indicates the I2C device ID.
 * @param baudrate Indicates the baud rate to set.
 * @return Returns {@link IOT_SUCCESS} if the baud rate is set;
 * returns {@link IOT_FAILURE} otherwise. For details about other return values, see the chip description.
 * @since 2.2
 * @version 2.2
 */
unsigned int IoTI2cSetBaudrate(unsigned int id, unsigned int baudrate)
{
    return IOT_SUCCESS;
}

/**
 * @brief Initializes an I2C device with a specified baud rate.
 *
 *
 *
 * @param id Indicates the I2C device ID.
 * @param baudrate Indicates the I2C baud rate.
 * @return Returns {@link IOT_SUCCESS} if the I2C device is initialized;
 * returns {@link IOT_FAILURE} otherwise. For details about other return values, see the chip description.
 * @since 2.2
 * @version 2.2
 */
unsigned int IoTI2cInit(unsigned int id, unsigned int baudrate)
{
    I2C_InitParam initParam;
    initParam.addrMode = I2C_ADDR_MODE_7BIT;
    initParam.clockFreq = baudrate;

    HAL_Status status = HAL_I2C_Init(id, &initParam);
    if (status != HAL_OK) {
        printf("IIC init error %d\n", status);
        return IOT_FAILURE;
    }
    // unsigned char testData = 0;
    // HAL_I2C_Master_Transmit_IT(0,0x50,&testData,1);
    // OS_MSleep(5);
    // HAL_I2C_Master_Receive_IT(0,0x50,&testData,1);

    return IOT_SUCCESS;
}

/**
 * @brief Deinitializes an I2C device.
 *
 * @param id Indicates the I2C device ID.
 * @return Returns {@link IOT_SUCCESS} if the I2C device is deinitialized;
 * returns {@link IOT_FAILURE} otherwise. For details about other return values, see the chip description.
 * @since 2.2
 * @version 2.2
 */
unsigned int IoTI2cDeinit(unsigned int id)
{
    HAL_Status status = HAL_I2C_DeInit(id);
    if (status != HAL_OK) {
        return IOT_FAILURE;
    }

    return IOT_SUCCESS;
}

/** @} */

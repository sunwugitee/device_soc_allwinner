# allwinner hardware组件

#### 简介
媒体及wifi南向接口实现、框架及对接层库目录。
#### 约束
目前支持T507和R818
#### 对应仓库

1.  device_allwinner/hardware/display
2.  device_allwinner/hardware/gpu
3.  device_allwinner/hardware/isp
4.  device_allwinner/hardware/mpp
5.  device_allwinner/hardware/rga
6.  device_allwinner/hardware/wifi


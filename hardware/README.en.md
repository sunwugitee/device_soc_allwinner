# allwinner hardware

## Introduction
Media and wifi southbound interface implementation, framework and docking layer library directory。
## Constraints

Currently, T507 and R818 are supported.
## Repositories Involved
1.  device_allwinner/hardware/display
2.  device_allwinner/hardware/gpu
3.  device_allwinner/hardware/isp
4.  device_allwinner/hardware/mpp
5.  device_allwinner/hardware/rga
6.  device_allwinner/hardware/wifi


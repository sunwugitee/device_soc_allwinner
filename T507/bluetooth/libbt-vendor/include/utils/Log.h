#include <stdarg.h>
#include <stdio.h>
#include "hilog/log.h"
#include "time.h"


#define LOGD(...) HiLogPrint(LOG_CORE, LOG_DEBUG, LOG_DOMAIN, "BTVENDOR", __VA_ARGS__)
#define LOGI(...) HiLogPrint(LOG_CORE, LOG_INFO, LOG_DOMAIN, "BTVENDOR", __VA_ARGS__)
#define LOGW(...) HiLogPrint(LOG_CORE, LOG_WARN, LOG_DOMAIN, "BTVENDOR", __VA_ARGS__)
#define LOGE(...) HiLogPrint(LOG_CORE, LOG_ERROR, LOG_DOMAIN, "BTVENDOR", __VA_ARGS__)
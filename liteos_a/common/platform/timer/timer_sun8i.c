/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "timer_sun8i.h"
#include "device_resource_if.h"
#include "hdf_base.h"
#include "hdf_log.h"
#include "osal_io.h"
#include "osal_irq.h"
#include "osal_mem.h"
#include "osal_time.h"

// 因为6个定时器需要独立使用，但是中断状态与使能是一个寄存器地址，
// 所以需要一个全局互斥锁来保护对中断状态和使能寄存器的写操作

static int32_t TimerSun8iEnable(struct TimerSun8iInfo *info, bool enable)
{
    CHECK_NULL_PTR_RETURN_VALUE(info, HDF_ERR_INVALID_OBJECT);
    uint32_t ctrl = 0;
    uint32_t number = info->number%6;
    /*enable timer*/
    ctrl = OSAL_READL(info->regBase + TIMER_CTL_REG(number));
    if (enable)
    {
        ctrl |= TIMER_CTL_ENABLE;
    }else{
        ctrl &= ~(TIMER_CTL_ENABLE);
    }
    OSAL_WRITEL(ctrl, (info->regBase + TIMER_CTL_REG(number)));
    return HDF_SUCCESS;
}


// mask interrupts true:not mask; false mask
static int32_t TimerSun8iIntEnable(struct TimerSun8iInfo *info, bool enable)
{
    CHECK_NULL_PTR_RETURN_VALUE(info, HDF_ERR_INVALID_OBJECT);

    uint32_t val = 0;
    uint32_t number = info->number%6;
    /* must need spinlock */
	val = OSAL_READL(info->regBase + TIMER_IRQ_EN_REG);
    if(enable)
	    val |= TIMER_IRQ_EN(number);
    else
        val &= ~TIMER_IRQ_EN(number);
	OSAL_WRITEL(val, info->regBase + TIMER_IRQ_EN_REG);
    return HDF_SUCCESS;
}

// clear timer interrupt
static int32_t TimerSun8iIntClear(struct TimerSun8iInfo *info)
{
    CHECK_NULL_PTR_RETURN_VALUE(info, HDF_ERR_INVALID_OBJECT);
    OSAL_WRITEL(TIMER_IRQ_ST(info->number%6), info->regBase + TIMER_IRQ_ST_REG);
    return HDF_SUCCESS;
}

static uint32_t TimerSun8iIrqHandle(uint32_t irqId, void *data)
{
    CHECK_NULL_PTR_RETURN_VALUE(data, HDF_ERR_INVALID_OBJECT);
    struct TimerSun8iInfo *info = NULL;
    info = (struct TimerSun8iInfo *)data;

    // dprintf("------------->%s[%s]: timer[%u], irqId [%u] in \n",
    //     __func__, __TIME__, info->number, irqId);

    // clear interrupt
    TimerSun8iIntClear(info);

    CHECK_NULL_PTR_RETURN_VALUE(info->cb, HDF_ERR_INVALID_OBJECT);
    info->cb(info->number);
    // HDF_LOGD("------------->%s: timer[%u], irqId [%u] process success", __func__, info->number, irqId);
    return HDF_SUCCESS;
}

static int32_t TimerSun8iSet(struct TimerCntrl *cntrl, uint32_t useconds, TimerHandleCb cb)
{
    CHECK_NULL_PTR_RETURN_VALUE(cntrl, HDF_ERR_INVALID_OBJECT);
    CHECK_NULL_PTR_RETURN_VALUE(cntrl->priv, HDF_ERR_INVALID_OBJECT);
    CHECK_NULL_PTR_RETURN_VALUE(cb, HDF_ERR_INVALID_OBJECT);
    struct TimerSun8iInfo *info = cntrl->priv;
    if (info->number < 6)
    {   //timer
        cntrl->info.useconds = useconds;
        cntrl->info.cb = cb;
        cntrl->info.isPeriod = false;

        uint32_t intval = useconds*(TIMER_TWOUSECOND) / 2;
        uint32_t ctrl = 0;
        info->cb = cb;
        info->isPeriod = false;

        /* set timer intervalue */
        OSAL_WRITEL(intval, info->regBase + TIMER_INTVAL_REG(info->number));
        OsalUDelay(1);

        /*reload the timer intervalue*/
        ctrl = OSAL_READL(info->regBase + TIMER_CTL_REG(info->number));
        ctrl |= TIMER_CTL_AUTORELOAD;
        OSAL_WRITEL(ctrl, (info->regBase + TIMER_CTL_REG(info->number)));
        // while (OSAL_READL(info->regBase + TIMER_CTL_REG(info->number)) & TIMER_CTL_AUTORELOAD);
    }else{//hstimer
        cntrl->info.useconds = useconds;
        cntrl->info.cb = cb;
        cntrl->info.isPeriod = false;
        int number = info->number%6;

        uint64_t intval = useconds*(HSTIMER_TWOUSECOND);
        uint32_t ctrl = 0;
        info->cb = cb;
        info->isPeriod = false;

        /* set timer intervalue */
        OSAL_WRITEL(intval & 0xFFFFFFFF, info->regBase + HSTIMER_INTVALL_REG(number));
        OSAL_WRITEL((intval >> 32) & 0xFFFFFFFF, info->regBase + HSTIMER_INTVALH_REG(number));
        OsalUDelay(1);

        /*reload the timer intervalue*/
        ctrl = OSAL_READL(info->regBase + HSTIMER_CTL_REG(number));
        ctrl |= HSTIMER_CTL_AUTORELOAD;
        OSAL_WRITEL(ctrl, (info->regBase + HSTIMER_CTL_REG(number)));
    }

    return HDF_SUCCESS;
}

static int32_t TimerSun8iSetOnce(struct TimerCntrl *cntrl, uint32_t useconds, TimerHandleCb cb)
{
    CHECK_NULL_PTR_RETURN_VALUE(cntrl, HDF_ERR_INVALID_OBJECT);
    CHECK_NULL_PTR_RETURN_VALUE(cntrl->priv, HDF_ERR_INVALID_OBJECT);
    CHECK_NULL_PTR_RETURN_VALUE(cb, HDF_ERR_INVALID_OBJECT);
    struct TimerSun8iInfo *info = cntrl->priv;

    if (info->number < 6)
    {   //timer
        cntrl->info.useconds = useconds;
        cntrl->info.cb = cb;
        cntrl->info.isPeriod = false;

        uint32_t intval = useconds*(TIMER_TWOUSECOND) / 2;
        uint32_t ctrl = 0;
        info->cb = cb;
        info->isPeriod = false;

        /* set timer intervalue */
        OSAL_WRITEL(intval, info->regBase + TIMER_INTVAL_REG(info->number));
        OsalUDelay(1);

        /*reload the timer intervalue*/
        ctrl = OSAL_READL(info->regBase + TIMER_CTL_REG(info->number));
        ctrl |= TIMER_CTL_AUTORELOAD;
        ctrl |= TIMER_CTL_ONESHOT;
        OSAL_WRITEL(ctrl, (info->regBase + TIMER_CTL_REG(info->number)));
        // while (OSAL_READL(info->regBase + TIMER_CTL_REG(info->number)) & TIMER_CTL_AUTORELOAD);
    }else{//hstimer
        cntrl->info.useconds = useconds;
        cntrl->info.cb = cb;
        cntrl->info.isPeriod = false;
        int number = info->number%6;

        uint64_t intval = useconds*(HSTIMER_TWOUSECOND);
        uint32_t ctrl = 0;
        info->cb = cb;
        info->isPeriod = false;
        uint32_t low = (u_int32_t)(intval & 0xFFFFFFFF);
        uint32_t high = (u_int32_t)((intval >> 32) & 0xFFFFFFFF);

        /* set timer intervalue */
        OSAL_WRITEL(low, info->regBase + HSTIMER_INTVALL_REG(number));
        OSAL_WRITEL(high, info->regBase + HSTIMER_INTVALH_REG(number));
        OsalUDelay(1);

        /*reload the timer intervalue*/
        ctrl = OSAL_READL(info->regBase + HSTIMER_CTL_REG(number));
        ctrl |= HSTIMER_CTL_AUTORELOAD;
        ctrl |= HSTIMER_CTL_ONESHOT;
        OSAL_WRITEL(ctrl, (info->regBase + HSTIMER_CTL_REG(number)));
    }
    return HDF_SUCCESS;
}

static int32_t TimerSun8iIrqRegister(struct TimerSun8iInfo *info)
{
    CHECK_NULL_PTR_RETURN_VALUE(info, HDF_ERR_INVALID_OBJECT);
    if (!info->isIrqReg)
    {    
        if (OsalRegisterIrq(info->irq, 0, TimerSun8iIrqHandle, "timer_alarm", (void *)info) != HDF_SUCCESS) {
            HDF_LOGE("%s: OsalRegisterIrq[%u][%u] fail!", __func__, info->irq, info->number);
            return HDF_FAILURE;
        }
    }
    info->isIrqReg = true;
    return HDF_SUCCESS;
}

static int32_t TimerSun8iIrqUnregister(struct TimerSun8iInfo *info)
{
    CHECK_NULL_PTR_RETURN_VALUE(info, HDF_ERR_INVALID_OBJECT);

    (void)OsalUnregisterIrq(info->irq, (void *)info);
    info->isIrqReg = false;
    return HDF_SUCCESS;
}

static int32_t TimerSun8iStart(struct TimerCntrl *cntrl)
{
    CHECK_NULL_PTR_RETURN_VALUE(cntrl, HDF_ERR_INVALID_OBJECT);
    CHECK_NULL_PTR_RETURN_VALUE(cntrl->priv, HDF_ERR_INVALID_OBJECT);
    struct TimerSun8iInfo *info = cntrl->priv;

    int ret = TimerSun8iIrqRegister(info);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: TimerSun8iIrqRegister fail!", __func__);
        return HDF_FAILURE;
    }

    // not mask interrupt
    ret = TimerSun8iIntEnable(info, true);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: TimerSun8iIntEnable fail!", __func__);
        return HDF_FAILURE;
    }

    ret = TimerSun8iEnable(info, true);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: TimerSun8iEnable fail!", __func__);
        return HDF_FAILURE;
    }

    PlatformDumperDump(info->dumper);
    return HDF_SUCCESS;
}

static int32_t TimerSun8iStop(struct TimerCntrl *cntrl)
{
    CHECK_NULL_PTR_RETURN_VALUE(cntrl, HDF_ERR_INVALID_OBJECT);
    CHECK_NULL_PTR_RETURN_VALUE(cntrl->priv, HDF_ERR_INVALID_OBJECT);
    struct TimerSun8iInfo *info = cntrl->priv;
    int ret = TimerSun8iEnable(info, false);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: TimerSun8iEnable fail!", __func__);
        return HDF_FAILURE;
    }

    ret = TimerSun8iIntEnable(info, false);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: TimerSun8iIntEnable fail!", __func__);
        return HDF_FAILURE;
    }

    ret = TimerSun8iIrqUnregister(info);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: TimerSun8iIrqUnregister fail!", __func__);
        return HDF_FAILURE;
    }
    PlatformDumperDump(info->dumper);
    return HDF_SUCCESS;
}

static int32_t TimerSun8iOpen(struct TimerCntrl *cntrl)
{
    CHECK_NULL_PTR_RETURN_VALUE(cntrl, HDF_ERR_INVALID_OBJECT);
    CHECK_NULL_PTR_RETURN_VALUE(cntrl->priv, HDF_ERR_INVALID_OBJECT);
    return HDF_SUCCESS;
}

static int32_t TimerSun8iClose(struct TimerCntrl *cntrl)
{
    CHECK_NULL_PTR_RETURN_VALUE(cntrl, HDF_ERR_INVALID_OBJECT);
    TimerSun8iStop(cntrl);
    return HDF_SUCCESS;
}

static int32_t TimerSun8iRemove(struct TimerCntrl *cntrl)
{
    CHECK_NULL_PTR_RETURN_VALUE(cntrl, HDF_ERR_INVALID_OBJECT);
    CHECK_NULL_PTR_RETURN_VALUE(cntrl->priv, HDF_ERR_INVALID_OBJECT);
    struct TimerSun8iInfo *info = cntrl->priv;

    TimerSun8iStop(cntrl);
    if (info->regBase != NULL) {
        OsalIoUnmap((void *)info->regBase);
        info->regBase = NULL;
    }
    PlatformDumperDestroy(info->dumper);
    OsalMemFree(info->dumperName);

    OsalMemFree(cntrl->priv);
    cntrl->priv = NULL;
    return HDF_SUCCESS;
}

static struct TimerCntrlMethod g_timerCntlrMethod = {
    .Remove = TimerSun8iRemove,
    .Open = TimerSun8iOpen,
    .Close = TimerSun8iClose,
    .Set = TimerSun8iSet,  //设置周期时间
    .SetOnce = TimerSun8iSetOnce,//设置单次时间
    .Start = TimerSun8iStart,//开始定时器
    .Stop = TimerSun8iStop,
};

static int32_t TimerSun8iInitRegSet(struct TimerSun8iInfo *info)
{
    CHECK_NULL_PTR_RETURN_VALUE(info, HDF_ERR_INVALID_OBJECT);
    uint32_t val = 0;
    if (info->number < 6)
    { //timer 的初始化    
        /* set clock source to HOSC, 16 pre-division */
        val = OSAL_READL(info->regBase + TIMER_CTL_REG(info->number));
        val &= ~(0x07 << 4);
        val &= ~(0x03 << 2);
        val |= (4 << 4) | (1 << 2);
        OSAL_WRITEL(val, info->regBase + TIMER_CTL_REG(info->number));

        /* set mode to auto reload */
        val = OSAL_READL(info->regBase + TIMER_CTL_REG(info->number));
        OSAL_WRITEL(val | TIMER_CTL_AUTORELOAD, info->regBase + TIMER_CTL_REG(info->number));
        return HDF_SUCCESS;
    }else
    {//hstimer 的初始化

        /* 用于配置 */
        uint32_t GatingReg0 = IO_DEVICE_ADDR(0x01C20060);
        uint32_t ResetReg0  = IO_DEVICE_ADDR(0x01C202c0);
        uint32_t value = 0;

        value = OSAL_READL(GatingReg0);
        //HS Timer
        value |= (1 << 19);
        OSAL_WRITEL(value, GatingReg0);
        OSAL_WRITEL(value, ResetReg0);


        /* set mode to auto reload */
        val = OSAL_READL(info->regBase + HSTIMER_CTL_REG(info->number%6));
        val |= HSTIMER_CTL_AUTORELOAD;
        OSAL_WRITEL(val, info->regBase + HSTIMER_CTL_REG(info->number%6));
        return HDF_SUCCESS;   
    }
}

static int32_t TimerSun8iReadHcs(struct TimerSun8iInfo *info, const struct DeviceResourceNode *node)
{
    int32_t ret;
    uint32_t tmp;
    struct DeviceResourceIface *iface = NULL;

    iface = DeviceResourceGetIfaceInstance(HDF_CONFIG_SOURCE);
    if (iface == NULL || iface->GetUint32 == NULL) {
        HDF_LOGE("%s: invalid drs ops fail!", __func__);
        return HDF_FAILURE;
    }

    ret = iface->GetUint32(node, "number", &info->number, 0);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: read id fail!", __func__);
        return HDF_FAILURE;
    }

    ret = iface->GetUint32(node, "bus_clock", &info->busClock, 0);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: read [%u] bus_clock fail!", __func__, info->number);
        return HDF_FAILURE;
    }

    ret = iface->GetUint32(node, "mode", &info->mode, 0);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: read [%u] mode fail!", __func__, info->number);
        return HDF_FAILURE;
    }

    ret = iface->GetUint32(node, "init_count_val", &info->initCountVal, 0);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: read [%u] init_count_val fail!", __func__, info->number);
        return HDF_FAILURE;
    }

    ret = iface->GetUint32(node, "irq", &info->irq, 0);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: read [%u] irq fail!", __func__, info->number);
        return HDF_FAILURE;
    }

    if (iface->GetUint32(node, "reg_base", &tmp, 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: read [%u] reg_base fail", __func__, info->number);
        return HDF_FAILURE;
    }
    info->regBase = OsalIoRemap(tmp, TIMER_MAX_REG_SIZE);
    if (info->regBase == NULL) {
        HDF_LOGE("%s:OsalIoRemap fail", __func__);
        return HDF_FAILURE;
    }

    // dprintf("%s:number[%u], reg_base[%x]->[%x],bus_clock[%u], mode[%u], init_count_val[%u] irq[%u]\n", __func__,
    //     info->number, tmp, info->regBase, info->busClock, info->mode, info->initCountVal, info->irq);

    return HDF_SUCCESS;
}

static int32_t TimerSun8iInitHandle(const struct DeviceResourceNode *node, struct TimerSun8iInfo *info)
{
    CHECK_NULL_PTR_RETURN_VALUE(node, HDF_ERR_INVALID_OBJECT);
    CHECK_NULL_PTR_RETURN_VALUE(info, HDF_ERR_INVALID_OBJECT);

    int32_t ret = TimerSun8iReadHcs(info, node);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: TimerSun8iReadHcs fail!", __func__);
        return ret;
    }

    ret = TimerSun8iInitRegSet(info);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: TimerSun8iInitRegSet fail!", __func__);
        return ret;
    }
    info->isIrqReg = false;

    return HDF_SUCCESS;
}

static void TimerSun8iInfoFree(struct TimerCntrl *cntrl)
{
    CHECK_NULL_PTR_RETURN(cntrl);
    if (cntrl->priv != NULL) {
        OsalMemFree(cntrl->priv);
        cntrl->priv = NULL;
    }

    if (cntrl != NULL) {
        OsalMemFree(cntrl);
    }
}

static void TimerDumperGet(struct TimerSun8iInfo *info)
{
    struct PlatformDumper *dumper = NULL;
    char *name = (char *)OsalMemAlloc(TIMER_DUMPER_NAME_LEN);
    if (name == NULL) {
        return;
    }
    if (snprintf_s(name, TIMER_DUMPER_NAME_LEN, TIMER_DUMPER_NAME_LEN - 1, "%s%d",
        TIMER_DUMPER_NAME_PREFIX, info->number) < 0) {
        HDF_LOGE("%s: snprintf_s name fail!", __func__);
        OsalMemFree(name);
        return;
    }
    dumper = PlatformDumperCreate(name);
    if (dumper == NULL) {
        HDF_LOGE("%s: get dumper for %s fail!", __func__, name);
        OsalMemFree(name);
        return;
    }
    info->dumperName = name;
    info->dumper = dumper;
}

static int32_t TimerSun8iParseAndInit(struct HdfDeviceObject *device, const struct DeviceResourceNode *node)
{
    int32_t ret;
    struct TimerCntrl *cntrl = NULL;
    struct TimerSun8iInfo *info = NULL;
    (void)device;

    cntrl = (struct TimerCntrl *)OsalMemCalloc(sizeof(*cntrl));
    CHECK_NULL_PTR_RETURN_VALUE(cntrl, HDF_ERR_INVALID_OBJECT);

    info = (struct TimerSun8iInfo *)OsalMemCalloc(sizeof(*info));
    if (cntrl == NULL) {
        HDF_LOGE("%s: malloc info fail!", __func__);
        TimerSun8iInfoFree(cntrl);
        return HDF_ERR_MALLOC_FAIL;
    }
    cntrl->priv = (void *)info;

    ret = TimerSun8iInitHandle(node, info);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: TimerSun8iInitHandle fail!", __func__);
        TimerSun8iInfoFree(cntrl);
        return ret;
    }

    cntrl->info.number = info->number;
    cntrl->ops = &g_timerCntlrMethod;
    TimerDumperGet(info);
    ret = TimerCntrlAdd(cntrl);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: TimerCntrlAdd fail!", __func__);
        TimerSun8iInfoFree(cntrl);
        return ret;
    }
    return HDF_SUCCESS;
}

static int32_t TimerSun8iDeviceInit(struct HdfDeviceObject *device)
{
    int32_t ret;
    const struct DeviceResourceNode *childNode = NULL;
    if (device == NULL || device->property == NULL) {
        HDF_LOGE("%s: device or property is NULL", __func__);
        return HDF_ERR_INVALID_OBJECT;
    }

    DEV_RES_NODE_FOR_EACH_CHILD_NODE(device->property, childNode) {
        ret = TimerSun8iParseAndInit(device, childNode);
        if (ret != HDF_SUCCESS) {
            HDF_LOGE("%s:TimerSun8iParseAndInit fail", __func__);
            return HDF_FAILURE;
        }
    }
    HDF_LOGD("%s: success", __func__);
    return HDF_SUCCESS;
}

static int32_t TimerSun8iDeviceBind(struct HdfDeviceObject *device)
{
    CHECK_NULL_PTR_RETURN_VALUE(device, HDF_ERR_INVALID_OBJECT);
    HDF_LOGI("%s: success", __func__);
    return HDF_SUCCESS;
}

static void TimerSun8iRemoveById(const struct DeviceResourceNode *node)
{
    int32_t ret;
    uint32_t timerId;
    struct DeviceResourceIface *drsOps = NULL;

    drsOps = DeviceResourceGetIfaceInstance(HDF_CONFIG_SOURCE);
    if (drsOps == NULL || drsOps->GetUint32 == NULL) {
        HDF_LOGE("%s: invalid drs ops fail!", __func__);
        return;
    }

    ret = drsOps->GetUint32(node, "id", (uint32_t *)&timerId, 0);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: read id fail!", __func__);
        return;
    }

    TimerCntrlRemoveByNumber(timerId);
    return;
}

static void TimerSun8iDeviceRelease(struct HdfDeviceObject *device)
{
    const struct DeviceResourceNode *childNode = NULL;

    HDF_LOGD("%s: in", __func__);
    if (device == NULL || device->property == NULL) {
        HDF_LOGE("%s: device is NULL", __func__);
        return;
    }

    DEV_RES_NODE_FOR_EACH_CHILD_NODE(device->property, childNode) {
        TimerSun8iRemoveById(childNode);
    }
}

struct HdfDriverEntry g_hdfTimerDevice = {
    .moduleVersion = 1,
    .moduleName = "sun8i_timer_driver",
    .Bind = TimerSun8iDeviceBind,
    .Init = TimerSun8iDeviceInit,
    .Release = TimerSun8iDeviceRelease,
};
HDF_INIT(g_hdfTimerDevice);
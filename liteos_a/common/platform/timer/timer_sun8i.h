/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TIMER_SUN8I_H
#define TIMER_SUN8I_H

#include "hdf_base.h"
#include "los_vm_zone.h"
#include "platform_dumper.h"
#include "timer_core.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif /* __cplusplus */
#endif /* __cplusplus */

#define TIMER_MAX_REG_SIZE (0x100)
#define TIMER_DUMPER_NAME_PREFIX "timer_dumper_"
#define TIMER_DUMPER_NAME_LEN 64
#define TIMER_DUMPER_DATAS_REGISTER_SIZE 10

struct TimerSun8iInfo {
    uint32_t number;
    uint32_t busClock;
    uint32_t mode;
    uint32_t initCountVal;
    uint32_t irq;
    bool isIrqReg;
    bool isPeriod;
    volatile uint8_t *regBase;
    TimerHandleCb cb;
    char *dumperName;
    struct PlatformDumper *dumper;
};


#define TIMER_TWOUSECOND    3 
#define TIMER_IRQ_EN_REG	0x00
#define TIMER_IRQ_EN(val)	(1 << val)
#define TIMER_IRQ_ST_REG	0x04
#define TIMER_IRQ_ST(val)	(1 << val)
#define TIMER_CTL_REG(val)	(0x10 * val + 0x10)
#define TIMER_CTL_ENABLE	(1 << 0)
#define TIMER_CTL_AUTORELOAD	(1 << 1)
#define TIMER_CTL_MODE_MASK	(1 << 7)
#define TIMER_CTL_PERIODIC	(0 << 7)
#define TIMER_CTL_ONESHOT	(1 << 7)
#define TIMER_INTVAL_REG(val)	(0x10 * val + 0x14)
#define TIMER_CNTVAL_REG(val)	(0x10 * val + 0x18)

#define HSTIMER_TWOUSECOND    200 
#define HSTIMER_IRQ_EN_REG	0x00
#define HSTIMER_IRQ_EN(val)	(1 << val)
#define HSTIMER_IRQ_ST_REG	0x04
#define HSTIMER_IRQ_ST(val)	(1 << val)
#define HSTIMER_CTL_REG(val)	(0x20 * val + 0x10)
#define HSTIMER_CTL_ENABLE	(1 << 0)
#define HSTIMER_CTL_AUTORELOAD	(1 << 1)
#define HSTIMER_CTL_MODE_MASK	(1 << 7)
#define HSTIMER_CTL_PERIODIC	(0 << 7)
#define HSTIMER_CTL_ONESHOT	(1 << 7)
#define HSTIMER_INTVALL_REG(val)	(0x20 * val + 0x14)
#define HSTIMER_INTVALH_REG(val)	(0x20 * val + 0x18)
#define HSTIMER_CNTVALL_REG(val)	(0x20 * val + 0x12)
#define HSTIMER_CNTVALH_REG(val)	(0x20 * val + 0x20)


#ifdef __cplusplus
#if __cplusplus
}
#endif /* __cplusplus */
#endif /* __cplusplus */
#endif /* TIMER_SUN8I_H */

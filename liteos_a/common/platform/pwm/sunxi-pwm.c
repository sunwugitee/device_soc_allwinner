#include "sunxi-pwm.h"
#include "device_resource_if.h"
#include "hdf_device_desc.h"
#include "hdf_log.h"
#include "osal_io.h"
#include "osal_mem.h"
#include "pwm_core.h"
#define HDF_LOG_TAG sunxi_pwm
#define PWM_MAX_PERIOD 100
#define PWM_CLK_CTR_BASE 0x0020
#define PWM_CLK_CTR_MASK 0x4
#define PWM_ENABLE_REG  0x0040
#define PWM_CTR_REG  0x0060
#define PWM_CNT_REG  0x0068
#define PWM_PERIOD_REG 0x0064
#define GENMASK_U32(h, l) \
    (((~0UL) << (l)) & (~0UL >> (32 - 1 - (h)))) 

#define CLK_SRC_SEL GENMASK_U32(8,7)
#define BYPASS_TO_BIT6 BIT(6)
#define BYPASS_TO_BIT5 BIT(5)
#define PASS_MASK GENMASK_U32(6,5)
#define CLK_GATING BIT(4)
#define PWM_CLK_DIV_M GENMASK_U32(3,0)
#define PW_CTR_MODE BIT(9)
#define PWM_POLARITY BIT(8)
#define PWM_PUL_START BIT(10)
#define PWM_PERIOD_RDY BIT(11)

struct Pwm {
    struct PwmDev dev;
    volatile unsigned char *base;
    uint32_t channel;
    uint16_t clk_sel;
    uint16_t clk_div;
    uint16_t pwm_prescal;
    bool supportPolarity;
};

static inline uint16_t SunXiPwmGetOffset(struct Pwm *hp)
{
    return hp->channel*0x20;    
}

static inline void SunXiPwmenable(struct Pwm *hp,uint16_t value)
{
    uint16_t val,mask;
    val=OSAL_READL(hp->base+PWM_ENABLE_REG);
    mask=value<< hp->channel;
    val&=mask;
    OSAL_WRITEL(val,hp->base+0x40);//对于通道写0,disable
}

static void PwmClkSet(struct Pwm *hp)
{   
    uint16_t offset,val,passmask;
    //确定pwm组
    offset=(hp->channel/2)*PWM_CLK_CTR_MASK+PWM_CLK_CTR_BASE;
    val=OSAL_READL(hp->base+offset);
    //选择时钟源CLK_SRC_SEL 在pwm结构体初始化
    // GENMASK(8, 7)
    val&=~CLK_SRC_SEL;
    val|=hp->clk_sel<<7;
    //双通道选择bypass
    if(hp->channel%2) passmask=BYPASS_TO_BIT5;
    else passmask=BYPASS_TO_BIT6;
    val&=~PASS_MASK;
    val|=passmask;
    //gating 门开关为1
    val&=~CLK_GATING;
    val|=CLK_GATING;
    //prescal选择
    val&=~PWM_CLK_DIV_M;
    val|=hp->clk_div;
    // 输出clk
    OSAL_WRITEL(val,hp->base+offset);
}

static uint32_t PwmClkGetVal(struct Pwm *hp)
{
    uint16_t val;
    uint32_t clk_val,div=1;
    val=OSAL_READL(hp->base+hp->channel/2*PWM_CLK_CTR_MASK+PWM_CLK_CTR_BASE);
    if(hp->clk_sel)
    {clk_val=2000000;}
    else clk_val=24000000;
    for(int i=0;i<hp->clk_div;i++)
    {
        div*=2;
    }
    clk_val/=div;
    return clk_val;
}

static inline void SunXiPwmSetPeriod(struct Pwm *hp,uint32_t period)
{
    //输入period为周期的时间
    uint32_t clk_val,val;
    
    uint16_t prescal,entire_cycle,ctr_val;
    PwmClkSet(hp);//得到时钟频率3MHZ 
    val=OSAL_READL(hp->base+PWM_PERIOD_REG+SunXiPwmGetOffset(hp));
    val&=0xffff;//清除高16位（整体周期）
    /*pwm分频系数的确定 在ctr reg的低八位*/
    prescal=hp->pwm_prescal;
    ctr_val=OSAL_READL(hp->base+PWM_CTR_REG+SunXiPwmGetOffset(hp));
    ctr_val&=~0xff;
    ctr_val|=prescal-1;
    OSAL_WRITEL(ctr_val,hp->base+PWM_CTR_REG+SunXiPwmGetOffset(hp));
    /*时钟的获取*/
    clk_val=PwmClkGetVal(hp);
    entire_cycle=clk_val/1000/prescal*period;//除以1000转化为ms
    val|=(entire_cycle<<16);
     //得到entire的值  
    OSAL_WRITEL(val,hp->base+PWM_PERIOD_REG+SunXiPwmGetOffset(hp));
}

static inline void SunXiPwmSetDuty(struct Pwm *hp,uint32_t duty)
{
    uint32_t active,val;
    uint32_t entire,status;
    val=OSAL_READL(hp->base+PWM_CTR_REG+SunXiPwmGetOffset(hp));//读取开始电平状态
    status=(val&PWM_POLARITY)>>8;  
    val=OSAL_READL(hp->base+PWM_PERIOD_REG+SunXiPwmGetOffset(hp));
    val&=~0xffff;//清除低16位
    entire=val>>16;  
    if(status)
    {
    active=entire/100*duty;
    val|=active;
    OSAL_WRITEL(val,hp->base+PWM_PERIOD_REG+SunXiPwmGetOffset(hp));
    }
    else
    {
    active=entire-entire/100*duty;
    val|=active;
    OSAL_WRITEL(val,hp->base+PWM_PERIOD_REG+SunXiPwmGetOffset(hp));   
    }
 
}


static inline void SunXiPwmSetPolarity(struct Pwm *hp,uint32_t polarity)
{
    uint16_t val;
    val=OSAL_READL(hp->base+PWM_CTR_REG+SunXiPwmGetOffset(hp));
    val&=~PWM_POLARITY;
    val|=polarity<<8;
    OSAL_WRITEL(val,hp->base+PWM_CTR_REG+SunXiPwmGetOffset(hp));
}
static inline void SunXiPwmAlwaysOutput(struct Pwm *hp )
{
    uint16_t val,temp;
    temp=OSAL_READL(hp->base+PWM_ENABLE_REG);
    OSAL_WRITEL(temp|BIT(hp->channel),hp->base+PWM_ENABLE_REG);//使能通道 enable
    val=OSAL_READL(hp->base+PWM_CTR_REG+SunXiPwmGetOffset(hp));
    val&=~BIT(9);
    OSAL_WRITEL(val,hp->base+PWM_CTR_REG+SunXiPwmGetOffset(hp));

}

static inline void SunXiPwmOutputNumberSquareWaves(struct Pwm *hp,uint32_t num)
{   
    uint16_t val;
    uint32_t i;
    uint16_t temp;
    temp=OSAL_READL(hp->base+PWM_ENABLE_REG);
    OSAL_WRITEL(temp|BIT(hp->channel),hp->base+PWM_ENABLE_REG);//使能通道 enable
    val=OSAL_READL(hp->base+PWM_CTR_REG+SunXiPwmGetOffset(hp));
    val&=~PW_CTR_MODE;
    val|=PW_CTR_MODE;//mode位置1
    for(i=0;i<num;i++)
    {
        val|=PWM_PUL_START;//几个方波几次置1信号
        OSAL_WRITEL(val,hp->base+PWM_CTR_REG+SunXiPwmGetOffset(hp));
    }

}

int32_t SunXiPwmSetConfig(struct PwmDev *pwm, struct PwmConfig *config)
{
    struct Pwm *hp = (struct Pwm *)pwm;
    if (hp == NULL || hp->base == NULL || config == NULL) {
        HDF_LOGE("%s: hp reg or config is null", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    if (config->polarity != PWM_NORMAL_POLARITY && config->polarity != PWM_INVERTED_POLARITY) {
        HDF_LOGE("%s: polarity %hhu is invalid", __func__, config->polarity);
        return HDF_ERR_INVALID_PARAM;
    }
    if (config->period > PWM_MAX_PERIOD) {
        HDF_LOGE("%s: period %u is not support, min period %d", __func__, config->period, PWM_MAX_PERIOD);      
        return HDF_ERR_INVALID_PARAM;

    }
    if (config->duty < 1 || config->duty > config->period) {
        HDF_LOGE("%s: duty %u is not support, duty must in [1, period = %u].",
            __func__, config->duty, config->period);
        return HDF_ERR_INVALID_PARAM;
    }
    if(pwm->cfg.status != config->status)
    {    
    SunXiPwmenable(hp,config->status);
    HDF_LOGI("%s: [SunXiPwmenable] done, status: %hhu -> %hhu.\n",__func__,pwm->cfg.status,config->status);
    }
    //判断传入参数是否输入（PwmConfig传入参数结构体，PwmDev驱动函数结构体）
    if (pwm->cfg.polarity != config->polarity && hp->supportPolarity) {
        SunXiPwmSetPolarity(hp, config->polarity);
        HDF_LOGI("%s: [SunXiPwmSetPolarity] done, polarity: %hhu -> %hhu.", __func__, pwm->cfg.polarity, config->polarity);
    }
    if (pwm->cfg.period != config->period) {
        SunXiPwmSetPeriod(hp, config->period);
        HDF_LOGI("%s: [SunXiPwmSetPeriod] done, period: %u -> %u", __func__, pwm->cfg.period, config->period);
    }
    if (pwm->cfg.duty != config->duty) {
        SunXiPwmSetDuty(hp, config->duty);
        HDF_LOGI("%s: [SunXiPwmSetDuty] done, duty: %u -> %u", __func__, pwm->cfg.duty, config->duty);
    }
    if (config->status == PWM_ENABLE_STATUS) {
        if (config->number == 0) {
            SunXiPwmAlwaysOutput(hp);
            HDF_LOGI("%s: [SunXiPwmAlwaysOutput] done, then enable.", __func__);
        } else {
            SunXiPwmOutputNumberSquareWaves(hp, config->number);
        }
    }
    HDF_LOGI("%s: set PwmConfig done: number %u, period %u, duty %u, polarity %hhu, enable %hhu.",
        __func__, config->number, config->period, config->duty, config->polarity, config->status);
    return HDF_SUCCESS;
}

struct PwmMethod g_pwmOps = {
    .setConfig = SunXiPwmSetConfig,
};

static void SunXiPwmRemove(struct Pwm *hp)
{
    if (hp->base != NULL) {
        OsalIoUnmap((void *)hp->base);
    }
    OsalMemFree(hp);
}


//读取设备树
static int32_t SunXiPwmProbe(struct Pwm *hp, struct HdfDeviceObject *obj)
{
    uint32_t tmp;
    struct DeviceResourceIface *iface = NULL;

    iface = DeviceResourceGetIfaceInstance(HDF_CONFIG_SOURCE);
    if (iface == NULL || iface->GetUint32 == NULL) {
        HDF_LOGE("%s: face is invalid", __func__);
        return HDF_FAILURE;
    }

    if (iface->GetUint32(obj->property, "num", &(hp->dev.num), 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: read num fail", __func__);
        return HDF_FAILURE;
    }
    if (iface->GetUint32(obj->property, "base", &tmp, 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: read base fail", __func__);
        return HDF_FAILURE;
    }
    //地址映射
    hp->base = OsalIoRemap(tmp, 0x1000);
    if (hp->base == NULL) {
        HDF_LOGE("%s: OsalIoRemap fail", __func__);
        return HDF_FAILURE;
    }

    hp->clk_div=3;//时钟8分频
    hp->clk_sel=0;//选择24MHZ
    hp->pwm_prescal=30;//预分频
    hp->supportPolarity = true;//极性支持
    hp->channel=hp->dev.num;
    hp->dev.method = &g_pwmOps;
    hp->dev.cfg.duty = 0;//占空比
    hp->dev.cfg.period = 0;//比例值
    hp->dev.cfg.polarity = 0;//极性
    hp->dev.cfg.status =0;//状态
    hp->dev.cfg.number = 0;
    hp->dev.busy = false;
    if (PwmDeviceAdd(obj, &(hp->dev)) != HDF_SUCCESS) {
        OsalIoUnmap((void *)hp->base);
        HDF_LOGE("%s: [PwmDeviceAdd] failed.", __func__);
        return HDF_FAILURE;
    }
    HDF_LOGI("%s: set PwmConfig: number %u, period %u, duty %u, polarity %hhu, enable %hhu.", __func__,
        hp->dev.cfg.number, hp->dev.cfg.period, hp->dev.cfg.duty, hp->dev.cfg.polarity, hp->dev.cfg.status);
    return HDF_SUCCESS;
}

static int32_t HdfPwmBind(struct HdfDeviceObject *obj)
{
    (void)obj;
    return HDF_SUCCESS;
}

static int32_t HdfPwmInit(struct HdfDeviceObject *obj)
{
    int ret;
    struct Pwm *hp = NULL;

    if (obj == NULL) {
        HDF_LOGE("%s: obj is null", __func__);
        return HDF_ERR_INVALID_OBJECT;
    }
    hp = (struct Pwm *)OsalMemCalloc(sizeof(*hp));
    if (hp == NULL) {
        HDF_LOGE("%s: OsalMemCalloc hp error", __func__);
        return HDF_ERR_MALLOC_FAIL;
    }

    ret = SunXiPwmProbe(hp, obj);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: error probe, ret is %d", __func__, ret);
        OsalMemFree(hp);
    }
    HDF_LOGI("%s: pwm init success", __func__);
    return ret;
}

static void HdfPwmRelease(struct HdfDeviceObject *obj)
{
    struct Pwm *hp = NULL;

    HDF_LOGI("%s: entry", __func__);
    if (obj == NULL) {
        HDF_LOGE("%s: obj is null", __func__);
        return;
    }
    hp = (struct Pwm *)obj->service;
    if (hp == NULL) {
        HDF_LOGE("%s: hp is null", __func__);
        return;
    }
    PwmDeviceRemove(obj, &(hp->dev));
    SunXiPwmRemove(hp);
}

struct HdfDriverEntry g_hdfPwm = {
    .moduleVersion = 1,
    .moduleName = "HDF_PLATFORM_PWM",
    .Bind = HdfPwmBind,
    .Init = HdfPwmInit,
    .Release = HdfPwmRelease,
};

HDF_INIT(g_hdfPwm);

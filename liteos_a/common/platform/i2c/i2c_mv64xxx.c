/*
 * Copyright (c) 2020 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "i2c_mv64xxx.h"
#include "los_event.h"
#include "los_hwi.h"
#include "securec.h"
#include "device_resource_if.h"
#include "hdf_device_desc.h"
#include "hdf_log.h"
#include "i2c_core.h"
#include "i2c_dev.h"
#include "osal_io.h"
#include "osal_mem.h"
#include "osal_spinlock.h"
#include "osal_time.h"
#include "osal_irq.h"
#include "osal_sem.h"

#define USER_VFS_SUPPORT


//自定义私有结构体
struct Mv64xxxI2cCntlr {
    struct I2cCntlr cntlr;
    OsalSpinlock spin;
    volatile unsigned char  *regBase;
    uint16_t regSize;
    int16_t  bus;
    uint32_t clk;
    uint32_t freq;
    uint32_t freqM;
    uint32_t freqN;
    uint32_t irq;
    uint32_t regBasePhy;
    uint32_t addr1;
    uint32_t addr2;
    uint32_t cntlBits;
    uint32_t state;
    uint32_t action;
    uint32_t aborting;
    uint32_t bytesLeft;
    uint32_t bytePosn;
    uint32_t sendStop;
    uint32_t block;
/* 5us delay in order to avoid repeated start timing violation */
	int rc;
    struct I2cMsg *msg;
    struct I2cMsg *msgs;
    uint32_t num_msgs;
    OsalSpinlock irqSpinlock;
    struct OsalSem sem;
    EVENT_CB_S stEvent;
};

#define WRITE_REG_BIT(value, offset, addr) \
    do {                                   \
        unsigned long t, mask;             \
        mask = 1 << (offset);              \
        t = OSAL_READL(addr);              \
        t &= ~mask;                        \
        t |= ((value) << (offset)) & mask; \
        OSAL_WRITEL(t, (addr));            \
    } while (0)


static inline void Mv64xxxI2cHwInitCfg(struct Mv64xxxI2cCntlr *mv64xxx)
{
    // dprintf("i2c freqM:%d freqN:%d \n", mv64xxx->freqM, mv64xxx->freqN);
    MV64XXX_WRITEL(1, (uintptr_t)mv64xxx->regBase + TWI_SRST);
    while(MV64XXX_READL((uintptr_t)mv64xxx->regBase + TWI_SRST) == 1)
    {
        OsalUDelay(50);
    }
    MV64XXX_WRITEL(MV64XXX_I2C_BAUD_DIV_M(mv64xxx->freqM) | MV64XXX_I2C_BAUD_DIV_N(mv64xxx->freqN), (uintptr_t)mv64xxx->regBase + TWI_CCR);
    MV64XXX_WRITEL(0, (uintptr_t)mv64xxx->regBase + TWI_ADDR);
    MV64XXX_WRITEL(0, (uintptr_t)mv64xxx->regBase + TWI_XADDR);
    MV64XXX_WRITEL(MV64XXX_I2C_REG_CONTROL_TWSIEN | MV64XXX_I2C_REG_CONTROL_STOP, (uintptr_t)mv64xxx->regBase + TWI_CNTR);
    /* whether need delay ? no*/

    mv64xxx->state = MV64XXX_I2C_STATE_IDLE;
}

static inline void Mv64xxxI2cFreqCfg(struct Mv64xxxI2cCntlr *mv64xxx, const uint32_t req_freq, const uint32_t tclk)
{	int32_t m, n;

	for (n = 0; n <= 7; n++)
		for (m = 0; m <= 15; m++) {
			if(tclk == req_freq / (10 * (m + 1) * (1 << n)))
            {	mv64xxx->freqM = m;
				mv64xxx->freqN = n;
                return;
			}		
		}
}

static void Mv64xxxI2cCntlrInit(struct Mv64xxxI2cCntlr *mv64xxx)
{
    /* 
     * only for debug TWI0 ,set PB0 PB1 function TWI 
     * must set addr:0x01C208 offset:0x0024 val:0x77777722
     */
    Mv64xxxI2cFreqCfg(mv64xxx, mv64xxx->clk, mv64xxx->freq);
    Mv64xxxI2cHwInitCfg(mv64xxx);
    HDF_LOGI("%s: cntlr:%u init done!", __func__, mv64xxx->bus);
}

static void Mv64xxxI2cFsm(struct Mv64xxxI2cCntlr *mv64xxx, uint32_t status)
{
    /*
	 * If state is idle, then this is likely the remnants of an old
	 * operation that driver has given up on or the user has killed.
	 * If so, issue the stop condition and go to idle.
	 */
	if (mv64xxx->state == MV64XXX_I2C_STATE_IDLE) {
		mv64xxx->action = MV64XXX_I2C_ACTION_SEND_STOP;
		return;
	}
    // dprintf("[%4d] %s status:%x\n", __LINE__, __func__, status);
    /* The status from the ctlr [mostly] tells us what to do next */
	switch (status) {
	/* Start condition interrupt */
	case MV64XXX_I2C_STATUS_MAST_START: /* 0x08 */
	case MV64XXX_I2C_STATUS_MAST_REPEAT_START: /* 0x10 */
		mv64xxx->action = MV64XXX_I2C_ACTION_SEND_ADDR_1;
		mv64xxx->state = MV64XXX_I2C_STATE_WAITING_FOR_ADDR_1_ACK;
		break;

	/* Performing a write */
	case MV64XXX_I2C_STATUS_MAST_WR_ADDR_ACK: /* 0x18 */
		if (mv64xxx->msg->flags & I2C_M_TEN) {
			mv64xxx->action = MV64XXX_I2C_ACTION_SEND_ADDR_2;
			mv64xxx->state =
				MV64XXX_I2C_STATE_WAITING_FOR_ADDR_2_ACK;
			break;
		}
	case MV64XXX_I2C_STATUS_MAST_WR_ADDR_2_ACK: /* 0xd0 */
	case MV64XXX_I2C_STATUS_MAST_WR_ACK: /* 0x28 */
		if ((mv64xxx->bytesLeft == 0)
				|| (mv64xxx->aborting
					&& (mv64xxx->bytePosn != 0))) {
			if (mv64xxx->sendStop || mv64xxx->aborting) {
				mv64xxx->action = MV64XXX_I2C_ACTION_SEND_STOP;
				mv64xxx->state = MV64XXX_I2C_STATE_IDLE;
			} else {
				mv64xxx->action =
					MV64XXX_I2C_ACTION_SEND_RESTART;
				mv64xxx->state =
					MV64XXX_I2C_STATE_WAITING_FOR_RESTART;
			}
		} else {
			mv64xxx->action = MV64XXX_I2C_ACTION_SEND_DATA;
			mv64xxx->state =
				MV64XXX_I2C_STATE_WAITING_FOR_SLAVE_ACK;
			mv64xxx->bytesLeft--;
		}
		break;

	/* Performing a read */
	case MV64XXX_I2C_STATUS_MAST_RD_ADDR_ACK: /* 40 */
		if (mv64xxx->msg->flags & I2C_M_TEN) {
			mv64xxx->action = MV64XXX_I2C_ACTION_SEND_ADDR_2;
			mv64xxx->state =
				MV64XXX_I2C_STATE_WAITING_FOR_ADDR_2_ACK;
			break;
		}
	case MV64XXX_I2C_STATUS_MAST_RD_ADDR_2_ACK: /* 0xe0 */
		if (mv64xxx->bytesLeft == 0) {
			mv64xxx->action = MV64XXX_I2C_ACTION_SEND_STOP;
			mv64xxx->state = MV64XXX_I2C_STATE_IDLE;
			break;
		}
	case MV64XXX_I2C_STATUS_MAST_RD_DATA_ACK: /* 0x50 */
		if (status != MV64XXX_I2C_STATUS_MAST_RD_DATA_ACK)
			mv64xxx->action = MV64XXX_I2C_ACTION_CONTINUE;
		else {
			mv64xxx->action = MV64XXX_I2C_ACTION_RCV_DATA;
			mv64xxx->bytesLeft--;
		}
		mv64xxx->state = MV64XXX_I2C_STATE_WAITING_FOR_SLAVE_DATA;

		if ((mv64xxx->bytesLeft == 1) || mv64xxx->aborting)
			mv64xxx->cntlBits &= ~MV64XXX_I2C_REG_CONTROL_ACK;
		break;

	case MV64XXX_I2C_STATUS_MAST_RD_DATA_NO_ACK: /* 0x58 */
		mv64xxx->action = MV64XXX_I2C_ACTION_RCV_DATA_STOP;
		mv64xxx->state = MV64XXX_I2C_STATE_IDLE;
		break;

	case MV64XXX_I2C_STATUS_MAST_WR_ADDR_NO_ACK: /* 0x20 */
	case MV64XXX_I2C_STATUS_MAST_WR_NO_ACK: /* 30 */
	case MV64XXX_I2C_STATUS_MAST_RD_ADDR_NO_ACK: /* 48 */
		/* Doesn't seem to be a device at other end */
		mv64xxx->action = MV64XXX_I2C_ACTION_SEND_STOP;
		mv64xxx->state = MV64XXX_I2C_STATE_IDLE;
		mv64xxx->rc = HDF_ERR_IO;
		break;

	default:
		mv64xxx->action = MV64XXX_I2C_ACTION_SEND_STOP;
		Mv64xxxI2cHwInitCfg(mv64xxx);
        /* 恢复I2C总线,在I2C通讯时出现了异常错误导致后续无法正常通讯,可以使用这个API尝试恢复I2C,若不行需要重新断电复位 */
		// i2c_recover_bus(&drv_data->adapter);
		mv64xxx->rc = HDF_ERR_INVALID_OBJECT;
	}
}

static int32_t Mv64xxxI2cXferPrepareForIo(struct Mv64xxxI2cCntlr *mv64xxx)
{
    int32_t status = 0;
    int32_t	dir = 0;
    struct I2cMsg *msg = mv64xxx->msgs;

    // dprintf("[%4d] %s msg:%p, addr:%x, flags:0x%x, len=%d\n", __LINE__, __func__, msg, msg->addr, msg->flags, msg->len);
    mv64xxx->cntlBits = MV64XXX_I2C_REG_CONTROL_ACK | MV64XXX_I2C_REG_CONTROL_INTEN | MV64XXX_I2C_REG_CONTROL_TWSIEN;
    /* read order */
    if (msg->flags & I2C_FLAG_READ) {
        dir = 1;
    }
    if(msg->flags & I2C_FLAG_ADDR_10BIT){//10bit地址
        mv64xxx->addr1 = 0xf0 | (((uint32_t)msg->addr & 0x300) >> 7) | dir;
        mv64xxx->addr2 = (uint32_t)msg->addr & 0xff;
    }else{
        mv64xxx->addr1 = MV64XXX_I2C_ADDR_ADDR((uint32_t)msg->addr) | dir;
        mv64xxx->addr2 = 0;
    }
    return status;
/* End of processing */
// end:
//     return status;
}
static void Mv64xxxI2cSendStart(struct Mv64xxxI2cCntlr *mv64xxx)
{
    mv64xxx->msg = mv64xxx->msgs;
    mv64xxx->bytePosn = 0;
    mv64xxx->bytesLeft = mv64xxx->msg->len;
    mv64xxx->aborting = 0;
    mv64xxx->rc = 0;
    Mv64xxxI2cXferPrepareForIo(mv64xxx);
    MV64XXX_WRITEL(mv64xxx->cntlBits | MV64XXX_I2C_REG_CONTROL_START, (uintptr_t)mv64xxx->regBase + TWI_CNTR);
}

static void Mv64xxxI2cDoAction(struct Mv64xxxI2cCntlr *mv64xxx)
{
    // dprintf("[%4d] %s action:%x\n", __LINE__, __func__, mv64xxx->action);
    // uint8_t val;
	switch(mv64xxx->action) {
        case MV64XXX_I2C_ACTION_SEND_RESTART:
            /* We should only get here if we have further messages */
            mv64xxx->msgs++;
            mv64xxx->num_msgs--;
            Mv64xxxI2cSendStart(mv64xxx);
            /*
            * We're never at the start of the message here, and by this
            * time it's already too late to do any protocol mangling.
            * Thankfully, do not advertise support for that feature.
            */
            mv64xxx->sendStop = mv64xxx->num_msgs == 1;
            break;

	case MV64XXX_I2C_ACTION_CONTINUE:
        MV64XXX_WRITEL(mv64xxx->cntlBits, (uintptr_t)mv64xxx->regBase + TWI_CNTR);
		break;

	case MV64XXX_I2C_ACTION_SEND_ADDR_1:
        MV64XXX_WRITEL(mv64xxx->addr1, (uintptr_t)mv64xxx->regBase + TWI_DATA);
        MV64XXX_WRITEL(mv64xxx->cntlBits, (uintptr_t)mv64xxx->regBase + TWI_CNTR);
		break;

	case MV64XXX_I2C_ACTION_SEND_ADDR_2:
        MV64XXX_WRITEL(mv64xxx->addr2, (uintptr_t)mv64xxx->regBase + TWI_DATA);
        MV64XXX_WRITEL(mv64xxx->cntlBits, (uintptr_t)mv64xxx->regBase + TWI_CNTR);
		break;

	case MV64XXX_I2C_ACTION_SEND_DATA:
        MV64XXX_WRITEL(mv64xxx->msg->buf[mv64xxx->bytePosn++], (uintptr_t)mv64xxx->regBase + TWI_DATA);
		MV64XXX_WRITEL(mv64xxx->cntlBits, (uintptr_t)mv64xxx->regBase + TWI_CNTR);
		break;

	case MV64XXX_I2C_ACTION_RCV_DATA:
        mv64xxx->msg->buf[mv64xxx->bytePosn++] = MV64XXX_READL((uintptr_t)mv64xxx->regBase + TWI_DATA);
		MV64XXX_WRITEL(mv64xxx->cntlBits, (uintptr_t)mv64xxx->regBase + TWI_CNTR);
		break;

	case MV64XXX_I2C_ACTION_RCV_DATA_STOP:
        mv64xxx->msg->buf[mv64xxx->bytePosn++] = MV64XXX_READL((uintptr_t)mv64xxx->regBase + TWI_DATA);
		mv64xxx->cntlBits &= ~MV64XXX_I2C_REG_CONTROL_INTEN;
        MV64XXX_WRITEL(mv64xxx->cntlBits | MV64XXX_I2C_REG_CONTROL_STOP, (uintptr_t)mv64xxx->regBase + TWI_CNTR);
		mv64xxx->block = 0;
        LOS_EventWrite(&mv64xxx->stEvent, EVENT_WAIT);
		break;

	case MV64XXX_I2C_ACTION_INVALID:
	default:
		mv64xxx->rc = HDF_ERR_IO;
	case MV64XXX_I2C_ACTION_SEND_STOP:
		mv64xxx->cntlBits &= ~MV64XXX_I2C_REG_CONTROL_INTEN;
        MV64XXX_WRITEL(mv64xxx->cntlBits | MV64XXX_I2C_REG_CONTROL_STOP, (uintptr_t)mv64xxx->regBase + TWI_CNTR);
		mv64xxx->block = 0;
        LOS_EventWrite(&mv64xxx->stEvent, EVENT_WAIT);
		break;
	}
}



/*  */
static uint32_t Mv64xxxI2cHandler(uint32_t irq, void *data)
{
    struct Mv64xxxI2cCntlr *mv64xxx = (struct Mv64xxxI2cCntlr *)data;
    // uint32_t writeEvent = 0;
    uint32_t status;
    (void)irq;
    OsalSpinLock(&mv64xxx->irqSpinlock);
	while (MV64XXX_READL((uintptr_t)mv64xxx->regBase + TWI_CNTR) &
						MV64XXX_I2C_REG_CONTROL_IFLG) {

		status = MV64XXX_READL((uintptr_t)mv64xxx->regBase + TWI_STAT);
		Mv64xxxI2cFsm(mv64xxx, status);
		Mv64xxxI2cDoAction(mv64xxx);
        MV64XXX_WRITEL(mv64xxx->cntlBits | MV64XXX_I2C_REG_CONTROL_IFLG,
                        (uintptr_t)mv64xxx->regBase + TWI_CNTR);
	}
    OsalSpinUnlock(&mv64xxx->irqSpinlock);
	return HDF_SUCCESS;
}

static void Mv64xxxI2cWaitForCompletion(struct Mv64xxxI2cCntlr *mv64xxx)
{
    // uint32_t timeout = 0; 
    uint32_t ret = 0;
    // uint64_t status = 0;
    // do {
    //     OsalUDelay(500);
    //     if(mv64xxx->block == 0)
    //         break;
    // } while (timeout++ < 0x800);
    ret = LOS_EventRead(&mv64xxx->stEvent, EVENT_WAIT, LOS_WAITMODE_OR | LOS_WAITMODE_CLR, (LOSCFG_BASE_CORE_TICK_PER_SECOND / 4));
    if(ret != EVENT_WAIT){
        dprintf("[%4d]:%s %x\n", __LINE__, __func__, ret);
		mv64xxx->rc = HDF_FAILURE;

	} else { /* Interrupted/Error */
        // dprintf("[%4d]:%s %d %x\n", __LINE__, __func__, mv64xxx->block, status);
		mv64xxx->rc = HDF_SUCCESS; /* errno value */
	}
    mv64xxx->num_msgs = 0;
    mv64xxx->msgs = NULL;
}

static int32_t Mv64xxxI2cTransfer(struct I2cCntlr *cntlr, struct I2cMsg *msgs, int16_t count)
{
    int32_t ret = HDF_SUCCESS;
    struct Mv64xxxI2cCntlr *mv64xxx = NULL;

    if (cntlr == NULL || cntlr->priv == NULL) {
        HDF_LOGE("Mv64xxxI2cTransfer: cntlr lor Mv64xxx null!");
        return HDF_ERR_INVALID_OBJECT;
    }
    mv64xxx = (struct Mv64xxxI2cCntlr *)cntlr;

    if (msgs == NULL || count <= 0) {
        HDF_LOGE("Mv64xxxI2cTransfer: err parms! count:%d", count);
        return HDF_ERR_INVALID_PARAM;
    }
    mv64xxx->msgs = msgs;
    mv64xxx->num_msgs = count;
    mv64xxx->state = MV64XXX_I2C_STATE_WAITING_FOR_START_COND;

	mv64xxx->sendStop = (count==1);
    Mv64xxxI2cSendStart(mv64xxx);    
    Mv64xxxI2cWaitForCompletion(mv64xxx);
    /* must return msg count */
    if(mv64xxx->rc == HDF_SUCCESS)
        ret = count;
    else    
        ret = count = mv64xxx->rc;
    return ret;
}

static const struct I2cMethod g_method = {
    .transfer = Mv64xxxI2cTransfer,
};

static int32_t Mv64xxxI2cLock(struct I2cCntlr *cntlr)
{
    // struct Mv64xxxI2cCntlr *mv64xxx = (struct Mv64xxxI2cCntlr *)cntlr;
    // if (mv64xxx != NULL) {
    //     return OsalSpinLock(&mv64xxx->spin);
    // }
    return HDF_SUCCESS;
}

static void Mv64xxxI2cUnlock(struct I2cCntlr *cntlr)
{
    // struct Mv64xxxI2cCntlr *mv64xxx = (struct Mv64xxxI2cCntlr *)cntlr;
    // if (mv64xxx != NULL) {
    //     (void)OsalSpinUnlock(&mv64xxx->spin);;
    // }
}

static const struct I2cLockMethod g_lockOps = {
    .lock = Mv64xxxI2cLock,
    .unlock = Mv64xxxI2cUnlock,
};

static int32_t Mv64xxxI2cReadDrs(struct Mv64xxxI2cCntlr *mv64xxx, const struct DeviceResourceNode *node)
{
    int32_t ret;
    struct DeviceResourceIface *drsOps = NULL;

    drsOps = DeviceResourceGetIfaceInstance(HDF_CONFIG_SOURCE);
    if (drsOps == NULL || drsOps->GetUint32 == NULL) {
        HDF_LOGE("%s: invalid drs ops fail!", __func__);
        return HDF_FAILURE;
    }

    ret = drsOps->GetUint32(node, "reg_pbase", &mv64xxx->regBasePhy, 0);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: read regBase fail!", __func__);
        return ret;
    }

    ret = drsOps->GetUint16(node, "reg_size", &mv64xxx->regSize, 0);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: read regsize fail!", __func__);
        return ret;
    }

    ret = drsOps->GetUint32(node, "freq", &mv64xxx->freq, 0);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: read freq fail!", __func__);
        return ret;
    }

    ret = drsOps->GetUint32(node, "irq", &mv64xxx->irq, 0);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: read irq fail!", __func__);
        return ret;
    }

    ret = drsOps->GetUint32(node, "clk", &mv64xxx->clk, 0);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: read clk fail!", __func__);
        return ret;
    }

    ret = drsOps->GetUint16(node, "bus", (uint16_t *)&mv64xxx->bus, 0);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: read bus fail!", __func__);
        return ret;
    }

    return HDF_SUCCESS;
}

static int32_t Mv64xxxI2cParseAndInit(struct HdfDeviceObject *device, const struct DeviceResourceNode *node)
{
    int32_t ret;
    struct Mv64xxxI2cCntlr *mv64xxx = NULL;
    (void)device;
    mv64xxx = (struct Mv64xxxI2cCntlr *)OsalMemCalloc(sizeof(struct Mv64xxxI2cCntlr));
    if (mv64xxx == NULL) {
        HDF_LOGE("%s: malloc mv64xxx fail!", __func__);
        return HDF_ERR_MALLOC_FAIL;
    }
    /* Read the HCS file  */
    ret = Mv64xxxI2cReadDrs(mv64xxx, node);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: read drs fail! ret:%d", __func__, ret);
        goto __ERR__;
    }

    mv64xxx->regBase = OsalIoRemap(mv64xxx->regBasePhy, mv64xxx->regSize);
    if (mv64xxx->regBase == NULL) {
        HDF_LOGE("%s: ioremap regBase fail!", __func__);
        ret = HDF_ERR_IO;
        goto __ERR__;
    }

    ret = OsalRegisterIrq(mv64xxx->irq, 0, Mv64xxxI2cHandler, "I2C_IRQ", mv64xxx);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("HimciHostInit: request irq for himci is err.");
        return HDF_FAILURE;
    }

    Mv64xxxI2cCntlrInit(mv64xxx);
    mv64xxx->cntlr.priv = (void *)node;
    mv64xxx->cntlr.busId = mv64xxx->bus;
    mv64xxx->cntlr.ops = &g_method;
    mv64xxx->cntlr.lockOps = &g_lockOps;
    (void)OsalSpinInit(&mv64xxx->spin);
    (void)OsalSpinInit(&mv64xxx->irqSpinlock);
    // LOS_BinarySemCreate(0, &mv64xxx->mv64xxSem);
    (void)LOS_EventInit(&mv64xxx->stEvent);
    ret = I2cCntlrAdd(&mv64xxx->cntlr);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: add i2c controller fail:%d!", __func__, ret);
        (void)OsalSpinDestroy(&mv64xxx->spin);
        (void)OsalSpinDestroy(&mv64xxx->irqSpinlock);
        goto __ERR__;
    }
    // dprintf("i2c:%d AndInit SUCCESS\n", mv64xxx->bus);
#ifdef USER_VFS_SUPPORT
    (void)I2cAddVfsById(mv64xxx->cntlr.busId);
#endif
    return HDF_SUCCESS;
__ERR__:
    if (mv64xxx != NULL) {
        if (mv64xxx->regBase != NULL) {
            OsalIoUnmap((void *)mv64xxx->regBase);
            mv64xxx->regBase = NULL;
        }
        OsalMemFree(mv64xxx);
        mv64xxx = NULL;
    }
    return ret;
}

static int32_t Mv64xxxI2cInit(struct HdfDeviceObject *device)
{
    int32_t ret;
    const struct DeviceResourceNode *childNode = NULL;
    HDF_LOGE("%s: Enter", __func__);
    if (device == NULL || device->property == NULL) {
        HDF_LOGE("%s: device or property is NULL", __func__);
        return HDF_ERR_INVALID_OBJECT;
    }

    ret = HDF_SUCCESS;
    DEV_RES_NODE_FOR_EACH_CHILD_NODE(device->property, childNode) {
        ret = Mv64xxxI2cParseAndInit(device, childNode);
        if (ret != HDF_SUCCESS) {
            break;
        }
    }
    LOS_Mdelay(100);
    return ret;
}

static void Mv64xxxI2cRemoveByNode(const struct DeviceResourceNode *node)
{
    int32_t ret;
    int16_t bus;
    struct I2cCntlr *cntlr = NULL;
    struct Mv64xxxI2cCntlr *mv64xxx = NULL;
    struct DeviceResourceIface *drsOps = NULL;

    drsOps = DeviceResourceGetIfaceInstance(HDF_CONFIG_SOURCE);
    if (drsOps == NULL || drsOps->GetUint32 == NULL) {
        HDF_LOGE("%s: invalid drs ops fail!", __func__);
        return;
    }

    ret = drsOps->GetUint16(node, "bus", (uint16_t *)&bus, 0);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: read bus fail!", __func__);
        return;
    }

    cntlr = I2cCntlrGet(bus);
    if (cntlr != NULL && cntlr->priv == node) {
        I2cCntlrPut(cntlr);
        I2cCntlrRemove(cntlr);
        mv64xxx = (struct Mv64xxxI2cCntlr *)cntlr;
        OsalIoUnmap((void *)mv64xxx->regBase);
        (void)OsalSpinDestroy(&mv64xxx->spin);
        (void)OsalSpinDestroy(&mv64xxx->irqSpinlock);
        OsalMemFree(mv64xxx);
    }
    return;
}

static void Mv64xxxI2cRelease(struct HdfDeviceObject *device)
{

    const struct DeviceResourceNode *childNode = NULL;

    HDF_LOGI("%s: enter", __func__);

    if (device == NULL || device->property == NULL) {
        HDF_LOGE("%s: device or property is NULL", __func__);
        return;
    }

    DEV_RES_NODE_FOR_EACH_CHILD_NODE(device->property, childNode) {
        Mv64xxxI2cRemoveByNode(childNode);
    }
    
}

struct HdfDriverEntry g_i2cDriverEntry = {
    .moduleVersion = 1,
    .Init = Mv64xxxI2cInit,
    .Release = Mv64xxxI2cRelease,
    .moduleName = "mv64xxx_i2c_driver",
};
HDF_INIT(g_i2cDriverEntry);

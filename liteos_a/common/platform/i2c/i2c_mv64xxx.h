/*
 * Copyright (c) 2020 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef I2C_MV64XXX_H
#define I2C_MV64XXX_H

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif /* __cplusplus */
#endif /* __cplusplus */

/* 等待的事件类型 */
#define EVENT_WAIT 0x00000001

#define MV64XXX_PEND_DTO_M     (0x01)

#define MV64XXX_TUNINT_REQ_TIMEOUT  (LOSCFG_BASE_CORE_TICK_PER_SECOND / 2)  /* 0.01s */

#define MV64XXX_READL(addr) OSAL_READL((uintptr_t)(addr))

#define MV64XXX_WRITEL(v, addr) OSAL_WRITEL((v), (uintptr_t)(addr))

#define MV64XXX_CLEARL(host, reg, v) OSAL_WRITEL(OSAL_READL((uintptr_t)(host)->base + (reg)) & (~(v)), \
    (uintptr_t)(host)->base + (reg));//把某几位置零

#define MV64XXX_SETL(host, reg, v) OSAL_WRITEL(OSAL_READL((uintptr_t)(host)->base + (reg)) | (v), \
    (uintptr_t)(host)->base + (reg));//把某几位置一

#define MV64XXX_I2C_ADDR_ADDR(val)			((val & 0x7f) << 1)
#define MV64XXX_I2C_BAUD_DIV_N(val)			(val & 0x7)
#define MV64XXX_I2C_BAUD_DIV_M(val)			((val & 0xf) << 3)

#define	MV64XXX_I2C_REG_CONTROL_ACK		     	BIT(2)
#define	MV64XXX_I2C_REG_CONTROL_IFLG			BIT(3)
#define	MV64XXX_I2C_REG_CONTROL_STOP			BIT(4)
#define	MV64XXX_I2C_REG_CONTROL_START			BIT(5)
#define	MV64XXX_I2C_REG_CONTROL_TWSIEN			BIT(6)
#define	MV64XXX_I2C_REG_CONTROL_INTEN			BIT(7)

/*  Register List */
#define TWI_ADDR  0x0000 /* TWI Slave Address */
#define TWI_XADDR 0x0004 /* TWI Extended Slave Address */
#define TWI_DATA  0x0008 /* TWI Data Byte */
#define TWI_CNTR  0x000C /* TWI Control Register */
#define TWI_STAT  0x0010 /* TWI Status Register */
#define TWI_CCR   0x0014 /* TWI Clock Control Register */
#define TWI_SRST  0x0018 /* TWI Software Reset */
#define TWI_EFR   0x001C /* TWI Enhance Feature Register */
#define TWI_LCR   0x0020 /* TWI Line Control Register */

#define MV64XXX_I2C_ADDR_ADDR(val)			((val & 0x7f) << 1)
#define MV64XXX_I2C_BAUD_DIV_N(val)			(val & 0x7)
#define MV64XXX_I2C_BAUD_DIV_M(val)			((val & 0xf) << 3)

#define	MV64XXX_I2C_REG_CONTROL_ACK			    BIT(2)
#define	MV64XXX_I2C_REG_CONTROL_IFLG			BIT(3)
#define	MV64XXX_I2C_REG_CONTROL_STOP			BIT(4)
#define	MV64XXX_I2C_REG_CONTROL_START			BIT(5)
#define	MV64XXX_I2C_REG_CONTROL_TWSIEN			BIT(6)
#define	MV64XXX_I2C_REG_CONTROL_INTEN			BIT(7)

/* Ctlr status values */
#define	MV64XXX_I2C_STATUS_BUS_ERR			0x00
#define	MV64XXX_I2C_STATUS_MAST_START			0x08
#define	MV64XXX_I2C_STATUS_MAST_REPEAT_START		0x10
#define	MV64XXX_I2C_STATUS_MAST_WR_ADDR_ACK		0x18
#define	MV64XXX_I2C_STATUS_MAST_WR_ADDR_NO_ACK		0x20
#define	MV64XXX_I2C_STATUS_MAST_WR_ACK			0x28
#define	MV64XXX_I2C_STATUS_MAST_WR_NO_ACK		0x30
#define	MV64XXX_I2C_STATUS_MAST_LOST_ARB		0x38
#define	MV64XXX_I2C_STATUS_MAST_RD_ADDR_ACK		0x40
#define	MV64XXX_I2C_STATUS_MAST_RD_ADDR_NO_ACK		0x48
#define	MV64XXX_I2C_STATUS_MAST_RD_DATA_ACK		0x50
#define	MV64XXX_I2C_STATUS_MAST_RD_DATA_NO_ACK		0x58
#define	MV64XXX_I2C_STATUS_MAST_WR_ADDR_2_ACK		0xd0
#define	MV64XXX_I2C_STATUS_MAST_WR_ADDR_2_NO_ACK	0xd8
#define	MV64XXX_I2C_STATUS_MAST_RD_ADDR_2_ACK		0xe0
#define	MV64XXX_I2C_STATUS_MAST_RD_ADDR_2_NO_ACK	0xe8
#define	MV64XXX_I2C_STATUS_NO_STATUS			0xf8

/* Driver states */
enum {
	MV64XXX_I2C_STATE_INVALID,
	MV64XXX_I2C_STATE_IDLE,
	MV64XXX_I2C_STATE_WAITING_FOR_START_COND,
	MV64XXX_I2C_STATE_WAITING_FOR_RESTART,
	MV64XXX_I2C_STATE_WAITING_FOR_ADDR_1_ACK,
	MV64XXX_I2C_STATE_WAITING_FOR_ADDR_2_ACK,
	MV64XXX_I2C_STATE_WAITING_FOR_SLAVE_ACK,
	MV64XXX_I2C_STATE_WAITING_FOR_SLAVE_DATA,
};

/* Driver actions */
enum {
	MV64XXX_I2C_ACTION_INVALID,
	MV64XXX_I2C_ACTION_CONTINUE,
	MV64XXX_I2C_ACTION_SEND_RESTART,
	MV64XXX_I2C_ACTION_SEND_ADDR_1,
	MV64XXX_I2C_ACTION_SEND_ADDR_2,
	MV64XXX_I2C_ACTION_SEND_DATA,
	MV64XXX_I2C_ACTION_RCV_DATA,
	MV64XXX_I2C_ACTION_RCV_DATA_STOP,
	MV64XXX_I2C_ACTION_SEND_STOP,
};

#ifdef __cplusplus
#if __cplusplus
}
#endif /* __cplusplus */
#endif /* __cplusplus */
#endif /* I2C_HI35XX_H */

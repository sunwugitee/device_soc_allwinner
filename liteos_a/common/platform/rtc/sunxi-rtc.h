#ifndef SUNXI_RTC_H
#define SUNXI_RTC_H

#include "rtc_core.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif /* __cplusplus */
#endif /* __cplusplus */

enum RtcErrorType {
    RTC_ERROR_READ_FAIL     = 1,
    RTC_ERROR_WRITE_FAIL    = 2,
    RTC_ERROR_READ_BUSY     = 3,
    RTC_ERROR_WRITE_BUSY    = 4,
    RTC_ERROR_NULL          = 5,
};

enum RtcFeatureSupportType {
    RTC_FEATURE_NO_SUPPORT = 0,
    RTC_FEATURE_SUPPORT    = 1,
};


struct RtcConfigInfo {
    uint32_t phyBaseAddr;
    volatile unsigned char *remapBaseAddr;
    uint16_t regAddrLength;
    uint8_t supportAnaCtrl;
    uint8_t supportLock;
    uint8_t irq;
    uint8_t alarmIndex;
    uint8_t anaCtrlAddr;
    RtcAlarmCallback cb;
    struct OsalMutex mutex;
};



#ifdef __cplusplus
#if __cplusplus
}
#endif /* __cplusplus */
#endif /* __cplusplus */

#endif /* SUNXI_RTC_H */

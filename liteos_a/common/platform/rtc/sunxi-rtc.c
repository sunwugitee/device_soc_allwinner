#include "device_resource_if.h"
#include "hdf_device_desc.h"
#include "hdf_log.h"
#include "osal_io.h"
#include "osal_irq.h"
#include "osal_mem.h"
#include "osal_time.h"
#include "rtc_base.h"
#include "rtc_core.h"
#include "sunxi-rtc.h"

#define U32_MAX		((uint32_t)~0U)
/* Control register */
#define SUNXI_LOSC_CTRL				0x0000
#define SUNXI_LOSC_CTRL_KEY			(0x16aa << 16)
#define SUNXI_LOSC_CTRL_AUTO_SWT_BYPASS		BIT(15)
#define SUNXI_LOSC_CTRL_ALM_DHMS_ACC		BIT(9)
#define SUNXI_LOSC_CTRL_RTC_HMS_ACC		BIT(8)
#define SUNXI_LOSC_CTRL_RTC_YMD_ACC		BIT(7)
#define SUNXI_LOSC_CTRL_EXT_LOSC_EN		BIT(4)
#define SUNXI_LOSC_CTRL_EXT_OSC			BIT(0)
#define SUNXI_LOSC_CTRL_ACC_MASK		GENMASK(9, 7)

#define SUNXI_LOSC_CLK_PRESCAL			0x0008

/* RTC */
#define SUNXI_RTC_YMD				0x0010
#define SUNXI_RTC_HMS				0x0014
#define SUNXI_RTC_ALARM1_HMS		0x0040
 

/*
 * Get date values
 */
#define SUNXI_DATE_GET_DAY_VALUE(x)		((x)  & 0x0000001f)
#define SUNXI_DATE_GET_MON_VALUE(x)		(((x) & 0x00000f00) >> 8)
#define SUNXI_DATE_GET_YEAR_VALUE(x)		(((x) & 0x003f0000) >> 16)
#define SUNXI_LEAP_GET_VALUE(x)			(((x) & 0x00400000) >> 22)
#define SUNXI_WEEKDAY_GET_VALUE(x)			(((x) & 0xE0000000) >> 29)

/*
 * Get time values
 */
#define SUNXI_TIME_GET_SEC_VALUE(x)		((x)  & 0x0000003f)
#define SUNXI_TIME_GET_MIN_VALUE(x)		(((x) & 0x00003f00) >> 8)
#define SUNXI_TIME_GET_HOUR_VALUE(x)		(((x) & 0x001f0000) >> 16)

/*
 * Get alarm values
 */
#define SUNXI_ALARM_GET_SEC_VALUE(x)		((x)  & 0x0000003f)
#define SUNXI_ALARM_GET_MIN_VALUE(x)		(((x) & 0x00003f00) >> 8)
#define SUNXI_ALARM_GET_HOUR_VALUE(x)		(((x) & 0x001f0000) >> 16)
/*
 * Set date values
 */
#define SUNXI_DATE_SET_DAY_VALUE(x)		((x) & 0x0000001f)
#define SUNXI_DATE_SET_MON_VALUE(x)		((x) <<  8 & 0x00000f00)
#define SUNXI_DATE_SET_YEAR_VALUE(x)		((x) << 16 & 0x003f0000)
#define SUNXI_LEAP_SET_VALUE(x)			((x) << 22 & 0x00400000)

/*
 * Set time values
 */
#define SUNXI_TIME_SET_SEC_VALUE(x)		((x) & 0x0000003f)
#define SUNXI_TIME_SET_MIN_VALUE(x)		((x) <<  8 & 0x00003f00)
#define SUNXI_TIME_SET_HOUR_VALUE(x)		((x) << 16 & 0x001f0000)

/*
 * Set ALARM values
 */
#define SUNXI_ALARM_SET_SEC_VALUE(x)		((x) & 0x0000003f)
#define SUNXI_ALARM_SET_MIN_VALUE(x)		((x) <<  8 & 0x00003f00)
#define SUNXI_ALARM_SET_HOUR_VALUE(x)		((x) << 16 & 0x001f0000)


#define SECS_PER_DAY				(24 * 3600ULL)
#define SUNXI_YEAR_MIN				1970
#define SUNXI_YEAR_OFF				(SUNXI_YEAR_MIN - 1900)

/* Alarm 0 (counter) */
#define SUNXI_ALRM_COUNTER			0x0020
/* This holds the remaining alarm seconds on older SoCs (current value) */
#define SUNXI_ALRM_COUNTER_HMS			0x0024
#define SUNXI_ALRM_EN				0x0028
#define SUNXI_ALRM_EN_CNT_EN			BIT(0)
#define SUNXI_ALRM_IRQ_EN			0x002c
#define SUNXI_ALRM_IRQ_EN_CNT_IRQ_EN		BIT(0)
#define SUNXI_ALRM_IRQ_STA			0x0030
#define SUNXI_ALRM_IRQ_STA_CNT_IRQ_PEND		BIT(0)

/* Alarm 1 (wall clock) */
#define SUNXI_ALRM1_EN				0x0044
#define SUNXI_ALRM1_IRQ_EN			0x0048
#define SUNXI_ALRM1_IRQ_STA			0x004c
#define SUNXI_ALRM1_IRQ_STA_WEEK_IRQ_PEND	BIT(0)

/* Alarm config */
#define SUNXI_ALARM_CONFIG			0x0050
#define SUNXI_ALARM_CONFIG_WAKEUP		BIT(0)

static int32_t SunXiRtcReadTime(struct RtcHost *host, struct RtcTime *time)
{
	// struct sun6i_rtc_dev *chip = dev_get_drvdata(dev);
	uint32_t date, times;
    struct RtcConfigInfo *rtcInfo = NULL;
    rtcInfo = (struct RtcConfigInfo *)host->data;
	/*
	 * read again in case it changes
	 */
	do {
		date = OSAL_READL(rtcInfo->remapBaseAddr + SUNXI_RTC_YMD);
		times = OSAL_READL(rtcInfo->remapBaseAddr + SUNXI_RTC_HMS);
	} while ((date != OSAL_READL(rtcInfo->remapBaseAddr + SUNXI_RTC_YMD)) ||
		 (times != OSAL_READL(rtcInfo->remapBaseAddr + SUNXI_RTC_HMS)));

		time->day = SUNXI_DATE_GET_DAY_VALUE(date);
		time->month  = SUNXI_DATE_GET_MON_VALUE(date);
		time->year = SUNXI_DATE_GET_YEAR_VALUE(date);
        time->year+=SUNXI_YEAR_MIN;
        time->weekday= SUNXI_WEEKDAY_GET_VALUE(date);
        time->second  = SUNXI_TIME_GET_SEC_VALUE(times);
        time->minute = SUNXI_TIME_GET_MIN_VALUE(times);
        time->hour = SUNXI_TIME_GET_HOUR_VALUE(times);

	return 0;
}

static int32_t SunXiRtcWriteTime(struct RtcHost *host, const struct RtcTime *time)
{   
    struct RtcConfigInfo *rtcInfo = NULL;
	rtcInfo = (struct RtcConfigInfo *)host->data;
	uint32_t date = 0;
	uint32_t times = 0;

    if (host == NULL || host->data == NULL) {
        HDF_LOGE("SunXiRtcWriteTime: host is null!");
        return HDF_ERR_INVALID_OBJECT;
    }
	times = SUNXI_TIME_SET_SEC_VALUE(time->second)  |
		SUNXI_TIME_SET_MIN_VALUE(time->minute)  |
		SUNXI_TIME_SET_HOUR_VALUE(time->hour);
	date = SUNXI_DATE_SET_DAY_VALUE(time->day) |
			SUNXI_DATE_SET_MON_VALUE(time->month)  |
			SUNXI_DATE_SET_YEAR_VALUE(time->year-SUNXI_YEAR_MIN);
	OSAL_WRITEL(times,rtcInfo->remapBaseAddr + SUNXI_RTC_HMS);
    OSAL_WRITEL(date,rtcInfo->remapBaseAddr + SUNXI_RTC_YMD);


	return 0;
}

//获取闹钟时间
static int32_t SunXiReadAlarm(struct RtcHost *host, enum RtcAlarmIndex alarmIndex, struct RtcTime *time)
{
    
    uint64_t counter_val,seconds;
    struct RtcConfigInfo *rtcInfo = NULL;
    
    if (host == NULL || host->data == NULL) {
        HDF_LOGE("HiReadAlarm: host is null!");
        return HDF_ERR_INVALID_OBJECT;
    }

    rtcInfo = (struct RtcConfigInfo *)host->data;
    if (alarmIndex != rtcInfo->alarmIndex) {
        HDF_LOGE("HiReadAlarm: alarmIndex para error!");
        return HDF_FAILURE;
    }
    counter_val=OSAL_READL( rtcInfo->remapBaseAddr + SUNXI_ALRM_COUNTER);
    SunXiRtcReadTime(host,time);
    seconds = RtcTimeToTimestamp(time)+counter_val;
    TimestampToRtcTime(time,seconds);

    return HDF_SUCCESS;
}

static int32_t SunXiWriteAlarm(struct RtcHost *host, enum RtcAlarmIndex alarmIndex, const struct RtcTime *time)
{
    struct RtcConfigInfo *rtcInfo = NULL;
    const struct RtcTime *alrm_tm = time;
    struct RtcTime tm_now;
    uint64_t time_set,time_now;
    uint32_t counter_val;
    if (host == NULL || host->data == NULL) {
        HDF_LOGE("WriteAlarm: host is null!");
        return HDF_ERR_INVALID_OBJECT;
    }

    rtcInfo = (struct RtcConfigInfo *)host->data;
    if (alarmIndex != rtcInfo->alarmIndex) {
        HDF_LOGE("WriteAlarm: alarmIndex para error!");
        return HDF_ERR_INVALID_PARAM;
    }


    time_set = RtcTimeToTimestamp(alrm_tm);
    SunXiRtcReadTime(host,&tm_now);
    time_now = RtcTimeToTimestamp(&tm_now);
    if (time_set <= time_now) {
			return HDF_FAILURE;
		}
		if ((time_set - time_now) > U32_MAX) {
			return -HDF_FAILURE;
		}

		counter_val = time_set - time_now;
    OSAL_WRITEL(0, rtcInfo->remapBaseAddr + SUNXI_ALRM_COUNTER);//清零
    OSAL_WRITEL(counter_val, rtcInfo->remapBaseAddr + SUNXI_ALRM_COUNTER);//总时间倒计时
    return HDF_SUCCESS;
}

static int32_t SunXiGetFreq(struct RtcHost *host, uint32_t *freq)
{
    struct RtcConfigInfo *rtcInfo = NULL;

    if (host == NULL || host->data == NULL || freq == NULL) {
        HDF_LOGE("HiReadFreq: host is null!");
        return HDF_ERR_INVALID_OBJECT;
    }
    rtcInfo = (struct RtcConfigInfo *)host->data;

   *freq = OSAL_READL(rtcInfo->remapBaseAddr  + SUNXI_LOSC_CTRL) & SUNXI_LOSC_CTRL_EXT_OSC;

    return HDF_SUCCESS;
}

static int32_t SunXiSetFreq(struct RtcHost *host, uint32_t freq)
{
    uint32_t val;
    struct RtcConfigInfo *rtcInfo = NULL;

    if (host == NULL || host->data == NULL) {
        HDF_LOGE("HiWriteFreq: host is null!");
        return HDF_ERR_INVALID_OBJECT;
    }
    rtcInfo = (struct RtcConfigInfo *)host->data;

    if (freq > 1)
		return HDF_ERR_INVALID_PARAM;
	val = OSAL_READL(rtcInfo->remapBaseAddr + SUNXI_LOSC_CTRL);//读取0x00控制位
	val &= ~SUNXI_LOSC_CTRL_EXT_OSC;//将0位（osc位）置0 
	val |= SUNXI_LOSC_CTRL_KEY;//输入0x16aa key值
	val |= freq ? SUNXI_LOSC_CTRL_EXT_OSC : 0;
	val &= ~SUNXI_LOSC_CTRL_EXT_LOSC_EN;//使能位清0
    val |= freq ? SUNXI_LOSC_CTRL_EXT_LOSC_EN : 0;//置位
	
	val=OSAL_READL( rtcInfo->remapBaseAddr + SUNXI_LOSC_CTRL);
    return HDF_SUCCESS;
}

static int32_t SunXiAlarmInterruptEnable(struct RtcHost *host, enum RtcAlarmIndex alarmIndex, uint8_t enable)
{
    uint32_t alrm_val = 0;
	uint32_t alrm_irq_val = 0;
	uint32_t alrm_wake_val = 0;
    struct RtcConfigInfo *rtcInfo = NULL;

    if (host == NULL || host->data == NULL || alarmIndex != RTC_ALARM_INDEX_A) {
        HDF_LOGE("HiAlarmInterruptEnable: para invalid!");
        return HDF_ERR_INVALID_OBJECT;
    }

    rtcInfo = (struct RtcConfigInfo *)host->data;
    if ((enable != RTC_TRUE) && (enable != RTC_FALSE)) {
        HDF_LOGE("HiAlarmInterruptEnable: enable para error!");
        return HDF_ERR_INVALID_PARAM;
    }
    
	if (enable) {
		alrm_val = SUNXI_ALRM_EN_CNT_EN;
		alrm_irq_val = SUNXI_ALRM_IRQ_EN_CNT_IRQ_EN;
		alrm_wake_val = SUNXI_ALARM_CONFIG_WAKEUP;
	} else {
		OSAL_WRITEL(SUNXI_ALRM_IRQ_STA_CNT_IRQ_PEND,
		       rtcInfo->remapBaseAddr + SUNXI_ALRM_IRQ_STA);
	}
	OSAL_WRITEL(alrm_val, rtcInfo->remapBaseAddr + SUNXI_ALRM_EN);//使能alarm
	OSAL_WRITEL(alrm_irq_val, rtcInfo->remapBaseAddr + SUNXI_ALRM_IRQ_EN);//alarm中断使能
	OSAL_WRITEL(alrm_wake_val, rtcInfo->remapBaseAddr + SUNXI_ALARM_CONFIG);//wake使能
    return HDF_SUCCESS;
}

static int32_t SunXiRegisterAlarmCallback(struct RtcHost *host, enum RtcAlarmIndex alarmIndex, RtcAlarmCallback cb)
{
    struct RtcConfigInfo *rtcInfo = NULL;

    if (host == NULL || host->data == NULL || cb == NULL) {
        HDF_LOGE("HiRegisterAlarmCallback: pointer is null!");
        return HDF_ERR_INVALID_OBJECT;
    }

    rtcInfo = (struct RtcConfigInfo *)host->data;
    if (alarmIndex != rtcInfo->alarmIndex) {
        HDF_LOGE("HiRegisterAlarmCallback: alarmIndex para error!");
        return HDF_ERR_INVALID_PARAM;
    }
    rtcInfo->cb = cb;
    return HDF_SUCCESS;
}

static struct RtcMethod g_method = {
    .ReadTime = SunXiRtcReadTime,
    .WriteTime = SunXiRtcWriteTime,
    .ReadAlarm = SunXiReadAlarm,
    .WriteAlarm = SunXiWriteAlarm,
    .RegisterAlarmCallback = SunXiRegisterAlarmCallback,
    .AlarmInterruptEnable = SunXiAlarmInterruptEnable,
    .GetFreq = SunXiGetFreq,
    .SetFreq = SunXiSetFreq,
    // .Reset = HiReset,//r40内部rtc掉电reset
    // .ReadReg = HiReadReg,
    // .WriteReg = HiWriteReg,
};

// 中断服务函数
static uint32_t SunXiRtcIrqHandle(uint32_t irqId, void *data)
{
    uint32_t value = 0;
    struct RtcConfigInfo *rtcInfo = NULL;

    UNUSED(irqId);
    rtcInfo = (struct RtcConfigInfo *)data;
    if (rtcInfo == NULL) {
        return RTC_ERROR_NULL;
    }
    //读取中断状态
    value=OSAL_READL(rtcInfo->remapBaseAddr+SUNXI_ALRM_IRQ_STA);
    
    if (rtcInfo->cb == NULL) {
        return RTC_ERROR_NULL;
    }
    //确定进入中断，检索
    if (value ) {
        OSAL_WRITEL(0,rtcInfo->remapBaseAddr+SUNXI_ALRM_IRQ_EN);
        OSAL_WRITEL(SUNXI_ALRM_IRQ_STA_CNT_IRQ_PEND,rtcInfo->remapBaseAddr+SUNXI_ALRM_IRQ_STA);
        return rtcInfo->cb(rtcInfo->alarmIndex);
    }
    return HDF_SUCCESS;
}

//设置分频
static int32_t SunXiRtcHwInit(struct RtcConfigInfo *rtcInfo)
{
    uint8_t val = 0;

	val = OSAL_READL(rtcInfo->remapBaseAddr + SUNXI_LOSC_CTRL);
	val &= ~SUNXI_LOSC_CTRL_EXT_OSC;
	val |= SUNXI_LOSC_CTRL_KEY;//填充0x16aa字段
	val |= SUNXI_LOSC_CTRL_EXT_OSC ;//置1，设置为: External 32.768kHz OSC.
	val &= ~SUNXI_LOSC_CTRL_EXT_LOSC_EN;//使能
	val |= SUNXI_LOSC_CTRL_EXT_LOSC_EN;
	OSAL_WRITEL(val, rtcInfo->remapBaseAddr + SUNXI_LOSC_CTRL);

    return HDF_SUCCESS;
}

static int32_t SunXiRtcSwInit(struct RtcConfigInfo *rtcInfo)
{
    bool ret = false;

    if (rtcInfo->phyBaseAddr == 0 || (rtcInfo->regAddrLength == 0)) {
        HDF_LOGE("SunXiRtcSwInit: para invalid!");
        return HDF_ERR_INVALID_PARAM;
    }

    if (OsalMutexInit(&rtcInfo->mutex) != HDF_SUCCESS) {
        HDF_LOGE("SunXiRtcSwInit: create mutex fail!");
        return HDF_FAILURE;
    }
    //映射  
    if (rtcInfo->remapBaseAddr == NULL) {
        rtcInfo->remapBaseAddr = (volatile void *)OsalIoRemap((uintptr_t)rtcInfo->phyBaseAddr,
            rtcInfo->regAddrLength);
    }
    //注册中断
    ret = OsalRegisterIrq(rtcInfo->irq, 0, SunXiRtcIrqHandle, "rtc_alarm", (void*)rtcInfo);
    if (ret != 0) {
        HDF_LOGE("SunXiRtcSwInit: register irq(%hhu) fail!", rtcInfo->irq);
        (void)OsalMutexDestroy(&rtcInfo->mutex);
        OsalIoUnmap((void*)rtcInfo->remapBaseAddr);
        rtcInfo->remapBaseAddr = NULL;
        return HDF_FAILURE;
    }
    dprintf("register irq successful\n");
    return HDF_SUCCESS;
}


//获取设备树的资源
static int32_t SunXiRtcConfigData(struct RtcConfigInfo *rtcInfo, const struct DeviceResourceNode *node)
{
    int32_t ret;
    uint32_t value;
    struct DeviceResourceIface *drsOps = NULL;

    drsOps = DeviceResourceGetIfaceInstance(HDF_CONFIG_SOURCE);
    if (drsOps == NULL || drsOps->GetBool == NULL || drsOps->GetUint32 == NULL) {
        HDF_LOGE("%s: invalid drs ops fail!", __func__);
        return HDF_FAILURE;
    }

    rtcInfo->supportAnaCtrl = drsOps->GetBool(node, "supportAnaCtrl");
    rtcInfo->supportLock = drsOps->GetBool(node, "supportLock");

    ret = drsOps->GetUint32(node, "phyBaseAddr", &rtcInfo->phyBaseAddr, 0);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: read regBase fail!", __func__);
        return ret;
    }

    ret = drsOps->GetUint32(node, "regAddrLength", &value, 0);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: read Length fail!", __func__);
        return ret;
    }
    rtcInfo->regAddrLength = (uint16_t)value;

    ret = drsOps->GetUint32(node, "irq", &value, 0);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: read irq fail!", __func__);
        return ret;
    }
    rtcInfo->irq = (uint8_t)value;
    rtcInfo->alarmIndex = RTC_ALARM_INDEX_A;
    rtcInfo->remapBaseAddr = NULL;
    return HDF_SUCCESS;
}

static int32_t SunXiRtcBind(struct HdfDeviceObject *device)
{
    struct RtcHost *host = NULL;

    host = RtcHostCreate(device);
    if (host == NULL) {
        HDF_LOGE("SunXiRtcBind: create host fail!");
        return HDF_ERR_INVALID_OBJECT;
    }

    host->device = device;
    device->service = &host->service;
    HDF_LOGI("%s: rtc bind success.", __func__);
    return HDF_SUCCESS;
}

static void SunXiRtcSwExit(struct RtcConfigInfo *rtcInfo)
{
    (void)OsalUnregisterIrq(rtcInfo->irq, (void *)rtcInfo);
    (void)OsalMutexDestroy(&rtcInfo->mutex);

    if (rtcInfo->remapBaseAddr != NULL) {
        OsalIoUnmap((void*)rtcInfo->remapBaseAddr);
        rtcInfo->remapBaseAddr = NULL;
    }
}

static int32_t SunXiRtcInit(struct HdfDeviceObject *device)
{
    struct RtcHost *host = NULL;
    struct RtcConfigInfo *rtcInfo = NULL;

    if (device == NULL || device->property == NULL) {
        return HDF_ERR_INVALID_OBJECT;
    }
    host = RtcHostFromDevice(device);
    rtcInfo = OsalMemCalloc(sizeof(*rtcInfo));
    if (rtcInfo == NULL) {
        HDF_LOGE("SunXiRtcInit: malloc info fail!");
        return HDF_ERR_MALLOC_FAIL;
    }

//设备树填充结构体，初始化数据
    if (SunXiRtcConfigData(rtcInfo, device->property) != 0) {
        HDF_LOGE("SunXiRtcInit: hcs config fail!");
        OsalMemFree(rtcInfo);
        return HDF_ERR_INVALID_OBJECT;
    }
//地址映射，中断注册
    if (SunXiRtcSwInit(rtcInfo) != 0) {
        HDF_LOGE("SunXiRtcInit: sw init fail!");
        OsalMemFree(rtcInfo);
        return HDF_DEV_ERR_DEV_INIT_FAIL;
    }
//设置分频
    if (SunXiRtcHwInit(rtcInfo) != 0) {
        HDF_LOGE("SunXiRtcInit: hw init fail!");
        SunXiRtcSwExit(rtcInfo);
        OsalMemFree(rtcInfo);
        return HDF_DEV_ERR_DEV_INIT_FAIL;
    }

    host->method = &g_method;
    host->data = rtcInfo;
    dprintf("Hdf dev service:%s init success!\n", HdfDeviceGetServiceName(device));
    HDF_LOGI("Hdf dev service:%s init success!", HdfDeviceGetServiceName(device));
    return HDF_SUCCESS;
}

static void SunXiRtcRelease(struct HdfDeviceObject *device)
{
    struct RtcHost *host = NULL;
    struct RtcConfigInfo *rtcInfo = NULL;

    HDF_LOGI("%s: enter", __func__);
    if (device == NULL) {
        return;
    }

    host = RtcHostFromDevice(device);
    rtcInfo = (struct RtcConfigInfo *)host->data;
    if (rtcInfo != NULL) {
        SunXiRtcSwExit(rtcInfo);
        OsalMemFree(rtcInfo);
        host->data = NULL;
    }
    RtcHostDestroy(host);
}

struct HdfDriverEntry g_rtcDriverEntry = {
    .moduleVersion = 1,
    .Bind = SunXiRtcBind,
    .Init = SunXiRtcInit,
    .Release = SunXiRtcRelease,
    .moduleName = "HDF_PLATFORM_RTC",
};

HDF_INIT(g_rtcDriverEntry);
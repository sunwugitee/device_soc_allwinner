/*
 * Copyright (c) 2020 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "sunxi-mmc.h"
#include "sunximci_proc.h"

#define HDF_LOG_TAG himci_adapter

#define SUNXIMCI_PIN_NUM 6
#define SUNXIMCI_VOLT_SWITCH_TIMEOUT 10
#define SUNXIMCI_PHASE_DLL_START_ELEMENT 2

#define __DEBUG    //日志模块总开关，注释掉将关闭日志输出

#ifdef __DEBUG
#define mmc_DEBUG(format, ...) \
          dprintf("FUNCTION: %s, LINE: %d: "format"\n", __func__, __LINE__,##__VA_ARGS__)
 #else
     #define mmc_DEBUG(format, ...)
 #endif


static void SunximciDumpRegs(struct SunximciHost *host)
{
    dprintf(": =========== DUMP (host%d) REGISTER===========\n", host->id);    
    dprintf(": 00 : 0x%08x 0x%08x 0x%08x 0x%08x\n", 
    SUNXIMCI_READL((uintptr_t)host->base + REG_GCTRL), SUNXIMCI_READL((uintptr_t)host->base + REG_CLKCR),
    SUNXIMCI_READL((uintptr_t)host->base + REG_TMOUT), SUNXIMCI_READL((uintptr_t)host->base + REG_WIDTH));
    dprintf(": 10 : 0x%08x 0x%08x 0x%08x 0x%08x\n", 
    SUNXIMCI_READL((uintptr_t)host->base + REG_BLKSZ), SUNXIMCI_READL((uintptr_t)host->base + REG_BCNTR),
    SUNXIMCI_READL((uintptr_t)host->base + REG_CMDR), SUNXIMCI_READL((uintptr_t)host->base + REG_CARG));
    dprintf(": 20 : 0x%08x 0x%08x 0x%08x 0x%08x\n", 
    SUNXIMCI_READL((uintptr_t)host->base + REG_RESP0), SUNXIMCI_READL((uintptr_t)host->base + REG_RESP1),
    SUNXIMCI_READL((uintptr_t)host->base + REG_RESP2), SUNXIMCI_READL((uintptr_t)host->base + REG_RESP3));
    dprintf(": 30 : 0x%08x 0x%08x 0x%08x 0x%08x\n", 
    SUNXIMCI_READL((uintptr_t)host->base + REG_IMASK), SUNXIMCI_READL((uintptr_t)host->base + REG_MISTA),
    SUNXIMCI_READL((uintptr_t)host->base + REG_RINTR), SUNXIMCI_READL((uintptr_t)host->base + REG_STAS));
    dprintf(": =============================================\n");
}

//DMA enable
static void SunximciDmaStart(struct SunximciHost *host)
{
    uint32_t val;
    SUNXIMCI_WRITEL((uint32_t)host->dmaPaddr, (uintptr_t)host->base + REG_DLBA);

	val = SUNXIMCI_READL((uintptr_t)host->base + REG_GCTRL);
	val |= SDXC_DMA_ENABLE_BIT;
	SUNXIMCI_WRITEL(val, (uintptr_t)host->base + REG_GCTRL);
	val |= SDXC_DMA_RESET;
	SUNXIMCI_WRITEL(val, (uintptr_t)host->base + REG_GCTRL);

	SUNXIMCI_WRITEL(SDXC_IDMAC_SOFT_RESET, (uintptr_t)host->base + REG_DMAC);

	SUNXIMCI_WRITEL(SDXC_IDMAC_FIX_BURST | SDXC_IDMAC_IDMA_ON, (uintptr_t)host->base + REG_DMAC);

    SUNXIMCI_WRITEL(SDXC_IDMAC_RECEIVE_INTERRUPT | SDXC_IDMAC_TRANSMIT_INTERRUPT | SDXC_IDMAC_ABNORMAL_INTERRUPT_SUM, (uintptr_t)host->base + REG_IDIE);//接收中断使能
}
//DMA disable
static void SunximciDmaStop(struct SunximciHost *host)
{
    uint32_t val;

    val = SUNXIMCI_READL((uintptr_t)host->base + REG_DMAC);
    val &= (~SDXC_IDMAC_IDMA_ON);
    SUNXIMCI_WRITEL(val, (uintptr_t)host->base + REG_DMAC);
}
//刷新cache
static void SunximciDmaCacheClean(void *addr, uint32_t size)
{
    addr = (void *)(uintptr_t)DMA_TO_VMM_ADDR((paddr_t)(uintptr_t)addr);
    uint32_t start = (uintptr_t)addr & ~(CACHE_ALIGNED_SIZE - 1);
    uint32_t end = (uintptr_t)addr + size;

    end = ALIGN(end, CACHE_ALIGNED_SIZE);
    DCacheFlushRange(start, end);
}
//一致性操作
static void SunximciDmaCacheInv(void *addr, uint32_t size)
{
    addr = (void *)(uintptr_t)DMA_TO_VMM_ADDR((paddr_t)(uintptr_t)addr);
    uint32_t start = (uintptr_t)addr & ~(CACHE_ALIGNED_SIZE - 1);
    uint32_t end = (uintptr_t)addr + size;

    end = ALIGN(end, CACHE_ALIGNED_SIZE);
    DCacheInvRange(start, end);
}

static bool SunximciIsMultiBlock(struct MmcCmd *cmd)
{
    if (cmd->cmdCode == WRITE_MULTIPLE_BLOCK || cmd->cmdCode == READ_MULTIPLE_BLOCK) {
        return true;
    }
    if (cmd->data->blockNum > 1) {
        return true;
    }
    return false;
}

/* 将数据写入磁盘 */
static bool SunximciNeedAutoStop(struct MmcCntlr *cntlr)
{
    if (cntlr->curDev->type == MMC_DEV_SDIO) {
        return false;
    }

    if (((cntlr->curDev->type == MMC_DEV_SD || cntlr->curDev->type == MMC_DEV_COMBO) &&
        MmcCntlrSdSupportCmd23(cntlr) == false) ||
        (cntlr->curDev->type == MMC_DEV_EMMC && MmcCntlrEmmcSupportCmd23(cntlr) == false)) {
        return true;
    }
    if (cntlr->caps.bits.cmd23 > 0) {
        /* both host and device support cmd23. */
        return false;
    }

    /* the device support cmd23 but host doesn't support cmd23 */
    return true;
}

static int32_t SunximciFillCmdReg(union SunximciCmdRegArg *reg, struct MmcCmd *cmd)
{
    if (cmd->cmdCode == STOP_TRANSMISSION) {
        reg->bits.stopAbortCmd = 1;
        reg->bits.waitDataComplete = 0;
    } else {
        reg->bits.stopAbortCmd = 0;
        reg->bits.waitDataComplete = 1;
    }

    switch (MMC_RESP_TYPE(cmd)) {
        case MMC_RESP_NONE:
            reg->bits.rspExpect = 0;
            reg->bits.rspLen = 0;
            reg->bits.checkRspCrc = 0;
            break;
        case MMC_RESP_R1:
        case MMC_RESP_R1B:
            reg->bits.rspExpect = 1;
            reg->bits.rspLen = 0;
            reg->bits.checkRspCrc = 1;
            break;
        case MMC_RESP_R2:
            reg->bits.rspExpect = 1;
            reg->bits.rspLen = 1;
            reg->bits.checkRspCrc = 1;
            break;
        case MMC_RESP_R3:
        case MMC_RESP_R1 & (~RESP_CRC):
            reg->bits.rspExpect = 1;
            reg->bits.rspLen = 0;
            reg->bits.checkRspCrc = 0;
            break;
        default:
            cmd->returnError = HDF_ERR_INVALID_PARAM;
            mmc_DEBUG("unhandled response type 0x%x", MMC_RESP_TYPE(cmd));
            return HDF_ERR_INVALID_PARAM;
    }

    reg->bits.sendInitialization = 0;
    if (cmd->cmdCode == GO_IDLE_STATE) {
        reg->bits.sendInitialization = 1;
    }
    /* CMD 11 check switch voltage */
    reg->bits.voltSwitch = 0;
    if (cmd->cmdCode == SD_CMD_SWITCH_VOLTAGE) {
        reg->bits.voltSwitch = 1;
    }

    reg->bits.cardNumber = 0;
    reg->bits.cmdIndex = cmd->cmdCode;
    reg->bits.startCmd = 1;
    reg->bits.updateClkRegOnly = 0;
    return HDF_SUCCESS;
}

static int32_t SunximciUpdateCmdReg(union SunximciCmdRegArg *reg, struct SunximciHost *host)
{
    struct MmcCmd *cmd = host->cmd;
    struct MmcData *data = cmd->data;

    if (data != NULL) {
        reg->bits.dataTransferExpected = 1;
        if ((data->dataFlags & (DATA_WRITE | DATA_READ)) > 0) {
            reg->bits.transferMode = 0;
        }
        if ((data->dataFlags & DATA_STREAM) > 0) {
            reg->bits.transferMode = 1;
        }
        if ((data->dataFlags & DATA_WRITE) > 0) {
            reg->bits.readWrite = 1;
        } else if ((data->dataFlags & DATA_READ) > 0) {
            reg->bits.readWrite = 0;
        }
        reg->bits.sendAutoStop = 0;
        if (SunximciIsMultiBlock(cmd) == true && SunximciNeedAutoStop(host->mmc) == true) {
            reg->bits.sendAutoStop = 1;
        }
    } else {
        reg->bits.dataTransferExpected = 0;
        reg->bits.transferMode = 0;
        reg->bits.readWrite = 0;
    }

    if (SunximciFillCmdReg(reg, cmd) != HDF_SUCCESS) {
        return HDF_FAILURE;
    }
    return HDF_SUCCESS;
}

static int32_t SunximciExecCmd(struct SunximciHost *host)
{
    union SunximciCmdRegArg cmdRegs;
    int32_t ret;
    struct MmcCmd *cmd = host->cmd;
    SUNXIMCI_WRITEL(cmd->argument, (uintptr_t)host->base + REG_CARG);
    cmdRegs.arg = SUNXIMCI_READL((uintptr_t)host->base + REG_CMDR);
    ret = SunximciUpdateCmdReg(&cmdRegs, host);
    if (ret != HDF_SUCCESS) {
        return ret;
    }
    // mmc_DEBUG("cmd %d(%08x) arf:%08x", cmdRegs.arg & 0x3f, cmdRegs.arg, cmd->argument);
    SUNXIMCI_WRITEL(cmdRegs.arg, (uintptr_t)host->base + REG_CMDR);
    return HDF_SUCCESS;
}

static void SunximciCmdDone(struct SunximciHost *host)
{
    uint32_t i;
    struct MmcCmd *cmd = host->cmd;

    // if ((cmd->respType & RESP_PRESENT) == 0) {
    //     return;
    // }

    if (MMC_RESP_TYPE(cmd) & RESP_136) {
        for (i = 0; i < MMC_CMD_RESP_SIZE; i++) {
            cmd->resp[i] = SUNXIMCI_READL((uintptr_t)host->base + REG_RESP3 - i * 0x4);
            /* R2 must delay some time here when use UHI card. */
            OsalUDelay(10);
        }
    }else{
        cmd->resp[0] = SUNXIMCI_READL((uintptr_t)host->base + REG_RESP0);
    }  
}

static void SunximciDataSync(struct SunximciHost *host, struct MmcData *data)
{
    uint32_t sgPhyAddr, sgLength, i;

    if ((data->dataFlags & DATA_READ) > 0) {
        for (i = 0; i < host->dmaSgNum; i++) {
            sgLength = SUNXIMCI_SG_DMA_LEN(&host->sg[i]);
            sgPhyAddr = SUNXIMCI_SG_DMA_ADDRESS(&host->sg[i]);
            SunximciDmaCacheInv((void *)(uintptr_t)sgPhyAddr, sgLength);
        }
    }
}

static void SunximciDataDone(struct SunximciHost *host, uint32_t state)
{
    struct MmcData *data = NULL;

    if (host->cmd == NULL) {
        return;
    }
    if (host->cmd->data == NULL) {
        return;
    }

    data = host->cmd->data;
    if ((state & (HTO_INT_STATUS | DRTO_INT_STATUS | RTO_INT_STATUS)) > 0) {
        data->returnError = HDF_ERR_TIMEOUT;
    } else if ((state & (EBE_INT_STATUS | SBE_INT_STATUS | FRUN_INT_STATUS | DCRC_INT_STATUS)) > 0) {
        data->returnError = HDF_MMC_ERR_ILLEGAL_SEQ;
    }
}

static void SunximciWaitCmdComplete(struct SunximciHost *host)
{
    struct MmcCmd *cmd = host->cmd;
    uint32_t timeout, status;
    uint32_t rval = 0;
    unsigned long flags = 0;

    if (host->isTuning == true) {
        timeout = SUNXIMCI_TUNINT_REQ_TIMEOUT;
    } else {
        timeout = SUNXIMCI_REQUEST_TIMEOUT;
    }
    SUNXIMCI_IRQ_LOCK(&flags);
    host->waitForEvent = true;
    SUNXIMCI_IRQ_UNLOCK(flags);
    status = SUNXIMCI_EVENT_WAIT(&host->irqEvent, (SUNXIMCI_PEND_DTO_M | SUNXIMCI_PEND_ACCIDENT), timeout);        
    SUNXIMCI_WRITEL(host->sdioImask, (uintptr_t)host->base + REG_IMASK);
    SUNXIMCI_WRITEL(0, (uintptr_t)host->base + REG_IDIE);
    if (status == LOS_ERRNO_EVENT_READ_TIMEOUT || status == SUNXIMCI_PEND_ACCIDENT) {//错误处理
        mmc_DEBUG("");
        if (status == SUNXIMCI_PEND_ACCIDENT) {
            cmd->returnError = HDF_ERR_IO;
        } else {
            cmd->returnError = HDF_ERR_TIMEOUT;
            if (host->isTuning == false) {
                SunximciDumpRegs(host);
                mmc_DEBUG("host%d cmd%d(arg 0x%x) timeout!", host->id, cmd->cmdCode, cmd->argument);
            }
        }
        if (host->cmd->data != NULL) {
            SunximciDmaStop(host);
            SunximciDataDone(host, 0);
        }
    } else {
            SunximciCmdDone(host);
    }
    if (host->cmd->data != NULL) {//收到数据
        SUNXIMCI_WRITEL(0x337, (uintptr_t)host->base + REG_IDST);
        SUNXIMCI_WRITEL(0, (uintptr_t)host->base + REG_DMAC);
        rval = SUNXIMCI_READL((uintptr_t)host->base + REG_GCTRL);
		rval |= SDXC_DMA_RESET;
        SUNXIMCI_WRITEL(rval, (uintptr_t)host->base + REG_GCTRL);
		rval &= ~SDXC_DMA_ENABLE_BIT;
		SUNXIMCI_WRITEL(rval, (uintptr_t)host->base + REG_GCTRL);
		rval |= SDXC_FIFO_RESET;
		SUNXIMCI_WRITEL(rval, (uintptr_t)host->base + REG_GCTRL);
        SunximciDataSync(host, host->cmd->data);
    }

    SUNXIMCI_IRQ_LOCK(&flags);
    host->waitForEvent = false;
    host->intSum = 0;
    SUNXIMCI_IRQ_UNLOCK(flags);
    SUNXIMCI_WRITEL(0xffff, (uintptr_t)host->base + REG_RINTR);
}
/*
	需要确定GPIO引脚
*/
static bool SunximciCardPlugged(struct MmcCntlr *cntlr)
{

	//值由管脚 SDIO0_CARD_DETECT 决定
    return true;
}

static int32_t SunximciSendCmd23(struct SunximciHost *host, uint32_t blockNum)
{
    int32_t ret;
    struct MmcCmd cmd = {0};

    cmd.cmdCode = SET_BLOCK_COUNT;
    cmd.argument = blockNum;
    host->cmd = &cmd;
    ret = SunximciExecCmd(host);
    if (ret != HDF_SUCCESS) {
        host->cmd = NULL;
        dprintf("cmd23 failed, ret = %d!", ret);
        return ret;
    }

    SunximciWaitCmdComplete(host);
    host->cmd = NULL;
    return cmd.returnError;
}
//等待fifo空闲
static int32_t SunximciFifoReset(struct SunximciHost *host)
{
    uint32_t i;

    SUNXIMCI_SETL(host, REG_GCTRL, FIFO_RESET);
    for (i = 0; i < SUNXIMCI_MAX_RETRY_COUNT; i++) {
        if ((SUNXIMCI_READL((uintptr_t)host->base + REG_GCTRL) & FIFO_RESET) == 0) {
            return HDF_SUCCESS;
        }
        LOS_Udelay(10);
    }
    mmc_DEBUG("time out!");
    return HDF_ERR_TIMEOUT;
}

static int32_t SunximciFillDmaSg(struct SunximciHost *host, struct MmcData *data)
{
    uint32_t len = data->blockNum * data->blockSize;
    int32_t ret;

    if (len == 0) {
        mmc_DEBUG("len == 0");
        return HDF_ERR_INVALID_PARAM;
    }

	//如果已经准备scatter数据，直接使用
    if (data->scatter != NULL && data->dataBuffer == NULL) {
        host->sg = data->scatter;
        host->dmaSgNum = data->scatterLen;
        return HDF_SUCCESS;
    }
    if (data->dataBuffer == NULL) {
        return HDF_ERR_INVALID_PARAM;
    }

    host->alignedBuff = (uint8_t *)OsalMemAllocAlign(CACHE_ALIGNED_SIZE, ALIGN(len, CACHE_ALIGNED_SIZE));
    if (host->alignedBuff == NULL) {
        dprintf("SunximciFillDmaSg: alloc fail.\n");
        return HDF_ERR_MALLOC_FAIL;
    }

    ret = memcpy_s(host->alignedBuff, len, data->dataBuffer, len);
    if (ret != EOK) {
        dprintf("memcpy_s fail ret = %d.", ret);
        OsalMemFree(host->alignedBuff);
        host->alignedBuff = NULL;
        return HDF_FAILURE;
    }
    //mmc_DEBUG("%x %x %x len:%x", data->dataBuffer[0], data->dataBuffer[1], data->dataBuffer[2], len);
    host->buffLen = len;
    //将alignedBuff转换成Scatterlist 
    sg_init_one(&host->dmaSg, (const void *)host->alignedBuff, len);
    host->dmaSgNum = 1;
    host->sg = &host->dmaSg;
    return HDF_SUCCESS;
}

static void SunximciClearDmaSg(struct SunximciHost *host, struct MmcData *data)
{
    uint32_t len;

    if (data == NULL) {
        return;
    }

    len = data->blockNum * data->blockSize;
    if (host->alignedBuff != NULL && data->dataBuffer != NULL && len > 0 && host->buffLen > 0) {
        if ((data->dataFlags & DATA_READ) > 0) {
            if (memcpy_s(data->dataBuffer, len, host->alignedBuff, host->buffLen) != EOK) {
                dprintf("%s: memcpy_s failed!", __func__);
            }
        }
    }
    if (host->alignedBuff != NULL) {
        OsalMemFree(host->alignedBuff);
        host->alignedBuff = NULL;
    }
    host->dmaSgNum = 0;
    host->buffLen = 0;
    host->sg = NULL;
}
//DMA 设置数据
//通过与运算和位移操作实现高低位交换操作 
// #define ___constant_swab32(x)  ((uint32_t)( \
//         (((uint32_t)(x) & (uint32_t)0x000000ffUL) << 24) | \
//         (((uint32_t)(x) & (uint32_t)0x0000ff00UL) << 8) | \
//         (((uint32_t)(x) & (uint32_t)0x00ff0000UL) >> 8) | \
//         (((uint32_t)(x) & (uint32_t)0xff000000UL) >> 24)))
#define ___constant_swab32(x)  (x)
static int32_t SunximciSetupData(struct SunximciHost *host, struct MmcData *data)
{
    int32_t ret;
    uint32_t sgPhyAddr, sgLength, i;
    uint32_t desCnt = 0;
    uint32_t maximum = SUNXIMCI_PAGE_SIZE / sizeof(struct SunximciDes);
    struct SunximciDes *des = NULL;
    uint32_t dmaDir = DMA_TO_DEVICE;
    uint32_t max_len = 1 << host->idmaDesSizeBits;

    if ((data->dataFlags & DATA_READ) > 0) {
        dmaDir = DMA_FROM_DEVICE;
    }

    des = (struct SunximciDes *)host->dmaVaddr;
    for (i = 0; (i < host->dmaSgNum) && (desCnt < maximum); i++) {
        sgLength = SUNXIMCI_SG_DMA_LEN(&host->sg[i]);
        sgPhyAddr = SUNXIMCI_SG_DMA_ADDRESS(&host->sg[i]);
        if ((sgPhyAddr & (CACHE_ALIGNED_SIZE- 1)) != 0) {
            dprintf("[ERROR] host:%dsg_phyaddr:0x%x sg_length:0x%x.\n", host->id, sgPhyAddr, sgLength);
            return HDF_FAILURE;
        }
        if (dmaDir == DMA_TO_DEVICE) {
            SunximciDmaCacheClean((void *)(uintptr_t)sgPhyAddr, sgLength);
        } else {
            SunximciDmaCacheInv((void *)(uintptr_t)sgPhyAddr, sgLength);
        }
        while (sgLength && (desCnt < maximum)) {
            des[desCnt].dmaDesCtrl = SDXC_IDMAC_DES0_CH | SDXC_IDMAC_DES0_OWN | SDXC_IDMAC_DES0_DIC;
            des[desCnt].dmaDesBufAddr = sgPhyAddr;
            /* idmac_des_next_addr is paddr for dma */
            des[desCnt].dmaDesNextAddr = host->dmaPaddr + (desCnt + 1) * sizeof(struct SunximciDes);
            if (sgLength >= max_len) {
                des[desCnt].dmaDesBufSize = max_len;
                sgLength  -= max_len;
                sgPhyAddr += max_len;
            } else {
                /* data alignment */
                des[desCnt].dmaDesBufSize = sgLength;
                sgLength = 0;
            }
            desCnt++;
        }
    }
    des[0].dmaDesCtrl |= (DMA_DES_FIRST_DES);
    des[desCnt - 1].dmaDesCtrl |= (SDXC_IDMAC_DES0_LD);
    des[desCnt - 1].dmaDesCtrl &= (~SDXC_IDMAC_DES0_DIC);
    des[desCnt - 1].dmaDesNextAddr = 0;

    /*
	 * Avoid the io-store starting the idmac hitting io-mem before the
	 * descriptors hit the main-mem.
	 */
    SunximciDmaCacheClean((void *)(uintptr_t)host->dmaPaddr, SUNXIMCI_PAGE_SIZE);

    desCnt = data->blockSize * data->blockNum;
    SUNXIMCI_WRITEL(desCnt, (uintptr_t)host->base + REG_BCNTR);
    SUNXIMCI_WRITEL(data->blockSize, (uintptr_t)host->base + REG_BLKSZ);
    // mmc_DEBUG("blockNum:0x%x blockSize:0x%x dmaSgNum:0x%x ", 
    // data->blockNum, data->blockSize, host->dmaSgNum);

    ret = SunximciFifoReset(host);
    if (ret != HDF_SUCCESS) {
        return ret;
    }
    SunximciDmaStart(host);
    return HDF_SUCCESS;
}

static int32_t SunximciCmdDatePrepare(struct MmcCntlr *cntlr, struct MmcCmd *cmd, struct SunximciHost *host)
{
    int32_t ret;
    int32_t val;
    host->cmd = cmd;
    int bflag = 0;
    if (cmd->data != NULL) {
        if(bflag){
            if (SunximciIsMultiBlock(cmd) == true && SunximciNeedAutoStop(cntlr) == false) {
                ret = SunximciSendCmd23(host, cmd->data->blockNum);
                if (ret != HDF_SUCCESS) {
                    cmd->returnError = ret;
                    return ret;
                }
            }
        }
        host->cmd = cmd;
        ret = SunximciFillDmaSg(host, cmd->data);//数据格式转换
        if (ret != HDF_SUCCESS) {
            return ret;
        }
        ret = SunximciSetupData(host, cmd->data);//设置数据
        if (ret != HDF_SUCCESS) {
            cmd->data->returnError = ret;
            dprintf("setup data fail, err = %d\n", ret);
            return ret;
        }
    } else {
        SUNXIMCI_WRITEL(0, (uintptr_t)host->base + REG_BCNTR);
        SUNXIMCI_WRITEL(0, (uintptr_t)host->base + REG_BLKSZ);
    }
    // SUNXIMCI_WRITEL(host->sdioImask | SDXC_AUTO_COMMAND_DONE, (uintptr_t)host->base + REG_IMASK);
    val = SUNXIMCI_READL((uintptr_t)host->base + REG_IMASK);
    // mmc_DEBUG("REG_IMASK: %08x", val);
    return HDF_SUCCESS;
}

static bool SunximciWaitCardComplete(struct SunximciHost *host)
{
    uint64_t timeout;
    uint32_t cycle, busy;

    timeout = LOS_TickCountGet() + SUNXIMCI_CARD_COMPLETE_TIMEOUT;
    do {
        for (cycle = 0; cycle < SUNXIMCI_MAX_RETRY_COUNT; cycle++) {
            busy = SUNXIMCI_READL((uintptr_t)host->base + REG_STAS);
            if ((busy & DATA_BUSY) == 0) {
                return true;
            }
        }
        if (SunximciCardPlugged(host->mmc) == false) {
            mmc_DEBUG("card is unplugged.");
            return false;
        }
        LOS_Schedule();
    } while (LOS_TickCountGet() < timeout);
    mmc_DEBUG("card is unplugged timeout.");
    return false;
}

static int32_t SunximciDoRequest(struct MmcCntlr *cntlr, struct MmcCmd *cmd)
{
    struct SunximciHost *host = NULL;
    int32_t ret = HDF_SUCCESS;

    if ((cntlr == NULL) || (cntlr->priv == NULL) || (cmd == NULL)) {
        return HDF_ERR_INVALID_OBJECT;
    }

    host = (struct SunximciHost *)cntlr->priv;
    (void)OsalMutexLock(&host->mutex);

    if (SunximciWaitCardComplete(host) == false) {
        mmc_DEBUG("card is busy, can not send cmd.");
        cmd->returnError = HDF_ERR_TIMEOUT;
        goto _END;
    }

    if (SunximciCmdDatePrepare(cntlr, cmd, host) !=  HDF_SUCCESS) {//dma传输数据
        goto _END;
    }
    ret = SunximciExecCmd(host);//执行命令
    if (ret != HDF_SUCCESS) {
        cmd->returnError = ret;
        SunximciDmaStop(host);
        dprintf("cmd%d exec fail, err = %d!", cmd->cmdCode, ret);
        goto _END;
    }
    SunximciWaitCmdComplete(host);//等待命令完成

_END:
    SunximciClearDmaSg(host, cmd->data);
    host->cmd = NULL;
    (void)OsalMutexUnlock(&host->mutex);
    return ret;
}

//控制时钟使能接口
static void SunximciControlClock(struct SunximciHost *host, bool enableClk)
{
    uint32_t value;

   	uint64_t timeout;
    timeout = LOS_TickCountGet() + SUNXIMCI_CARD_OCLK_ONOFF_TIMEOUT;
	
	value = SUNXIMCI_READL((uintptr_t)host->base + REG_CLKCR);
	value &= ~(SDXC_CARD_CLOCK_ON | SDXC_LOW_POWER_ON | SDXC_MASK_DATA0);

	if (enableClk == true)
		value |= SDXC_CARD_CLOCK_ON;

	SUNXIMCI_WRITEL(value, (uintptr_t)host->base + REG_CLKCR);

	value = SDXC_START | SDXC_UPCLK_ONLY | SDXC_WAIT_PRE_OVER;
	SUNXIMCI_WRITEL(value, (uintptr_t)host->base + REG_CMDR);

	do {
		value = SUNXIMCI_READL((uintptr_t)host->base + REG_CMDR);
	} while (LOS_TickCountGet() < timeout && (value  & SDXC_START));

	/* clear irq status bits set by the command */	
	SUNXIMCI_WRITEL(SUNXIMCI_READL((uintptr_t)host->base + REG_RINTR) & ~SDXC_SDIO_INTERRUPT, 
						(uintptr_t)host->base + REG_RINTR);

	if (value & SDXC_START) {
		dprintf("fatal err update clk timeout\n");
        return;
	}
}

static int32_t SunximciSetClock(struct MmcCntlr *cntlr, uint32_t clock)
{
    struct SunximciHost *host = NULL;
    uint32_t curClock = clock;
	uint32_t value;

    if ((cntlr == NULL) || (cntlr->priv == NULL) || (cntlr->curDev == NULL)) {
        return HDF_ERR_INVALID_OBJECT;
    }
    /* can not greater than max of host. */
    if (curClock > cntlr->freqMax) {
        curClock = cntlr->freqMax;
    }

    host = (struct SunximciHost *)cntlr->priv;
	value = SUNXIMCI_READL((uintptr_t)host->base + REG_GCTRL);
	if (cntlr->curDev->workPara.timing == MMC_TIMING_UHS_DDR50 ||
	    cntlr->curDev->workPara.timing == MMC_TIMING_MMC_DDR52)
		value |= SDXC_DDR_MODE;
	else
    {
        value &= ~SDXC_DDR_MODE;
    }
	SUNXIMCI_WRITEL(value, (uintptr_t)host->base + REG_GCTRL);

	SunximciControlClock(host, false);

	/* Clear internal divider */
    value = SUNXIMCI_READL((uintptr_t)host->base + REG_CLKCR);
	value &= ~(0xff);
	SUNXIMCI_WRITEL(value, (uintptr_t)host->base + REG_CLKCR);

    value = SUNXIMCI_READL((uintptr_t)host->base + SDXC_REG_SD_NTSR);
    value |= SDXC_2X_TIMING_MODE;
    SUNXIMCI_WRITEL(value, (uintptr_t)host->base + SDXC_REG_SD_NTSR);

    SUNXIMCI_WRITEL(SDXC_CAL_DL_SW_EN, (uintptr_t)host->base + SDXC_REG_SAMP_DL_REG);

    
    if(curClock > 0){
        if(curClock == 400000)
            SUNXIMCI_WRITEL(0x8002000e, (uintptr_t)host->ccuBase);
        else if(curClock == 25000000)
            SUNXIMCI_WRITEL(0x8101000b, (uintptr_t)host->ccuBase);      
        else if(curClock == 50000000)
            SUNXIMCI_WRITEL(0x8100000b, (uintptr_t)host->ccuBase);
        else if(curClock == 52000000)
            SUNXIMCI_WRITEL(0x8100000b, (uintptr_t)host->ccuBase);
        SunximciControlClock(host, true);
    }

    return HDF_SUCCESS;
}

//目前只是改变了状态,继承UBOOT的配置
static int32_t SunximciSetPowerMode(struct MmcCntlr *cntlr, enum MmcPowerMode mode)
{
    struct SunximciHost *host = NULL;
    if ((cntlr == NULL) || (cntlr->priv == NULL)) {
        return HDF_ERR_INVALID_OBJECT;
    }

    host = (struct SunximciHost *)cntlr->priv;
   
    if (mode == MMC_POWER_MODE_POWER_OFF) {
		host->powerStatus = HOST_POWER_OFF;
    } else {
		host->powerStatus = HOST_POWER_ON;
    }
    return HDF_SUCCESS;
}
//TODO: 已经完成
static int32_t SunximciSetBusWidth(struct MmcCntlr *cntlr, enum MmcBusWidth width)
{
    struct SunximciHost *host = NULL;
    if ((cntlr == NULL) || (cntlr->priv == NULL)) {
        return HDF_ERR_INVALID_OBJECT;
    }
    host = (struct SunximciHost *)cntlr->priv;
	switch (width) {
		case MMC_BUS_WIDTH_1:
			SUNXIMCI_WRITEL(SDXC_WIDTH1, (uintptr_t)host->base + REG_WIDTH);
			break;
		case MMC_BUS_WIDTH_4:
			SUNXIMCI_WRITEL(SDXC_WIDTH4, (uintptr_t)host->base + REG_WIDTH);
			break;
		case MMC_BUS_WIDTH_8:
			SUNXIMCI_WRITEL(SDXC_WIDTH8, (uintptr_t)host->base + REG_WIDTH);
			break;
	}
    return HDF_SUCCESS;
}

static int32_t SunximciSetBusTiming(struct MmcCntlr *cntlr, enum MmcBusTiming timing)
{
    struct SunximciHost *host = NULL;
    uint32_t value;
    // mmc_DEBUG("");
    if ((cntlr == NULL) || (cntlr->priv == NULL)) {
        return HDF_ERR_INVALID_OBJECT;
    }

    host = (struct SunximciHost *)cntlr->priv;
	/* set ddr mode */
	value = SUNXIMCI_READL((uintptr_t)host->base + REG_GCTRL);
	if (timing == MMC_TIMING_UHS_DDR50 ||
	    timing == MMC_TIMING_MMC_DDR52)
		value |= SDXC_DDR_MODE;
	else
		value &= ~SDXC_DDR_MODE;
	SUNXIMCI_WRITEL(value, (uintptr_t)host->base + REG_GCTRL);
//TODO: 这个函数应该不需要,设置相位转换都为0
//SunximciCfgPhase(host, timing);
	
    return HDF_SUCCESS;
}
//TODO 已完成
static int32_t SunximciSetSdioIrq(struct MmcCntlr *cntlr, bool enable)
{
    struct SunximciHost *host = NULL;
    uint32_t value;
    unsigned long flags = 0;
    if ((cntlr == NULL) || (cntlr->priv == NULL)) {
        return HDF_ERR_INVALID_OBJECT;
    }

    host = (struct SunximciHost *)cntlr->priv;
    SUNXIMCI_IRQ_LOCK(&flags);
	value = SUNXIMCI_READL((uintptr_t)host->base + REG_IMASK);
	if (enable == true) {
		host->sdioImask = SDXC_SDIO_INTERRUPT;
        value |= SDIO_INT_MASK;
	} else {
		host->sdioImask = 0;
        value &= (~SDIO_INT_MASK);
	}
	SUNXIMCI_WRITEL(value, (uintptr_t)host->base + REG_IMASK);
    SUNXIMCI_IRQ_UNLOCK(flags);
    return HDF_SUCCESS;
}

static int32_t SunximciHardwareReset(struct MmcCntlr *cntlr)
{
    struct SunximciHost *host = NULL;
    mmc_DEBUG("");
    if ((cntlr == NULL) || (cntlr->priv == NULL)) {
        return HDF_ERR_INVALID_OBJECT;
    }

    host = (struct SunximciHost *)cntlr->priv;
	SUNXIMCI_WRITEL(0, (uintptr_t)host->base + REG_HWRST);
	OsalUDelay(10);
	SUNXIMCI_WRITEL(1, (uintptr_t)host->base + REG_HWRST);
	OsalUDelay(300);
    return HDF_SUCCESS;
}

static int32_t SunximciSetEnhanceStrobe(struct MmcCntlr *cntlr, bool enable)
{
    (void)cntlr;
    (void)enable;
    return HDF_SUCCESS;
}

//支持
static int32_t SunximciSwitchVoltage(struct MmcCntlr *cntlr, enum MmcVolt volt)
{
    struct SunximciHost *host = NULL;

    if ((cntlr == NULL) || (cntlr->priv == NULL)) {
        return HDF_ERR_INVALID_OBJECT;
    }

    host = (struct SunximciHost *)cntlr->priv;
    if (volt == VOLT_3V3) {
         return HDF_SUCCESS;
    } else 
    	return HDF_FAILURE;
}

static bool SunximciDevReadOnly(struct MmcCntlr *cntlr)
{
    mmc_DEBUG("");
    return false;
}

static bool SunximciDevBusy(struct MmcCntlr *cntlr)
{
    (void)cntlr;
    // mmc_DEBUG("");
    struct SunximciHost *host = NULL;
    if ((cntlr == NULL) || (cntlr->priv == NULL)) {
        return false;
    }
    host = (struct SunximciHost *)cntlr->priv;
	return !!(SUNXIMCI_READL((uintptr_t)host->base + REG_STAS) & SDXC_CARD_DATA_BUSY);
}


static void SunximciClockCfg(struct SunximciHost *host)
{
    //0x8002000e 初始化为400k 24M N4 M14
    SUNXIMCI_WRITEL(0x8002000e, (uintptr_t)host->ccuBase);
}



static void SunximciSysCtrlInit(struct SunximciHost *host)
{
    SUNXIMCI_TASK_LOCK();
    SunximciClockCfg(host);
    SUNXIMCI_TASK_UNLOCK();
}

static int32_t SunximciHostReset(struct SunximciHost *host)
{
    uint32_t value;	
	uint64_t timeout;
	timeout = LOS_TickCountGet() + SUNXIMCI_RESET_HOST_TIMEOUT;

	SUNXIMCI_WRITEL(SDXC_HARDWARE_RESET, (uintptr_t)host->base + REG_GCTRL);
	do {
		value = SUNXIMCI_READL((uintptr_t)host->base + REG_GCTRL);
	} while (LOS_TickCountGet() < timeout && (value & SDXC_HARDWARE_RESET));

	if (value & SDXC_HARDWARE_RESET) {
		mmc_DEBUG(" fatal err reset timeout");
		return HDF_FAILURE;
	}

	return HDF_SUCCESS;
}

static void SunximciHostRegistersInit(struct SunximciHost *host)
{
    uint32_t value;	
    
	SunximciHostReset(host);
	/*
	 * Burst 8 transfers, RX trigger level: 7, TX trigger level: 8
	 *
	 * TODO: sun9i has a larger FIFO and supports higher trigger values
	 */
	SUNXIMCI_WRITEL(0x20070008, (uintptr_t)host->base + REG_FTRGL);
	/* Maximum timeout value */
	SUNXIMCI_WRITEL(0xffffffff, (uintptr_t)host->base + REG_TMOUT);
	/* Unmask SDIO interrupt if needed */
	//!!!!!注意确定
    //host->sdioImask = SDXC_SDIO_INTERRUPT;
    value = ALL_INT_MASK & (~(ACD_INT_STATUS | RXDR_INT_STATUS | TXDR_INT_STATUS | SDIO_INT_MASK));
    host->sdioImask = value;
	SUNXIMCI_WRITEL(host->sdioImask, (uintptr_t)host->base + REG_IMASK);
    // mmc_DEBUG("sdioImask:%08x.\n", host->sdioImask);
	/* Clear all pending interrupts */
	SUNXIMCI_WRITEL(0xffffffff, (uintptr_t)host->base + REG_RINTR);
	/* Debug register? undocumented */
	SUNXIMCI_WRITEL(0xdeb, (uintptr_t)host->base + REG_DBGC);
	/* Enable CEATA support */
	SUNXIMCI_WRITEL(SDXC_CEATA_ON, (uintptr_t)host->base + REG_FUNS);
	/* Set DMA descriptor list base address */
	SUNXIMCI_WRITEL((uint32_t)host->dmaPaddr, (uintptr_t)host->base + REG_DLBA);

	value = SUNXIMCI_READL((uintptr_t)host->base + REG_GCTRL);
	value |= SDXC_INTERRUPT_ENABLE_BIT;
	/* Undocumented, but found in Allwinner code */
	value &= ~SDXC_ACCESS_DONE_DIRECT;
	SUNXIMCI_WRITEL(value, (uintptr_t)host->base + REG_GCTRL);
}

static int32_t SunximciRescanSdioDev(struct MmcCntlr *cntlr)
{
    struct SunximciHost *host = NULL;
    if ((cntlr == NULL) || (cntlr->priv == NULL)) {
        return HDF_ERR_INVALID_OBJECT;
    }

    host = (struct SunximciHost *)cntlr->priv;
    if (host->waitForEvent == true) {
        (void)SUNXIMCI_EVENT_SIGNAL(&host->irqEvent, SUNXIMCI_PEND_ACCIDENT);
    }

    return MmcCntlrAddSdioRescanMsgToQueue(cntlr);
}

static int32_t SunximciSystemInit(struct MmcCntlr *cntlr)
{
    struct SunximciHost *host = NULL;
    if ((cntlr == NULL) || (cntlr->priv == NULL)) {
        return HDF_ERR_INVALID_OBJECT;
    }

    host = (struct SunximciHost *)cntlr->priv;
    SunximciSysCtrlInit(host);
    SunximciHostRegistersInit(host);
    return HDF_SUCCESS;
}

static struct MmcCntlrOps g_himciHostOps = {
    .request = SunximciDoRequest,
    .setClock = SunximciSetClock,
    .setPowerMode = SunximciSetPowerMode,
    .setBusWidth = SunximciSetBusWidth,
    .setBusTiming = SunximciSetBusTiming,
    .setSdioIrq = SunximciSetSdioIrq,
    .hardwareReset = SunximciHardwareReset,
    .systemInit = SunximciSystemInit,
    .setEnhanceStrobe = SunximciSetEnhanceStrobe,
    .switchVoltage = SunximciSwitchVoltage,
    .devReadOnly = SunximciDevReadOnly,
    .devPlugged = SunximciCardPlugged,
    .devBusy = SunximciDevBusy,//1
    //.tune = SunximciTune,//用于计算最佳采样点
    .tune = NULL,
    .rescanSdioDev = SunximciRescanSdioDev,
};
//111
static uint32_t SunximciCmdIrq(struct SunximciHost *host, uint32_t state)
{
    struct MmcCmd *cmd = host->cmd;
    struct MmcData *data = NULL;
    uint32_t writeEvent = 0;
    uint32_t mask;
    int32_t error = HDF_SUCCESS;

    if ((state & RTO_INT_STATUS) > 0) {//1
        error = HDF_ERR_TIMEOUT;
    } else if ((state & (RCRC_INT_STATUS | RE_INT_STATUS)) > 0) {//1
        error = HDF_MMC_ERR_ILLEGAL_SEQ;
    }

    mask = (CD_INT_STATUS | VOLT_SWITCH_INT_STATUS);
    if (cmd != NULL) {
        data = cmd->data;
    }
    if (data == NULL && (state & mask) > 0) {
        writeEvent = 1;
    }

    /*
     * If there is a response timeout(RTO) error,
     * then the DWC_mobile_storage does not attempt any data transfer and
     * the "data Transfer Over" bit is never set.
     */
    mask = (CD_INT_STATUS | RTO_INT_STATUS);
    if ((state & mask) == mask) {
        writeEvent = 1;
    }
    if (cmd != NULL) {
        cmd->returnError = error;
    }
    return writeEvent;
}

static uint32_t SunximciDataIrq(struct SunximciHost *host, struct MmcData *data, uint32_t state)
{
    uint32_t writeEvent = 0;

    if (host == NULL || data == NULL) {
        return writeEvent;
    }

    if ((data->dataFlags & DATA_READ) == 0) {
        if ((state & SBE_INT_STATUS) > 0) {
            SunximciDmaStop(host);
            SunximciDataDone(host, state & (~SBE_INT_STATUS));
            writeEvent++;
        }
    } else {
        SunximciDmaStop(host);
        SunximciDataDone(host, state);
        writeEvent++;
    }
    return writeEvent;
}

static uint32_t SunximciIrqHandler(uint32_t irq, void *data)
{
    struct SunximciHost *host = (struct SunximciHost *)data;
    struct MmcCmd *cmd = NULL;
    uint32_t writeEvent = 0;
    uint32_t msk_int, idma_int;
    (void)irq;
    if (host == NULL) {
        HDF_LOGE("SunximciIrqHandler: data is null!");
        return HDF_SUCCESS;
    }
    OsalSpinLock(&host->spinlock);
    idma_int = SUNXIMCI_READL((uintptr_t)host->base + REG_IDST);//DMA interrput status reg
    msk_int  = SUNXIMCI_READL((uintptr_t)host->base + REG_MISTA);//读中断状态
    host->intSum |= msk_int;
    msk_int = host->intSum;
    if ((msk_int & SDIO_INT_STATUS) > 0) {//有没有SDIO中断产生
        SUNXIMCI_CLEARL(host, REG_MISTA, SDIO_INT_MASK);
        (void)MmcCntlrNotifySdioIrqThread(host->mmc);
    }

    if ((msk_int & CARD_DETECT_INT_STATUS) > 0) { //卡检测中断，检测到卡的插入
        (void)MmcCntlrAddPlugMsgToQueue(host->mmc);
        if (host->waitForEvent == true) {
            (void)SUNXIMCI_EVENT_SIGNAL(&host->irqEvent, SUNXIMCI_PEND_ACCIDENT);
            return HDF_SUCCESS;
        }
    }

    cmd = host->cmd;
    if (cmd == NULL) {
        return HDF_SUCCESS;
    }

    if ((msk_int & CMD_INT_MASK) > 0) {//CMD 
        writeEvent += SunximciCmdIrq(host, msk_int);
    }

    if ((msk_int & DATA_INT_MASK) > 0) {//DATA 
        /*
         * SBE_INT_STATUS:
         * Busy Clear Interrupt when data is written to the card
         * In this case, we'd wait for it.
         * Error in data start bit when data is read from a card
         * In this case, we don't need to wait for it. if it's triggered, something is wrong
         */
        if (cmd->data != NULL) {
            writeEvent += SunximciDataIrq(host, cmd->data, msk_int);
        } else {
            writeEvent += SunximciCmdIrq(host, msk_int);//
        }
    }
	SUNXIMCI_WRITEL(msk_int, (uintptr_t)host->base + REG_RINTR);//w1c
	SUNXIMCI_WRITEL(idma_int, (uintptr_t)host->base + REG_IDST);//w1c

    if(idma_int&0x03)
        SUNXIMCI_WRITEL(0x337, (uintptr_t)host->base + REG_IDST);
    if (writeEvent != 0) {
        (void)SUNXIMCI_EVENT_SIGNAL(&host->irqEvent, SUNXIMCI_PEND_DTO_M);
    }
    else{  
        if((msk_int&SDXC_DATA_OVER))
            (void)SUNXIMCI_EVENT_SIGNAL(&host->irqEvent, SUNXIMCI_PEND_DTO_M);
    }
    OsalSpinUnlock(&host->spinlock);
    
    return HDF_SUCCESS;
}

static int32_t SunximciHostInit(struct SunximciHost *host, struct MmcCntlr *cntlr)
{
    int32_t ret;

    host->id = (uint32_t)cntlr->index;
    host->dmaVaddr = (uint32_t *)LOS_DmaMemAlloc(&host->dmaPaddr, SUNXIMCI_PAGE_SIZE, CACHE_ALIGNED_SIZE, DMA_CACHE);
    if (host->dmaVaddr == NULL) {
        mmc_DEBUG(": no mem for himci dma!");
        return HDF_ERR_MALLOC_FAIL;
    }

    if(OsalSpinInit(&host->spinlock) != HDF_SUCCESS){
        mmc_DEBUG(": init spin lock  fail!");
        return HDF_FAILURE;
    }

    if (LOS_EventInit(&host->irqEvent) != HDF_SUCCESS) {
        mmc_DEBUG(": init irqEvent  fail!");
        return HDF_FAILURE;
    }

    if (OsalMutexInit(&host->mutex) != HDF_SUCCESS) {
        mmc_DEBUG(": init mutex lock fail!");
        return HDF_FAILURE;
    }

    SunximciHostRegistersInit(host);
    //注册中断
    ret = OsalRegisterIrq(host->irqNum, 0, SunximciIrqHandler, "MMC_IRQ", host);
    if (ret != HDF_SUCCESS) {
        mmc_DEBUG(": request irq for himci is err.");
        return HDF_FAILURE;
    }
    return HDF_SUCCESS;
}

static int32_t SunximciHostParse(struct SunximciHost *host, struct HdfDeviceObject *obj)
{
    const struct DeviceResourceNode *node = NULL;
    struct DeviceResourceIface *drsOps = NULL;
    int32_t ret;
    uint32_t regBase, regSize,  ccuregBase;

    if (obj == NULL || host == NULL) {
        dprintf("%s: input param is NULL.", __func__);
        return HDF_FAILURE;
    }

    node = obj->property;
    if (node == NULL) {
        dprintf("%s: drs node is NULL.", __func__);
        return HDF_FAILURE;
    }

    drsOps = DeviceResourceGetIfaceInstance(HDF_CONFIG_SOURCE);
    if (drsOps == NULL || drsOps->GetUint32 == NULL) {
        dprintf("%s: invalid drs ops fail!", __func__);
        return HDF_FAILURE;
    }

    ret = drsOps->GetUint32(node, "regBasePhy", &regBase, 0);
    if (ret != HDF_SUCCESS) {
        dprintf("%s: read regBasePhy fail!", __func__);
        return ret;
    }

    ret = drsOps->GetUint32(node, "regSize", &regSize, 0);
    if (ret != HDF_SUCCESS) {
        dprintf("%s: read regSize fail!", __func__);
        return ret;
    }

    host->base = OsalIoRemap(regBase, regSize);
    if (host->base == NULL) {
        dprintf("%s: ioremap regBase fail!\n", __func__);
        return HDF_ERR_IO;
    }
    // dprintf("%s: ioremap regBase %08x size %x to %08x \n", __func__, regBase, regSize, host->base);

    ret = drsOps->GetUint32(node, "ccuBasePhy", &ccuregBase, 0);
    if (ret != HDF_SUCCESS) {
        dprintf("%s: read ccuBasePhy fail!", __func__);
        return ret;
    }
    host->ccuBase = OsalIoRemap(ccuregBase, 0x04);
    if (host->base == NULL) {
        dprintf("%s: ioremap regBase fail!\n", __func__);
        return HDF_ERR_IO;
    }

    ret = drsOps->GetUint32(node, "irqNum", &(host->irqNum), 0);
    if (ret != HDF_SUCCESS) {
        dprintf("%s: read irqNum fail!", __func__);
    }

    ret = drsOps->GetUint32(node, "idmaDesSizeBits", &(host->idmaDesSizeBits), 0);
    if (ret != HDF_SUCCESS) {
        dprintf("%s: read idmaDesSizeBits fail!", __func__);
    }

    return ret;
}

static void SunximciDeleteHost(struct SunximciHost *host)
{
    struct MmcCntlr *cntlr = NULL;

    if (host == NULL) {
        return;
    }
    cntlr = host->mmc;
    if (cntlr != NULL) {
        if (cntlr->curDev != NULL) {
            MmcDeviceRemove(cntlr->curDev);
            OsalMemFree(cntlr->curDev);
            cntlr->curDev = NULL;
        }
        MmcCntlrRemove(cntlr);
        cntlr->hdfDevObj = NULL;
        cntlr->priv = NULL;
        cntlr->ops = NULL;
        OsalMemFree(cntlr);
        host->mmc = NULL;
    }

    OsalUnregisterIrq(host->irqNum, host);
    if (host->dmaVaddr != NULL) {
        LOS_DmaMemFree(host->dmaVaddr);
    }
    if (host->base != NULL) {
        OsalIoUnmap(host->base);
    }

    (void)SUNXIMCI_EVENT_DELETE(&host->irqEvent);
    (void)OsalMutexDestroy(&host->mutex);
    OsalMemFree(host);
}

static int32_t SMHCMmcBind(struct HdfDeviceObject *obj)
{
    struct MmcCntlr *cntlr = NULL;
    struct SunximciHost *host = NULL;
    int32_t ret;
    if (obj == NULL) {
        dprintf("SunximciMmcBind: Fail, device is NULL.\n");
        return HDF_ERR_INVALID_OBJECT;
    }

    cntlr = (struct MmcCntlr *)OsalMemCalloc(sizeof(struct MmcCntlr));
    if (cntlr == NULL) {
        dprintf("SunximciMmcBind: no mem for MmcCntlr.\n");
        return HDF_ERR_MALLOC_FAIL;
    }

    host = (struct SunximciHost *)OsalMemCalloc(sizeof(struct SunximciHost));
    if (host == NULL) {
        dprintf("SunximciMmcBind: no mem for SunximciHost.\n");
        OsalMemFree(cntlr);
        return HDF_ERR_MALLOC_FAIL;
    }

    host->mmc = cntlr;
	cntlr->maxBlkNum	= 8192;
	cntlr->maxBlkSize	= 4096;
	/* 400kHz ~ 52MHz */
	cntlr->freqMin		=   400000;
	cntlr->freqMax		= 52000000;
	cntlr->caps.capsData |= MMC_CAP_MMC_HIGHSPEED | MMC_CAP_SD_HIGHSPEED |
				  MMC_CAP_SDIO_IRQ | MMC_CAP_1_8V_DDR | MMC_CAP_3_3V_DDR;
    cntlr->caps2.caps2Data &= ~MMC_CAP2_HS400;
    cntlr->priv = (void *)host;
    cntlr->ops = &g_himciHostOps;
    cntlr->hdfDevObj = obj;
    obj->service = &cntlr->service;
    /* init cntlr. */
    //从info.hcs中读取配置信息
    ret = MmcCntlrParse(cntlr, obj);
    if (ret != HDF_SUCCESS) {
        goto _ERR;
    }
    /* init host. */
    //从mmc.hcs读取私有配置信息
    ret = SunximciHostParse(host, obj);
    if (ret != HDF_SUCCESS) {
        goto _ERR;
    }

    ret = SunximciHostInit(host, cntlr);
    if (ret != HDF_SUCCESS) {
        goto _ERR;
    }

    ret = MmcCntlrAdd(cntlr, true);
    if (ret != HDF_SUCCESS) {
        goto _ERR;
    }
    /* add card detect msg to queue. */
    (void)MmcCntlrAddDetectMsgToQueue(cntlr);
    OsalMDelay(300);
    return HDF_SUCCESS;
_ERR:
    SunximciDeleteHost(host);
    dprintf("SunximciMmcBind: fail, err = %d \n", ret);
    return ret;
}

static int32_t SMHCMmcInit(struct HdfDeviceObject *obj)
{
    static bool procInit = false;
    (void)obj;

    if (procInit == false) {
        if (ProcMciInit() == HDF_SUCCESS) {
            procInit = true;
        }
    }

    return HDF_SUCCESS;
}

static void SMHCMmcRelease(struct HdfDeviceObject *obj)
{
    struct MmcCntlr *cntlr = NULL;
    mmc_DEBUG("");
    if (obj == NULL) {
        return;
    }

    cntlr = (struct MmcCntlr *)obj->service;
    if (cntlr == NULL) {
        return;
    }
    SunximciDeleteHost((struct SunximciHost *)cntlr->priv);
}

struct HdfDriverEntry g_mmcDriverEntry = {
    .moduleVersion = 1,
    .Bind = SMHCMmcBind,
    .Init = SMHCMmcInit,
    .Release = SMHCMmcRelease,
    .moduleName = "R40_MMC_DRIVER",
};
HDF_INIT(g_mmcDriverEntry);

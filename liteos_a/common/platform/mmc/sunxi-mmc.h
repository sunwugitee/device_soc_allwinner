/*
 * Copyright (c) 2020 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SUNXIMCI_H
#define SUNXIMCI_H

#include "device_resource_if.h"
#include "linux/scatterlist.h"
#include "los_event.h"
#include "los_vm_iomap.h"
#include "los_vm_zone.h"
#include "mmc_corex.h"
#include "osal_io.h"
#include "osal_irq.h"
#include "osal_time.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif /* __cplusplus */
#endif /* __cplusplus */

#define SUNXIMCI_MAX_RETRY_COUNT 100
#define SUNXIMCI_PAGE_SIZE 4096
#define SUNXIMCI_DMA_MAX_BUFF_SIZE 0x1000

/* HI MCI CONFIGS */
#define SUNXIMCI_REQUEST_TIMEOUT         (LOSCFG_BASE_CORE_TICK_PER_SECOND * 10) /* 10s */
#define SUNXIMCI_TUNINT_REQ_TIMEOUT      (LOSCFG_BASE_CORE_TICK_PER_SECOND / 5)  /* 0.2s */
#define SUNXIMCI_CARD_COMPLETE_TIMEOUT   (LOSCFG_BASE_CORE_TICK_PER_SECOND * 5) /* 5s */
#define SUNXIMCI_RESET_HOST_TIMEOUT      (LOSCFG_BASE_CORE_TICK_PER_SECOND / 4)  /* 0.25s */
#define SUNXIMCI_CARD_OCLK_ONOFF_TIMEOUT (LOSCFG_BASE_CORE_TICK_PER_SECOND) /* 1s */

#define SUNXIMCI_READL(addr) OSAL_READL((uintptr_t)(addr))
#define SUNXIMCI_WRITEL(v, addr) OSAL_WRITEL((v), (uintptr_t)(addr))

#define SUNXIMCI_CLEARL(host, reg, v) OSAL_WRITEL(OSAL_READL((uintptr_t)(host)->base + (reg)) & (~(v)), \
    (uintptr_t)(host)->base + (reg));//把某几位置零
#define SUNXIMCI_SETL(host, reg, v) OSAL_WRITEL(OSAL_READL((uintptr_t)(host)->base + (reg)) | (v), \
    (uintptr_t)(host)->base + (reg));//把某几位置一

/* define event lock */
typedef EVENT_CB_S SUNXIMCI_EVENT;
#define SUNXIMCI_EVENT_INIT(event)               LOS_EventInit(event)
#define SUNXIMCI_EVENT_SIGNAL(event, bit)        LOS_EventWrite(event, bit)
#define SUNXIMCI_EVENT_WAIT(event, bit, timeout) LOS_EventRead(event, bit, (LOS_WAITMODE_OR | LOS_WAITMODE_CLR), timeout)
#define SUNXIMCI_EVENT_DELETE(event)             LOS_EventDestroy(event)

/* define task/irq lock */
#define SUNXIMCI_TASK_LOCK(lock)    do { LOS_TaskLock(); } while (0)
#define SUNXIMCI_TASK_UNLOCK(lock)  do { LOS_TaskUnlock(); } while (0)
#define SUNXIMCI_IRQ_LOCK(flags)    do { (*(flags)) = LOS_IntLock(); } while (0)
#define SUNXIMCI_IRQ_UNLOCK(flags)  do { LOS_IntRestore(flags); } while (0)

#define SUNXIMCI_SG_DMA_ADDRESS(sg) ((sg)->dma_address)
#ifdef CONFIG_NEED_SG_DMA_LENGTH
#define SUNXIMCI_SG_DMA_LEN(sg)     ((sg)->dma_length)
#else
#define SUNXIMCI_SG_DMA_LEN(sg)     ((sg)->length)
#endif

enum SunximciPowerStatus {
    HOST_POWER_OFF,
    HOST_POWER_ON,
};

enum SunximciDmaDataDirection {
    DMA_BIDIRECTIONAL = 0,
    DMA_TO_DEVICE = 1,
    DMA_FROM_DEVICE = 2,
    DMA_NONE = 3,
};

/*
 * MMC_CTRL(0x0000) details.
 * [25]Whether to use the built-in DMA to transfer data.
 * 0: The CPU uses the device interface to transfer data. 1: The internal DMA is used to transfer data.
 * [4]Global interrupt enable. 0: disabled; 1: enabled.
 * The interrupt output is valid only when this bit is valid and an interrupt source is enabled.
 * [2]Soft reset control for the internal DMAC. 0: invalid; 1: Reset the internal DMA interface.
 * This bit is automatically reset after two AHB clock cycles.
 * [1]Soft reset control for the internal FIFO. 0: invalid; 1: Reset the FIFO pointer.
 * This bit is automatically reset after the reset operation is complete.
 * [0]Soft reset control for the controller. 0: invalid; 1: Reset the eMMC/SD/SDIO host module.
 */
#define CTRL_RESET       (1U << 0)
#define FIFO_RESET       (1U << 1)
#define DMA_RESET        (1U << 2)
#define INTR_EN          (1U << 4)
#define USE_INTERNAL_DMA (1U << 25)

/* MCI_INTMASK(0x24) details.
 * [16:0]mask MMC host controller each interrupt. 0: disable; 1: enabled.
 * [16]SDIO interrupt; [3]data transfer over(DTO).
 */
#define ALL_INT_MASK   0x1ffff
#define DTO_INT_MASK   (1 << 3)
#define SDIO_INT_MASK  (1 << 16)

/*
 * MCI_INTSTS(0x44) details.
 * [16]sdio interrupt status; [15]end-bit error (read)/write no CRC interrupt status;
 * [14]auto command done interrupt status; [13]start bit error interrupt status;
 * [12]hardware locked write error interrupt status; [11]FIFO underrun/overrun error interrupt status;
 * [10]data starvation-by-host timeout/volt_switch to 1.8v for sdxc interrupt status;
 * [9]data read timeout interrupt status; [8]response timeout interrupt status; [7]data CRC error interrupt status;
 * [6]response CRC error interrupt status; [5]receive FIFO data request interrupt status;
 * [4]transmit FIFO data request interrupt status; [3]data transfer Over interrupt status;
 * [2]command done interrupt status; [1]response error interrupt status; [0]card detect interrupt status.
 */
#define SDIO_INT_STATUS        (1U << 16)
#define EBE_INT_STATUS         (1U << 15)
#define ACD_INT_STATUS         (1U << 14)
#define SBE_INT_STATUS         (1U << 13)
#define HLE_INT_STATUS         (1U << 12)
#define FRUN_INT_STATUS        (1U << 11)
#define HTO_INT_STATUS         (1U << 10)
#define VOLT_SWITCH_INT_STATUS (1U << 10)
#define DRTO_INT_STATUS        (1U << 9)
#define RTO_INT_STATUS         (1U << 8)
#define DCRC_INT_STATUS        (1U << 7)
#define RCRC_INT_STATUS        (1U << 6)
#define RXDR_INT_STATUS        (1U << 5)
#define TXDR_INT_STATUS        (1U << 4)
#define DTO_INT_STATUS         (1U << 3)
#define CD_INT_STATUS          (1U << 2)
#define RE_INT_STATUS          (1U << 1)
#define CARD_DETECT_INT_STATUS (1U << 0)
#define DATA_INT_MASK (DTO_INT_STATUS | DCRC_INT_STATUS | SBE_INT_STATUS | EBE_INT_STATUS)
#define CMD_INT_MASK  (RTO_INT_STATUS | RCRC_INT_STATUS | RE_INT_STATUS | CD_INT_STATUS | VOLT_SWITCH_INT_STATUS)
#define ALL_INT_CLR   0x1efff

/*
 * MMC_STATUS(0x48) details.
 * [9]Status of data_busy indicated by DAT[0]. 0: idle; 1: The card is busy.
 */
#define DATA_BUSY (1U << 9)

/* MMC_FIFOTH(0x4c) details.
 * [30:28]Indicates the transmission burst length.
 * 000: 1; 001: 4; 010: 8; 011: 16; 100: 32; 101: 64; 110: 128; 111:256.
 * [27:16]FIFO threshold watermarklevel when data is read.
 * When the FIFO count is greater than the value of this parameter, the DMA request is enabled.
 * To complete the remaining data after data transfer, a DMA request is generated.
 * [11:0]FIFO threshold watermark level when data is transmitted.
 * When the FIFO count is less than the value of this parameter, the DMA request is enabled.
 * To complete the remaining data after data transfer, a DMA request is generated.
 */
#define BURST_SIZE      (0x6 << 28)
#define RX_WMARK        (0x7f << 16)
#define TX_WMARK        0x80

/*
 * MMC_CARD_RSTN(0x0078) details.
 * [16] eMMC reset controller. 0: reset; 1: reset deasserted.
 */
#define CARD_RESET (1U << 0)

/*
 * MMC_TUNING_CTRL(0x118) details.
 */
#define HW_TUNING_EN    (1U << 0)
#define EDGE_CTRL       (1U << 1)
#define FOUND_EDGE      (1U << 5)

/* IDMAC DEST0 details */
#define DMA_DES_OWN         (1U << 31)
#define DMA_DES_NEXT_DES    (1U << 4)
#define DMA_DES_FIRST_DES   (1U << 3)
#define DMA_DES_LAST_DES    (1U << 2)
#define DMA_DES_DIC_DES     (1U << 1)

/* MMC_CMD(0x002C) register bits define. */
union SunximciCmdRegArg {
    uint32_t arg;
    struct CmdBits {
        uint32_t cmdIndex : 6;   /* [5:0]Command sequence number. */
        uint32_t rspExpect : 1;  /*
                                  * Indicates whether a response exists.
                                  * 0: No response is output from the card.
                                  * 1: A response is output from the card.
                                  */
        uint32_t rspLen : 1;     /*
                                  * Response length. 0: The short response is output from the card.
                                  * 1: The long response is output from the card.
                                  * The long response is 128 bits, and the short response is 32 bits.
                                  */
        uint32_t checkRspCrc : 1; /*
                                   * Indicates whether the CRC check is performed.
                                   * 0: The CRC response is not checked. 1: Check the CRC response.
                                   */
        uint32_t dataTransferExpected : 1; /*
                                            * Data transfer indicator.
                                            * 0: No data is output from the card. 1: Data is output from the card.
                                            */
        uint32_t readWrite : 1;  /*
                                  * Read/write control. 0: Read data from the card. 1: Write data to the card.
                                  * This bit is ignored in non-data transmission.
                                  */
        uint32_t transferMode : 1; /*
                                    * 0: block transfer command; 1: stream transmission command.
                                    * This bit is ignored in non-data transmission.
                                    */
        uint32_t sendAutoStop : 1; /*
                                    * Indicates whether to send the stop command.
                                    * 0: The stop command is not sent after the data transfer is complete.
                                    * 1: The stop command is sent after data transfer is complete.
                                    * This bit is ignored in non-data transmission.
                                    */
        uint32_t waitDataComplete : 1; /*
                                        * Indicates whether to send an instruction immediately.
                                        * 0: Send the command immediately;
                                        * 1: Send the command after the previous data transfer is complete.
                                        * 0 is a typical value, which is used to read the status or interrupt the
                                        * transfer during data transfer.
                                        */
        uint32_t stopAbortCmd : 1; /*
                                    * When the data transfer operation is in progress, the values are as follows:
                                    * 0: The stop/abort command is not sent.
                                    * 1: The stop/abort command is sent to stop the ongoing data transfer.
                                    */
        uint32_t sendInitialization : 1; /*
                                          * Indicates whether to send the initial sequence.
                                          * 0: The initial sequence is not sent before the Send_initialization is sent.
                                          * 1: The initial sequence is sent before the Send_initialization is sent.
                                          * When the card is powered on, the initial sequence must be sent for
                                          * initialization before any command is sent. That is, this bit is set to 1.
                                          */
        uint32_t cardNumber : 5;  /* Sequence number of the card in use. */
        uint32_t updateClkRegOnly : 1; /*
                                        * Indicates whether to automatically update.
                                        * 0: normal command sequence; 1: No command is sent. Only the clock register
                                        * value of the card clock domain is updated.
                                        * Set this bit to 1 each time the card clock is changed. In this case,
                                        * no command is transmitted to the card,
                                        * and no command-done interrupt is generated.
                                        */
        uint32_t reserved1 : 2;
        uint32_t enableBoot : 1; /*
                                  * Enable the boot function. This bit can be used only in forcible boot mode.
                                  * When software enables this bit and Start_cmd at the same time,
                                  * the controller pulls down the CMD signal to start the boot process.
                                  * Enable_boot and Disable_boot cannot be enabled at the same time.
                                  */
        uint32_t expectBootAck : 1; /*
                                     * Enables the boot response. When the software enables this bit and Enable_boot at
                                     * the same time, the controller detects the boot response signal,
                                     * that is, the 0-1-0 sequence.
                                     */
        uint32_t disableBoot : 1; /*
                                   * Disable the boot. When the software enables this bit and Start_cmd at the same
                                   * time, the controller stops the boot operation.
                                   * Enable_boot and Disable_boot cannot be enabled at the same time.
                                   */
        uint32_t bootMode : 1;    /* Boot mode. 0: forcible boot mode; 1: alternate boot mode. */
        uint32_t voltSwitch : 1;  /* Voltage switching control. 0: The voltage switching is disabled. 1: enabled. */
        uint32_t useHoldReg : 1;  /*
                                   * 0: The CMD and DATA signals sent to the card do not pass through the HOLD register.
                                   * 1: The CMD and DATA signals sent to the card pass through the HOLD register.
                                   */
        uint32_t reserved2 : 1;
        uint32_t startCmd : 1;    /*
                                   * Start control. 0: not enabled; 1: start command.
                                   * This bit is cleared when the command has been sent to the CIU.
                                   * The CPU cannot modify this register.
                                   * If the value is changed, a hardware lock error interrupt is generated.
                                   * After sending a command, the CPU needs to query this bit.
                                   * After the bit becomes 0, the CPU sends the next command.
                                   */
    } bits;
};

struct SunximciDes {
    unsigned long dmaDesCtrl;
    unsigned long dmaDesBufSize;
    unsigned long dmaDesBufAddr;
    unsigned long dmaDesNextAddr;
};

#define SUNXIMCI_PEND_DTO_M     (1U << 0)
#define SUNXIMCI_PEND_ACCIDENT  (1U << 1)
#define SUNXIMCI_HOST_INIT_DONE (1U << 2)
struct SunximciHost {
    struct MmcCntlr *mmc;
    struct MmcCmd *cmd;
    void *base;
    enum SunximciPowerStatus powerStatus;
    uint8_t *alignedBuff;
    uint32_t buffLen;
    struct scatterlist dmaSg;
    struct scatterlist *sg;
    uint32_t dmaSgNum;
    DMA_ADDR_T dmaPaddr;//DMA ADDR给CPU用
    uint32_t *dmaVaddr;//驱动用
    uint32_t irqNum;
    bool isTuning;
    uint32_t id;
    struct OsalMutex mutex;
    bool waitForEvent;
    SUNXIMCI_EVENT irqEvent;
    OsalSpinlock spinlock;
	/* 2022年4月26日 from linux add */
	/* irq */
	uint32_t		sdioImask;
	uint32_t		intSum;
    /* BUFF_SIZE */
    uint32_t idmaDesSizeBits;
    /* ccu */
    void   *ccuBase;
	int		ferror;
};

/* global control register bits */
#define SDXC_SOFT_RESET			BIT(0)
#define SDXC_FIFO_RESET			BIT(1)
#define SDXC_DMA_RESET			BIT(2)
#define SDXC_INTERRUPT_ENABLE_BIT	BIT(4)
#define SDXC_DMA_ENABLE_BIT		BIT(5)
#define SDXC_DEBOUNCE_ENABLE_BIT	BIT(8)
#define SDXC_POSEDGE_LATCH_DATA		BIT(9)
#define SDXC_DDR_MODE			BIT(10)
#define SDXC_MEMORY_ACCESS_DONE		BIT(29)
#define SDXC_ACCESS_DONE_DIRECT		BIT(30)
#define SDXC_ACCESS_BY_AHB		BIT(31)
#define SDXC_ACCESS_BY_DMA		(0 << 31)
#define SDXC_HARDWARE_RESET \
		(SDXC_SOFT_RESET | SDXC_FIFO_RESET | SDXC_DMA_RESET)
	
	/* clock control bits */
#define SDXC_MASK_DATA0			BIT(31)
#define SDXC_CARD_CLOCK_ON		BIT(16)
#define SDXC_LOW_POWER_ON		BIT(17)

 /* data bus width */
#define MMC_BUS_WIDTH_1         0
#define MMC_BUS_WIDTH_4         2
#define MMC_BUS_WIDTH_8         3
	/* bus width */
#define SDXC_WIDTH1			0
#define SDXC_WIDTH4			1
#define SDXC_WIDTH8			2
	
	/* smc command bits */
#define SDXC_RESP_EXPIRE		BIT(6)
#define SDXC_LONG_RESPONSE		BIT(7)
#define SDXC_CHECK_RESPONSE_CRC		BIT(8)
#define SDXC_DATA_EXPIRE		BIT(9)
#define SDXC_WRITE			BIT(10)
#define SDXC_SEQUENCE_MODE		BIT(11)
#define SDXC_SEND_AUTO_STOP		BIT(12)
#define SDXC_WAIT_PRE_OVER		BIT(13)
#define SDXC_STOP_ABORT_CMD		BIT(14)
#define SDXC_SEND_INIT_SEQUENCE		BIT(15)
#define SDXC_UPCLK_ONLY			BIT(21)
#define SDXC_READ_CEATA_DEV		BIT(22)
#define SDXC_CCS_EXPIRE			BIT(23)
#define SDXC_ENABLE_BIT_BOOT		BIT(24)
#define SDXC_ALT_BOOT_OPTIONS		BIT(25)
#define SDXC_BOOT_ACK_EXPIRE		BIT(26)
#define SDXC_BOOT_ABORT			BIT(27)
#define SDXC_VOLTAGE_SWITCH	        BIT(28)
#define SDXC_USE_HOLD_REGISTER	        BIT(29)
#define SDXC_START			BIT(31)
	
	/* interrupt bits */
#define SDXC_RESP_ERROR			BIT(1)
#define SDXC_COMMAND_DONE		BIT(2)
#define SDXC_DATA_OVER			BIT(3)
#define SDXC_TX_DATA_REQUEST		BIT(4)
#define SDXC_RX_DATA_REQUEST		BIT(5)
#define SDXC_RESP_CRC_ERROR		BIT(6)
#define SDXC_DATA_CRC_ERROR		BIT(7)
#define SDXC_RESP_TIMEOUT		BIT(8)
#define SDXC_DATA_TIMEOUT		BIT(9)
#define SDXC_VOLTAGE_CHANGE_DONE	BIT(10)
#define SDXC_FIFO_RUN_ERROR		BIT(11)
#define SDXC_HARD_WARE_LOCKED		BIT(12)
#define SDXC_START_BIT_ERROR		BIT(13)
#define SDXC_AUTO_COMMAND_DONE		BIT(14)
#define SDXC_END_BIT_ERROR		BIT(15)
#define SDXC_SDIO_INTERRUPT		BIT(16)
#define SDXC_CARD_INSERT		BIT(30)
#define SDXC_CARD_REMOVE		BIT(31)
#define SDXC_INTERRUPT_ERROR_BIT \
		(SDXC_RESP_ERROR | SDXC_RESP_CRC_ERROR | SDXC_DATA_CRC_ERROR | \
		 SDXC_RESP_TIMEOUT | SDXC_DATA_TIMEOUT | SDXC_FIFO_RUN_ERROR | \
		 SDXC_HARD_WARE_LOCKED | SDXC_START_BIT_ERROR | SDXC_END_BIT_ERROR)
#define SDXC_INTERRUPT_DONE_BIT \
		(SDXC_AUTO_COMMAND_DONE | SDXC_DATA_OVER | \
		 SDXC_COMMAND_DONE | SDXC_VOLTAGE_CHANGE_DONE)
	
	/* status */
#define SDXC_RXWL_FLAG			BIT(0)
#define SDXC_TXWL_FLAG			BIT(1)
#define SDXC_FIFO_EMPTY			BIT(2)
#define SDXC_FIFO_FULL			BIT(3)
#define SDXC_CARD_PRESENT		BIT(8)
#define SDXC_CARD_DATA_BUSY		BIT(9)
#define SDXC_DATA_FSM_BUSY		BIT(10)
#define SDXC_DMA_REQUEST		BIT(31)
#define SDXC_FIFO_SIZE			16
	
	/* Function select */
#define SDXC_CEATA_ON			(0xceaa << 16)
#define SDXC_SEND_IRQ_RESPONSE		BIT(0)
#define SDXC_SDIO_READ_WAIT		BIT(1)
#define SDXC_ABORT_READ_DATA		BIT(2)
#define SDXC_SEND_CCSD			BIT(8)
#define SDXC_SEND_AUTO_STOPCCSD		BIT(9)
#define SDXC_CEATA_DEV_IRQ_ENABLE	BIT(10)
	
	/* IDMA controller bus mod bit field */
#define SDXC_IDMAC_SOFT_RESET		BIT(0)
#define SDXC_IDMAC_FIX_BURST		BIT(1)
#define SDXC_IDMAC_IDMA_ON		BIT(7)
#define SDXC_IDMAC_REFETCH_DES		BIT(31)
	
	/* IDMA status bit field */
#define SDXC_IDMAC_TRANSMIT_INTERRUPT		BIT(0)
#define SDXC_IDMAC_RECEIVE_INTERRUPT		BIT(1)
#define SDXC_IDMAC_FATAL_BUS_ERROR		BIT(2)
#define SDXC_IDMAC_DESTINATION_INVALID		BIT(4)
#define SDXC_IDMAC_CARD_ERROR_SUM		BIT(5)
#define SDXC_IDMAC_NORMAL_INTERRUPT_SUM		BIT(8)
#define SDXC_IDMAC_ABNORMAL_INTERRUPT_SUM	BIT(9)
#define SDXC_IDMAC_HOST_ABORT_INTERRUPT		BIT(10)
#define SDXC_IDMAC_IDLE				(0 << 13)
#define SDXC_IDMAC_SUSPEND			(1 << 13)
#define SDXC_IDMAC_DESC_READ			(2 << 13)
#define SDXC_IDMAC_DESC_CHECK			(3 << 13)
#define SDXC_IDMAC_READ_REQUEST_WAIT		(4 << 13)
#define SDXC_IDMAC_WRITE_REQUEST_WAIT		(5 << 13)
#define SDXC_IDMAC_READ				(6 << 13)
#define SDXC_IDMAC_WRITE			(7 << 13)
#define SDXC_IDMAC_DESC_CLOSE			(8 << 13)
	
	/*
	* If the idma-des-size-bits of property is ie 13, bufsize bits are:
	*  Bits  0-12: buf1 size
	*  Bits 13-25: buf2 size
	*  Bits 26-31: not used
	* Since we only ever set buf1 size, we can simply store it directly.
	*/
#define SDXC_IDMAC_DES0_DIC	BIT(1)  /* disable interrupt on completion */
#define SDXC_IDMAC_DES0_LD	BIT(2)  /* last descriptor */
#define SDXC_IDMAC_DES0_FD	BIT(3)  /* first descriptor */
#define SDXC_IDMAC_DES0_CH	BIT(4)  /* chain mode */
#define SDXC_IDMAC_DES0_ER	BIT(5)  /* end of ring */
#define SDXC_IDMAC_DES0_CES	BIT(30) /* card error summary */
#define SDXC_IDMAC_DES0_OWN	BIT(31) /* 1-idma owns it, 0-host owns it */
	
#define SDXC_CLK_400K		0
#define SDXC_CLK_25M		1
#define SDXC_CLK_50M		2
#define SDXC_CLK_50M_DDR	3
#define SDXC_CLK_50M_DDR_8BIT	4
	
#define SDXC_2X_TIMING_MODE	BIT(31)
	
#define SDXC_CAL_START		BIT(15)
#define SDXC_CAL_DONE		BIT(14)
#define SDXC_CAL_DL_SHIFT	8
#define SDXC_CAL_DL_SW_EN	BIT(7)
#define SDXC_CAL_DL_SW_SHIFT	0
#define SDXC_CAL_DL_MASK	0x3f
	
#define SDXC_CAL_TIMEOUT	3	/* in seconds, 3s is enough*/

/* register offset definitions */
#define REG_GCTRL	(0x00) /* SMC Global Control Register */
#define REG_CLKCR	(0x04) /* SMC Clock Control Register */
#define REG_TMOUT	(0x08) /* SMC Time Out Register */
#define REG_WIDTH	(0x0C) /* SMC Bus Width Register */
#define REG_BLKSZ	(0x10) /* SMC Block Size Register */
#define REG_BCNTR	(0x14) /* SMC Byte Count Register */
#define REG_CMDR	(0x18) /* SMC Command Register */
#define REG_CARG	(0x1C) /* SMC Argument Register */
#define REG_RESP0	(0x20) /* SMC Response Register 0 */
#define REG_RESP1	(0x24) /* SMC Response Register 1 */
#define REG_RESP2	(0x28) /* SMC Response Register 2 */
#define REG_RESP3	(0x2C) /* SMC Response Register 3 */
#define REG_IMASK	(0x30) /* SMC Interrupt Mask Register */
#define REG_MISTA	(0x34) /* SMC Masked Interrupt Status Register */
#define REG_RINTR	(0x38) /* SMC Raw Interrupt Status Register */
#define REG_STAS	(0x3C) /* SMC Status Register */
#define REG_FTRGL	(0x40) /* SMC FIFO Threshold Watermark Registe */
#define REG_FUNS	(0x44) /* SMC Function Select Register */
#define REG_CBCR	(0x48) /* SMC CIU Byte Count Register */
#define REG_BBCR	(0x4C) /* SMC BIU Byte Count Register */
#define REG_DBGC	(0x50) /* SMC Debug Enable Register */
#define REG_HWRST	(0x78) /* SMC Card Hardware Reset for Register */
#define REG_DMAC	(0x80) /* SMC IDMAC Control Register */
#define REG_DLBA	(0x84) /* SMC IDMAC Descriptor List Base Addre */
#define REG_IDST	(0x88) /* SMC IDMAC Status Register */
#define REG_IDIE	(0x8C) /* SMC IDMAC Interrupt Enable Register */
#define REG_CHDA	(0x90)
#define REG_CBDA	(0x94)

/* New registers introduced in A64 */
#define SDXC_REG_A12A		 0x058 /* SMC Auto Command 12 Register */
#define SDXC_REG_SD_NTSR	 0x05C /* SMC New Timing Set Register */
#define SDXC_REG_DRV_DL		 0x140 /* Drive Delay Control Register */
#define SDXC_REG_SAMP_DL_REG 0x144 /* SMC sample delay control */
#define SDXC_REG_DS_DL_REG	 0x148 /* SMC data strobe delay control */

/* timing specification used */
#define MMC_TIMING_LEGACY       0
#define MMC_TIMING_MMC_HS       1
#define MMC_TIMING_SD_HS        2
#define MMC_TIMING_UHS_SDR12    3
#define MMC_TIMING_UHS_SDR25    4
#define MMC_TIMING_UHS_SDR50    5
#define MMC_TIMING_UHS_SDR104   6
#define MMC_TIMING_UHS_DDR50    7
#define MMC_TIMING_MMC_DDR52    8
#define MMC_TIMING_MMC_HS200    9
#define MMC_TIMING_MMC_HS400    10
#define MMC_TIMING_SD_EXP       11
#define MMC_TIMING_SD_EXP_1_2V  12

	 /* Host capabilities */
	
#define MMC_CAP_4_BIT_DATA      (1 << 0)        /* Can the host do 4 bit transfers */
#define MMC_CAP_MMC_HIGHSPEED   (1 << 1)        /* Can do MMC high-speed timing */
#define MMC_CAP_SD_HIGHSPEED    (1 << 2)        /* Can do SD high-speed timing */
#define MMC_CAP_SDIO_IRQ        (1 << 3)        /* Can signal pending SDIO IRQs */
#define MMC_CAP_SPI             (1 << 4)        /* Talks only SPI protocols */
#define MMC_CAP_NEEDS_POLL      (1 << 5)        /* Needs polling for card-detection */
#define MMC_CAP_8_BIT_DATA      (1 << 6)        /* Can the host do 8 bit transfers */
#define MMC_CAP_AGGRESSIVE_PM   (1 << 7)        /* Suspend (e)MMC/SD at idle  */
#define MMC_CAP_NONREMOVABLE    (1 << 8)        /* Nonremovable e.g. eMMC */
#define MMC_CAP_WAIT_WHILE_BUSY (1 << 9)        /* Waits while card is busy */
#define MMC_CAP_3_3V_DDR        (1 << 11)       /* Host supports eMMC DDR 3.3V */
#define MMC_CAP_1_8V_DDR        (1 << 12)       /* Host supports eMMC DDR 1.8V */
#define MMC_CAP_1_2V_DDR        (1 << 13)       /* Host supports eMMC DDR 1.2V */
#define MMC_CAP_DDR             (MMC_CAP_3_3V_DDR | MMC_CAP_1_8V_DDR | \
									 MMC_CAP_1_2V_DDR)
#define MMC_CAP_POWER_OFF_CARD  (1 << 14)       /* Can power off after boot */
#define MMC_CAP_BUS_WIDTH_TEST  (1 << 15)       /* CMD14/CMD19 bus width ok */
#define MMC_CAP_UHS_SDR12       (1 << 16)       /* Host supports UHS SDR12 mode */
#define MMC_CAP_UHS_SDR25       (1 << 17)       /* Host supports UHS SDR25 mode */
#define MMC_CAP_UHS_SDR50       (1 << 18)       /* Host supports UHS SDR50 mode */
#define MMC_CAP_UHS_SDR104      (1 << 19)       /* Host supports UHS SDR104 mode */
#define MMC_CAP_UHS_DDR50       (1 << 20)       /* Host supports UHS DDR50 mode */
#define MMC_CAP_UHS             (MMC_CAP_UHS_SDR12 | MMC_CAP_UHS_SDR25 | \
									 MMC_CAP_UHS_SDR50 | MMC_CAP_UHS_SDR104 | \
									 MMC_CAP_UHS_DDR50)
#define MMC_CAP_SYNC_RUNTIME_PM (1 << 21)       /* Synced runtime PM suspends. */
#define MMC_CAP_NEED_RSP_BUSY   (1 << 22)       /* Commands with R1B can't use R1. */
#define MMC_CAP_DRIVER_TYPE_A   (1 << 23)       /* Host supports Driver Type A */
#define MMC_CAP_DRIVER_TYPE_C   (1 << 24)       /* Host supports Driver Type C */
#define MMC_CAP_DRIVER_TYPE_D   (1 << 25)       /* Host supports Driver Type D */
#define MMC_CAP_DONE_COMPLETE   (1 << 27)       /* RW reqs can be completed within mmc_request_done() */
#define MMC_CAP_CD_WAKE         (1 << 28)       /* Enable card detect wake */
#define MMC_CAP_CMD_DURING_TFR  (1 << 29)       /* Commands during data transfer */
#define MMC_CAP_CMD23           (1 << 30)       /* CMD23 supported. */
#define MMC_CAP_HW_RESET        (1 << 31)       /* Reset the eMMC card via RST_n */

#define MMC_CAP2_HS400_1_8V     (1 << 15)       /* Can support HS400 1.8V */
#define MMC_CAP2_HS400_1_2V     (1 << 16)       /* Can support HS400 1.2V */
#define MMC_CAP2_HS400          (MMC_CAP2_HS400_1_8V | \
                                 MMC_CAP2_HS400_1_2V)

/* Standard MMC commands (4.1)           type  argument     response */
   /* class 1 */
#define MMC_GO_IDLE_STATE         0   /* bc                          */
#define MMC_SEND_OP_COND          1   /* bcr  [31:0] OCR         R3  */
#define MMC_ALL_SEND_CID          2   /* bcr                     R2  */
#define MMC_SET_RELATIVE_ADDR     3   /* ac   [31:16] RCA        R1  */
#define MMC_SET_DSR               4   /* bc   [31:16] RCA            */
#define MMC_SLEEP_AWAKE		  5   /* ac   [31:16] RCA 15:flg R1b */
#define MMC_SWITCH                6   /* ac   [31:0] See below   R1b */
#define MMC_SELECT_CARD           7   /* ac   [31:16] RCA        R1  */
#define MMC_SEND_EXT_CSD          8   /* adtc                    R1  */
#define MMC_SEND_CSD              9   /* ac   [31:16] RCA        R2  */
#define MMC_SEND_CID             10   /* ac   [31:16] RCA        R2  */
#define MMC_READ_DAT_UNTIL_STOP  11   /* adtc [31:0] dadr        R1  */
#define MMC_STOP_TRANSMISSION    12   /* ac                      R1b */
#define MMC_SEND_STATUS          13   /* ac   [31:16] RCA        R1  */
#define MMC_BUS_TEST_R           14   /* adtc                    R1  */
#define MMC_GO_INACTIVE_STATE    15   /* ac   [31:16] RCA            */
#define MMC_BUS_TEST_W           19   /* adtc                    R1  */
#define MMC_SPI_READ_OCR         58   /* spi                  spi_R3 */
#define MMC_SPI_CRC_ON_OFF       59   /* spi  [0:0] flag      spi_R1 */

#define MMC_RSP_PRESENT	(1 << 0)
#define MMC_RSP_136	(1 << 1)		/* 136 bit response */
#define MMC_RSP_CRC	(1 << 2)		/* expect valid crc */
#define MMC_RSP_BUSY	(1 << 3)		/* card may send busy */
#define MMC_RSP_OPCODE	(1 << 4)		/* response contains opcode */

#define MMC_CMD_MASK	(3 << 5)		/* non-SPI command type */
#define MMC_CMD_AC	(0 << 5)
#define MMC_CMD_ADTC	(1 << 5)
#define MMC_CMD_BC	(2 << 5)
#define MMC_CMD_BCR	(3 << 5)

#define MMC_RSP_SPI_S1	(1 << 7)		/* one status byte */
#define MMC_RSP_SPI_S2	(1 << 8)		/* second byte */
#define MMC_RSP_SPI_B4	(1 << 9)		/* four data bytes */
#define MMC_RSP_SPI_BUSY (1 << 10)		/* card may send busy */     

#define MMC_DATA_WRITE		BIT(8)
#define MMC_DATA_READ		BIT(9)
/* Extra flags used by CQE */
#define MMC_DATA_QBR		BIT(10)		/* CQE queue barrier*/
#define MMC_DATA_PRIO		BIT(11)		/* CQE high priority */
#define MMC_DATA_REL_WR		BIT(12)		/* Reliable write */
#define MMC_DATA_DAT_TAG	BIT(13)		/* Tag request */
#define MMC_DATA_FORCED_PRG	BIT(14)		/* Forced programming */



#ifdef __cplusplus
#if __cplusplus
}
#endif /* __cplusplus */
#endif /* __cplusplus */

#endif /* SUNXIMCI_H */

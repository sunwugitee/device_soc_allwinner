/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CAN_TEMP_H
#define CAN_TEMP_H

#include "hdf_base.h"
#include "los_vm_zone.h"
#include "platform_dumper.h"
#include "can_core.h"
#include "osal_sem.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif /* __cplusplus */
#endif /* __cplusplus */

/* can设备的私有结构信息 */
struct CanChannel
{
    //第一个必须是CanCtrl
    struct CanCntlr cntlr;
    // SPI相关
    uint32_t   csNum; 
    uint32_t   spiNum;    /* spi总线ID */
    DevHandle spiHandle;  /* spi总线句柄 */
    // CAN相关
    uint32_t busNum;               // bus number
    int32_t speed;                // bit rate
    int32_t state;                // bus status
    int32_t mode;                 // work mode
    uint32_t intNum;               // 中断引脚号
    struct OsalSem sem;


};

#ifdef __cplusplus
#if __cplusplus
}
#endif /* __cplusplus */
#endif /* __cplusplus */
#endif /* TIMER_HI35XX_H */

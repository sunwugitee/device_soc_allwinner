/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "can_temp.h"
#include "MCP2515.h"
#include "device_resource_if.h"
#include "hdf_base.h"
#include "hdf_log.h"
#include "osal_io.h"
#include "osal_irq.h"
#include "osal_mem.h"
#include "osal_time.h"
#include "spi_if.h"
#include "gpio_if.h"
#include "osal_time.h"


static void MCP2515WriteByte(DevHandle spiHandle, unsigned char addr,unsigned char dat)
{
    unsigned char wbuf[3] = {CAN_WRITE, addr, dat};
    SpiWrite(spiHandle, wbuf, 3);
}
//SpiTransfer 因为这个是一边写一边返回有效数据，不能分开读，与器件有关
static unsigned char MCP2515ReadByte(DevHandle spiHandle, unsigned char addr)
{
	unsigned char rByte;
    unsigned char wbuf[3] = {CAN_READ, addr, 0x00};
    struct SpiMsg msg = {0};
    msg.wbuf = wbuf;
    msg.rbuf = wbuf;
    msg.len = 3;
    msg.speed=0;
    SpiTransfer(spiHandle, &msg, 1);
    rByte = wbuf[2];
	return rByte;				//返回读到的一个字节数据
}
//初始化操作函数
void MCP2515_Init(DevHandle spifd)
{ 
#if 1  
    usleep(1);
    //设置波特率为125Kbps
	//set CNF1,SJW=00,长度为1TQ,BRP=49,TQ=[2*(BRP+1)]/Fsoc=2*50/8M=12.5us
	MCP2515WriteByte(spifd, CNF1, 0x3);
	//set CNF2,SAM=0,在采样点对总线进行一次采样，PHSEG1=(2+1)TQ=3TQ,PRSEG=(0+1)TQ=1TQ
	MCP2515WriteByte(spifd, CNF2,0x80|PHSEG1_3TQ|PRSEG_1TQ);
	//set CNF3,PHSEG2=(2+1)TQ=3TQ,同时当CANCTRL.CLKEN=1时设定CLKOUT引脚为时间输出使能位
	MCP2515WriteByte(spifd, CNF3,PHSEG2_3TQ);
	
    MCP2515WriteByte(spifd, TXB0SIDH,0xFF);//发送缓冲器0标准标识符高位
	MCP2515WriteByte(spifd, TXB0SIDL,0xE0);//发送缓冲器0标准标识符低位
	
	MCP2515WriteByte(spifd, RXB0SIDH,0x00);//清空接收缓冲器0的标准标识符高位
	MCP2515WriteByte(spifd, RXB0SIDL,0x00);//清空接收缓冲器0的标准标识符低位
	MCP2515WriteByte(spifd, RXB0CTRL,0x20);//仅仅接收标准标识符的有效信息
	MCP2515WriteByte(spifd, RXB0DLC,DLC_8);//设置接收数据的长度为8个字节
	
	MCP2515WriteByte(spifd, RXF0SIDH,0xFF);//配置验收滤波寄存器n标准标识符高位
	MCP2515WriteByte(spifd, RXF0SIDL,0xE0);//配置验收滤波寄存器n标准标识符低位
	MCP2515WriteByte(spifd, RXM0SIDH,0xFF);//配置验收屏蔽寄存器n标准标识符高位
	MCP2515WriteByte(spifd, RXM0SIDL,0xE0);//配置验收屏蔽寄存器n标准标识符低位
	
	MCP2515WriteByte(spifd, CANINTF,0x00);//清空CAN中断标志寄存器的所有位(必须由MCU清空)
	MCP2515WriteByte(spifd, CANINTE,0x01);//配置CAN中断使能寄存器的接收缓冲器0满中断使能,其它位禁止中断
    MCP2515ReadByte(spifd, CANCTRL);
	MCP2515WriteByte(spifd, CANCTRL, REQOP_NORMAL | CLKOUT_ENABLED | OSM_ENABLED);//将MCP2515设置为正常模式,退出配置模式
    MCP2515ReadByte(spifd, CANSTAT);
#endif
}
//发送函数 
//TODO 3个通道
void CANSendBuffer(DevHandle spifd, const unsigned char *CAN_TX_Buf, unsigned char len)
{
	unsigned char j,dly,count;

	count=0;
	while(count<len)
	{
		dly=0;//等待通道发送完成
		while((MCP2515ReadByte(spifd, TXB0CTRL)&0x08) && (dly<50))//快速读某些状态指令,等待TXREQ标志清零
		{
			LOS_Udelay(10);//通过软件延时约nms(不准确)
			dly++;
		}
				
		for(j=0;j<8;)
		{
			MCP2515WriteByte(spifd, TXB0D0+j, CAN_TX_Buf[count++]);//将待发送的数据写入发送缓冲寄存器
			j++;
			if(count>=len) break;
		}
		MCP2515WriteByte(spifd, TXB0DLC, j);//将本帧待发送的数据长度写入发送缓冲器0的发送长度寄存器
		MCP2515WriteByte(spifd, TXB0CTRL, 0x08);//请求发送报文
	}
    MCP2515ReadByte(spifd, TXB0CTRL);//确定是否发送成功
}

unsigned char CANReadBuffer(DevHandle spifd, unsigned char *CAN_RX_Buf)
{
	unsigned char i=0,len=0,temp=0;

	temp = MCP2515ReadByte(spifd, CANINTF);
	if(temp & 0x01)
	{
		len=MCP2515ReadByte(spifd, RXB0DLC);//读取接收缓冲器0接收到的数据长度(0~8个字节)
		while(i<len)
		{	
			CAN_RX_Buf[i]=MCP2515ReadByte(spifd, RXB0D0+i);//把CAN接收到的数据放入指定缓冲区
			i++;
		}
	}
	MCP2515WriteByte(spifd, CANINTF,0);//清除中断标志位(中断标志寄存器必须由MCU清零)
	return len;
}

int32_t MCP2515sendMsg(struct CanCntlr *cntlr, const struct CanMsg *msg)
{
    struct CanChannel *pchannel = (struct CanChannel *)cntlr;

    CANSendBuffer(pchannel->spiHandle, msg->data, msg->dlc);

    return HDF_SUCCESS;
}



static int CanGpioIrqHandler(uint16_t gpio, void *arg)
{
    struct CanChannel *pchannel = (struct CanChannel *)arg;
    // struct CanMsg msg = {0};
    dprintf("%s entry CAN%d\n", __func__, pchannel->busNum);
    (void)OsalSemPost(&pchannel->sem);
    // MCP2515ReadByte(pchannel->spiHandle, CANINTF);
    // msg.dlc = CAN_Receive_Buffer(pchannel->spiHandle, msg.data);
    return 0;
}
/* can控制器需要实现的接口 */
static struct CanCntlrMethod g_canCntlrMethod = {
    .sendMsg = MCP2515sendMsg, //主要实现 1
    .setCfg = NULL,  //主要实现 2
    .getCfg = NULL,  //主要实现 3
};

static int32_t CanGetRegCfgFromHcs(struct CanChannel *pchannel, const struct DeviceResourceNode *node)
{
    struct DeviceResourceIface *iface = DeviceResourceGetIfaceInstance(HDF_CONFIG_SOURCE);

    if (iface == NULL || iface->GetUint32 == NULL) {
        HDF_LOGE("%s: face is invalid", __func__);
        return HDF_FAILURE;
    }
    //read spi config
    if (iface->GetUint32(node, "spiNum", &pchannel->spiNum, 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: read numCs fail", __func__);
        return HDF_FAILURE;
    }
    if (iface->GetUint32(node, "csNum", &pchannel->csNum, 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: read numCs fail", __func__);
        return HDF_FAILURE;
    }
    //read can config
    if (iface->GetUint32(node, "busNum", &pchannel->busNum, 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: read busNum fail", __func__);
        return HDF_FAILURE;
    }    
    //获取中断引脚号
    if (iface->GetUint32(node, "intNum", &pchannel->intNum, 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: read busNum fail", __func__);
        return HDF_FAILURE;
    }
    // dprintf("spi :%d can %d \n", pchannel->spiNum, pchannel->busNum);
    return HDF_SUCCESS;
}
static int32_t MCP2515InitIrq(struct CanChannel *pchannel)
{
    int ret = 0;
    uint16_t gpio = pchannel->intNum;
    uint16_t mode = 0;
    // dprintf("can irq set %d\n", gpio);
    GpioSetDir(gpio, 6);//设置中断模式
    /* Disable the IRP of this pin. */
    ret = GpioDisableIrq(gpio);
    mode = 0x1;
    ret = GpioSetIrq(gpio, mode, CanGpioIrqHandler, (void*)pchannel);
    /* Enable the IRQ for this pin. */
    ret = GpioEnableIrq(gpio);
    return HDF_SUCCESS;
} 

/* 申请spi总线，初始化控制器，读取控制器ID */
static int32_t MCP2515InitContro(struct CanChannel *pchannel)
{
    struct SpiDevInfo spiInfo = {0};
    dprintf("%s: entry busNum:%d \n", __func__, pchannel->busNum);
    spiInfo.csNum = pchannel->csNum;
    spiInfo.busNum = pchannel->spiNum;
    pchannel->spiHandle = SpiOpen(&spiInfo);
    if(pchannel->spiHandle == NULL)
    {
        dprintf("%s SpiOpen error\n", __func__);
        return HDF_FAILURE;
    }
    /* set spi */
    struct SpiCfg cfg = {0};
    cfg.maxSpeedHz = 8000000;
    cfg.mode = 3;
    uint8_t buf[1] ={0x0E};//进入软重启
    SpiSetCfg(pchannel->spiHandle, &cfg);
    SpiWrite(pchannel->spiHandle, buf, 1);
    //初始化125kbps
    MCP2515_Init(pchannel->spiHandle);
    //注册中断
    MCP2515InitIrq(pchannel);
    return HDF_SUCCESS;
    
}
static int32_t CanRecvTask(void *arg)
{
    struct CanChannel *pchannel = (struct CanChannel *)arg;
    struct CanMsg msg = {0};
    while (1)
    {
        (void)OsalSemWait(&pchannel->sem, OSAL_WAIT_FOREVER);
        msg.dlc = CANReadBuffer(pchannel->spiHandle, msg.data);
        // dprintf("recv %x %x %x\n", msg.dlc, msg.data[0], msg.data[1]);
        CanCntlrOnNewMsg(&pchannel->cntlr, &msg);
    }
    return HDF_SUCCESS;

}

static int32_t CreateCanIrqThread(struct CanChannel *pchannel)
{
    struct OsalThread thread;
    char taskName[] = "canRecvTask_1";
    taskName[12] = pchannel->busNum+'0';

    struct OsalThreadParam para = {
        .name = taskName,
        .stackSize = 0x20000,
        .priority = OSAL_THREAD_PRI_HIGHEST,
    };

    if (OsalThreadCreate(&thread, CanRecvTask, (void *)pchannel) != HDF_SUCCESS) {
        HDF_LOGE("create isr thread failed");
        return HDF_FAILURE;
    }
    if (OsalThreadStart(&thread, &para) != HDF_SUCCESS) {
        HDF_LOGE("isr thread start failed");
        return HDF_FAILURE;
    }
    return HDF_SUCCESS;
}

static int32_t CanParseAndInit(struct HdfDeviceObject *device)
{
    int32_t ret;
    struct CanChannel *pchannel = NULL;
    (void)device;
    dprintf("%s: entry\n", __func__);

    pchannel = (struct CanChannel *)OsalMemCalloc(sizeof(struct CanChannel));

    /* TODO: read HCS file */
    CanGetRegCfgFromHcs(pchannel, device->property);
    // dprintf("%d\n", __LINE__);
    /* TODO: init CAN controller */
    if(MCP2515InitContro(pchannel) != HDF_SUCCESS)
    {
        return HDF_FAILURE;
    }
    ret = OsalSemInit(&pchannel->sem, 0);
    if (ret != HDF_SUCCESS) {
        OsalMemFree(pchannel);
        return ret;
    }
    pchannel->cntlr.ops = &g_canCntlrMethod;
    pchannel->cntlr.device.priv = pchannel;
    ret = CanCntlrAdd(&pchannel->cntlr);
    if (ret != HDF_SUCCESS) {
        OsalMemFree(pchannel);
        HDF_LOGE("%s: add cntlr failed: %d", __func__, ret);
        return ret;
    }
    ret = CanCntlrSetHdfDev(&pchannel->cntlr, device);
    if (ret != HDF_SUCCESS) {
        HDF_LOGW("%s: can controller attach failed:%d", __func__, ret);
    }

    CreateCanIrqThread(pchannel);

    return HDF_SUCCESS;
}

static int32_t CanDeviceInit(struct HdfDeviceObject *device)
{
    int32_t ret;

    if (device == NULL || device->property == NULL) {
        dprintf("%s: device or property is NULL", __func__);
        return HDF_ERR_INVALID_OBJECT;
    }
    // dprintf("%d\n", __LINE__);
    ret = CanParseAndInit(device);
    if (ret != HDF_SUCCESS) {
        dprintf("%s:CanParseAndInit fail", __func__);
        return HDF_FAILURE;
    }

    dprintf("%s: success\n", __func__);
    return HDF_SUCCESS;
}

static int32_t CanDeviceBind(struct HdfDeviceObject *device)
{
    HDF_LOGE("%s: success\n", __func__);
    return HDF_SUCCESS;
}

static void CanDeviceRelease(struct HdfDeviceObject *device)
{
    struct CanChannel *pchannel = NULL;

    HDF_LOGI("%s: entry", __func__);
    if (device == NULL) {
        HDF_LOGE("%s: device is null", __func__);
        return;
    }

    pchannel = (struct CanChannel *)CanCntlrFromHdfDev(device);
    if (pchannel == NULL) {
        HDF_LOGE("%s: platform device not bind", __func__);
        return;
    }

    CanServiceRelease(device);
    (void)CanCntlrDel(&pchannel->cntlr);
    OsalMemFree(pchannel);
    HDF_LOGE("%s: success\n", __func__);
}

struct HdfDriverEntry g_hdfCanDevice = {
    .moduleVersion = 1,
    .moduleName = "mcp2515_can_driver",
    .Bind = CanDeviceBind,
    .Init = CanDeviceInit,
    .Release = CanDeviceRelease,
};

HDF_INIT(g_hdfCanDevice);

/*********************************************************************************************************
** 文   件   名: dmac_sun8i.c
**
** 创   建   人: ZengQB
**
** 文件创建日期: 2022 年 07 月 29 日
**
** 描        述: DMA Control驱动程序
*********************************************************************************************************/
#include "dmac_sun8i.h"
#include "device_resource_if.h"
#include "dmac_core.h"
#include "hdf_device_desc.h"
#include "hdf_dlist.h"
#include "hdf_log.h"
#include "los_hw.h"
#include "los_hwi.h"
#include "los_vm_phys.h"
#include "osal_io.h"
#include "osal_mem.h"
#include "osal_time.h"
#include "los_vm_iomap.h"
#include "los_vm_zone.h"

static int32_t Sun8iDmacBind(struct HdfDeviceObject *device)
{
    return HDF_SUCCESS;
}

static int32_t Sun8iDmacInit(struct HdfDeviceObject *device)
{
    return HDF_SUCCESS;
}

static void Sun8iDmacRelease(struct HdfDeviceObject *device)
{
    return;
}

struct HdfDriverEntry g_Sun8iDmacEntry = {
    .moduleVersion = 1,
    .Bind = Sun8iDmacBind,
    .Init = Sun8iDmacInit,
    .Release = Sun8iDmacRelease,
    .moduleName = "HDF_SUNXI_DMAC",
};
HDF_INIT(g_Sun8iDmacEntry);

/*********************************************************************************************************
** 文   件   名: dmac_sun8i.h
**
** 创   建   人: ZengQB
**
** 文件创建日期: 2022 年 07 月 29 日
**
** 描        述: DMA Control驱动程序头文件
*********************************************************************************************************/
#ifndef DMAC_SUN8I_H
#define DMAC_SUN8I_H

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif /* __cplusplus */
#endif /* __cplusplus */

#define DMA_PERIPH_ID_MAX       32
#define DMA_CHANNEL_NUM         16
#define DMA_IRQ_CHAN_NR			8
#define HIGH_CHAN	            8

#define DMA_STAT	0x30			/* DMA Status Register RO */

#define DMA_IRQ_EN(x)	(0x000 + ((x) << 2))	/* Interrupt enable register */
#define DMA_IRQ_STAT(x)	(0x010 + ((x) << 2))	/* Inetrrupt status register */

#define DMA_ENABLE(x)	(0x100 + ((x) << 6))	/* Channels enable register */
#define DMA_PAUSE(x)	(0x104 + ((x) << 6))	/* DMA Channels pause register */
#define DMA_LLI_ADDR(x)	(0x108 + ((x) << 6))	/* Descriptor address register */
#define DMA_CFG(x)	    (0x10C + ((x) << 6))	/* Configuration register RO */
#define DMA_CUR_SRC(x)	(0x110 + ((x) << 6))	/* Current source address RO */
#define DMA_CUR_DST(x)	(0x114 + ((x) << 6))	/* Current destination address RO */
#define DMA_CNT(x)	    (0x118 + ((x) << 6))	/* Byte counter left register RO */
#define DMA_PARA(x)	    (0x11C + ((x) << 6))	/* Parameter register RO */

#define DMA_OP_MODE(x)	(0x128 + ((x) << 6))	/* DMA mode options register */
#define SRC_HS_MASK	(0x1 << 2)		/* bit 2: Source handshark mode */
#define DST_HS_MASK	(0x1 << 3)		/* bit 3: Destination handshark mode */


#define SRC_DRQ(x)	    ((x) << 0)    /* DMA Source DRQ Type. */
#define DST_DRQ(x)	    ((x) << 16)   /* DMA Destination DRQ Type. */
#define SRC_BURST(x)	((x) << 6)    /* DMA Source Block Size. */
#define DST_BURST(x)	((x) << 22)   /* DMA Destination Block Size. */
#define SRC_WIDTH(x)	((x) << 9)    /* DMA Source Data Width. */
#define DST_WIDTH(x)	((x) << 25)   /* DMA Destination Data Width. */
#define SRC_LINEAR_MODE	(0x00 << 5)   /* DMA Source Address Mode. */
#define DST_IO_MODE	(0x01 << 21)
#define DST_LINEAR_MODE	(0x00 << 21)  /* DMA Destination Address Mode. */

#define NORMAL_WAIT	(8 << 0)

#define IRQ_HALF	0x01		/* Half package transfer interrupt pending */
#define IRQ_PKG		0x02		/* One package complete interrupt pending */
#define IRQ_QUEUE	0x04		/* All list complete transfer interrupt pending */

#define SHIFT_IRQ_MASK(val, ch) ({	\
		(ch) >= HIGH_CHAN	\
		? (val) << ((ch - HIGH_CHAN) << 2) \
		: (val) << ((ch) << 2 );	\
		})

/**
 * enum dma_slave_buswidth - defines bus with of the DMA slave
 * device, source or target buses
 */
enum dma_slave_buswidth {
	DMA_SLAVE_BUSWIDTH_UNDEFINED = 0,
	DMA_SLAVE_BUSWIDTH_1_BYTE = 1,
	DMA_SLAVE_BUSWIDTH_2_BYTES = 2,
	DMA_SLAVE_BUSWIDTH_4_BYTES = 4,
	DMA_SLAVE_BUSWIDTH_8_BYTES = 8,
};

#ifdef __cplusplus
#if __cplusplus
}
#endif /* __cplusplus */
#endif /* __cplusplus */
#endif /* DMAC_HI35XX_H */


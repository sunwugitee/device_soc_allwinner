/*********************************************************************************************************
** 文件创建日期: 2022 年 07 月 1 日
**
** 描        述: GMAC 千兆以太网控制器的 phy 芯片初始化代码。在phy 初始化函数中，系统会搜索 0～31
*********************************************************************************************************/
#include "emac_priv.h"
#include "emac_phy.h"
#include "osal_time.h"

#define PHY_JL11X1_ID         (0x937c4023)

#define PHY_MAX_ADDR            (32)
#define PHY_ID1_SHIFT           (16)

/* Advertisement control register. */
#define ADVERTISE_10HALF		0x0020  /* Try for 10mbps half-duplex  */
#define ADVERTISE_10FULL		0x0040  /* Try for 10mbps full-duplex  */
#define ADVERTISE_100HALF		0x0080  /* Try for 100mbps half-duplex */
#define ADVERTISE_100FULL		0x0100  /* Try for 100mbps full-duplex */

#define ADVERTISE_100			(ADVERTISE_100FULL | ADVERTISE_100HALF)
#define ADVERTISE_10			(ADVERTISE_10FULL | ADVERTISE_10HALF)
#define ADVERTISE_1000			0x0300
#define IEEE_CONTROL_REG_OFFSET				0
#define IEEE_STATUS_REG_OFFSET				1
#define IEEE_AUTONEGO_ADVERTISE_REG			4
#define IEEE_PARTNER_ABILITIES_1_REG_OFFSET	5
#define IEEE_1000_ADVERTISE_REG_OFFSET		9
#define IEEE_COPPER_SPECIFIC_CONTROL_REG	16
#define IEEE_SPECIFIC_STATUS_REG			17
#define IEEE_COPPER_SPECIFIC_STATUS_REG_2	19
#define IEEE_CONTROL_REG_MAC				21
#define IEEE_PAGE_ADDRESS_REGISTER			22
#define IEEE_CTRL_1GBPS_LINKSPEED_MASK		0x2040
#define IEEE_CTRL_LINKSPEED_MASK			0x0040
#define IEEE_CTRL_LINKSPEED_1000M			0x0040
#define IEEE_CTRL_LINKSPEED_100M			0x2000
#define IEEE_CTRL_LINKSPEED_10M				0x0000
#define IEEE_CTRL_RESET_MASK				0x8000
#define BMCR_PDOWN		0x0800	/* Enable low power state      */


static void jl11x1EPhyInit(void *pvCtx, uint16_t usPhyAddr, PHY_READ pfuncPhyRead, PHY_WRITE pfuncPhyWrite)
{
	// uint16_t  usValue   = 0;

	// pfuncPhyWrite(pvCtx, usPhyAddr, IEEE_CONTROL_REG_OFFSET, IEEE_CTRL_RESET_MASK);
	// while (1) {
	// 	pfuncPhyRead(pvCtx, usPhyAddr, IEEE_CONTROL_REG_OFFSET, &usValue);
	// 	if (usValue & IEEE_CTRL_RESET_MASK)
	// 		continue;
	// 	else
	// 		break;
	// }
	// //
	// pfuncPhyWrite(pvCtx, usPhyAddr, 0x4, 0x300);
	// pfuncPhyWrite(pvCtx, usPhyAddr, 0x0, 0x2100);

	//modify JL1111 led state
	pfuncPhyWrite(pvCtx, usPhyAddr, 0x1f, 0x7);
	pfuncPhyWrite(pvCtx, usPhyAddr, 0xf3, 0x10);
}

void  emacPhyInit (void *pvCtx, PHY_READ pfuncPhyRead, PHY_WRITE pfuncPhyWrite, uint16_t *pusPhyAddr)
{
	uint32_t  uiID;
	uint16_t  uiAddr, usValue = 0;

	for(uiAddr = 0; uiAddr < PHY_MAX_ADDR; uiAddr++) {
	    pfuncPhyRead(pvCtx, uiAddr, GMII_PHYID1, &usValue);
	    uiID = usValue << PHY_ID1_SHIFT;
	    pfuncPhyRead(pvCtx, uiAddr, GMII_PHYID2, &usValue);
	    uiID |= usValue;
	    uiID &= 0xFFFFFFFF;

	    if((uiID != 0x0) & (uiID != 0xFFFFFFFF)) {
	    	break;
	    }
	}
	dprintf("EMAC PHY:%d: %08x\n", uiAddr, uiID);

	*pusPhyAddr = uiAddr;

    switch (uiID) {
		case PHY_JL11X1_ID:
			jl11x1EPhyInit(pvCtx, uiAddr, pfuncPhyRead, pfuncPhyWrite);
			break;
		default:
			dprintf("[Warning] unsupport PHY: ID=%lx\n", uiID);
    }
}
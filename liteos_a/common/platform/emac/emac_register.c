/*
 * Copyright (c) 2021 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "devsvc_manager_clnt.h"
#include "eth_chip_driver.h"
#include "hdf_device_desc.h"
#include "hdf_log.h"
#include "osal_mem.h"
#include "emac_phy.h"
#include "sunxi_emac.h"

static const char* const EMAC_ETHERNET_DRIVER_NAME = "eth-emac";


static int32_t HdfEthRegHrgxyDriverFactory(void)
{
    static struct HdfEthChipDriverFactory tmpFactory = { 0 };
    struct HdfEthChipDriverManager *driverMgr = HdfEthGetChipDriverMgr();

    if (driverMgr == NULL || driverMgr->RegChipDriver == NULL) {
        HDF_LOGE("%s fail: driverMgr is NULL", __func__);
        return HDF_FAILURE;
    }
    /* 只要实现这些接口 */
    tmpFactory.driverName = EMAC_ETHERNET_DRIVER_NAME;
    tmpFactory.InitEthDriver = InitEmacDriver;
    tmpFactory.GetMacAddr = EmacRandomAddr;
    tmpFactory.DeinitEthDriver = DeinitEmacDriver;
    tmpFactory.BuildMacDriver = BuildEmacDriver;
    tmpFactory.ReleaseMacDriver = ReleaseEmacDriver;
    if (driverMgr->RegChipDriver(&tmpFactory) != HDF_SUCCESS) {
        HDF_LOGE("%s fail: driverMgr is NULL", __func__);
        return HDF_FAILURE;
    }
    HDF_LOGI("emac eth driver register success");
    return HDF_SUCCESS;
}

static int32_t HdfEthHrgxyChipDriverInit(struct HdfDeviceObject *device)
{
    (void)device;
    return HdfEthRegHrgxyDriverFactory();
}

static int32_t HdfEthHrgxyDriverBind(struct HdfDeviceObject *dev)
{
    (void)dev;
    return HDF_SUCCESS;
}

static void HdfEthHrgxyChipRelease(struct HdfDeviceObject *object)
{
    (void)object;
}

struct HdfDriverEntry g_hdfEmacEthChipEntry = {
    .moduleVersion = 1,
    .Bind = HdfEthHrgxyDriverBind,
    .Init = HdfEthHrgxyChipDriverInit,
    .Release = HdfEthHrgxyChipRelease,
    .moduleName = "HDF_ETHERNET_CHIPS1"
};

HDF_INIT(g_hdfEmacEthChipEntry);

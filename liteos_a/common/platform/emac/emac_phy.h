#ifndef HIETH_PHY_H
#define HIETH_PHY_H

#include "eth_chip_driver.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif /* __cplusplus */
#endif /* __cplusplus */

/*********************************************************************************************************
   千兆以太网 PHY 芯片的 IEEE 标准寄存器
*********************************************************************************************************/
#define GMII_BMCR        0                                              /* Basic Mode Control Register  */
#define GMII_BMSR        1                                              /* Basic Mode Status Register   */
#define GMII_PHYID1      2                                              /* PHY Idendifier Register 1    */
#define GMII_PHYID2      3                                              /* PHY Idendifier Register 2    */
#define GMII_ANAR        4                                              /* Auto_Neg Advertisement       */
#define GMII_ANLPAR      5                                              /* Auto_neg Link Partner Ability*/
#define GMII_ANER        6                                              /* Auto-neg Expansion Register  */
#define GMII_ANNPR       7                                              /* Auto-neg Next Page Register  */
#define GMII_ANLPNPAR    8                                              /* Auto_neg Link NextPageAbility*/
#define GMII_1000BTCR    9                                              /* 1000Base-T Control           */
#define GMII_1000BTSR   10                                              /* 1000Base-T Status            */
#define GMII_ERCR       11                                              /* Control Register             */
#define GMII_ERDWR      12                                              /* Data Write Register          */
#define GMII_ERDRR      13                                              /* Data Read Register           */
#define GMII_REV        14                                              /*                              */
#define GMII_EMSR       15                                              /* Extend MII Status Register   */

/*********************************************************************************************************
   PHY 寄存器位定义
*********************************************************************************************************/
#define GMII_RESET                         (1 << 15)                    /* GMII_BMCR                    */
#define GMII_LOOPBACK                      (1 << 14)
#define GMII_SPEED_SELECT_LSB              (1 << 13)
#define GMII_AUTONEG                       (1 << 12)
#define GMII_POWER_DOWN                    (1 << 11)
#define GMII_ISOLATE                       (1 << 10)
#define GMII_RESTART_AUTONEG               (1 << 9)
#define GMII_DUPLEX_MODE                   (1 << 8)
#define GMII_SPEED_SELECT_MSB              (1 << 6)

#define GMII_100BASE_T4                    (1 << 15)                    /* GMII_BMSR                    */
#define GMII_100BASE_TX_FD                 (1 << 14)
#define GMII_100BASE_T4_HD                 (1 << 13)
#define GMII_10BASE_T_FD                   (1 << 12)
#define GMII_10BASE_T_HD                   (1 << 11)
#define GMII_EXTEND_STATUS                 (1 << 8)
#define GMII_MF_PREAMB_SUPPR               (1 << 6)
#define GMII_AUTONEG_COMP                  (1 << 5)
#define GMII_REMOTE_FAULT                  (1 << 4)
#define GMII_AUTONEG_ABILITY               (1 << 3)
#define GMII_LINK_STATUS                   (1 << 2)
#define GMII_JABBER_DETECT                 (1 << 1)
#define GMII_EXTEND_CAPAB                  (1 << 0)

#define GMII_NP                            (1 << 15)                    /* GMII_ANAR GMII_ANLPAR        */
#define GMII_RF                            (1 << 13)
#define GMII_PAUSE_MASK                    (3 << 11)
#define GMII_T4                            (1 << 9)
#define GMII_100TX_FDX                     (1 << 8)
#define GMII_100TX_HDX                     (1 << 7)
#define GMII_10_FDX                        (1 << 6)
#define GMII_10_HDX                        (1 << 5)
#define GMII_AN_IEEE_802_3                 (1 << 0)

#define GMII_PDF                           (1 << 4)                     /* GMII_ANER                    */
#define GMII_LP_NP_ABLE                    (1 << 3)
#define GMII_NP_ABLE                       (1 << 2)
#define GMII_PAGE_RX                       (1 << 1)
#define GMII_LP_AN_ABLE                    (1 << 0)

#define GMII_1000BaseT_HALF_DUPLEX         (1 << 8)                     /* GMII_1000BTCR                */
#define GMII_1000BaseT_FULL_DUPLEX         (1 << 9)
#define GMII_MARSTER_SLAVE_ENABLE          (1 << 12)
#define GMII_MARSTER_SLAVE_CONFIG          (1 << 11)
#define GMII_PORT_TYPE                     (1 << 10)

#define GMII_LINKP_1000BaseT_HALF_DUPLEX   (1 << 10)                    /* GMII_1000BTSR                */
#define GMII_LINKP_1000BaseT_FULL_DUPLEX   (1 << 11)


typedef int  (*PHY_WRITE)(void *pvCtx, uint8_t phyAddr, uint8_t regAddr, uint16_t value);
typedef int  (*PHY_READ)(void *pvCtx, uint8_t phyAddr, uint8_t regAddr, uint16_t *value);

VOID    emacPhyInit(void *pvCtx, PHY_READ pfuncPhyRead, PHY_WRITE pfuncPhyWrite, uint16_t *pusPhyAddr);


#ifdef __cplusplus
#if __cplusplus
}
#endif /* __cplusplus */
#endif /* __cplusplus */

#endif /* HIETH_PHY_H */

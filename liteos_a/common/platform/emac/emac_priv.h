#ifndef __CDNS_GEMPRIV_H
#define __CDNS_GEMPRIV_H

#define SETBITS_LE32(value,address) OSAL_WRITEL(OSAL_READL(address) | value, address)
#define CLRBITS_LE32(value,address) OSAL_WRITEL(OSAL_READL(address) & ~value, address)

#endif
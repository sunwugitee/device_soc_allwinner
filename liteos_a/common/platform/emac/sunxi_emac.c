/*********************************************************************************************************
** 文件创建日期: 2022 年 07 月 1 日
**
** 描        述: GMAC 千兆以太网控制器驱动
*********************************************************************************************************/
#include <netinet/if_ether.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include "dmac_core.h"
#include "osal_mem.h"
#include "osal_io.h"
#include "osal_time.h"
#include "osal_irq.h"
#include "osal_thread.h"
#include "osal_spinlock.h"
#include "osal_timer.h"
#include "los_vm_iomap.h"
#include "los_vm_zone.h"
#include "los_event.h"
#include "los_hw.h"
#include "sunxi_emac.h"
#include "emac_phy.h"
#include "emac_priv.h"


#define EVENT_NET_RX    0x1
#define EVENT_NET_TX    0x2
/*********************************************************************************************************
  GMAC 私有结构
*********************************************************************************************************/
struct EmacPriv {
    const char *devName;
    uint32_t iobase;           /* Base address of device */
    uint32_t vector;           /* 中断号，用于操作前禁能中断 */
    uint32_t index;            /* dev id */
    uint32_t txHwCnt;
    uint32_t rxHwCnt;

    uint32_t emacRxCompletedFlag; //可以接收标志
    uint32_t txFifoStat; //两个发送通道状态

    /* forced speed & duplex (no autoneg)
	 * partner speed & duplex & pause (autoneg)
	 */
	uint32_t speed;
	uint32_t duplex;
    
    int32_t phyMode;
    int16_t phyAddr;
    OsalSpinlock tx_lock;
    OsalSpinlock rx_lock;
    /*
     * 接收队列事件
     */
    EVENT_CB_S stEvent;
    /*
     * 定时器
     */
    OsalTimer phyTimer;
};

// static int32_t intCount = 0;

/**/
static int mdioRead (struct EmacPriv *priv, uint8_t ucPhyAddr, uint8_t ucRegAddr, uint16_t *pusValue);
static int mdioWrite (struct EmacPriv *priv, uint8_t ucPhyAddr, uint8_t ucRegAddr, uint16_t usValue);

/* 随机生成mac地址 */
static void EmacRandomAddr(uint8_t *addr, int32_t len)
{
    addr[0] = 0x03;
    addr[1] = 0x53;
    addr[2] = 0xd2;
    addr[3] = 0xb1;
    addr[4] = 0xea;
    addr[5] = 0x3f;
}

/* 数据发送 */
static NetDevTxResult EthXmit(struct NetDevice *netDev, NetBuf *netBuf)
{
    struct EthDevice * ethDevice = (struct EthDevice *)netDev->mlPriv;
    struct EmacPriv *Emac = (struct EmacPriv *)ethDevice->priv;
    int32_t channel;
    int32_t sendLen = 0;
    int32_t cnt = 0;
    void * sendBuf = NULL;

    //地址需要向上取整
    sendLen = NetBufGetDataLen(netBuf);
    sendBuf = (void *)NetBufGetAddress(netBuf, E_DATA_BUF);
    
    channel = Emac->txFifoStat & 3;
    if (channel == 3)
		return NETDEV_TX_BUSY;

	channel = (channel == 1 ? 1 : 0);
    OsalSpinLockIrq(&(Emac->tx_lock));
    OSAL_WRITEL(channel, Emac->iobase + EMAC_TX_INS_REG);
    cnt = (sendLen + 3) >> 2;
	if (cnt) {
		const uint32_t *buf = sendBuf;
		do {
			OSAL_WRITEL(*buf++, Emac->iobase + EMAC_TX_IO_DATA_REG);
		} while (--cnt);
	}

    Emac->txFifoStat |= 1 << channel;
    /* TX control: First packet immediately send, second packet queue */
	if (channel == 0) {
		/* set TX len */
        OSAL_WRITEL(sendLen, Emac->iobase + EMAC_TX_PL0_REG);
		/* start translate from fifo to phy */
        SETBITS_LE32(1, Emac->iobase + EMAC_TX_CTL0_REG);
		/* save the time stamp */

	} else if (channel == 1) {
		/* set TX len */
        OSAL_WRITEL(sendLen, Emac->iobase + EMAC_TX_PL1_REG);
		/* start translate from fifo to phy */
        SETBITS_LE32(1, Emac->iobase + EMAC_TX_CTL1_REG);
		/* save the time stamp */

	}
    OsalSpinUnlockIrq(&(Emac->tx_lock));
    NetBufFree(netBuf);

    return NETDEV_TX_OK;
}

static void LinkStatusChanged(struct NetDevice *netDev)
{
    struct EthDevice * ethDevice = (struct EthDevice *)netDev->mlPriv;
    struct EmacPriv *Emac = (struct EmacPriv *)ethDevice->priv;
    uint16_t status = 0;

    mdioRead(Emac, Emac->phyAddr, 0x01, &status);
    if ((status & 0x04) == 0){//更新连接状态
        NetIfSetLinkStatus(ethDevice->netdev, NETIF_LINK_DOWN);
    }else{
        NetIfSetLinkStatus(ethDevice->netdev, NETIF_LINK_UP);
    }
}

static int32_t EthSetMacAddr(struct NetDevice *netDev, void *addr)
{
    int32_t ret = HDF_SUCCESS;

    if (netDev == NULL) {
        HDF_LOGE("%s:input is NULL!", __func__);
        return HDF_FAILURE;
    }

    struct EthDevice * ethDevice = (struct EthDevice *)netDev->mlPriv;
    struct EmacPriv *Emac = (struct EmacPriv *)ethDevice->priv;
    uint32_t macid_lo, macid_hi;
    uint8_t  *mac_id = (uint8_t *)addr;


    /* Rewrite mac address after reset */
	macid_lo = mac_id[0] + (mac_id[1] << 8) + (mac_id[2] << 16);
	macid_hi = mac_id[3] + (mac_id[4] << 8) + (mac_id[5] << 16);
    OSAL_WRITEL(macid_hi, Emac->iobase + EMAC_MAC_A0_REG);
    OSAL_WRITEL(macid_lo, Emac->iobase + EMAC_MAC_A1_REG);

    return ret;
}

struct NetDeviceInterFace g_emacDevOps = {
    .xmit = EthXmit,
    .setMacAddr = EthSetMacAddr,
    .linkStatusChanged = LinkStatusChanged,
};

static int32_t EthernetInitNetdev(NetDevice *netdev)
{
    int32_t ret;

    if (netdev == NULL) {
        HDF_LOGE("%s netdev is null!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    netdev->netDeviceIf = &g_emacDevOps;

    ret = NetDeviceAdd(netdev);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s NetDeviceAdd return error code %d!", __func__, ret);
        return ret;
    }
    return ret;
}

static void EmacTxDone(struct EmacPriv *Emac, uint32_t intStatus)
{
    /* One packet sent complete */
	Emac->txFifoStat &= ~(intStatus & 3);
	if (3 == (intStatus & 3))
		Emac->txHwCnt += 2;
	else
		Emac->txHwCnt++;

}

static void EmacRx(struct EthDevice *ethDevice)
{
	uint32_t rxcount;
    uint32_t value;
    uint32_t head;
    int rxLen;
	int rxStatus;
    int goodPacket;
    NetBuf *netBuf = NULL;
    struct EmacPriv *Emac = (struct EmacPriv *)ethDevice->priv;
	/* Check packet ready or not */
	while (1) {
		/* race warning: the first packet might arrive with
		 * the interrupts disabled, but the second will fix
		 * it
		 */
		rxcount = OSAL_READL(Emac->iobase + EMAC_RX_FBC_REG);

		if (!rxcount) {
			Emac->emacRxCompletedFlag = 1;
			value = OSAL_READL(Emac->iobase + EMAC_INT_CTL_REG);
			value |= (0xf << 0) | (0x01 << 8);
			OSAL_WRITEL(value, Emac->iobase + EMAC_INT_CTL_REG);

			/* had one stuck? */
			rxcount = OSAL_READL(Emac->iobase + EMAC_RX_FBC_REG);
			if (!rxcount)
				break;
		}

        head = OSAL_READL(Emac->iobase + EMAC_RX_IO_DATA_REG);
        if (head != 0x0143414d)
        {
            /* code */
            return;
        }
        head = OSAL_READL(Emac->iobase + EMAC_RX_IO_DATA_REG);
        rxLen = EMAC_RX_IO_DATA_LEN(head);
        rxStatus = EMAC_RX_IO_DATA_STATUS(head);
        /* Packet Status check */
        if (rxLen < 0x40) {
            goodPacket = 0;
            dprintf("RX: Bad Packet (runt)\n");
        }
        netBuf = NetBufAlloc(rxLen);
        // dprintf("rx len %x  head:%x \n", rxLen, head);
        int cnt = (rxLen + 3) >> 2;

        if (cnt) {
            uint32_t *buf = (uint32_t *)NetBufGetAddress(netBuf, E_DATA_BUF);
            do {
                uint32_t x = OSAL_READL(Emac->iobase + EMAC_RX_IO_DATA_REG);
                *buf++ = x;
            } while (--cnt);
        }
        NetBufPush(netBuf, E_DATA_BUF, rxLen);
        NetIfRxNi(ethDevice->netdev, netBuf);
	}
}

static uint32_t EmacEthIsr(uint32_t irq, void *data)
{
    (void)irq;
    struct EthDevice *ethDevice = (struct EthDevice *)data;
    struct EmacPriv *Emac = (struct EmacPriv *)ethDevice->priv;
    uint32_t intStatus = 0;
    uint32_t val = 0;

    // OsalSpinLockIrq(&(Emac->rx_lock));  

    /* Disable all interrupts */
	OSAL_WRITEL(0, Emac->iobase + EMAC_INT_CTL_REG);

    /* Got EMAC interrupt status */
	/* Got ISR */
	intStatus = OSAL_READL(Emac->iobase + EMAC_INT_STA_REG);
    // dprintf("enter irq:%d intStatus:%x \n", Emac->vector, intStatus);
	/* Clear ISR status */
	OSAL_WRITEL(intStatus, Emac->iobase + EMAC_INT_STA_REG);

    /* Received the coming packet */
	if ((intStatus & 0x100) && (Emac->emacRxCompletedFlag == 1)) {
		/* carrier lost */
		Emac->emacRxCompletedFlag = 0;
		//接收数据
        EmacRx(ethDevice);
	}

    /* Transmit Interrupt check */
	if (intStatus & (0x01 | 0x02))
		EmacTxDone(Emac, intStatus);

	// if (intStatus & (0x04 | 0x08))
		// netdev_info(dev, " ab : %x\n", int_status);

    /* Re-enable interrupt mask */
	if (Emac->emacRxCompletedFlag == 1) {
		val = OSAL_READL(Emac->iobase + EMAC_INT_CTL_REG);
		val |= (0xf << 0) | (0x01 << 8);
		OSAL_WRITEL(val, Emac->iobase + EMAC_INT_CTL_REG);
	}
    // val = OSAL_READL(Emac->iobase + EMAC_INT_CTL_REG);
    // val |= (0xf << 0) | (0x01 << 8);
    // OSAL_WRITEL(val, Emac->iobase + EMAC_INT_CTL_REG);

    return HDF_SUCCESS;
}

/*
* 对相关引脚进行配置
*/
static int32_t PinsInit()
{
    // allwinner,pins = "PH8", "PH9", "PH10", "PH11","PH14", "PH15", 0x33003333
    // "PH16", "PH17", "PH18", "PH19", "PH20", "PH21", "PH22", "PH23", 
    // "PH24", "PH25", "PH26", "PH27";    
    uint32_t pinCtrl = IO_DEVICE_ADDR(0x01C20900);
    uint32_t value = 0;

    value = OSAL_READL(pinCtrl);
    value &= ~(0xFF00FFFF);
    value |= 0x33003333;
    OSAL_WRITEL(value, pinCtrl);

    pinCtrl = IO_DEVICE_ADDR(0x01C20904);
    OSAL_WRITEL(0x33333333, pinCtrl);

    pinCtrl = IO_DEVICE_ADDR(0x01C20908);
    OSAL_WRITEL(0x3333, pinCtrl);

    return HDF_SUCCESS;
}

static int32_t InitEmacSyscon(void)
{   
    // 0x01C20000 0x60 Gating Clock for EMAC  (1<<17) 
    // 0x01C20000 0x2c0 EMAC Reset.           (1<<17)
    uint32_t ccu = IO_DEVICE_ADDR(0x01C20060);
    uint32_t value = 0;
    //2c0
    value = OSAL_READL(ccu);
    value |= (1 << 17);
    OSAL_WRITEL(value, ccu);
    //rts
    ccu = IO_DEVICE_ADDR(0x01C202c0);
    value = OSAL_READL(ccu);
    value |= (1 << 17);
    OSAL_WRITEL(value, ccu);
    //map to sram
    uint32_t sram = IO_DEVICE_ADDR(0x01C00004);
    value = OSAL_READL(sram);
    value |= 0x5 << 2;
    OSAL_WRITEL(value, sram);
    return HDF_SUCCESS;
}

static int32_t PhyReset(struct EmacPriv *Emac)
{
    // 0x01C20800 0x08 通过GPIO对phy重启 PB9
    uint32_t rst = IO_DEVICE_ADDR(0x01C20828);
    uint32_t value = 0;
    value = OSAL_READL(rst);
    value &= 0xFFFFFF0F;
    value |= 0x70;
    OSAL_WRITEL(value, rst);
    OsalMSleep(10);
    value = OSAL_READL(rst);
    value &= 0xFFFFFF0F;
    value |= 0x10;
    OSAL_WRITEL(value, rst);
    OsalMSleep(100);

    return 0;
}

static unsigned int EmacSetup(struct EmacPriv *Emac)
{
    uint32_t value = 0;
    /* set up TX */
	value = OSAL_READL(Emac->iobase + EMAC_TX_MODE_REG);

	OSAL_WRITEL(value | EMAC_TX_MODE_ABORTED_FRAME_EN,
		Emac->iobase + EMAC_TX_MODE_REG);
    /* set up RX */
	value = OSAL_READL(Emac->iobase + EMAC_RX_CTL_REG);

	OSAL_WRITEL(value | EMAC_RX_CTL_PASS_LEN_OOR_EN |
		EMAC_RX_CTL_ACCEPT_UNICAST_EN | EMAC_RX_CTL_DA_FILTER_EN |
		EMAC_RX_CTL_ACCEPT_MULTICAST_EN |
		EMAC_RX_CTL_ACCEPT_BROADCAST_EN,
		Emac->iobase + EMAC_RX_CTL_REG);


	/* set MAC */
	/* set MAC CTL0 */
	value = OSAL_READL(Emac->iobase + EMAC_MAC_CTL0_REG);
	OSAL_WRITEL(value | EMAC_MAC_CTL0_RX_FLOW_CTL_EN |
		EMAC_MAC_CTL0_TX_FLOW_CTL_EN,
		Emac->iobase + EMAC_MAC_CTL0_REG);

	/* set MAC CTL1 */
	value = OSAL_READL(Emac->iobase + EMAC_MAC_CTL1_REG);
	value |= EMAC_MAC_CTL1_LEN_CHECK_EN;
	value |= EMAC_MAC_CTL1_CRC_EN;
	value |= EMAC_MAC_CTL1_PAD_EN;
	OSAL_WRITEL(value, Emac->iobase + EMAC_MAC_CTL1_REG);

	/* set up IPGT */
	OSAL_WRITEL(EMAC_MAC_IPGT_FULL_DUPLEX, Emac->iobase + EMAC_MAC_IPGT_REG);

	/* set up IPGR */
	OSAL_WRITEL((EMAC_MAC_IPGR_IPG1 << 8) | EMAC_MAC_IPGR_IPG2,
		Emac->iobase + EMAC_MAC_IPGR_REG);

	/* set up Collison window */
	OSAL_WRITEL((EMAC_MAC_CLRT_COLLISION_WINDOW << 8) | EMAC_MAC_CLRT_RM,
		Emac->iobase + EMAC_MAC_CLRT_REG);

	/* set up Max Frame Length */
	OSAL_WRITEL(EMAC_MAX_FRAME_LEN, Emac->iobase + EMAC_MAC_MAXF_REG);
    
    return 0;
}
static unsigned int EmacPowerup(struct EmacPriv *Emac)
{
	unsigned int reg_val;

	/* initial EMAC */
	/* flush RX FIFO */
	reg_val = OSAL_READL(Emac->iobase + EMAC_RX_CTL_REG);
	reg_val |= 0x8;
	OSAL_WRITEL(reg_val, Emac->iobase + EMAC_RX_CTL_REG);
	OsalUDelay(1);

	/* initial MAC */
	/* soft reset MAC */
	reg_val = OSAL_READL(Emac->iobase + EMAC_MAC_CTL0_REG);
	reg_val &= ~EMAC_MAC_CTL0_SOFT_RESET;
	OSAL_WRITEL(reg_val, Emac->iobase + EMAC_MAC_CTL0_REG);

	/* set MII clock */
	reg_val = OSAL_READL(Emac->iobase + EMAC_MAC_MCFG_REG);
	reg_val &= (~(0xf << 2));
	reg_val |= (0xD << 2);
	OSAL_WRITEL(reg_val, Emac->iobase + EMAC_MAC_MCFG_REG);

	/* clear RX counter */
	OSAL_WRITEL(0x0, Emac->iobase + EMAC_RX_FBC_REG);

	/* disable all interrupt and clear interrupt status */
	OSAL_WRITEL(0, Emac->iobase + EMAC_INT_CTL_REG);
	reg_val = OSAL_READL(Emac->iobase + EMAC_INT_STA_REG);
	OSAL_WRITEL(reg_val, Emac->iobase + EMAC_INT_STA_REG);

	OsalUDelay(1);
    /* set up EMAC */
    EmacSetup(Emac);
    
	/* set mac_address to chip */
	OSAL_WRITEL(0xd2 << 16 | 0x53 << 8 | 0x03, Emac->iobase + EMAC_MAC_A1_REG);
	OSAL_WRITEL(0x3f << 16 | 0xea << 8 | 0xb1, Emac->iobase + EMAC_MAC_A0_REG);
    OsalMDelay(1);

	return 0;
}

static int32_t HWReset(struct EmacPriv *Emac)
{
    /* RESET device */
	OSAL_WRITEL(0, Emac->iobase + EMAC_CTL_REG);
	OsalUDelay(200);
	OSAL_WRITEL(EMAC_CTL_RESET, Emac->iobase + EMAC_CTL_REG);
	OsalUDelay(200);
    return HDF_SUCCESS;
}

int32_t InitEmacDriver(struct EthDevice *ethDevice)
{
    int32_t ret;
    uint16_t uiPhyAddr = 0;
    struct EmacPriv *pstPrivData = NULL;

    if (ethDevice == NULL) {
        HDF_LOGE("%s input is NULL!", __func__);
        return HDF_FAILURE;
    }
    ret = EthernetInitNetdev(ethDevice->netdev);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s failed to init ethernet netDev", __func__);
        return HDF_FAILURE;
    }
    
    pstPrivData = (struct EmacPriv *)OsalMemCalloc(sizeof(struct EmacPriv));
    if (pstPrivData == NULL) {
        HDF_LOGE("%s fail : HiethPriv OsalMemCalloc is fail!", __func__);
        return HDF_FAILURE;
    }

    pstPrivData->index   = 0;
    pstPrivData->phyAddr = 0;
    pstPrivData->emacRxCompletedFlag = 1;
    pstPrivData->duplex  = 1;
    pstPrivData->speed   = 100;
    pstPrivData->txHwCnt = 0;
    pstPrivData->rxHwCnt = 0;
    pstPrivData->vector  = ethDevice->config->ethMac.irqVector;
    pstPrivData->iobase  = IO_DEVICE_ADDR(ethDevice->config->ethMac.regBase);
    ethDevice->config->ethMac.iobase = pstPrivData->iobase;

    pstPrivData->devName = "eth1";

    OsalSpinInit(&(pstPrivData->tx_lock));
    OsalSpinInit(&(pstPrivData->rx_lock));
    (void)LOS_EventInit(&(pstPrivData->stEvent));
    /* 初始化相关引脚 */
    PinsInit();
    /* 配置相关时钟 */
    InitEmacSyscon();
    
    EmacPowerup(pstPrivData);
    /* emac 软重启 */
    HWReset(pstPrivData);
    /* phy硬重启 */
    PhyReset(pstPrivData);
    /* 配置phy */
    emacPhyInit((VOID *)pstPrivData, (PHY_READ)mdioRead, (PHY_WRITE)mdioWrite, &uiPhyAddr);

    pstPrivData->phyAddr = uiPhyAddr;
    ethDevice->priv = pstPrivData;

    if (OsalRegisterIrq(pstPrivData->vector, OSAL_IRQF_TRIGGER_NONE, EmacEthIsr, "emac", (void *)ethDevice) != HDF_SUCCESS) {
        HDF_LOGE("register irq failed");
    }
    OsalEnableIrq(pstPrivData->vector);

    return HDF_SUCCESS;
}

int32_t DeinitEmacDriver(struct EthDevice *ethDevice)
{
    return HDF_SUCCESS;
}

void EmacCoreInit(void)
{

}

int32_t EmacPortReset(struct EthDevice *ethDevice)
{
    // struct EmacPriv *Emac = (struct EmacPriv *)ethDevice->priv;
    // uint32_t value;

    return HDF_SUCCESS;
}

int32_t EmacUpdateSpeed(struct EmacPriv *emac)
{
    uint32_t value;
    /* set EMAC SPEED, depend on PHY  */
	value = OSAL_READL(emac->iobase + EMAC_MAC_SUPP_REG);
	value &= ~(0x1 << 8);
	if (emac->speed == 100)
		value |= 1 << 8;
	OSAL_WRITEL(value, emac->iobase + EMAC_MAC_SUPP_REG);
    return HDF_SUCCESS;
}

int32_t EmacUpdateDuplex(struct EmacPriv *emac)
{
    uint32_t value;
    /* set duplex depend on phy */
	value = readl(emac->iobase + EMAC_MAC_CTL1_REG);
	value &= ~EMAC_MAC_CTL1_DUPLEX_EN;
	if (emac->duplex)
		value |= EMAC_MAC_CTL1_DUPLEX_EN;
	writel(value, emac->iobase + EMAC_MAC_CTL1_REG);
    return HDF_SUCCESS;
}

#define PHY_STATE_TIME 1000
int32_t EmacPortInit(struct EthDevice *ethDevice)
{
    struct EmacPriv *Emac = (struct EmacPriv *)ethDevice->priv;
    uint32_t value;
    EmacUpdateSpeed(Emac);
	EmacUpdateDuplex(Emac);

	/* enable RX/TX */
	value = OSAL_READL(Emac->iobase + EMAC_CTL_REG);
	OSAL_WRITEL(value | EMAC_CTL_RESET | EMAC_CTL_TX_EN | EMAC_CTL_RX_EN,
		Emac->iobase + EMAC_CTL_REG);

	/* enable RX/TX0/RX Hlevel interrup */
	value = OSAL_READL(Emac->iobase + EMAC_INT_CTL_REG);
	value |= (0xf << 0) | (0x01 << 8);
	OSAL_WRITEL(value, Emac->iobase + EMAC_INT_CTL_REG);


    return HDF_SUCCESS;
}

static struct EthMacOps g_macOps = {
    .MacInit = EmacCoreInit,
    .PortReset = EmacPortReset,
    .PortInit = EmacPortInit,
};

struct HdfEthMacChipDriver *BuildEmacDriver(void)
{
    struct HdfEthMacChipDriver *macChipDriver = (struct HdfEthMacChipDriver *)OsalMemCalloc(
        sizeof(struct HdfEthMacChipDriver));
    if (macChipDriver == NULL) {
        HDF_LOGE("%s fail: OsalMemCalloc fail!", __func__);
        return NULL;
    }
    macChipDriver->ethMacOps = &g_macOps;
    return macChipDriver;
}


void ReleaseEmacDriver(struct HdfEthMacChipDriver *chipDriver)
{

}

#define MDIO_CMD_MII_BUSY		BIT(0)
#define MDIO_CMD_MII_WRITE		BIT(1)

static int  mdioRead (struct EmacPriv *priv, uint8_t ucPhyAddr, uint8_t ucRegAddr, uint16_t *pusValue)
{
    uint32_t value = 0;
    int count = 10;
    /* issue the phy address and reg */
	OSAL_WRITEL((ucPhyAddr << 8) | ucRegAddr, priv->iobase + EMAC_MAC_MADR_REG);
	/* pull up the phy io line */
	OSAL_WRITEL(0x1, priv->iobase + EMAC_MAC_MCMD_REG);

    while (count--)
    {
        value = OSAL_READL(priv->iobase + EMAC_MAC_MIND_REG);
        if(0 == (value & MDIO_CMD_MII_BUSY))
            break;
        OsalMSleep(1);
    }
    /* push down the phy io line */
	OSAL_WRITEL(0x0, priv->iobase + EMAC_MAC_MCMD_REG);
	/* and read data */
	*pusValue = OSAL_READL(priv->iobase + EMAC_MAC_MRDD_REG);

    return  (HDF_SUCCESS);
}

static int mdioWrite (struct EmacPriv *priv, uint8_t ucPhyAddr, uint8_t ucRegAddr, uint16_t usValue)
{
    uint32_t value = 0x00;
    int  count = 10;

    //JY1111, reg 0xf3, Must be delayed before writing 
	if(ucRegAddr == 243)
		OsalMSleep(1);  
	/* issue the phy address and reg */
	OSAL_WRITEL((ucPhyAddr << 8) | ucRegAddr, priv->iobase + EMAC_MAC_MADR_REG);
	/* pull up the phy io line */
	OSAL_WRITEL(0x1, priv->iobase + EMAC_MAC_MCMD_REG);

    while (count--)
    {
        value = OSAL_READL(priv->iobase + EMAC_MAC_MIND_REG);
        if(0 == (value & MDIO_CMD_MII_BUSY))
            break;
        OsalMSleep(10);
    }
    /* push down the phy io line */
	OSAL_WRITEL(0x0, priv->iobase + EMAC_MAC_MCMD_REG);
	/* and write data */
	OSAL_WRITEL(usValue, priv->iobase + EMAC_MAC_MWTD_REG);
    return  (HDF_SUCCESS);
}
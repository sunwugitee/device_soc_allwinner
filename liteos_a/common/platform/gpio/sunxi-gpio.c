#define R40_GPIO_USER_SUPPORT
#include "device_resource_if.h"
#include "gpio_core.h"
#ifdef R40_GPIO_USER_SUPPORT
#include "gpio_dev.h"
#endif
#include "hdf_device_desc.h"
#include "hdf_log.h"
#include "osal_io.h"
#include "osal_irq.h"
#include "osal_mem.h"
#include "osal_spinlock.h"
#include "sunxi-gpio.h"
#include "platform_core.h"
#define HDF_LOG_TAG sunxi-gpio
#define PL061_GROUP_MAX 9
#define PL061_BIT_MAX   32
#define BANK_MEM_SIZE		0x24
#define MUX_REGS_OFFSET		0x0
#define DATA_REGS_OFFSET	0x10
#define DLEVEL_REGS_OFFSET	0x14
#define PULL_REGS_OFFSET	0x1c
#define IRQ_MODE_REG		0x200
#define IRQ_CTRL_REG		0x210
#define IRQ_STATUS_REG		0x214


struct sunxi_pinctrl_group {
    volatile unsigned char *regBase;
    unsigned int index;
    OsalSpinlock lock;
    uint32_t irqSave;
    bool irqShare;
};


struct sunxiGpioData {
    struct GpioCntlr cntlr;
    volatile unsigned char *regBase;
    OsalIRQHandle irqFunc;
    uint32_t phyBase;
    uint32_t regStep;
    uint16_t groupNum;              
    uint16_t bitNum;                 
    unsigned int irq;
    uint8_t irqShare;
    struct sunxi_pinctrl_group *groups;
};

static struct sunxiGpioData g_pl061 = {
    .groups = NULL,
    .groupNum = PL061_GROUP_MAX,
    .bitNum = PL061_BIT_MAX,        
};


/*计算bank的值偏移*/
static inline uint32_t sunxi_mux_reg(uint16_t pin)
{
	uint8_t bank = pin / PINS_PER_BANK;
	uint32_t offset = bank * BANK_MEM_SIZE;
	offset += MUX_REGS_OFFSET;
	offset += pin % PINS_PER_BANK / MUX_PINS_PER_REG * 0x04;
	return round_down(offset, 4);
}
/*计算具体的pin偏移几位*/
static inline uint32_t sunxi_mux_offset(uint16_t pin)
{
	uint32_t pin_num = pin % MUX_PINS_PER_REG;
	return pin_num * MUX_PINS_BITS;
}
/*计算data偏移的值*/
static inline uint32_t sunxi_data_reg(uint16_t pin)
{
	uint8_t bank = pin / PINS_PER_BANK;
	uint32_t offset = bank * BANK_MEM_SIZE;
	offset += DATA_REGS_OFFSET;
    offset += pin % PINS_PER_BANK / DATA_PINS_PER_REG * 0x04;
	return round_down(offset, 4);
}
/*取出具体的引脚寄存器的偏移几位*/
static inline uint32_t sunxi_data_offset(uint16_t pin)
{
	uint32_t pin_num = pin % 32;
	return pin_num ;
}

static int32_t R40GpioSetDir(struct GpioCntlr *cntlr,
			uint16_t pin,
			uint16_t dir)
{
    uint32_t val, mask;
    struct sunxiGpioData *group = (struct sunxiGpioData *)cntlr;
    if (OsalSpinLockIrqSave(&group->groups->lock, &group->groups->irqSave) != HDF_SUCCESS) {
        return HDF_ERR_DEVICE_BUSY;
    }
    val = OSAL_READL(group->regBase + sunxi_mux_reg(pin));//读取寄存器的值
    mask = MUX_PINS_MASK << sunxi_mux_offset(pin);//偏移得到掩码
    OSAL_WRITEL((val & ~mask) | dir << sunxi_mux_offset(pin),
	group->regBase + sunxi_mux_reg(pin));   
    (void)OsalSpinUnlockIrqRestore(&group->groups->lock, &group->groups->irqSave);
	return HDF_SUCCESS;
}
  

static int32_t R40GpioGetDir(struct GpioCntlr *cntlr,
			uint16_t pin,uint16_t *val)
{   uint32_t valcur, mask;
    struct sunxiGpioData *group = (struct sunxiGpioData *)cntlr;
	valcur = OSAL_READL(group->regBase + sunxi_mux_reg(pin));
    mask = MUX_PINS_MASK << sunxi_mux_offset(pin);//偏移得到掩码
    *val=(valcur & mask)>>sunxi_mux_offset(pin);
	return HDF_SUCCESS;
}

static int32_t R40GpioRead(struct GpioCntlr *cntlr, uint16_t pin, uint16_t *val)
{   uint32_t mask;
    unsigned int valCur;
    struct sunxiGpioData *group = (struct sunxiGpioData *)cntlr;
    valCur = OSAL_READL(group->regBase +sunxi_data_reg(pin));  
    mask = DATA_PINS_MASK << sunxi_data_offset(pin);//偏移得到掩码
    valCur=(valCur & mask)>>sunxi_data_offset(pin);
    if (valCur) {
        *val = GPIO_VAL_HIGH;
    } else {
        *val = GPIO_VAL_LOW;
    }
    return HDF_SUCCESS;
}

static int32_t R40GpioWrite(struct GpioCntlr *cntlr, uint16_t pin, uint16_t val)
{  
    uint32_t mask;
    unsigned int valCur;
    struct sunxiGpioData *group = (struct sunxiGpioData *)cntlr;
    if (OsalSpinLockIrqSave(&group->groups->lock, &group->groups->irqSave) != HDF_SUCCESS) {
        return HDF_ERR_DEVICE_BUSY;
    }
    valCur = OSAL_READL(group->regBase + sunxi_data_reg(pin));
    mask = DATA_PINS_MASK << sunxi_data_offset(pin);//偏移得到掩码
    if (val == GPIO_VAL_LOW) {
        OSAL_WRITEL((valCur & ~mask) | val << sunxi_data_offset(pin),group->regBase + sunxi_data_reg(pin));
    } else {
        OSAL_WRITEL((valCur & ~mask) | val << sunxi_data_offset(pin),group->regBase + sunxi_data_reg(pin));
    }
    (void)OsalSpinUnlockIrqRestore(&group->groups->lock, &group->groups->irqSave);
    return HDF_SUCCESS;
}

static int16_t GetEXITnumbypin(uint16_t pin)
{   
    uint16_t exitnum;
    if(pin< PI_BASE) exitnum=pin%32;
    else exitnum=(pin+12)%32;
    return exitnum;
}

static void R40GpioSetIrqEnableUnsafe(struct sunxiGpioData *group, uint16_t bitNum, int flag)
{
    unsigned int val;
    volatile unsigned char *addr = NULL;
    addr=group->regBase+IRQ_CTRL_REG;
    val = OSAL_READL(addr);
    bitNum = GetEXITnumbypin(bitNum);
    if (flag == 1) {
        val |= (1 << bitNum);
    } else {
        val &= ~(1 << bitNum);
    }
    OSAL_WRITEL(val, addr);
}

static void R40GpioClearIrqUnsafe(struct sunxiGpioData *group, uint16_t bitNum)
{
    unsigned int val;

    val = OSAL_READL(group->regBase+IRQ_STATUS_REG);
    val |= 1 << (bitNum%32);
    OSAL_WRITEL(val, group->regBase+IRQ_STATUS_REG);
}

static int32_t R40GpioUnsetIrq(struct GpioCntlr *cntlr, uint16_t local)
{
    unsigned int bitNum = local;
     struct sunxiGpioData *group = (struct sunxiGpioData *)cntlr;

    R40GpioSetIrqEnableUnsafe(group, bitNum, 0); // disable irq when unset
    R40GpioClearIrqUnsafe(group, bitNum);        // clear irq when unset

    return HDF_SUCCESS;
}



static int32_t R40GpioEnableIrq(struct GpioCntlr *cntlr, uint16_t local)
{
    unsigned int bitNum = local;
    struct sunxiGpioData *group = (struct sunxiGpioData *)cntlr;
    R40GpioSetIrqEnableUnsafe(group, bitNum, 1);
    return HDF_SUCCESS;
}

static int32_t R40GpioDisableIrq(struct GpioCntlr *cntlr, uint16_t local)
{
    unsigned int bitNum = local;
    struct sunxiGpioData *group = (struct sunxiGpioData *)cntlr;
    R40GpioSetIrqEnableUnsafe(group, bitNum, 0);
    return HDF_SUCCESS;
}


static void R40GpioSetIrqTypeUnsafe(struct sunxiGpioData *group, uint16_t bitNum, uint16_t mode)
{
    unsigned int offerset,mask;
    uint16_t exitnum,val;
    exitnum=GetEXITnumbypin(bitNum);
    offerset= (exitnum/8)*0x4;
    mask=0xf<<((exitnum%8)*4);
    val=OSAL_READL(group->regBase+IRQ_MODE_REG+offerset);
    OSAL_WRITEL((val&~mask)|mode<<((exitnum%8)*4), group->regBase+IRQ_MODE_REG+offerset);

}

static uint32_t IrqHandleShare(uint32_t irq, void *data)
{
    (void)irq;
    (void)data;
    return HDF_SUCCESS;
}

static uint32_t IrqHandleNoShare(uint32_t irq, void *data)
{
    unsigned int i;
    unsigned int val;
     struct sunxiGpioData *group = (struct sunxiGpioData *)data;

    if (data == NULL) {
        PLAT_LOGW("%s: data is NULL!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    val = OSAL_READL(group->regBase+IRQ_STATUS_REG);
    OSAL_WRITEL(val,group->regBase+IRQ_STATUS_REG);//clear status
    for (i = 0; i < 32 && val != 0; i++, val >>= 1) {
        if ((val & 0x1) != 0) {
            GpioCntlrIrqCallback(&group->cntlr, i);
            
        }
    }
    return HDF_SUCCESS;
}
static int32_t R40GpioRegisterGroupIrqUnsafe(struct sunxiGpioData *group,uint16_t bitNum)
{
    int ret;
    if (group->irqShare == 1) {
        ret = OsalRegisterIrq(group->irq, 0, IrqHandleShare, "GPIO", NULL);//注册gpio的中断号
        if (ret != 0) {
            PLAT_LOGE("%s: share irq:%u reg fail:%d!", __func__,group->irq, ret);
            return HDF_FAILURE;
        }
        group->irqFunc = IrqHandleShare;
    } else {
        ret = OsalRegisterIrq(group->irq, 0, IrqHandleNoShare, "GPIO", group);
        if (ret != 0) {
            (void)OsalUnregisterIrq(group->irq, group);
            ret = OsalRegisterIrq(group->irq, 0, IrqHandleNoShare, "GPIO", group);
        }
        if (ret != 0) {
            PLAT_LOGE("%s: noshare irq:%u reg fail:%d!", __func__, group->irq, ret);
            return HDF_FAILURE;
        }
        ret = OsalEnableIrq(group->irq);
        if (ret != 0) {
            PLAT_LOGE("%s: noshare irq:%u enable fail:%d!", __func__, group->irq, ret);
            (void)OsalUnregisterIrq(group->irq, group);
            return HDF_FAILURE;
        }
        group->irqFunc = IrqHandleNoShare;
    }

    return HDF_SUCCESS;
}


static int32_t R40GpioSetIrq(struct GpioCntlr *cntlr, uint16_t local, uint16_t mode)
{
    int32_t ret;
    unsigned int bitNum = local;
    struct sunxiGpioData *group = (struct sunxiGpioData *)cntlr;

    if (OsalSpinLockIrqSave(&group->groups->lock, &group->groups->irqSave) != HDF_SUCCESS) {
        return HDF_ERR_DEVICE_BUSY;
    }
    R40GpioSetIrqTypeUnsafe(group, bitNum, mode);
    R40GpioSetIrqEnableUnsafe(group, bitNum, 0); // disable irq on set
    R40GpioClearIrqUnsafe(group, bitNum);        // clear irq on set
    if (group->irqFunc != NULL) {
        (void)OsalSpinUnlockIrqRestore(&group->groups->lock, &group->groups->irqSave);
        PLAT_LOGI("%s: group irq(%u) already registered!", __func__, group->irq);
        return HDF_SUCCESS;
    }
    ret = R40GpioRegisterGroupIrqUnsafe(group,bitNum);
    (void)OsalSpinUnlockIrqRestore(&group->groups->lock, &group->groups->irqSave);
    PLAT_LOGI("%s: group irq :%d", __func__, ret);
    return ret;
}


static struct GpioMethod g_method = {
    .request = NULL,
    .release = NULL,
    .write = R40GpioWrite,
    .read = R40GpioRead,
    .setDir = R40GpioSetDir,
    .getDir = R40GpioGetDir,
    .toIrq = NULL,
    .setIrq = R40GpioSetIrq,
    .unsetIrq = R40GpioUnsetIrq,
    .enableIrq = R40GpioEnableIrq,
    .disableIrq = R40GpioDisableIrq,
};
static void ReleaseGpioCntlrMem(struct sunxiGpioData *cntlr)
{
    if (cntlr == NULL) {
        return;
    }
    if (cntlr->groups != NULL) {
        for (uint16_t i = 0; i < cntlr->groupNum; i++) {
            (void)OsalSpinDestroy(&cntlr->groups[i].lock);
        }
        OsalMemFree(cntlr->groups);
        cntlr->groups = NULL;
    }
}
static int32_t GpioInitGroups(struct sunxiGpioData *pl061)
{
    uint16_t i;
    struct sunxi_pinctrl_group *groups = NULL;
    if (pl061 == NULL) {
        return HDF_ERR_INVALID_PARAM;
    }

    groups = (struct sunxi_pinctrl_group *)OsalMemCalloc(sizeof(*groups) * pl061->groupNum);
    if (groups == NULL) {
        return HDF_ERR_MALLOC_FAIL;
    }
    pl061->groups = groups;
    for (i = 0; i < pl061->groupNum; i++) {
        groups[i].index = i;
        groups[i].regBase = pl061->regBase;
        groups[i].irqShare = pl061->irqShare;   
       if (OsalSpinInit(&groups[i].lock) != HDF_SUCCESS) {
            for (; i > 0; i--) {
                (void)OsalSpinDestroy(&groups[i - 1].lock);
            }
            OsalMemFree(groups);
            return HDF_FAILURE;
        }
    }
    return HDF_SUCCESS;
}

static int32_t GpioReadDrs(struct sunxiGpioData *pl061, const struct DeviceResourceNode *node)
{
    int32_t ret;
    struct DeviceResourceIface *drsOps = NULL;

    drsOps = DeviceResourceGetIfaceInstance(HDF_CONFIG_SOURCE);
    if (drsOps == NULL || drsOps->GetUint32 == NULL) {
        PLAT_LOGE("%s: invalid drs ops fail!", __func__);
        return HDF_FAILURE;
    }

    ret = drsOps->GetUint32(node, "reg_pbase", &pl061->phyBase, 0);
    if (ret != HDF_SUCCESS) {
        PLAT_LOGE("%s: read regBase fail!", __func__);
        return ret;
    }

    ret = drsOps->GetUint32(node, "regStep", &pl061->regStep, 0);
    if (ret != HDF_SUCCESS) {
        PLAT_LOGE("%s: read regStep fail!", __func__);
        return ret;
    }

    ret = drsOps->GetUint16(node, "groupNum", &pl061->groupNum, 0);
    if (ret != HDF_SUCCESS) {
        PLAT_LOGE("%s: read groupNum fail!", __func__);
        return ret;
    }
    ret = drsOps->GetUint16(node, "bitNum", &pl061->bitNum, 0);
    if (ret != HDF_SUCCESS) {
        PLAT_LOGE("%s: read bitNum fail!", __func__);
        return ret;
    }

    ret = drsOps->GetUint32(node, "irq", &pl061->irq, 0);
    if (ret != HDF_SUCCESS) {
        PLAT_LOGE("%s: read irq fail!", __func__);
        return ret;
    }

    ret = drsOps->GetUint8(node, "irqShare", &pl061->irqShare, 0);
    if (ret != HDF_SUCCESS) {
        PLAT_LOGE("%s: read irqShare fail!", __func__);
        return ret;
    }

    return HDF_SUCCESS;
}
static int32_t GpioBind(struct HdfDeviceObject *device)
{
    (void)device;
    return HDF_SUCCESS;
}

static int32_t GpioInit(struct HdfDeviceObject *device)
{
    int32_t ret;
    struct sunxiGpioData *pl061 = &g_pl061;
    if (device == NULL || device->property == NULL) {
        PLAT_LOGE("%s: device or property null!", __func__);
        return HDF_ERR_INVALID_OBJECT;
    }
    ret = GpioReadDrs(pl061, device->property);
    if (ret != HDF_SUCCESS) {
        PLAT_LOGE("%s: read drs fail:%d", __func__, ret);
        return ret;
    }
   if (pl061->groupNum > PL061_GROUP_MAX || pl061->groupNum <= 0 ||
        pl061->bitNum > PL061_BIT_MAX || pl061->bitNum <= 0) {
        PLAT_LOGE("%s: err bitnum:%hu", __func__, pl061->bitNum);
        return HDF_ERR_INVALID_PARAM;
    }
    
//物理地址映射
    pl061->regBase = OsalIoRemap(pl061->phyBase, 0x1000);
    if (pl061->regBase == NULL) {
        PLAT_LOGE("%s: err remap phy:0x%x", __func__, pl061->phyBase);
        return HDF_ERR_IO;
    }
    ret = GpioInitGroups(pl061);
    if (ret != HDF_SUCCESS) {
       PLAT_LOGE("%s: err init groups:%d", __func__, ret);
        OsalIoUnmap((void *)pl061->regBase);
        pl061->regBase = NULL;
        return ret;
    }
    pl061->cntlr.count = pl061->groupNum * pl061->bitNum;
    pl061->cntlr.priv = (void *)device->property;
    pl061->cntlr.device.hdfDev = device;
    pl061->cntlr.ops = &g_method;
    ret = GpioCntlrAdd(&pl061->cntlr);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: err add controller: %d", __func__, ret);
        return ret;
    }
#ifdef  R40_GPIO_USER_SUPPORT
    if (GpioAddVfs(pl061->bitNum) != HDF_SUCCESS) {
        PLAT_LOGE("%s: add vfs fail!", __func__);
    }
#endif
    dprintf("%s: dev service:%s init success!\n", __func__, HdfDeviceGetServiceName(device));
    return HDF_SUCCESS;
}

static void GpioRelease(struct HdfDeviceObject *device)
{   struct GpioCntlr *gpioCntlr = NULL;
    struct sunxiGpioData *pl061 = NULL;

    PLAT_LOGI("%s: enter", __func__);
    if (device == NULL) {
        PLAT_LOGE("%s: device is null!", __func__);
        return;
    }

    gpioCntlr = GpioCntlrFromHdfDev(device);
    if (gpioCntlr == NULL) {
        HDF_LOGE("%s: no service bound!", __func__);
        return;
    }
    GpioCntlrRemove(gpioCntlr);

    pl061 = (struct sunxiGpioData *)gpioCntlr;
    ReleaseGpioCntlrMem(pl061);
    OsalIoUnmap((void *)pl061->regBase);
    pl061->regBase = NULL;
}

struct HdfDriverEntry g_gpioDriverEntry = {
    .moduleVersion = 1,
    .Bind = GpioBind,
    .Init = GpioInit,
    .Release = GpioRelease,
    .moduleName = "sunxi_gpio_driver",
};
HDF_INIT(g_gpioDriverEntry);
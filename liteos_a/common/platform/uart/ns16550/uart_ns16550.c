#include "los_event.h"
#include "device_resource_if.h"
#include "hdf_base.h"
#include "hdf_log.h"
#include "osal_io.h"
#include "osal_mem.h"
#include "osal_time.h"

#include "uart_core.h"
#include "uart_dev.h"
#include "uart_if.h"

#include "uart_ns16550.h"

#define HDF_LOG_TAG uart_ns16550

#undef HDF_LOGE
#define HDF_LOGE dprintf

extern SPIN_LOCK_S g_uartOutputSpin;

static uint8_t Ns16550MmioRead(struct Ns16550 *port, uint32_t reg)
{
    return OSAL_READB((void*)((unsigned long)port->mappedReg + reg * 4));
}

static void Ns16550MmioWrite(struct Ns16550 *port, uint32_t reg, uint8_t data)
{
    OSAL_WRITEB(data, (void*)((unsigned long)port->mappedReg + reg * 4));
}

struct Ns16550RegOps g_ns16550MmioOps = {
    .read = Ns16550MmioRead,
    .write = Ns16550MmioWrite,
};

static uint32_t RecvQueueSize(struct RecvQueue *q) {
    return (q->tail + RCV_QUEUE_SIZE - q->head) % RCV_QUEUE_SIZE;
}

static uint32_t RecvQueueRead(struct RecvQueue *q, uint8_t *data, uint32_t size)
{
    uint32_t h = q->head, t = q->tail;
    uint32_t qSize = RecvQueueSize(q);
    uint32_t lowerSize = 0;
    if (h == t) {
        return 0;
    }
    if (size > RCV_QUEUE_SIZE) {
        size = RCV_QUEUE_SIZE;
    }
    size = size > qSize ? qSize : size;

    if (h < t) {
        memcpy_s(data, size, (void*)(q->data + h), size);
        q->head = (h + size) % RCV_QUEUE_SIZE;
    } else {
        lowerSize = RCV_QUEUE_SIZE - h;
        if (lowerSize >= size) {
            memcpy_s(data, size, (void*)(q->data + h), size);
            q->head = (h + size) % RCV_QUEUE_SIZE;
        } else {
            memcpy_s(data, lowerSize,
                    (void*)(q->data + h), lowerSize);
            memcpy_s(data + lowerSize, size - lowerSize,
                    (void*)(q->data), size - lowerSize);
            q->head = (h + size - lowerSize) % RCV_QUEUE_SIZE;
        }
    }

    return size;
}

static int32_t RecvQueuePush(struct RecvQueue *q, uint8_t data)
{
    uint32_t nextT = 0;
    nextT = (q->tail + 1) % RCV_QUEUE_SIZE;
    if (nextT == q->head) {
        return -1;
    }
    q->data[q->tail] = data;
    q->tail = nextT;
    return 0;
}

static int32_t Ns16550RecvQueueEmpty(struct Ns16550 *port)
{
    return RecvQueueSize(port->rxQueue) == 0;
}

static int32_t Ns16550RxEmpty(struct Ns16550 *port)
{
    return (port->ops->read(port, UART_LSR) & LSR_DR) ? 0 : 1;
}

static int32_t Ns16550TxEmpty(struct Ns16550 *port)
{

    return (port->ops->read(port, UART_LSR) & LSR_THRE) ? 1 : 0;
}

static void Ns16550Putc(struct Ns16550 *port, uint8_t data)
{
    if ((char)data == '\n') {
        Ns16550Putc(port, '\r');
    }
    while(!Ns16550TxEmpty(port)) {}
    (void)port->ops->write(port, UART_THR, data);
}

static int32_t Ns16550Read(struct Ns16550 *port, uint8_t *data, uint32_t size)
{

    uint32_t count = 0;
    count = RecvQueueRead(port->rxQueue, data, size);
    return count;
}

static int32_t Ns16550Write(struct Ns16550 *port, uint8_t *data, uint32_t size)
{
    uint32_t intSave;
    uint32_t count = 0;
    bool isLocked = port->isDebug;

    OsalSpinLock(&(port->lock));
    if (isLocked) {
        LOS_SpinLockSave(&g_uartOutputSpin, &intSave);
    }

    for (count = 0; count < size; count++) {
        Ns16550Putc(port, data[count]);
    }

    if (isLocked) {
        LOS_SpinUnlockRestore(&g_uartOutputSpin, intSave);
    }
    OsalSpinUnlock(&(port->lock));
    return count;
}

static void Ns16550ConfigFCR(struct Ns16550 *port)
{
    struct UartAttribute *attr = &port->attr;
    uint8_t fcr = 0;

    if (attr->fifoRxEn == UART_ATTR_RX_FIFO_EN) {
        fcr |= (FCR_EN | FCR_CLRR | FCR_CLRT | (0x40));
    }
    port->ops->write(port, UART_FCR, fcr);
}

static void Ns16550ConfigMCR(struct Ns16550 *port)
{
    uint8_t mcr = 0;
    port->ops->write(port, UART_MCR, mcr);
}

static void Ns16550ConfigLCR(struct Ns16550 *port)
{
    struct UartAttribute *attr = &port->attr;
    uint8_t lcr = 0;
    /* set databits to LCR */
    switch (attr->dataBits) {
        case UART_ATTR_DATABIT_5:
            lcr |= LCR_WL_5;
            break;
        case UART_ATTR_DATABIT_6:
            lcr |= LCR_WL_6;
            break;
        case UART_ATTR_DATABIT_7:
            lcr |= LCR_WL_7;
            break;
        case UART_ATTR_DATABIT_8:
        default:
            lcr |= LCR_WL_8;
    }

    /* set stopbits to LCR */
    switch (attr->stopBits) {
        case UART_ATTR_STOPBIT_2:
            lcr |= LCR_STB;
        default:
            break;
    }

    /* set parity */
    switch (attr->parity) {
        case UART_ATTR_PARITY_EVEN:
            lcr |= (LCR_PEN | LCR_EVP);
            break;
        case UART_ATTR_PARITY_ODD:
            lcr |= LCR_PEN;
            break;
        case UART_ATTR_PARITY_NONE:
        default:
            break;
    }
    /* set uart_lcr*/
    port->ops->write(port, UART_HALT, BIT(1));
    port->ops->write(port, UART_LCR, lcr);
    port->ops->write(port, UART_HALT, BIT(2)); 
}

static int32_t Ns16550Config(struct Ns16550 *port)
{
    dprintf("%s\n", __func__);
    OsalSpinLock(&(port->lock));

    while(!(port->ops->read(port, UART_LSR) & LSR_TEMT)) {}
    if (port->num)
    Ns16550ConfigLCR(port);
    Ns16550ConfigFCR(port);
    Ns16550ConfigMCR(port);
    OsalSpinUnlock(&(port->lock));
    return HDF_SUCCESS;
}

static int32_t Ns16550ConfigBaud(struct Ns16550 *port)
{
    uint32_t div = 0, clk = 0, baud = 0;
    uint8_t lcr,halt;
    clk = port->clkFreq;
    baud = port->baudrate;
    div = (clk + (baud * 8)) / (baud * 16);
    
    // dprintf("%s -div %d \n",__func__,div);
    OsalSpinLock(&(port->lock));

    while(!(port->ops->read(port, UART_LSR) & LSR_TEMT)) {}
    //set chcfg_busy
    halt = port->ops->read(port, UART_HALT);
    halt  |= (HALT_CHCFG_AT_BUSY);
    port->ops->write(port, UART_HALT, halt);
    //set DLAB ,change to set baud
    lcr = port->ops->read(port, UART_LCR);
    lcr |= LCR_DLAB;
    port->ops->write(port,UART_LCR,lcr);
    //set baud 115200 0xd
    // port->ops->write(port,UART_DLL,div);
    port->ops->write(port, UART_DLL, (div & 0xff));
    port->ops->write(port, UART_DLH, (div >> 8) & 0xff);
    //chage modem
    lcr &= ~(LCR_DLAB);
    port->ops->write(port, UART_LCR, lcr);
    //update config
    halt  |= (HALT_CHANGE_UPDATE);
    port->ops->write(port, UART_HALT, halt);

    OsalSpinUnlock(&(port->lock));
    return HDF_SUCCESS;
}

static uint32_t Ns16550Irq(uint32_t irq, void *data)
{
    struct Ns16550 *port = (struct Ns16550 *)data;
    uint8_t iir = 0, tmp;
    if (port == NULL) {
        return HDF_FAILURE;
    }
    iir = port->ops->read(port, UART_IIR);
    if ((iir & IIR_ID_MASK) == IIR_ID_RDI) {
        while(!Ns16550RxEmpty(port)) {
            tmp = port->ops->read(port, UART_RBR);
            (void)RecvQueuePush(port->rxQueue, tmp);
        }

        LOS_EventWrite(&(port->wait.stEvent), 0x1);
        notify_poll(&(port->wait));
    }
    UNUSED(irq);
    return HDF_SUCCESS;
}

static void Ns16550ConfigIrq(struct Ns16550 *port, bool enable)
{
    uint8_t ier = 0;
    if (enable) {
        ier |= IER_RDI;
    }
    port->ops->write(port, UART_IER, ier);
}

static int32_t Ns16550Open(struct Ns16550 *port)
{
 
    int32_t ret;
    ret = Ns16550Config(port);
    if (ret != HDF_SUCCESS) {
        return ret;
    }
    ret = Ns16550ConfigBaud(port);
    if (ret != HDF_SUCCESS) {
        return ret;
    }
    Ns16550ConfigIrq(port, 1);
    return HDF_SUCCESS;
}

static void Ns16550Close(struct Ns16550 *port)
{
    Ns16550ConfigIrq(port, 0);
}

static int32_t Ns16550HostRead(struct UartHost *host, uint8_t *data, uint32_t size)
{
    int32_t ret;
    struct Ns16550 *port = NULL;

    if (host == NULL || host->priv == NULL) {
        HDF_LOGE("%s: host is NULL", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    port = (struct Ns16550*)host->priv;

    if (port->state != UART_STATE_USABLE) {
        return HDF_FAILURE;
    }

    if ((port->flags & UART_FLG_RD_BLOCK) && (Ns16550RecvQueueEmpty(port))) {
        (void)LOS_EventRead(&port->wait.stEvent, 0x1, LOS_WAITMODE_OR, LOS_WAIT_FOREVER);
    }
    ret = Ns16550Read(port, data, size);
    if ((port->flags & UART_FLG_RD_BLOCK) && (Ns16550RecvQueueEmpty(port))) {
        (void)LOS_EventClear(&port->wait.stEvent, ~(0x1));
    }
    return ret;
}
static int32_t Ns16550HostWrite(struct UartHost *host, uint8_t *data, uint32_t size)
{
    struct Ns16550 *port = NULL;

    if (host == NULL || host->priv == NULL) {
        HDF_LOGE("%s: host is NULL", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    port = (struct Ns16550*)host->priv;

    if (port->state != UART_STATE_USABLE) {
        return HDF_FAILURE;
    }

    return Ns16550Write(port, data, size);
}

static int32_t Ns16550HostGetBaud(struct UartHost *host, uint32_t *baudRate)
{
    struct Ns16550 *port = NULL;

    dprintf("%s\n", __func__);
    if (host == NULL || host->priv == NULL || baudRate == NULL) {
        HDF_LOGE("%s: invalid parameter", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    port = (struct Ns16550*)host->priv;
    if (port->state != UART_STATE_USABLE) {
        return HDF_FAILURE;
    }

    *baudRate = port->baudrate;
    return HDF_SUCCESS;
}

static int32_t Ns16550HostSetBaud(struct UartHost *host, uint32_t baudRate)
{
    int32_t ret;
    struct Ns16550 *port = NULL;

    dprintf("%s\n", __func__);
    if (host == NULL || host->priv == NULL) {
        HDF_LOGE("%s: invalid parameter", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    port = (struct Ns16550*)host->priv;

    if (port->state != UART_STATE_USABLE) {
        return HDF_FAILURE;
    }

    if ((baudRate > 0) && (baudRate <= UART_MAX_BAUDRATE)) {
        port->baudrate = baudRate;
        ret = Ns16550ConfigBaud(port);
        if (ret != 0) {
            HDF_LOGE("%s: error configuring baudrate %d", __func__, baudRate);
            return HDF_FAILURE;
        }
    } else {
        HDF_LOGE("%s: invalid baudrate, which is:%d", __func__, baudRate);
        return HDF_FAILURE;
    }

    return HDF_SUCCESS;
}

static int32_t Ns16550HostGetAttribute(struct UartHost *host, struct UartAttribute *attribute)
{
    struct Ns16550 *port = NULL;

    dprintf("%s\n", __func__);
    if (host == NULL || host->priv == NULL || attribute == NULL) {
        HDF_LOGE("%s: invalid parameter", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    port = (struct Ns16550*)host->priv;
    if (port->state != UART_STATE_USABLE) {
        return HDF_FAILURE;
    }

    attribute->cts = port->attr.cts;
    attribute->dataBits = port->attr.dataBits;
    attribute->fifoRxEn = port->attr.fifoRxEn;
    attribute->fifoTxEn = port->attr.fifoTxEn;
    attribute->parity = port->attr.parity;
    attribute->rts = port->attr.rts;
    attribute->stopBits = port->attr.stopBits;
    return HDF_SUCCESS;
}

static int32_t Ns16550HostSetAttribute(struct UartHost *host, struct UartAttribute *attribute)
{
    struct Ns16550 *port = NULL;

    dprintf("%s\n", __func__);
    if (host == NULL || host->priv == NULL || attribute == NULL) {
        HDF_LOGE("%s: invalid parameter", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    port = (struct Ns16550*)host->priv;

    if (port->state != UART_STATE_USABLE) {
        return HDF_FAILURE;
    }

    /* check unsupported attributes */
    if (attribute->parity == UART_ATTR_PARITY_MARK ||
        attribute->parity == UART_ATTR_PARITY_SPACE) {
        HDF_LOGE("%s: unspported parity setting", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    if (attribute->fifoRxEn != attribute->fifoTxEn) {
        HDF_LOGE("%s: unsppoted fifo setting", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    /* cts rts not supported yet */
    if (attribute->cts == UART_ATTR_CTS_EN ||
        attribute->rts == UART_ATTR_RTS_EN) {
        HDF_LOGE("%s: unsppoted cts or rts setting", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    port->attr.cts = attribute->cts;
    port->attr.dataBits = attribute->dataBits;
    port->attr.fifoRxEn = attribute->fifoRxEn;
    port->attr.fifoTxEn = attribute->fifoTxEn;
    port->attr.parity = attribute->parity;
    port->attr.rts = attribute->rts;
    port->attr.stopBits = attribute->stopBits;

    if (Ns16550Config(port) != 0) {
        HDF_LOGE("%s: failed to config port", __func__);
        return HDF_FAILURE;
    }
    return HDF_SUCCESS;
}

static int32_t Ns16550HostSetTransMode(struct UartHost *host, enum UartTransMode mode)
{
    struct Ns16550 *port = NULL;

    dprintf("%s\n", __func__);
    if (host == NULL || host->priv == NULL) {
        HDF_LOGE("%s: invalid parameter", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    port = (struct Ns16550*)host->priv;
    if (port->state != UART_STATE_USABLE) {
        HDF_LOGE("%s: port not usable", __func__);
        return HDF_FAILURE;
    }

    if (mode == UART_MODE_RD_BLOCK) {
        port->flags |= UART_FLG_RD_BLOCK;
    } else if (mode == UART_MODE_RD_NONBLOCK) {
        port->flags &= ~UART_FLG_RD_BLOCK;
        (void)LOS_EventWrite(&port->wait.stEvent, 0x1);
    }

    return HDF_SUCCESS;
}

static void SetBitFunc(uint32_t phyaddr,uint8_t bit)
{
    uint32_t val,addr;
    addr = IO_DEVICE_ADDR(phyaddr);
    val = OSAL_READL(addr);
    //bit位清零操作
    val &= ~BIT(bit);
    //置位操作
    val |=  BIT(bit);
    OSAL_WRITEL(val,addr);

}

static void SetPinMode(uint32_t phyaddr,uint8_t pinbit,uint8_t mode)
{
    uint32_t val,addr,mask;
    addr = IO_DEVICE_ADDR(phyaddr);
    val = OSAL_READL(addr);
    mask = 0xf<<(4*pinbit);
    val &= ~mask;
    val |= mode<<(4*pinbit);
    OSAL_WRITEL(val,addr);

}

static void Ns16550SystemPortInit(struct Ns16550 *port)
{   
    dprintf("%s\n ", __func__);
    //set ccu
    SetBitFunc(CCU_UART_GATUNG_PHYBASE,port->num+16);
    SetBitFunc(CCU_UART_SOFT_RET_PHYBASR,port->num+16);
    //set pin
    switch (port->num)
    {
    case 0: 
        break; 
    case 3:
        SetPinMode(0x01C208D8,6,4);//PG6
        SetPinMode(0x01C208D8,7,4);//PG7
        break;
    case 4:
        SetPinMode(0x01C208Dc,2,4);//PG10
        SetPinMode(0x01C208Dc,3,4);//PG11   
        break;
    case 7:
        SetPinMode(0x01C20928,4,3);//PI20
        SetPinMode(0x01C20928,5,3);//PI21
        break;
    default:
        break;
    }
}

static int32_t Ns16550HostInit(struct UartHost *host)
{
    int32_t ret = 0;
    struct Ns16550 *port = NULL;
    struct wait_queue_head *wait = NULL;
    //debug
    // dprintf("%s\n ", __func__);
    if (host == NULL || host->priv == NULL) {
        HDF_LOGE("%s: invalid parameter", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    port = (struct Ns16550 *)host->priv;
    wait = &port->wait;
    Ns16550SystemPortInit(port);
    // Openport
    OsalSpinLock(&(port->lock));
    if (port->state == UART_STATE_CLOSED) {
        port->state = UART_STATE_OPENING;
        (void)LOS_EventInit(&wait->stEvent);
        spin_lock_init(&wait->lock);
        LOS_ListInit(&wait->poll_queue);
        port->rxQueue = (struct RecvQueue*)OsalMemCalloc(sizeof(struct RecvQueue));
        if (port->rxQueue == NULL) {
            HDF_LOGE("%s: failed to alloc receive buffer", __func__);
            ret = HDF_ERR_MALLOC_FAIL;
            goto out;
        }
        port->rxQueue->head = 0;
        port->rxQueue->tail = 0;
        // dprintf("%s register port-irq %x\n", __func__,port->irq);
        ret = OsalRegisterIrq(port->irq, 0, Ns16550Irq, "uart_ns16550", port);
        if (ret != 0) {
            HDF_LOGE("%s: failed to register irq %d", __func__, port->irq);
            ret = HDF_FAILURE;
            goto free_queue;
        }

        ret = Ns16550Open(port);
        if (ret != 0) {
            HDF_LOGE("%s: failed to open device %d", __func__, port->num);
            ret = HDF_FAILURE;
            goto free_irq;
        }
    }
    port->state = UART_STATE_USABLE;
    port->openCount++;
    ret = HDF_SUCCESS;
    goto out;

free_irq:
    OsalUnregisterIrq(port->irq, port);
free_queue:
    (void)OsalMemFree(port->rxQueue);
    port->rxQueue = NULL;
out:
    if (ret != HDF_SUCCESS) {
        port->state = UART_STATE_CLOSED;
    }
    OsalSpinUnlock(&(port->lock));
    return ret;
}

static int32_t Ns16550HostDeinit(struct UartHost *host)
{
    int32_t ret;
    struct wait_queue_head *wait = NULL;
    struct Ns16550 *port = NULL;

    // dprintf("%s: open", __func__);
    if (host == NULL || host->priv == NULL) {
        HDF_LOGE("%s: invalid parameter", __func__);
        return HDF_ERR_INVALID_PARAM;
    }

    port = (struct Ns16550 *)host->priv;
    OsalSpinLock(&(port->lock));

    if (--port->openCount > 0) {
        ret = HDF_SUCCESS;
        goto out;
    }
    wait = &port->wait;
    LOS_ListDelete(&wait->poll_queue);
    LOS_EventDestroy(&wait->stEvent);
    Ns16550Close(port);
    OsalUnregisterIrq(port->irq, port);
    if (port->rxQueue != NULL) {
        (void)OsalMemFree(port->rxQueue);
        port->rxQueue = NULL;
    }
    port->state = UART_STATE_CLOSED;
    ret = HDF_SUCCESS;
out:
    OsalSpinUnlock(&(port->lock));
    return ret;
}

static int32_t Ns16550HostPollEvent(struct UartHost *host, void *filep, void *table)
{
    struct Ns16550 *port = NULL;

    if (host == NULL || host->priv == NULL) {
        HDF_LOGE("%s: host is NULL", __func__);
        return HDF_FAILURE;
    }
    port = (struct Ns16550 *)host->priv;
    if (port->state != UART_STATE_USABLE) {
        return -EFAULT;
    }

    poll_wait((struct file *)filep, &port->wait, (poll_table *)table);

    if (port->rxQueue->head != port->rxQueue->tail) {
        return POLLIN | POLLRDNORM;
    }
    return 0;
}


struct UartHostMethod g_uartHostMethod = {
    .Init = Ns16550HostInit,
    .Deinit = Ns16550HostDeinit,
    .Read = Ns16550HostRead,
    .Write = Ns16550HostWrite,
    .SetBaud = Ns16550HostSetBaud,
    .GetBaud = Ns16550HostGetBaud,
    .SetAttribute = Ns16550HostSetAttribute,
    .GetAttribute = Ns16550HostGetAttribute,
    .SetTransMode = Ns16550HostSetTransMode,
    .pollEvent = Ns16550HostPollEvent,
};


static int32_t Ns16550DevicePropertyParse(struct Ns16550 *port, const struct DeviceResourceNode *node)
{
    uint32_t tmp;
    struct DeviceResourceIface *i = DeviceResourceGetIfaceInstance(HDF_CONFIG_SOURCE);
    if (i == NULL || i->GetUint32 == NULL) {
        HDF_LOGE("%s: invalid iface", __func__);
        return HDF_FAILURE;
    }
    if (i->GetUint32(node, "num", &port->num, 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: failed to read device num", __func__);
        return HDF_FAILURE;
    }

    if (i->GetUint32(node, "baudrate", &port->baudrate, 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: failed to read baudrate", __func__);
        return HDF_FAILURE;
    }
    /* when we handle clock porperly, this can be removed */
    if (i->GetUint32(node, "clockFreq", &port->clkFreq, 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: failed to read clock freq", __func__);
        return HDF_FAILURE;
    }
    if (i->GetUint32(node, "fifoEn", &tmp, 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: failed to read fifo setting", __func__);
        return HDF_FAILURE;
    }
    port->attr.fifoRxEn = tmp;
    port->attr.fifoTxEn = tmp;

    if (i->GetUint32(node, "flags", &port->flags, 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: failed to read tansport flag", __func__);
        return HDF_FAILURE;
    }

    if (i->GetUint32(node, "regPbase", &tmp, 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: failed to read reg pbase", __func__);
        return HDF_FAILURE;
    }
    port->regBase = (unsigned long)tmp;

    if (i->GetUint32(node, "regSize", &port->regSize, 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: failed to read reg size", __func__);
        return HDF_FAILURE;
    }

    if (i->GetUint32(node, "interrupt", &port->irq, 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: failed to read irq num", __func__);
        return HDF_FAILURE;
    }
    return HDF_SUCCESS;
}


static int32_t HdfUartDeviceBind(struct HdfDeviceObject *device)
{
    struct UartHost *host;
    HDF_LOGI("%s: in driver", __func__);
    if (device == NULL) {
        HDF_LOGE("%s: device is null", __func__);
        return HDF_ERR_INVALID_OBJECT;
    }

    host = UartHostCreate(device);
    if (host == NULL) {
        HDF_LOGE("%s: uart host create error", __func__);
        return HDF_FAILURE;
    }

    return HDF_SUCCESS;
}

static int32_t HdfUartDeviceInit(struct HdfDeviceObject *device)
{
    int32_t ret;
    struct UartHost *host = NULL;
    struct Ns16550 *port = NULL;
    if (device == NULL) {
        HDF_LOGE("%s: device is null", __func__);
        return HDF_ERR_INVALID_OBJECT;
    }
    host = UartHostFromDevice(device);
    if (host == NULL) {
        HDF_LOGE("%s: host is null", __func__);
        return HDF_FAILURE;
    }
    if (device->property == NULL) {
        HDF_LOGE("%s: device property is null", __func__);
        return HDF_FAILURE;
    }
    port = (struct Ns16550 *)OsalMemCalloc(sizeof(struct Ns16550));
    port->ops = &g_ns16550MmioOps;
    ret = Ns16550DevicePropertyParse(port, device->property);
    if (ret != HDF_SUCCESS ) {
        (void)OsalMemFree(port);
        return HDF_FAILURE;
    }
    port->mappedReg = OsalIoRemap(port->regBase, port->regSize);
    if (port->mappedReg == NULL) {
        HDF_LOGE("%s: failed to map uart port register", __func__);
        (void)OsalMemFree(port);
        return HDF_FAILURE;
    }
    host->priv = port;
    host->num = port->num;
    host->method = &g_uartHostMethod;
    /* assume no device num duplication */
    UartAddDev(host);
    return HDF_SUCCESS;
}


static void HdfUartDeviceRelease(struct HdfDeviceObject *device)
{
    struct UartHost *host = NULL;
    struct Ns16550 *port = NULL;
    if (device == NULL) {
        HDF_LOGE("%s: device is null", __func__);
        return;
    }
    host = UartHostFromDevice(device);
    if (host == NULL) {
        HDF_LOGE("%s: host is null", __func__);
        return;
    }
    if (host->priv == NULL) {
        HDF_LOGE("%s: host with null driver data", __func__);
        return;
    }
    port = host->priv;
    if (port->state != UART_STATE_CLOSED) {
        HDF_LOGE("%s: uart driver data state invalid", __func__);
        return;
    }
    UartRemoveDev(host);
    if (port->mappedReg != 0) {
        OsalIoUnmap((void *)port->mappedReg);
    }
    (void)OsalMemFree(port);
    host->priv = NULL;
    UartHostDestroy(host);
}

struct HdfDriverEntry g_ns16550UartDevice = {
    .moduleVersion = 1,
    .moduleName = "uart_ns16550",
    .Bind = HdfUartDeviceBind,
    .Init = HdfUartDeviceInit,
    .Release = HdfUartDeviceRelease,
};

HDF_INIT(g_ns16550UartDevice);

#ifndef UART_NS16550_H
#define UART_NS16550_H

#include "los_typedef.h"
#include "osal.h"
#include "osal_io.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif /* __cplusplus */
#endif /* __cplusplus */

#define UART_RBR    0x0000
#define UART_THR    0x0000
#define UART_DLL    0x0000
#define UART_DLH    0x0001
#define UART_IER    0x0001
#define UART_IIR    0x0002
#define UART_FCR    0x0002
#define UART_LCR    0x0003
#define UART_MCR    0x0004
#define UART_LSR    0x0005
#define UART_MSR    0x0006
#define UART_SPR    0x0007

#define IER_RDI    0x1
#define IER_THRI    0x2
#define IER_RLSI    0x4
#define IER_MSI    0x8

#define IIR_NOINT    0x1
#define IIR_ID_MASK    0x6
#define IIR_ID_MSI    0x0
#define IIR_ID_THRI    0x2
#define IIR_ID_RDI    0x4
#define IIR_ID_RLSI    0x6
#define IIR_FIFO_MASK    0xc0
#define IIR_FIFO_EN    0xc0

#define FCR_EN    0x1
#define FCR_CLRR    0x2
#define FCR_CLRT    0x4
#define FCR_DMAS    0x8
#define FCR_TRGL_MASK    0xc0
#define FCR_TRGL_1    0x0
#define FCR_TRGL_2    0x1
#define FCR_TRGL_3    0x2
#define FCR_TRGL_4    0x3

#define LCR_WL_MASK    0x3
#define LCR_WL_5    0x0
#define LCR_WL_6    0x1
#define LCR_WL_7    0x2
#define LCR_WL_8    0x3
#define LCR_STB    0x4
#define LCR_PEN    0x8
#define LCR_EVP    0x10
#define LCR_FCP    0x20
#define LCR_STBK    0x40
#define LCR_DLAB    0x80

#define MCR_DTR    0x1
#define MCR_RTS    0x2
#define MCR_OUT1    0x4
#define MCR_OUT2    0x8
#define MCR_LOOP    0x10

#define LSR_DR    0x1
#define LSR_OE    0x2
#define LSR_PE    0x4
#define LSR_FE    0x8
#define LSR_BI    0x10
#define LSR_THRE    0x20
#define LSR_TEMT    0x40
#define LSR_FIFOERR    0x80

#define MSR_DCTS    0x1
#define MSR_DDSR    0x2
#define MSR_TERI    0x4
#define MSR_DDCD    0x8
#define MSR_CTS    0x10
#define MSR_DSR    0x20
#define MSR_RI    0x40
#define MSR_DCD    0x80

/* sunxi extended */
#define UART_USR    0x001f
#define UART_TFL    0x0020
#define UART_RFL    0x0021
#define UART_HALT   0x0029

#define USR_BUSY    0x1
#define USR_TFNF    0x2
#define USR_TFE     0x4
#define USR_RFNE    0x8
#define USR_RFF     0x10

#define HALT_HALT_TX        0x1
#define HALT_CHCFG_AT_BUSY  0x2
#define HALT_CHANGE_UPDATE  0x4
#define HALT_SIR_TX_INVERT  0x8
#define HALT_SIR_RX_INVERT  0x10
#define HALT_DMA_PTE_RX     0x20
#define HALT_PTE            0x40

#define RCV_QUEUE_SIZE 0x4000
#define UART_MAX_BAUDRATE 921600

#define CCU_UART_GATUNG_PHYBASE  0x01C2006C
#define CCU_UART_SOFT_RET_PHYBASR 0x01C202D8





struct RecvQueue {
    uint8_t data[RCV_QUEUE_SIZE];
    uint32_t head;
    uint32_t tail;
    uint32_t status;
};

struct Ns16550;

struct Ns16550RegOps {
    uint8_t (*read)(struct Ns16550 *port, uint32_t reg);
    void (*write)(struct Ns16550 *port, uint32_t reg, uint8_t data);
};

struct Ns16550Cfg {
    uint32_t baudrate;
};

struct Ns16550 {
    struct UartAttribute attr;
    struct Ns16550RegOps *ops;
    struct RecvQueue *rxQueue;
    wait_queue_head_t wait;
    unsigned long regBase;
    uint32_t regSize;
    void* mappedReg;
    uint32_t num;
    uint32_t irq;
    uint32_t clkFreq;
#define UART_FLG_RD_BLOCK       (1 << 2)
    uint32_t flags;
    uint32_t isDebug;
#define UART_STATE_CLOSED       (0)
#define UART_STATE_OPENING      (1)
#define UART_STATE_USABLE       (2)
    uint32_t state;
    uint32_t baudrate;
    uint32_t openCount;
    OSAL_DECLARE_SPINLOCK(lock);
};

#ifdef __cplusplus
#if __cplusplus
}
#endif /* __cplusplus */
#endif /* __cplusplus */

#endif
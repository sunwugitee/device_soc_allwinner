#include "spi_sun8i.h"
#include "device_resource_if.h"
#include "dmac_core.h"
#include "hdf_base.h"
#include "hdf_log.h"
#include "los_vm_phys.h"
#include "osal_io.h"
#include "osal_irq.h"
#include "osal_mem.h"
#include "osal_sem.h"
#include "osal_time.h"
#include "spi_core.h"
#include "spi_dev.h"


struct Sun8iSpi {
    struct SpiCntlr *cntlr;
    struct DListHead deviceList;
    struct OsalSem sem;
    volatile unsigned char *phyBase;
    uint32_t ccuVirtBase;
    uint32_t regBase;
    uint32_t irqNum;/* 中断号 */
    uint32_t busNum;
    uint32_t numCs;
    uint32_t curCs;
    uint32_t speed;
    uint32_t fifoSize;
    uint32_t len;
    uint32_t clkRate;
    uint32_t clkBase;
    uint32_t maxSpeedHz;
    uint32_t minSpeedHz;
    uint16_t mode;
    uint8_t bitsPerWord;/* 数据位宽 */
    uint8_t transferMode;
    const uint8_t	*txBuf;
	uint8_t			*rxBuf;
};

static inline uint32_t Sun8iSpiRead(struct Sun8iSpi *sspi, uint32_t reg)
{
	return OSAL_READL(sspi->phyBase + reg);
}

static inline void Sun8iSpiWrite(struct Sun8iSpi *sspi, uint32_t reg, uint32_t value)
{
	OSAL_WRITEL(value, sspi->phyBase + reg);
}

static inline void Sun8iSpiDisableInterrupt(struct Sun8iSpi *sspi, uint32_t mask)
{
	uint32_t reg = Sun8iSpiRead(sspi, SPI_INT_CTL_REG);

	reg &= ~mask;
	Sun8iSpiWrite(sspi, SPI_INT_CTL_REG, reg);
}

static inline void Sun8iSpiDrainFifo(struct Sun8iSpi *sspi)
{
	uint32_t len;
	uint8_t byte;

	/* See how much data is available */
	len =  (Sun8iSpiRead(sspi, SPI_FIFO_STA_REG) & 0xFF);
    // if(len){
    //     dprintf("recv byte:");
    // }
	while (len--) {
		byte = OSAL_READB((uint32_t)sspi->phyBase + SPI_RXDATA_REG);
        // dprintf("0x%02x ", byte);
		if (sspi->rxBuf)
			*sspi->rxBuf++ = byte;
	}
    // dprintf(" \n");
}

static inline void Sun8iSpiFillFifo(struct Sun8iSpi *sspi)
{
	uint32_t cnt;
	int len;
	uint8_t byte;
    uint32_t value;

    value = Sun8iSpiRead(sspi, SPI_FIFO_STA_REG) & 0xFF0000;
    value = value >> 16;
	/* See how much data we can fit */
	cnt = sspi->fifoSize - value;

    len = (cnt < sspi->len ? cnt : sspi->len);
    // if(len){
    //     dprintf("send byte:");
    // }        
	while (len--) {
		byte = sspi->txBuf ? *sspi->txBuf++ : 0;
        // dprintf("0x%02x ", byte);
		OSAL_WRITEB(byte, sspi->phyBase + SPI_TXDATA_REG);
		sspi->len--;
	}
    // dprintf(" \n");
}

static struct SpiDev *Sun8iFindDeviceByCsNum(const struct Sun8iSpi *sspi, uint32_t cs)
{
    struct SpiDev *dev = NULL;
    struct SpiDev *tmpDev = NULL;

    if (sspi == NULL || sspi->numCs <= cs) {
        return NULL;
    }
    DLIST_FOR_EACH_ENTRY_SAFE(dev, tmpDev, &(sspi->deviceList), struct SpiDev, list) {
        if (dev->csNum  == cs) {
            break;
        }
    }
    return dev;
}

/* set spi clock */
static void SpiSetClk(struct Sun8iSpi *sspi, uint32_t spiClk, uint32_t ahbClk)
{
	uint32_t value = 0;
    uint32_t divClk = ahbClk / (spiClk << 1);
    if(spiClk == ahbClk){
        return;
    }
        
	dprintf("set spi clock %d, mclk %d\n", spiClk, ahbClk);
    value = Sun8iSpiRead(sspi, SPI_CLK_CTL_REG);

	/* CDR2 */
	if(divClk <= SPI_CLK_SCOPE) {
		if (divClk != 0) {
			divClk--;
		}
		value &= ~SPI_CLK_CTL_CDR2;
		value |= (divClk | SPI_CLK_CTL_DRS);
		dprintf("CDR2 - n = %d \n", divClk);
	}/* CDR1 */
	else {
		divClk = 0;
		while(ahbClk > spiClk){
			divClk++;
			divClk >>= 1;
		}
		value &= ~(SPI_CLK_CTL_CDR1 | SPI_CLK_CTL_DRS);
		value |= (divClk << 8);
		dprintf("CDR1 - n = %d \n", divClk);
	}

    Sun8iSpiWrite(sspi, SPI_CLK_CTL_REG, value);
}

static int32_t Sun8iSpiTransferOne(struct Sun8iSpi *sspi, struct SpiMsg *msg)
{
    int32_t ret = HDF_FAILURE;
    uint32_t value = 0;
    uint32_t trigLevel = 0;
    uint32_t tx_len = 0;

    //TODO: 传输模式
    // if(sspi->transferMode == SPI_DMA_TRANSFER){
    //     dprintf("SPI DMA mode\n");
    // }else if(sspi->transferMode == SPI_POLLING_TRANSFER){
    //     dprintf("SPI POLL mode\n");
    // }else if(sspi->transferMode == SPI_INTERRUPT_TRANSFER){
    //     dprintf("SPI INT mode\n");
    // }

    if (msg->len > SPI_MAX_XFER_SIZE)
        return HDF_ERR_INVALID_PARAM;
    if(msg->wbuf != NULL){
        sspi->txBuf = msg->wbuf;
    }
    if(msg->rbuf != NULL){
        sspi->rxBuf = msg->rbuf;
    }
    sspi->len = msg->len;
    
    /* Clear pending interrupts */
    Sun8iSpiWrite(sspi, SPI_INT_STA_REG, ~0);

    /* Reset FIFO */
	Sun8iSpiWrite(sspi, SPI_FIFO_CTL_REG,
			SPI_FIFO_CTL_RX_RST | SPI_FIFO_CTL_TX_RST);          

    if (sspi->transferMode != SPI_DMA_TRANSFER) {
        trigLevel = sspi->fifoSize / 4 * 3;
    } else {
        trigLevel = sspi->fifoSize / 2;
        if (msg->wbuf)
			value |= SPI_FIFO_CTL_TX_DRQEN;
		if (msg->rbuf)
			value |= SPI_FIFO_CTL_RX_DRQEN;
    }

    value |= (trigLevel << SPI_RXCNT_BIT_POS) |
             (trigLevel << SPI_TXCNT_BIT_POS);

    Sun8iSpiWrite(sspi, SPI_FIFO_CTL_REG, value);

	/*
	 * Setup the transfer control register: Chip Select,
	 * polarities, etc.
	 */
	value = Sun8iSpiRead(sspi, SPI_TC_REG);

    /* CPOL 极性 空闲电平 */
    if (sspi->mode & SPI_CLK_POLARITY)
        value |= SPI_TC_POL;
	else
		value &= ~SPI_TC_POL;

    /* CPHA 相位 采样时间 */
	if (sspi->mode & SPI_CLK_PHASE)
		value |= SPI_TC_PHA;
	else
		value &= ~SPI_TC_PHA;
    
    /*3. SSPOL,chip select signal polarity */
	value |= SPI_TC_SPOL;/*default SSPOL = 1,Low level effective */
	/*4. LMTF--LSB/MSB transfer first select */
    if (sspi->mode & SPI_MODE_LSBFE)
        value |= SPI_TC_FBS;
	else
		value &= ~SPI_TC_FBS;/*default LMTF =0, MSB first */

	/*
	 * If it's a TX only transfer, we don't want to fill the RX
	 * FIFO with bogus data
	 */
	if (msg->rbuf) {
		value &= ~SPI_TC_DHB;
	} else {
		value |= SPI_TC_DHB;
	}

    /* We want to control the chip select manually */
	value |= SPI_TC_SS_OWNER;//手动控制 SS Signal
    value &= ~SPI_TC_SS_LEVEL;
    // dprintf("sspi->mode %x value:%x\n", sspi->mode, value);
    Sun8iSpiWrite(sspi, SPI_TC_REG, value);

    /* 计算需要的频率 */
    if(msg->speed != 0){
        SpiSetClk(sspi, msg->speed, sspi->speed);
        sspi->speed = msg->speed;
    }
    /* Finally enable the bus - doing so before might raise SCK to HIGH */
	value = Sun8iSpiRead(sspi, SPI_GC_REG);
	Sun8iSpiWrite(sspi, SPI_GC_REG, value | SPI_GC_EN);

    /* Setup the transfer now... */
	if (msg->wbuf)
		tx_len = msg->len;

	/* Setup the counters */
	Sun8iSpiWrite(sspi, SPI_BURST_CNT_REG, msg->len);
	Sun8iSpiWrite(sspi, SPI_TRANSMIT_CNT_REG, tx_len);
	Sun8iSpiWrite(sspi, SPI_BCC_REG, tx_len);

    if (sspi->transferMode != SPI_DMA_TRANSFER) {
		/* Fill the TX FIFO */
		Sun8iSpiFillFifo(sspi);
	} else {
        dprintf("not config DMA\n");
	}

    /* Enable the interrupts */
	value = SPI_INT_STA_TC;
	Sun8iSpiWrite(sspi, SPI_INT_CTL_REG, value);

    /* Start the transfer */
	value = Sun8iSpiRead(sspi, SPI_TC_REG);
	Sun8iSpiWrite(sspi, SPI_TC_REG, value | SPI_TC_XCH);
    /* 等待中断完成禁能spi */
    OsalSemWait((struct OsalSem *)(&sspi->sem), RX_INT_WAIT_TIMEOUT);
    value = Sun8iSpiRead(sspi, SPI_TC_REG);
	Sun8iSpiWrite(sspi, SPI_TC_REG, value & ~SPI_TC_SS_OWNER);
    return ret;
}

static int32_t Sun8iSpiTransfer(struct SpiCntlr *cntlr, struct SpiMsg *msg, uint32_t count)
{
    int i = 0;
    int32_t ret = HDF_SUCCESS;
    struct Sun8iSpi *sspi = NULL;
    struct SpiDev *dev = NULL;
    // dprintf("[%4d]:%s \n", __LINE__, __func__);
    if (cntlr == NULL || cntlr->priv == NULL) {
        HDF_LOGE("%s: invalid controller", __func__);
        return HDF_ERR_INVALID_OBJECT;
    }
    if (msg == NULL || (msg->rbuf == NULL && msg->wbuf == NULL) || count == 0) {
        HDF_LOGE("%s: invalid parameter", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    sspi = (struct Sun8iSpi *)cntlr->priv;
    
    dev = Sun8iFindDeviceByCsNum(sspi, cntlr->curCs);
    sspi->mode = dev->cfg.mode;
    sspi->transferMode = dev->cfg.transferMode;
    sspi->bitsPerWord = dev->cfg.bitsPerWord;
    sspi->maxSpeedHz = dev->cfg.maxSpeedHz;
    for (i = 0; i < count; i++)
    {
        Sun8iSpiTransferOne(sspi, &(msg[i]));
    }
    
    return ret;
}

static int32_t Sun8iSetCfg(struct SpiCntlr *cntlr, struct SpiCfg *cfg)
{
    struct Sun8iSpi *sspi = NULL;
    struct SpiDev *dev = NULL;
    if (cntlr == NULL || cntlr->priv == NULL || cfg == NULL) {
        HDF_LOGE("%s: cntlr priv or cfg is NULL", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    sspi = (struct Sun8iSpi *)cntlr->priv;

    dev = Sun8iFindDeviceByCsNum(sspi, cntlr->curCs);
    if (dev == NULL) {
        return HDF_FAILURE;
    }
    dev->cfg.mode = cfg->mode;
    dev->cfg.transferMode = cfg->transferMode;
    dev->cfg.bitsPerWord = cfg->bitsPerWord;
    if (cfg->maxSpeedHz != 0 && sspi->speed) {
        dev->cfg.maxSpeedHz = cfg->maxSpeedHz;
        SpiSetClk(sspi, cfg->maxSpeedHz, sspi->speed);
        sspi->speed = cfg->maxSpeedHz;
    }
    return 0;
}

static int32_t Sun8iGetCfg(struct SpiCntlr *cntlr, struct SpiCfg *cfg)
{
    struct Sun8iSpi *sspi = NULL;
    struct SpiDev *dev = NULL;

    if (cntlr == NULL || cntlr->priv == NULL || cfg == NULL) {
        HDF_LOGE("%s: cntlr priv or cfg is NULL", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    sspi = (struct Sun8iSpi *)cntlr->priv;
    // dprintf("spi[%4d]:%s \n", sspi->busNum, __func__);
    dev = Sun8iFindDeviceByCsNum(sspi, cntlr->curCs);
    if (dev == NULL) {
        return HDF_FAILURE;
    }
    *cfg = dev->cfg;
    return HDF_SUCCESS;
}

int32_t Sun8iSpiOpen(struct SpiCntlr *cntlr)
{
    (void)cntlr;
    struct Sun8iSpi *sspi = (struct Sun8iSpi *)cntlr->priv;
    dprintf("[%4d]:%s spi:%d\n", __LINE__, __func__, sspi->busNum);
    return HDF_SUCCESS;
}

int32_t Sun8iSpiClose(struct SpiCntlr *cntlr)
{
    (void)cntlr;
    return HDF_SUCCESS;
}

struct SpiCntlrMethod g_method = {
    .Transfer = Sun8iSpiTransfer,
    .SetCfg = Sun8iSetCfg,
    .GetCfg = Sun8iGetCfg,
    .Open = Sun8iSpiOpen,
    .Close = Sun8iSpiClose,
};
static int32_t SpiConfigHW(struct Sun8iSpi *sspi)
{
    Sun8iSpiWrite(sspi, SPI_GC_REG,
			SPI_GC_MODE | SPI_GC_TP_EN);
    return HDF_SUCCESS;
}

static int32_t SpiConfigClock(struct Sun8iSpi *sspi)
{    
    /* 用于配置 */
    uint32_t gatingClock = IO_DEVICE_ADDR(0x01C20060);
    uint32_t resetClock = IO_DEVICE_ADDR(0x01C202c0);
    uint32_t value = 0;

    value = OSAL_READL(gatingClock);
    //4 channel SPI
    if(sspi->busNum == 0){
        value |= (1 << 20); //SPI0
    }
    else if(sspi->busNum == 1){
        value |= (1 << 21); //SPI1
    }
    else if(sspi->busNum == 2){
        value |= (1 << 22); //SPI2
    }
    else if(sspi->busNum == 3){
        value |= (1 << 23); //SPI3
    }
    OSAL_WRITEL(value, gatingClock);
    OSAL_WRITEL(value, resetClock);

    //PLL_PERIPH0(1X) 600MHZ
    sspi->ccuVirtBase = IO_DEVICE_ADDR(sspi->clkBase);
    value = OSAL_READL(sspi->ccuVirtBase);
    if(sspi->clkRate == 100000000)
        value  = 0x81010002;//100M
    else if(sspi->clkRate == 24000000)
        value  = 0x80000000;//clock 24M 
    else
        value  = 0x80000000;//clock 24M 
    OSAL_WRITEL(value, sspi->ccuVirtBase);

    //初始化相关引脚
    // dprintf("set spi bus[%d] io \n", sspi->busNum);
    if(sspi->busNum == 0){//init spi1 pin
     //user PC0 PC1 PC2 PI23
        uint32_t GpioReg = IO_DEVICE_ADDR(0x01C20848);
        value = OSAL_READL(GpioReg);
        value &= 0xFFFF0000;
        value = value | 0x3 | 0x03 << 4 | 0x03 << 8;
        OSAL_WRITEL(value, GpioReg);
        GpioReg = IO_DEVICE_ADDR(0x01C20850);
        value = OSAL_READL(GpioReg);
        value &= 0x0FFFFFFF;
        value = value | 0x03 << 28;
        OSAL_WRITEL(value, GpioReg);   
    } 
    else if(sspi->busNum == 1){//init spi1 pin
        //use PI16 PI17 PI18 PI19
        uint32_t GpioReg = IO_DEVICE_ADDR(0x01C20928);
        value = OSAL_READL(GpioReg);
        value &= ~0xFFFF;
        value = value | 0x02 | 0x02 << 4 | 0x02 << 8 | 0x02 << 12;
        OSAL_WRITEL(value, GpioReg);
    }
    else if(sspi->busNum == 2){//init spi2 pin
        //use PC19 PC20 PC21 PC22
        uint32_t GpioReg = IO_DEVICE_ADDR(0x01C20850);
        value = OSAL_READL(GpioReg);
        value &= ~0x0FFFF000;
        value = value | 0x03 << 12 | 0x03 << 16 | 0x03 << 20 | 0x03 << 24;
        OSAL_WRITEL(value, GpioReg);

    }
    else{
        dprintf("unsupport spi %d\n\n", sspi->busNum);
        return HDF_FAILURE;
    }
    return HDF_SUCCESS;
}

static int32_t SpiGetRegCfgFromHcs(struct Sun8iSpi *sspi, const struct DeviceResourceNode *node)
{
    struct DeviceResourceIface *iface = DeviceResourceGetIfaceInstance(HDF_CONFIG_SOURCE);

    if (iface == NULL || iface->GetUint32 == NULL) {
        HDF_LOGE("%s: face is invalid", __func__);
        return HDF_FAILURE;
    }
    if (iface->GetUint32(node, "regBase", &sspi->regBase, 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: read regBase fail", __func__);
        return HDF_FAILURE;
    }
    sspi->phyBase = OsalIoRemap(sspi->regBase, 0x1000);

    if (iface->GetUint32(node, "clkBase", &sspi->clkBase, 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: read clkBase fail", __func__);
        return HDF_FAILURE;
    }

    if (iface->GetUint32(node, "irqNum", &sspi->irqNum, 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: read irqNum fail", __func__);
        return HDF_FAILURE;
    }
    if (iface->GetUint32(node, "busNum", &sspi->busNum, 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: read busNum fail", __func__);
        return HDF_FAILURE;
    }
    if (iface->GetUint32(node, "numCs", &sspi->numCs, 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: read numCs fail", __func__);
        return HDF_FAILURE;
    }
    if (iface->GetUint32(node, "speed", &sspi->speed, 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: read speed fail", __func__);
        return HDF_FAILURE;
    }
    if (iface->GetUint32(node, "fifoSize", &sspi->fifoSize, 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: read fifoSize fail", __func__);
        return HDF_FAILURE;
    }
    if (iface->GetUint32(node, "clkRate", &sspi->clkRate, 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: read clkRate fail", __func__);
        return HDF_FAILURE;
    }
    if (iface->GetUint16(node, "mode", &sspi->mode, 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: read mode fail", __func__);
        return HDF_FAILURE;
    }
    if (iface->GetUint8(node, "bitsPerWord", &sspi->bitsPerWord, 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: read bitsPerWord fail", __func__);
        return HDF_FAILURE;
    }
    if (iface->GetUint8(node, "transferMode", &sspi->transferMode, 0) != HDF_SUCCESS) {
        HDF_LOGE("%s: read comMode fail", __func__);
        return HDF_FAILURE;
    }
    return 0;
}

static int32_t Sun8iSpiCreatAndInitDevice(struct Sun8iSpi *sspi)
{
    uint32_t i;
    struct SpiDev *device = NULL;

    for (i = 0; i < sspi->numCs; i++) {
        device = (struct SpiDev *)OsalMemCalloc(sizeof(*device));
        if (device == NULL) {
            HDF_LOGE("%s: OsalMemCalloc error", __func__);
            return HDF_FAILURE;
        }
        device->cntlr = sspi->cntlr;
        device->csNum = i;
        device->cfg.bitsPerWord = sspi->bitsPerWord;
        device->cfg.transferMode = sspi->transferMode;
        device->cfg.maxSpeedHz = sspi->maxSpeedHz;
        device->cfg.mode = sspi->mode;
        DListHeadInit(&device->list);
        DListInsertTail(&device->list, &sspi->deviceList);
        SpiAddDev(device);
    }
    return 0;
}

static uint32_t Sun8iIrqHandle(uint32_t irq, void *data)
{
    uint32_t status;
    struct Sun8iSpi *sspi = (struct Sun8iSpi *)data;

    (void)irq;
    if (sspi == NULL) {
        HDF_LOGE("%s: data is NULL!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    status = Sun8iSpiRead(sspi, SPI_INT_STA_REG);
    // dprintf("SPI_INT_STA_REG: %x\n", status);
    /* Transfer complete */
	if (status & SPI_INT_STA_TC) {
		Sun8iSpiWrite(sspi, SPI_INT_STA_REG, SPI_INT_STA_TC);
		Sun8iSpiDrainFifo(sspi);
        (void)OsalSemPost(&sspi->sem);
		return HDF_SUCCESS;
	}

    /* Receive FIFO 3/4 full */
	if (status & SPI_INTEN_RX_RDY) {
		Sun8iSpiDrainFifo(sspi);
		/* Only clear the interrupt _after_ draining the FIFO */
		Sun8iSpiWrite(sspi, SPI_INT_STA_REG, SPI_INTEN_RX_RDY);
        (void)OsalSemPost(&sspi->sem);
		return HDF_SUCCESS;
	}

    /* Transmit FIFO 3/4 empty */
	if (status & SPI_INTEN_TX_ERQ) {
		Sun8iSpiFillFifo(sspi);

		if (!sspi->len)
			/* nothing left to transmit */
			Sun8iSpiDisableInterrupt(sspi, SPI_INTEN_TX_ERQ);

		/* Only clear the interrupt _after_ re-seeding the FIFO */
		Sun8iSpiWrite(sspi, SPI_INT_STA_REG, SPI_INTEN_TX_ERQ);
        (void)OsalSemPost(&sspi->sem);
		return HDF_SUCCESS;
	}
    (void)OsalSemPost(&sspi->sem);
    return HDF_SUCCESS;
}

static int32_t Sun8iSpiInit(struct SpiCntlr *cntlr, const struct HdfDeviceObject *device)
{
    int32_t ret;
    struct Sun8iSpi *sspi = NULL;
 
    if (device->property == NULL) {
        HDF_LOGE("%s: property is NULL", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
 
    sspi = (struct Sun8iSpi *)OsalMemCalloc(sizeof(struct Sun8iSpi));
    if (sspi == NULL) {
        HDF_LOGE("%s: OsalMemCalloc error", __func__);
        return HDF_FAILURE;
    }
    /* 读取配置信息 */
    ret = SpiGetRegCfgFromHcs(sspi, device->property);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s: SpiGetRegCfgFromHcs error", __func__);
        OsalMemFree(sspi);
        return HDF_FAILURE;
    }
 
    sspi->maxSpeedHz = sspi->clkRate;
    sspi->minSpeedHz = 3000;
    DListHeadInit(&sspi->deviceList);
    sspi->cntlr = cntlr;
    cntlr->priv = sspi;
    cntlr->busNum = sspi->busNum;
    cntlr->method = &g_method;
    ret = OsalSemInit(&sspi->sem, 0);
    if (ret != HDF_SUCCESS) {
        OsalMemFree(sspi);
        return ret;
    }
 
    SpiConfigClock(sspi);
    SpiConfigHW(sspi);

    /* Register an interrupt handler */
    if (sspi->irqNum != 0) {
        ret = OsalRegisterIrq(sspi->irqNum, 0, Sun8iIrqHandle, "SPI_SUN8i", sspi);
        if (ret != HDF_SUCCESS) {
            OsalMemFree(sspi);
            (void)OsalSemDestroy(&sspi->sem);
            return ret;
        }
    }
    /* Creating a device file */
    ret = Sun8iSpiCreatAndInitDevice(sspi);
    if (ret != 0) {
        OsalMemFree(sspi);
        return ret;
    }
    return 0;
}


static int32_t HdfSpiDeviceBind(struct HdfDeviceObject *device)
{
    HDF_LOGI("%s: entry", __func__);
    if (device == NULL) {
        return HDF_ERR_INVALID_OBJECT;
    }
    return (SpiCntlrCreate(device) == NULL) ? HDF_FAILURE : HDF_SUCCESS;
}

static int32_t HdfSpiDeviceInit(struct HdfDeviceObject *device)
{
    int32_t ret;
    struct SpiCntlr *cntlr = NULL;

    if (device == NULL) {
        HDF_LOGE("%s: ptr is null", __func__);
        return HDF_ERR_INVALID_OBJECT;
    }
 
    cntlr = SpiCntlrFromDevice(device);
    if (cntlr == NULL) {
        HDF_LOGE("%s: cntlr is null", __func__);
        return HDF_FAILURE;
    }
 
    ret = Sun8iSpiInit(cntlr, device);
    if (ret != 0) {
        HDF_LOGE("%s: error init", __func__);
        return HDF_FAILURE;
    }
 
    return ret;
}

static void HdfSpiDeviceRelease(struct HdfDeviceObject *device)
{
    struct SpiCntlr *cntlr = NULL;

    HDF_LOGI("%s: entry", __func__);
    if (device == NULL) {
        HDF_LOGE("%s: device is null", __func__);
        return;
    }
    cntlr = SpiCntlrFromDevice(device);
    if (cntlr == NULL) {
        HDF_LOGE("%s: cntlr is null", __func__);
        return;
    }
    if (cntlr->priv != NULL) {
        // Pl022Remove((struct Pl022 *)cntlr->priv);
    }
    SpiCntlrDestroy(cntlr);
}

struct HdfDriverEntry g_hdfSpiDevice = {
    .moduleVersion = 1,
    .moduleName = "HDF_PLATFORM_SPI",
    .Bind = HdfSpiDeviceBind,
    .Init = HdfSpiDeviceInit,
    .Release = HdfSpiDeviceRelease,
};

HDF_INIT(g_hdfSpiDevice);
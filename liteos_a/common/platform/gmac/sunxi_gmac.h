#ifndef __SUNXI_GMAC_H
#define __SUNXI_GMAC_H

#include "eth_chip_driver.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif /* __cplusplus */
#endif /* __cplusplus */

void RandomMACAddr(uint8_t *addr, int32_t len);
int32_t InitGmacDriver(struct EthDevice *ethDevice);
int32_t DeInitGmacDriver(struct EthDevice *ethDevice);
struct HdfEthMacChipDriver *BuildGmmacDriver(void);
void ReleaseGmacDriver(struct HdfEthMacChipDriver *chipDriver);

#ifdef __cplusplus
#if __cplusplus
}
#endif /* __cplusplus */
#endif /* __cplusplus */

#endif /* __SUNXI_GMAC_H */

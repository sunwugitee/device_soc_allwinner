#ifndef __GMAC_PRIV_H
#define __GMAC_PRIV_H

#define SETBITS_LE32(value,address) OSAL_WRITEL(OSAL_READL(address) | value, address)
#define CLRBITS_LE32(value,address) OSAL_WRITEL(OSAL_READL(address) & ~value, address)

/* 寄存器地址 */
#define GMAC_BASIC_CTL0     0x00
#define GMAC_CTL0_FULL_DUPLEX		BIT(0)
#define GMAC_CTL0_SPEED_MASK		GENMASK(3, 2)
#define GMAC_CTL0_SPEED_10		(0x2 << 2)
#define GMAC_CTL0_SPEED_100		(0x3 << 2)
#define GMAC_CTL0_SPEED_1000		(0x0 << 2)


#define GMAC_BASIC_CTL1     0x04
#define GMAC_CTL1_SOFT_RST		BIT(0)
#define GMAC_CTL1_BURST_LEN_SHIFT	24

#define GMAC_INT_STA        0x08
/* Used in GMAC_INT_STA */
#define GMAC_TX_INT             BIT(0)
#define GMAC_TX_DMA_STOP_INT    BIT(1)
#define GMAC_TX_BUF_UA_INT      BIT(2)
#define GMAC_TX_TIMEOUT_INT     BIT(3)
#define GMAC_TX_UNDERFLOW_INT   BIT(4)
#define GMAC_TX_EARLY_INT       BIT(5)
#define GMAC_RX_INT             BIT(8)
#define GMAC_RX_BUF_UA_INT      BIT(9)
#define GMAC_RX_DMA_STOP_INT    BIT(10)
#define GMAC_RX_TIMEOUT_INT     BIT(11)
#define GMAC_RX_OVERFLOW_INT    BIT(12)
#define GMAC_RX_EARLY_INT       BIT(13)
#define GMAC_RGMII_STA_INT      BIT(16)

#define GMAC_INT_MSK_COMMON	GMAC_RGMII_STA_INT
#define GMAC_INT_MSK_TX		(GMAC_TX_INT | \
				 GMAC_TX_DMA_STOP_INT | \
				 GMAC_TX_BUF_UA_INT | \
				 GMAC_TX_TIMEOUT_INT | \
				 GMAC_TX_UNDERFLOW_INT | \
				 GMAC_TX_EARLY_INT |\
				 GMAC_INT_MSK_COMMON)
#define GMAC_INT_MSK_RX		(GMAC_RX_INT | \
				 GMAC_RX_BUF_UA_INT | \
				 GMAC_RX_DMA_STOP_INT | \
				 GMAC_RX_TIMEOUT_INT | \
				 GMAC_RX_OVERFLOW_INT | \
				 GMAC_RX_EARLY_INT | \
				 GMAC_INT_MSK_COMMON)


#define GMAC_INT_EN         0x0C
#define GMAC_TX_INT             BIT(0)
#define GMAC_TX_DMA_STOP_INT    BIT(1)
#define GMAC_TX_BUF_UA_INT      BIT(2)
#define GMAC_TX_TIMEOUT_INT     BIT(3)
#define GMAC_TX_UNDERFLOW_INT   BIT(4)
#define GMAC_TX_EARLY_INT       BIT(5)
#define GMAC_RX_INT             BIT(8)
#define GMAC_RX_BUF_UA_INT      BIT(9)
#define GMAC_RX_DMA_STOP_INT    BIT(10)
#define GMAC_RX_TIMEOUT_INT     BIT(11)
#define GMAC_RX_OVERFLOW_INT    BIT(12)
#define GMAC_RX_EARLY_INT       BIT(13)
#define GMAC_RGMII_STA_INT      BIT(16)

#define GMAC_TX_CTL0        0x10
#define	GMAC_TX_CTL0_TX_EN		BIT(31)

#define GMAC_TX_CTL1        0x14
#define	GMAC_TX_CTL1_TX_MD		BIT(1)
#define GMAC_TX_NEXT_FRM        BIT(2)
#define	GMAC_TX_CTL1_TX_DMA_EN		BIT(30)
#define	GMAC_TX_CTL1_TX_DMA_START	BIT(31)

#define GMAC_TX_FLOW_CTL    0x1C
#define GMAC_TX_DESC_LIST   0x20
#define GMAC_RX_CTL0        0x24
#define	GMAC_RX_CTL0_RX_EN		BIT(31)

#define GMAC_RX_CTL1        0x28
#define	GMAC_RX_CTL1_RX_MD		BIT(1)
#define	GMAC_RX_CTL1_RX_RUNT_FRM	BIT(2)
#define	GMAC_RX_CTL1_RX_ERR_FRM		BIT(3)
#define	GMAC_RX_CTL1_RX_DMA_EN		BIT(30)
#define	GMAC_RX_CTL1_RX_DMA_START	BIT(31)

#define GMAC_RX_DESC_LIST   0x34
#define GMAC_RX_FRM_FLT     0x38
#define GMAC_MDIO_CMD       0x48
#define GMAC_MDIO_DATA      0x4C
#define GMAC_MACADDR_HI     0x50
#define GMAC_MACADDR_LO     0x54
#define GMAC_TX_DMA_STA     0xB0
#define GMAC_TX_CUR_DESC    0xB4
#define GMAC_TX_CUR_BUF     0xB8
#define GMAC_RX_DMA_STA     0xC0
#define GMAC_RX_CUR_DESC    0xC4
#define GMAC_RX_CUR_BUF     0xC8
/* DMA描述符配置 */

#define GMAC_DESC_OWN_DMA	BIT(31)
#define GMAC_DESC_LAST_DESC	BIT(30)
#define GMAC_DESC_FIRST_DESC	BIT(29)
#define GMAC_DESC_CHAIN_SECOND	BIT(24)

#define CONFIG_TX_DESCR_NUM	32
#define CONFIG_RX_DESCR_NUM	32
#define CONFIG_ETH_BUFSIZE	2048 /* Note must be dma aligned */

#define CONFIG_ETH_RXSIZE	2044 /* Note must fit in ETH_BUFSIZE */

#define TX_TOTAL_BUFSIZE	(CONFIG_ETH_BUFSIZE * CONFIG_TX_DESCR_NUM)
#define RX_TOTAL_BUFSIZE	(CONFIG_ETH_BUFSIZE * CONFIG_RX_DESCR_NUM)

#endif
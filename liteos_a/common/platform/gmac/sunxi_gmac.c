/*********************************************************************************************************
** 文件创建日期: 2022 年 07 月 1 日
**
** 描        述: GMAC 千兆以太网控制器驱动
*********************************************************************************************************/
#include <netinet/if_ether.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include "osal_mem.h"
#include "osal_io.h"
#include "osal_time.h"
#include "osal_irq.h"
#include "osal_thread.h"
#include "osal_spinlock.h"
#include "osal_timer.h"
#include "los_vm_iomap.h"
#include "los_vm_zone.h"
#include "los_event.h"
#include "los_hw.h"
#include "sunxi_gmac.h"
#include "gmac_phy.h"
#include "gmac_priv.h"


#define EVENT_NET_TX_RX    0x1
/*********************************************************************************************************
  GMAC 私有结构
*********************************************************************************************************/
struct GmacDmaDesc {
	volatile uint32_t status;
	volatile uint32_t ctlSize;
	volatile uint32_t bufAddr;
	volatile uint32_t next;
}__attribute__((aligned(CACHE_ALIGNED_SIZE)));

struct GmacPriv {
    const char *devName;
    uint32_t iobase;           /* Base address of device */
    uint32_t vector;           /* 中断号，用于操作前禁能中断 */
    uint32_t index;            /* dev id */
    uint32_t txHwCnt;
    uint32_t rxHwCnt;
    struct GmacDmaDesc *rxDesc;/* 接收描述符队列 */
	struct GmacDmaDesc *txDesc;/* 发送描述符队列 */
    unsigned long rxDmaDesc;/* 接收描述符队列(物理) */
	unsigned long txDmaDesc;/* 发送描述符队列(物理)*/
    void *rxBuffer;
    unsigned long rxDmaBuffer;/* 接收描述符队列(物理) */
    NetBuf *txNetBuf[CONFIG_TX_DESCR_NUM];

    /* forced speed & duplex (no autoneg)
	 * partner speed & duplex & pause (autoneg)
	 */
	uint32_t speed;
	uint32_t duplex;
    
    uint8_t phyMode;
    int16_t phyAddr;
    OsalSpinlock tx_lock;
    OsalSpinlock rx_lock;
    /*
     * 接收队列事件
     */
    EVENT_CB_S stEvent;
    /*
     * 定时器
     */
    OsalTimer phyTimer;
    /*
     * 网络数据包接收线程
     */
    struct OsalThread *pushThread;
};

// static int32_t intCount = 0;

/**/
static int mdioRead (struct GmacPriv *priv, uint8_t ucPhyAddr, uint8_t ucRegAddr, uint16_t *pusValue);
static int mdioWrite (struct GmacPriv *priv, uint8_t ucPhyAddr, uint8_t ucRegAddr, uint16_t usValue);

/* 随机生成mac地址 */
void RandomMACAddr(uint8_t *addr, int32_t len)
{
    addr[0] = 0x02;
    addr[1] = 0x53;
    addr[2] = 0xd1;
    addr[3] = 0xb1;
    addr[4] = 0xea;
    addr[5] = 0x3f;
}

void NetDmaCacheInv(void *addr, uint32_t size)
{
    uint32_t start = (uintptr_t)addr & ~(CACHE_ALIGNED_SIZE - 1);
    uint32_t end = (uintptr_t)addr + size;

    end = ALIGN(end, CACHE_ALIGNED_SIZE);
    DCacheInvRange(start, end);
}

void NetDmaCacheClean(void *addr, uint32_t size)
{
    uint32_t start = (uintptr_t)addr & ~(CACHE_ALIGNED_SIZE - 1);
    uint32_t end = (uintptr_t)addr + size;

    end = ALIGN(end, CACHE_ALIGNED_SIZE);
    DCacheFlushRange(start, end);
}

void GmacPrintInfo(struct GmacPriv *Gmac)
{
    dprintf("GMAC_INT_STA 0x08:     %08x \n",OSAL_READL(Gmac->iobase + GMAC_INT_STA));
    dprintf("GMAC_INT_EN  0x0C:     %08x \n",OSAL_READL(Gmac->iobase + GMAC_INT_EN));    
    dprintf("GMAC_TX_CTL0 0x10:     %08x \n",OSAL_READL(Gmac->iobase + GMAC_TX_CTL0));
    dprintf("GMAC_TX_CTL1 0x14:     %08x \n",OSAL_READL(Gmac->iobase + GMAC_TX_CTL1));
    dprintf("GMAC_RX_CTL0 0x24:     %08x \n",OSAL_READL(Gmac->iobase + GMAC_RX_CTL0));
    dprintf("GMAC_RX_CTL1 0x28:     %08x \n",OSAL_READL(Gmac->iobase + GMAC_RX_CTL1));
    dprintf("GMAC_TX_DMA_STA 0xB0:  %08x \n",OSAL_READL(Gmac->iobase + GMAC_TX_DMA_STA));
    dprintf("GMAC_TX_CUR_DESC 0xB4: %08x \n",OSAL_READL(Gmac->iobase + GMAC_TX_CUR_DESC));
    dprintf("GMAC_RX_DMA_STA 0xC0:  %08x \n",OSAL_READL(Gmac->iobase + GMAC_RX_DMA_STA));
    dprintf("GMAC_RX_CUR_DESC 0xC4: %08x \n",OSAL_READL(Gmac->iobase + GMAC_RX_CUR_DESC));
}

/* 初始化发送接收DMA描述符 */
static int32_t GmacDescriptorInit(struct GmacPriv *Gmac)
{
    int i = 0;
	struct GmacDmaDesc *descPoint = NULL;

	for(i = 0; i < CONFIG_TX_DESCR_NUM; i++) {
        Gmac->txNetBuf[i] = NULL;
		descPoint = &Gmac->txDesc[i];
        descPoint->status = 0;
        descPoint->ctlSize = 0;

        if (i == CONFIG_TX_DESCR_NUM -1)
            descPoint->next = Gmac->txDmaDesc;
        else        
		    descPoint->next = Gmac->txDmaDesc + ((i+1) * CACHE_ALIGNED_SIZE);
        // NetDmaCacheClean((void*)descPoint, sizeof(struct GmacDmaDesc));
        // dprintf("txdesc:%d  vadr:%x next:%x \n", i, descPoint, descPoint->next);
	}
	OSAL_WRITEL(Gmac->txDmaDesc, Gmac->iobase + GMAC_TX_DESC_LIST);

    for (i = 0; i < CONFIG_RX_DESCR_NUM; i++) {
		descPoint = &Gmac->rxDesc[i];
		descPoint->status = GMAC_DESC_OWN_DMA;
        descPoint->ctlSize = CONFIG_ETH_RXSIZE;
        descPoint->bufAddr = VMM_TO_DMA_ADDR((uint32_t)Gmac->rxBuffer + (i *CONFIG_ETH_BUFSIZE));
        // descPoint->bufAddr = ((uint32_t)Gmac->rxBuffer + (i *CONFIG_ETH_BUFSIZE));   
        // descPoint->bufAddr = ((uint32_t)Gmac->rxDmaBuffer + (i *CONFIG_ETH_BUFSIZE));
        if (i == CONFIG_RX_DESCR_NUM -1)
            descPoint->next = Gmac->rxDmaDesc;
        else        
		    descPoint->next = Gmac->rxDmaDesc + ((i+1) * CACHE_ALIGNED_SIZE);
        // NetDmaCacheClean((void*)descPoint, sizeof(struct GmacDmaDesc));
        // dprintf("rxdesc:%d  vadr:%x next:%x \n", i, descPoint, descPoint->next);
	}
	OSAL_WRITEL(Gmac->rxDmaDesc, Gmac->iobase + GMAC_RX_DESC_LIST);

    return HDF_SUCCESS;
}

static int CanSend(struct GmacPriv *Gmac)
{
    uint16_t status;
    int count = 0;

    do {
        OsalMSleep(1);
        count++;
        if (count == 100)
            return HDF_FAILURE;
        mdioRead(Gmac, 0, 0x01, &status);
    }while ((status & 0x04) == 0);

    return HDF_SUCCESS;
}
/* 数据发送 */
static NetDevTxResult EthXmit(struct NetDevice *netDev, NetBuf *netBuf)
{
    struct EthDevice * ethDevice = (struct EthDevice *)netDev->mlPriv;
    struct GmacPriv *Gmac = (struct GmacPriv *)ethDevice->priv;
    struct GmacDmaDesc *descPoint = NULL;
    int32_t iCnt = 0;
    int32_t sendPktLen;
    
    sendPktLen = NetBufGetDataLen(netBuf);
    if (sendPktLen > CONFIG_ETH_BUFSIZE) {
        dprintf("%s: xmit error len=%d\n", __func__, sendPktLen);
        return NETDEV_TX_BUSY;   
    }

    if(CanSend(Gmac) != HDF_SUCCESS){
        dprintf("can not send \n");
        return NETDEV_TX_BUSY;   
    }
    OsalSpinLockIrq(&(Gmac->tx_lock));   
    iCnt = Gmac->txHwCnt;
    if(Gmac->txNetBuf[iCnt] != NULL){
        NetBufFree(Gmac->txNetBuf[iCnt]);
    }
    Gmac->txNetBuf[iCnt] = netBuf;
    descPoint = &Gmac->txDesc[iCnt];
    descPoint->bufAddr = VMM_TO_DMA_ADDR((uint32_t)NetBufGetAddress(netBuf, E_DATA_BUF));
    // dprintf("iCnt :%d %x %x  len:%d \n", iCnt, descPoint->bufAddr, NetBufGetAddress(netBuf, E_DATA_BUF), sendPktLen);
    NetDmaCacheClean((void *)NetBufGetAddress(netBuf, E_DATA_BUF), sendPktLen);
    descPoint->ctlSize = sendPktLen & 0x7ff;
	descPoint->ctlSize |= GMAC_DESC_LAST_DESC | GMAC_DESC_FIRST_DESC;
	descPoint->status = GMAC_DESC_OWN_DMA ;
    // NetDmaCacheClean((void*)descPoint, sizeof(struct GmacDmaDesc));
    iCnt++;
    if(iCnt >= CONFIG_TX_DESCR_NUM)
        iCnt = 0;
    Gmac->txHwCnt = iCnt;
    SETBITS_LE32(GMAC_TX_CTL1_TX_DMA_START | GMAC_TX_CTL1_TX_DMA_EN, Gmac->iobase + GMAC_TX_CTL1);
    OsalSpinUnlockIrq(&(Gmac->tx_lock));
    if(0)
        GmacPrintInfo(Gmac);

    return NETDEV_TX_OK;
}

static void LinkStatusChanged(struct NetDevice *netDev)
{
    struct EthDevice * ethDevice = (struct EthDevice *)netDev->mlPriv;
    struct GmacPriv *Gmac = (struct GmacPriv *)ethDevice->priv;
    uint16_t status = 0;

    mdioRead(Gmac, Gmac->phyAddr, 0x01, &status);
    if ((status & 0x04) == 0){//更新连接状态
        NetIfSetLinkStatus(ethDevice->netdev, NETIF_LINK_DOWN);
    }else{
        NetIfSetLinkStatus(ethDevice->netdev, NETIF_LINK_UP);
    }

}

// static void LinkSpeeedChecked(struct NetDevice *netDev)
// {
//     struct EthDevice * ethDevice = (struct EthDevice *)netDev->mlPriv;
//     struct GmacPriv *Gmac = (struct GmacPriv *)ethDevice->priv;
//     uint16_t status = 0;

//     mdioRead(Gmac, Gmac->phyAddr, 0x01, &status);
//     if (){//更新连接状态



//     //TODO :add speed get

// }

static int32_t EthSetMacAddr(struct NetDevice *netDev, void *addr)
{
    int32_t ret = HDF_SUCCESS;
    
    if (netDev == NULL) {
        HDF_LOGE("%s:input is NULL!", __func__);
        return HDF_FAILURE;
    }

    struct EthDevice * ethDevice = (struct EthDevice *)netDev->mlPriv;
    struct GmacPriv *Gmac = (struct GmacPriv *)ethDevice->priv;
    uint32_t macid_lo, macid_hi;
    uint8_t  *mac_id = (uint8_t *)addr;


    /* Rewrite mac address after reset */
	macid_lo = mac_id[0] + (mac_id[1] << 8) + (mac_id[2] << 16) +
		(mac_id[3] << 24);
	macid_hi = mac_id[4] + (mac_id[5] << 8);
    OSAL_WRITEL(macid_hi, Gmac->iobase + GMAC_MACADDR_HI);
    OSAL_WRITEL(macid_lo, Gmac->iobase + GMAC_MACADDR_LO);

    return ret;
}

struct NetDeviceInterFace g_ethNetDevOps = {
    .xmit = EthXmit,
    .setMacAddr = EthSetMacAddr,
    .linkStatusChanged = LinkStatusChanged,
};

int32_t EthernetInitNetdev(NetDevice *netdev)
{
    int32_t ret;

    if (netdev == NULL) {
        HDF_LOGE("%s netdev is null!", __func__);
        return HDF_ERR_INVALID_PARAM;
    }
    netdev->netDeviceIf = &g_ethNetDevOps;

    ret = NetDeviceAdd(netdev);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s NetDeviceAdd return error code %d!", __func__, ret);
        return ret;
    }
    return ret;
}


static uint32_t GmacInterruptHandle(uint32_t irq, void *data)
{
    (void)irq;
    struct GmacPriv *Gmac = (struct GmacPriv *)data;
    // dprintf("-->>GmacInterruptHandle\n");
    OsalDisableIrq(Gmac->vector); // 禁止rq
    LOS_EventWrite(&(Gmac->stEvent), EVENT_NET_TX_RX);

    return HDF_SUCCESS;
}
/* Submit network data to the protocol stack for processing */
static int32_t DataTreatTask(void *arg)
{
    struct EthDevice *ethDevice = (struct EthDevice *)arg;
    struct GmacPriv *Gmac = (struct GmacPriv *)ethDevice->priv;
    uint32_t uwRet;
    uint32_t descNum = 0;
    uint32_t length = 0;
    struct GmacDmaDesc* desc = NULL;
    NetBuf *netBuf = NULL;
    uint32_t irqMask = 0;
    uint8_t *pbuf;
    uint32_t next;
    while (true) {
        uwRet = LOS_EventRead(&(Gmac->stEvent), EVENT_NET_TX_RX, LOS_WAITMODE_OR | LOS_WAITMODE_CLR, LOS_WAIT_FOREVER);      
        irqMask = OSAL_READL(Gmac->iobase + GMAC_INT_STA);
        // dprintf("irqMask:%08x \n", irqMask);
        OSAL_WRITEL(irqMask, Gmac->iobase + GMAC_INT_STA);   
        if (irqMask & GMAC_RX_INT) {//收到数据
            // dprintf("GMAC_RX_INT \n");
            descNum = Gmac->rxHwCnt;
            desc = &Gmac->rxDesc[descNum];

            NetDmaCacheInv((void*)desc, sizeof(struct GmacDmaDesc));
            while ((desc->status & GMAC_DESC_OWN_DMA) == 0)
            {
                OsalSpinUnlockIrq(&(Gmac->rx_lock));
                length = (desc->status >> 16) & 0x3fff;
                OsalSpinUnlockIrq(&(Gmac->rx_lock));
                
                netBuf = NetBufAlloc(ALIGN(CONFIG_ETH_BUFSIZE, CACHE_ALIGNED_SIZE));
                pbuf = (uint8_t *)Gmac->rxBuffer + (descNum * CONFIG_ETH_BUFSIZE);
                NetDmaCacheInv((void*)pbuf, length);
                //dprintf("%02d: rx  len:%3d %08x %02x %02x %02x\n", descNum, length, desc->bufAddr, pbuf[17], pbuf[18], pbuf[19]);
                (void)memcpy_s(NetBufGetAddress(netBuf, E_DATA_BUF), length, (void*)pbuf, length);
                NetBufPush(netBuf, E_DATA_BUF, length);
                NetIfRxNi(ethDevice->netdev, netBuf);
    
                desc->status = GMAC_DESC_OWN_DMA;

                // NetDmaCacheClean((void*)desc, sizeof(struct GmacDmaDesc));
                descNum++;
                if(descNum == CONFIG_RX_DESCR_NUM)
                    descNum = 0;
                next = desc->next;
                Gmac->rxHwCnt = descNum;
                desc = &Gmac->rxDesc[descNum];
                /* 
                 * 下一个描述符如果DMAC正在使用，break
                 */
                if(next == OSAL_READL(Gmac->iobase + GMAC_RX_CUR_DESC))
                    break;
                NetDmaCacheInv((void*)desc, sizeof(struct GmacDmaDesc));
            }
        }
        // if (irqMask & GMAC_TX_INT){ //发送完成
        //     descNum = Gmac->txHwCnt;
        //     if(descNum-1 == -1)
        //     {
        //         descNum = CONFIG_RX_DESCR_NUM-1;
        //     }
        //     if(Gmac->txNetBuf[descNum] != NULL){
        //         NetBufFree(Gmac->txNetBuf[descNum]);
        //         Gmac->txNetBuf[descNum] = NULL;
        //     }
        // }

        OsalEnableIrq(Gmac->vector);
    }
    return HDF_SUCCESS;
}

/**
 * @brief		Create a data processing thread.
 * @details	    Create a data processing thread to process the received data. 
 * @param[in]	ethDevice Network device handle.
 * @retval		HDF_SUCCESS		成功
 * @retval		HDF_FAILURE	    错误 
 */
static int32_t CreateEthIrqThread(struct EthDevice *ethDevice)
{
    struct GmacPriv *priv = (struct GmacPriv *)ethDevice->priv;
    struct OsalThread thread;
    struct OsalThreadParam para = {
        .name = "gmac_recv_Task",
        .stackSize = 0x20000,
        .priority = OSAL_THREAD_PRI_HIGHEST,
    };

    if (OsalThreadCreate(&thread, DataTreatTask, (void *)ethDevice) != HDF_SUCCESS) {
        HDF_LOGE("create isr thread failed");
        return HDF_FAILURE;
    }
    if (OsalThreadStart(&thread, &para) != HDF_SUCCESS) {
        HDF_LOGE("isr thread start failed");
        return HDF_FAILURE;
    }
    priv->pushThread = &thread;

    return HDF_SUCCESS;
}

/**
 * @brief		destroy a data processing thread.
 * @details	    destroy a data processing thread. 
 * @param[in]	ethDevice Network device handle.
 * @retval		HDF_SUCCESS		成功
 * @retval		HDF_FAILURE	    错误 
 */
static int32_t DestroyEthIrqThread(struct EthDevice *ethDevice)
{
    struct GmacPriv *priv = (struct GmacPriv *)ethDevice->priv;
    if(HDF_SUCCESS != OsalThreadSuspend(priv->pushThread))
    {
        return HDF_FAILURE;
    }

    OsalThreadDestroy(priv->pushThread);

    return HDF_SUCCESS;
}

/**
 * @brief		Example Initialize the mac controller clock.
 * @details	    Example Initialize the mac controller clock. 
 * @param[in]	phyMode MII or RGMII.
 * @retval		HDF_SUCCESS		成功
 * @retval		HDF_FAILURE	    错误 
 */
int32_t InitGmacSyscon(uint8_t phyMode)
{   
    // 0x01C20000 0x164 
    // 0x01C20000 0x64  tx clk
    // 0x01C20000 0x2c4 tx rst
    uint32_t ccu = IO_DEVICE_ADDR(0x01C20064);
    uint32_t value = 0;
    OSAL_WRITEL(0x20000, ccu);//tx clk

    ccu = IO_DEVICE_ADDR(0x01C202c4);//set tx clock
    OSAL_WRITEL(0x20000, ccu);//tx rst

    ccu = IO_DEVICE_ADDR(0x01C20164);//set input clock
    value = OSAL_READL(ccu);
    if(phyMode == 0)//MII mode
        value = 0x0;
    else
        value = 0x6;
    OSAL_WRITEL(value, ccu);
    return HDF_SUCCESS;
}

/**
 * @brief		Clear the mac controller descriptor.
 * @details	    Clear the mac controller TX/RX descriptor. 
 * @param[in]	ioaddr mac controller address.
 * @retval		HDF_SUCCESS		成功
 * @retval		HDF_FAILURE	    错误 
 */
static int GmacDmaClean(uint32_t ioaddr)
{
	OSAL_WRITEL(0, ioaddr + GMAC_RX_CTL1);
	OSAL_WRITEL(0, ioaddr + GMAC_TX_CTL1);
	OSAL_WRITEL(0, ioaddr + GMAC_RX_FRM_FLT);
	OSAL_WRITEL(0, ioaddr + GMAC_RX_DESC_LIST);
	OSAL_WRITEL(0, ioaddr + GMAC_TX_DESC_LIST);
	OSAL_WRITEL(0, ioaddr + GMAC_INT_EN);
	OSAL_WRITEL(0x1FFFFFF, ioaddr + GMAC_INT_STA);

	return HDF_SUCCESS;
}

/**
 * @brief		phy controller restart.
 * @details	    Reset by reset pin. 
 * @retval		HDF_SUCCESS		成功
 * @retval		HDF_FAILURE	    错误 
 */
int32_t PhyPowerReset(void)
{
    // 0x01C20800 0x08 通过GPIO对phy重启
    uint32_t rst = IO_DEVICE_ADDR(0x01C20808);
    uint32_t value = 0;
    value = OSAL_READL(rst);
    value &= 0x0F;
    value |= 0x70;
    OSAL_WRITEB(value, rst);
    OsalMSleep(10);
    value = OSAL_READL(rst);
    value &= 0x0F;
    value |= 0x10;
    OSAL_WRITEB(value, rst);
    OsalMSleep(100);

    return HDF_SUCCESS;
}

 /**
 * @brief		mac controller restart.
 * @details	    mac controller restart. 
 * @param[in]	Gmac mac controller descriptor.
 * @retval		HDF_SUCCESS		成功
 * @retval		HDF_FAILURE	    错误 
 */
int32_t HWReset(struct GmacPriv *Gmac)
{
    uint32_t value;
    int count = 0;

    GmacDmaClean(Gmac->iobase);
    /* DMA SW reset */
    value = OSAL_READL(Gmac->iobase + GMAC_BASIC_CTL1);
    OSAL_WRITEL(value | GMAC_CTL1_SOFT_RST, Gmac->iobase + GMAC_BASIC_CTL1);
    /* The timeout was previoulsy set to 10ms, but some board (OrangePI0)
	 * need more if no cable plugged. 100ms seems OK
	 */
    do{
        value = OSAL_READL(Gmac->iobase + GMAC_BASIC_CTL1);
        if((value & GMAC_CTL1_SOFT_RST) == 0)//wait reset success
            break;
        OsalMSleep(1);
    }while (count++ < 100);

    return HDF_SUCCESS;
}

int32_t InitGmacDriver(struct EthDevice *ethDevice)
{
    int32_t ret;
    uint16_t phyAddr = 0;
    struct GmacPriv *gmacPriv = NULL;

    if (ethDevice == NULL) {
        HDF_LOGE("%s input is NULL!", __func__);
        return HDF_FAILURE;
    }
    ret = EthernetInitNetdev(ethDevice->netdev);
    if (ret != HDF_SUCCESS) {
        HDF_LOGE("%s failed to init ethernet netDev", __func__);
        return HDF_FAILURE;
    }
    
    gmacPriv = (struct GmacPriv *)OsalMemCalloc(sizeof(struct GmacPriv));
    if (gmacPriv == NULL) {
        HDF_LOGE("%s fail : HiethPriv OsalMemCalloc is fail!", __func__);
        return HDF_FAILURE;
    }

    gmacPriv->index   = 0;
    gmacPriv->phyAddr = 0;
    gmacPriv->duplex  = 1;
    gmacPriv->txHwCnt = 0;
    gmacPriv->rxHwCnt = 0;
    gmacPriv->phyMode =  ethDevice->config->ethPhy.phyMode;
    if(gmacPriv->phyMode)//RGMII mode
        gmacPriv->speed = 1000;
    else
        gmacPriv->speed = 100;

    gmacPriv->vector  = ethDevice->config->ethMac.irqVector;
    gmacPriv->iobase  = IO_DEVICE_ADDR(ethDevice->config->ethMac.regBase);
    ethDevice->config->ethMac.iobase = gmacPriv->iobase;

    gmacPriv->devName = "eth0";
    /* 申请DMA描述符空间 */
    gmacPriv->txDesc = (struct GmacDmaDesc *)LOS_DmaMemAlloc(&gmacPriv->txDmaDesc, 
                    (sizeof(struct GmacDmaDesc) * CONFIG_TX_DESCR_NUM), CACHE_ALIGNED_SIZE, DMA_NOCACHE);

    gmacPriv->rxDesc = (struct GmacDmaDesc *)LOS_DmaMemAlloc(&gmacPriv->rxDmaDesc, 
                    (sizeof(struct GmacDmaDesc) * CONFIG_RX_DESCR_NUM), CACHE_ALIGNED_SIZE, DMA_NOCACHE);

    // gmacPriv->rxBuffer = (void *)LOS_DmaMemAlloc(&gmacPriv->rxDmaBuffer,TX_TOTAL_BUFSIZE, CACHE_ALIGNED_SIZE, DMA_NOCACHE);
    // gmacPriv->rxBuffer = (void *)OsalMemCalloc(TX_TOTAL_BUFSIZE);
    gmacPriv->rxBuffer = (uint8_t *)OsalMemAllocAlign(CACHE_ALIGNED_SIZE, ALIGN(TX_TOTAL_BUFSIZE, CACHE_ALIGNED_SIZE));
                    
    OsalSpinInit(&(gmacPriv->tx_lock));
    OsalSpinInit(&(gmacPriv->rx_lock));

    (void)LOS_EventInit(&(gmacPriv->stEvent));
    /* Configuring the NIC Clock and MDIO mode*/
    InitGmacSyscon(gmacPriv->phyMode);
    
    HWReset(gmacPriv);
    
    PhyPowerReset();

    /* Initialize the phy chip */
    GmacPhyInit((VOID *)gmacPriv, (PHY_READ)mdioRead, (PHY_WRITE)mdioWrite, &phyAddr);
    gmacPriv->phyAddr = phyAddr;
    ethDevice->priv = gmacPriv;

    if (OsalRegisterIrq(gmacPriv->vector, OSAL_IRQF_TRIGGER_NONE, GmacInterruptHandle, "gmacInterrupt", (void *)gmacPriv) != HDF_SUCCESS) {
        HDF_LOGE("register irq failed\n");
        return HDF_FAILURE;
    }

    CreateEthIrqThread(ethDevice);
    OsalEnableIrq(gmacPriv->vector);
    
    dprintf("phyMode %d\n", gmacPriv->phyMode);

    return HDF_SUCCESS;
}

int32_t DeInitGmacDriver(struct EthDevice *ethDevice)
{
    struct GmacPriv *Gmac =  (struct GmacPriv *)ethDevice->priv;
    int value = 0;
    OsalDisableIrq(Gmac->vector);
    value = DestroyEthIrqThread(ethDevice);
    if(value != HDF_SUCCESS)
    {
        HDF_LOGE("Irq thread destruction failure \n");
        return HDF_FAILURE;
    }
    return HDF_SUCCESS;
}

void GmacCoreInit(void)
{

}

int32_t GmacPortReset(struct EthDevice *ethDevice)
{
    struct GmacPriv *Gmac = (struct GmacPriv *)ethDevice->priv;
    uint32_t value;
    uint32_t count = 0;
    uint32_t macid_lo, macid_hi;
    uint8_t  mac_id[6] = {0};

    Gmac->txHwCnt = 0;
    value = OSAL_READL(Gmac->iobase + GMAC_BASIC_CTL1);
    OSAL_WRITEL(value | GMAC_CTL1_SOFT_RST, Gmac->iobase + GMAC_BASIC_CTL1); /* SOFT_RST */
    /* The timeout was previoulsy set to 10ms, but some board (OrangePI0)
	 * need more if no cable plugged. 100ms seems OK
	 */
    do{
        value = OSAL_READL(Gmac->iobase + GMAC_BASIC_CTL1);
        if((value & GMAC_CTL1_SOFT_RST) == 0)
            break;
        OsalMSleep(1);
    }while (count++ < 100);

    /* Rewrite mac address after reset */
    RandomMACAddr(mac_id, 6);
	macid_lo = mac_id[0] + (mac_id[1] << 8) + (mac_id[2] << 16) +
		(mac_id[3] << 24);
	macid_hi = mac_id[4] + (mac_id[5] << 8);
    OSAL_WRITEL(macid_hi, Gmac->iobase + GMAC_MACADDR_HI);
    OSAL_WRITEL(macid_lo, Gmac->iobase + GMAC_MACADDR_LO);

    /* transmission starts after the full frame arrived in TX DMA FIFO */
	SETBITS_LE32(GMAC_TX_CTL1_TX_MD | GMAC_TX_NEXT_FRM, Gmac->iobase + GMAC_TX_CTL1);

	/*
	 * RX DMA reads data from RX DMA FIFO to host memory after a
	 * complete frame has been written to RX DMA FIFO
	 */
	SETBITS_LE32(GMAC_RX_CTL1_RX_MD, Gmac->iobase + GMAC_RX_CTL1);

    return HDF_SUCCESS;
}

static void GmacAdjustLink(struct GmacPriv *Gmac)
{
    uint32_t value;
    value = OSAL_READL(Gmac->iobase + GMAC_BASIC_CTL0);
    if(Gmac->duplex)
		value |= GMAC_CTL0_FULL_DUPLEX;
	else
		value &= ~GMAC_CTL0_FULL_DUPLEX;

    switch (Gmac->speed)
    {
        case 1000:
            value |= GMAC_CTL0_SPEED_1000;
            break;
        case 100:
            value |= GMAC_CTL0_SPEED_100;
            break;
        case 10:
            value |= GMAC_CTL0_SPEED_10;
            break;
    }

    OSAL_WRITEL(value, Gmac->iobase + GMAC_BASIC_CTL0);
}

#define PHY_STATE_TIME 1000
int32_t GmacPortInit(struct EthDevice *ethDevice)
{
    struct GmacPriv *Gmac = (struct GmacPriv *)ethDevice->priv;
    uint32_t value;
   
    GmacAdjustLink(Gmac);

    GmacDescriptorInit(Gmac);

    /* TODO:Periodic status detection */
    // if (OsalTimerCreate(&Gmac->phyTimer, PHY_STATE_TIME, PhyStateMachine, (uintptr_t)ethDevice) != HDF_SUCCESS) {
    //     HDF_LOGE("create phy state machine timer failed");
    // }
    // if (OsalTimerStartLoop(&Gmac->phyTimer) != HDF_SUCCESS) {
    //     HDF_LOGE("start phy state machine timer failed");
    // }
    
    /* SET BURST LEN */
    value = (8 << GMAC_CTL1_BURST_LEN_SHIFT); /* burst len */
    OSAL_WRITEL(value, Gmac->iobase + GMAC_BASIC_CTL1);
    /* Start RX/TX DMA */
    SETBITS_LE32(GMAC_RX_CTL1_RX_DMA_EN | GMAC_RX_CTL1_RX_DMA_START, Gmac->iobase + GMAC_RX_CTL1);
    SETBITS_LE32(GMAC_TX_CTL1_TX_DMA_EN, Gmac->iobase + GMAC_TX_CTL1);

	/* Enable RX/TX */
    SETBITS_LE32(GMAC_RX_CTL0_RX_EN, Gmac->iobase + GMAC_RX_CTL0);
    SETBITS_LE32(GMAC_TX_CTL0_TX_EN, Gmac->iobase + GMAC_TX_CTL0);	
    
    /* Enable RX/TX INT */
    OSAL_WRITEL(GMAC_TX_INT | GMAC_RX_INT | GMAC_TX_UNDERFLOW_INT, Gmac->iobase + GMAC_INT_EN);
    OSAL_WRITEL(0x1FFFFFF, Gmac->iobase + GMAC_INT_STA);

    return HDF_SUCCESS;
}

static struct EthMacOps g_macOps = {
    .MacInit = GmacCoreInit,
    .PortReset = GmacPortReset,
    .PortInit = GmacPortInit,
};

struct HdfEthMacChipDriver *BuildGmmacDriver(void)
{
    struct HdfEthMacChipDriver *macChipDriver = (struct HdfEthMacChipDriver *)OsalMemCalloc(
        sizeof(struct HdfEthMacChipDriver));
    if (macChipDriver == NULL) {
        HDF_LOGE("%s fail: OsalMemCalloc fail!", __func__);
        return NULL;
    }
    macChipDriver->ethMacOps = &g_macOps;
    return macChipDriver;
}


void ReleaseGmacDriver(struct HdfEthMacChipDriver *chipDriver)
{
    dprintf("-->>ReleaseGmacDriver \n");
    if (chipDriver == NULL) {
        return;
    }
    OsalMemFree(chipDriver);
}
#define MDIO_CMD_MII_PHY_REG_ADDR_MASK	0x000001f0
#define MDIO_CMD_MII_PHY_REG_ADDR_SHIFT	4
#define MDIO_CMD_MII_PHY_ADDR_MASK	0x0001f000
#define MDIO_CMD_MII_PHY_ADDR_SHIFT	12
#define MDIO_CMD_MII_CLK_CSR_DIV_128	0x3
#define MDIO_CMD_MII_CLK_CSR_SHIFT	20
#define MDIO_CMD_MII_BUSY		BIT(0)
#define MDIO_CMD_MII_WRITE		BIT(1)

static int  mdioRead (struct GmacPriv *priv, uint8_t ucPhyAddr, uint8_t ucRegAddr, uint16_t *pusValue)
{
    uint32_t miiCmd = 0x01;
    uint32_t value = 0;
    int count = 10;

    miiCmd = (ucRegAddr << MDIO_CMD_MII_PHY_REG_ADDR_SHIFT) &
		MDIO_CMD_MII_PHY_REG_ADDR_MASK;
	miiCmd |= (ucPhyAddr << MDIO_CMD_MII_PHY_ADDR_SHIFT) &
		MDIO_CMD_MII_PHY_ADDR_MASK;
    miiCmd |= MDIO_CMD_MII_CLK_CSR_DIV_128 <<
			   MDIO_CMD_MII_CLK_CSR_SHIFT;
	miiCmd |= MDIO_CMD_MII_BUSY;

    OSAL_WRITEL(miiCmd, priv->iobase + GMAC_MDIO_CMD);
    while (count--)
    {
        value = OSAL_READL(priv->iobase + GMAC_MDIO_CMD);
        if(0 == (value & MDIO_CMD_MII_BUSY))
        {
            break;
        }
        OsalMSleep(10);
    }
    *pusValue = OSAL_READL(priv->iobase + GMAC_MDIO_DATA);

    return  (HDF_SUCCESS);
}

static int mdioWrite (struct GmacPriv *priv, uint8_t ucPhyAddr, uint8_t ucRegAddr, uint16_t usValue)
{
    uint32_t value = 0x00;
    uint32_t miiData = usValue;
    uint32_t miiCmd = 0;
    int  count = 10;

	miiCmd = (ucRegAddr << MDIO_CMD_MII_PHY_REG_ADDR_SHIFT) &
		MDIO_CMD_MII_PHY_REG_ADDR_MASK;
	miiCmd |= (ucPhyAddr << MDIO_CMD_MII_PHY_ADDR_SHIFT) &
		MDIO_CMD_MII_PHY_ADDR_MASK;
    miiCmd |= MDIO_CMD_MII_CLK_CSR_DIV_128 <<
			   MDIO_CMD_MII_CLK_CSR_SHIFT;
	miiCmd |= MDIO_CMD_MII_WRITE;
	miiCmd |= MDIO_CMD_MII_BUSY;

    OSAL_WRITEL(miiData, priv->iobase + GMAC_MDIO_DATA);
    OSAL_WRITEL(miiCmd, priv->iobase + GMAC_MDIO_CMD);
    while (count--)
    {
        value = OSAL_READL(priv->iobase + GMAC_MDIO_CMD);
        if(0 == (value & MDIO_CMD_MII_BUSY))
        {
            break;
        }
        OsalMSleep(10);
    }
    return  (HDF_SUCCESS);
}
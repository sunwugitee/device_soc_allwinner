/*********************************************************************************************************
** 文件创建日期: 2022 年 07 月 1 日
**
** 描        述: GMAC 千兆以太网控制器的 phy 芯片初始化代码。在phy 初始化函数中，系统会搜索 0～31
*********************************************************************************************************/
#include "gmac_priv.h"
#include "gmac_phy.h"
#include "osal_time.h"

#define PHY_RTL8211E_ID         (0x001CC915)
#define PHY_YT8521_ID		    (0x0000011A)
#define PHY_IP101A_G_ID		    (0x02430C54)

#define PHY_MAX_ADDR            (32)
#define PHY_ID1_SHIFT           (16)

void rtl8211EPhyInit(void *pvCtx, uint16_t usPhyAddr, PHY_READ PhyRead, PHY_WRITE PhyWrite)
{
	uint16_t  usValue   = 0;

	PhyWrite(pvCtx, usPhyAddr, GMII_BMCR, GMII_RESET);
	while (1) {
		PhyRead(pvCtx, usPhyAddr, GMII_BMCR, &usValue);
		if (usValue & GMII_RESET)
			continue;
		else
			break;
	}

	usValue = 0x1340;
	PhyWrite(pvCtx, usPhyAddr, GMII_BMCR, usValue);
}


void yt8521PhyInit(void *pvCtx, uint16_t usPhyAddr, PHY_READ PhyRead, PHY_WRITE PhyWrite)
{
	uint16_t  usValue   = 0;

	PhyWrite(pvCtx, usPhyAddr, GMII_BMCR, GMII_RESET);
	while (1) {
		PhyRead(pvCtx, usPhyAddr, GMII_BMCR, &usValue);
		if (usValue & GMII_RESET)
			continue;
		else
			break;
	}

	//modify YT8521 led state
	PhyWrite(pvCtx, usPhyAddr, 0x1e, 0xa00e);
	PhyWrite(pvCtx, usPhyAddr, 0x1f, 0x70);
	PhyWrite(pvCtx, usPhyAddr, 0x1e, 0xa00d);
	PhyWrite(pvCtx, usPhyAddr, 0x1f, 0x670);
	//output 125M clk, increase txdelay
	PhyWrite(pvCtx, usPhyAddr, 0x1e, 0x27);
	PhyWrite(pvCtx, usPhyAddr, 0x1f, 0x2010);
	PhyWrite(pvCtx, usPhyAddr, 0x1e, 0xa012);
	PhyWrite(pvCtx, usPhyAddr, 0x1f, 0x68);
	PhyWrite(pvCtx, usPhyAddr, 0x1e, 0xa003);
	PhyWrite(pvCtx, usPhyAddr, 0x1f, 0xfb);

	usValue = 0x1340;
	PhyWrite(pvCtx, usPhyAddr, GMII_BMCR, usValue);

}

void  ip101a_gPhyInit(void *pvCtx, uint16_t usPhyAddr, PHY_READ PhyRead, PHY_WRITE PhyWrite)
{
	uint16_t  bmcr   = 0;

	/* Software Reset PHY */
	uint16_t  usValue   = 0;

	PhyWrite(pvCtx, usPhyAddr, GMII_BMCR, GMII_RESET);
	while (1) {
		PhyRead(pvCtx, usPhyAddr, GMII_BMCR, &usValue);
		if (usValue & GMII_RESET)
			continue;
		else
			break;
	}

	bmcr = BMCR_ANENABLE | BMCR_ANRESTART;
	PhyWrite(pvCtx, usPhyAddr, GMII_BMCR, bmcr);
}

void  generalPhyInit(void *pvCtx, uint16_t usPhyAddr, PHY_READ PhyRead, PHY_WRITE PhyWrite)
{
	uint16_t  bmcr   = 0;

	/* Software Reset PHY */
	PhyRead(pvCtx, usPhyAddr, GMII_BMCR, &bmcr);
	if (bmcr < 0)
		return;
	bmcr |= BMCR_RESET;
	bmcr = PhyWrite(pvCtx, usPhyAddr, GMII_BMCR, bmcr);
	if (bmcr < 0)
		return;

	do {
		PhyRead(pvCtx, usPhyAddr, GMII_BMCR, &bmcr);
		if (bmcr < 0)
			return;
	} while (bmcr & BMCR_RESET);//Reset finish

	bmcr = BMCR_ANENABLE | BMCR_ANRESTART;
	PhyWrite(pvCtx, usPhyAddr, GMII_BMCR, bmcr);
}

void  GmacPhyInit (void *pvCtx, PHY_READ PhyRead, PHY_WRITE PhyWrite, uint16_t *PhyAddr)
{
	uint32_t  uiID;
	uint16_t  uiAddr, usValue = 0;

	for(uiAddr = 0; uiAddr < PHY_MAX_ADDR; uiAddr++) {
	    PhyRead(pvCtx, uiAddr, GMII_PHYID1, &usValue);
	    uiID = usValue << PHY_ID1_SHIFT;
	    PhyRead(pvCtx, uiAddr, GMII_PHYID2, &usValue);
	    uiID |= usValue;
	    uiID &= 0xFFFFFFFF;

	    if((uiID != 0x0) & (uiID != 0xFFFFFFFF)) {
	    	break;
	    }
	}
	
	*PhyAddr = uiAddr;
    switch (uiID) {
		case PHY_RTL8211E_ID:
			rtl8211EPhyInit(pvCtx, uiAddr, PhyRead, PhyWrite);
			break;
		case PHY_YT8521_ID:
			yt8521PhyInit(pvCtx, uiAddr, PhyRead, PhyWrite);
			break;
		case PHY_IP101A_G_ID:
			ip101a_gPhyInit(pvCtx, uiAddr, PhyRead, PhyWrite);
			break;
		default:
			generalPhyInit(pvCtx, uiAddr, PhyRead, PhyWrite);
			dprintf("[Warning] unsupport PHY: ID=%lx\n", uiID);
    }
}
#include "hdf_wifi_product.h"
#include "wifi_mac80211_ops.h"
#include "hdf_wlan_utils.h"

#define HDF_LOG_TAG Xr829Driver

/***********************************************************/
/*      variable and function declare                      */
/***********************************************************/
struct net_device *save_kernel_net = NULL;

/***********************************************************/
/*      variable and function declare                      */
/***********************************************************/
extern int32_t hdf_netdev_init(struct NetDevice *netDev);
extern int32_t hdf_netdev_open(struct NetDevice *netDev);
extern int32_t hdf_netdev_stop(struct NetDevice *netDev);

/***********************************************************/
/*      Function declare                                   */
/***********************************************************/
int32_t InitXr829Chip(struct HdfWlanDevice *device)
{
    HDF_LOGE("%s: start...", __func__);
    return HDF_SUCCESS; 
}

int32_t DeinitXr829Chip(struct HdfWlanDevice *device)
{
    int32_t ret = 0;

    (void)device;
    HDF_LOGE("%s: start...", __func__);
    if (ret != 0)
    {
        HDF_LOGE("%s:Deinit failed!ret=%d", __func__, ret);
    }
    return ret;
}

int32_t Xr829Init(struct HdfChipDriver *chipDriver, struct NetDevice *netDevice)
{
    struct HdfWifiNetDeviceData *data = NULL;

    (void)chipDriver;
    HDF_LOGE("%s: start...", __func__);

    if (netDevice == NULL) 
    {
        HDF_LOGE("%s:para is null!", __func__);
        return HDF_FAILURE;
    }

    data = GetPlatformData(netDevice);
    if (data == NULL) 
    {
        HDF_LOGE("%s:netdevice data null!", __func__);
        return HDF_FAILURE;
    }

    /* set netdevice ops to netDevice */
    hdf_netdev_init(netDevice);
	netDevice->classDriverName = netDevice->classDriverName;
    netDevice->classDriverPriv = data;

    xradio_init();
    NetDeviceAdd(netDevice);

    HDF_LOGE("%s: success", __func__);

    //ret = hdf_netdev_open(netDevice);
    return HDF_SUCCESS;
}

int32_t Xr829Deinit(struct HdfChipDriver *chipDriver, struct NetDevice *netDevice)
{
    HDF_LOGE("%s: start...", __func__);
    (void)netDevice;
    (void)chipDriver;
    hdf_netdev_stop(netDevice);
    return HDF_SUCCESS;
}

void set_krn_netdev(struct net_device *dev) {
    save_kernel_net = dev;
}

struct net_device *get_krn_netdev(void) {
    return save_kernel_net;
}

EXPORT_SYMBOL(set_krn_netdev);
EXPORT_SYMBOL(get_krn_netdev);

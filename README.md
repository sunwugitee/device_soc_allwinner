# device_soc_allwinner

#### 介绍
​		在device/soc/allwinner/目录下，托管珠海研果科技有限公司基于T507小熊派开发板开发的样例代码。




#### 获取代码流程
```
mkdir openharmony

cd openharmony

repo init -u https://gitee.com/openharmony-sig/manifest.git -b master --no-repo-verify -m devboard_seed.xml 

repo sync -c

repo forall -c 'git lfs pull'

./build/prebuilts_download.sh

```



#### 代码框架

```
/device/soc/allwinner
├── build                      # 进行编译打包配置脚本
├── device                     # 标准系统内核相关配置文件
├── drivers                    # 第三方外设驱动代码
├── hardware                   # 各模块二进制文件目录
├── LICENSE
├── R818
├── README.en.md
├── README.md
├── T507                       # 芯片平台
├── tags 
├── tools                      # 烧录工具
└── XR806
```



#### 编译流程

```
1. cd ./device/soc/allwinner/build

2. python3 patch.py T507             # 打鸿蒙系统相关补丁

3. ./build.sh load                   # 准备linux4.19环境
prepare environment...
ACTION List: mkload;========
Execute command: mkload
start to load kernel file
All available ic:                   # 选择芯片平台
   0. r818
   1. t507
Choice [t507]:
All available kern_ver:             # 选择内核版本
   0. linux-4.19
   1. linux-5.10
Choice [linux-4.19]:
delete old kernel file success
......

4. ./build.sh config                #  选择相关板级配置
Welcome to mkscript setup progress   
All available platform:
   0. linux
Choice [linux]:                     #默认配置可一直按回车键
All available linux_dev:
   0. bsp
Choice [bsp]:
All available kernel version:
INFO: default kernel version is linux-4.19, if reselet, please run ./build.sh load again
All available ic:
INFO: default ic type is t507, if reselet, please run ./build.sh load again
All available board:               #选择版级包
   0. demo2.0_bearpi
   1. demo2.0_harmony
Choice [demo2.0_bearpi]:
All available flash:
   0. default
   1. nor
Choice [default]:
sun50iw9p1smp_t507_L2_defconfig
......

5. ./build.sh                         # 编译linux4.19内核

6. cd ../../../..                     # 回退到harmony根目录

7. ./build.sh --product-name T507 --gn-args linux_kernel_version=\"linux-4.19\" # 编译T507 L2的system.img/vendor.img等

8. cd ./device/soc/allwinner/build    # 重新进入allwinner/build目录

9. ./build.sh pack                    # 生成img镜像文件

10. cd ../../../../out                # 进入out目录可看到生成的镜像文件

# 镜像文件为：t507_linux_demo2.0_harmony_uart0.img
```



#### 使用工具LiveSuit_V1.0.0.1烧录

1、连接好数据线和串口线；

2、打开烧录软件，选择好要烧录的镜像文件；

3、使用类MobaXterm等工具打开串口，串口可读写的前提按住键盘键"2",然后按这样机的reset键，软件自动进入烧写模式，等待烧写成功即可。